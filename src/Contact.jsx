import React, { useState } from 'react';

const Contact = () => {
    const [data, setData] = useState ({
        service:"", 
        name:"",
        email:"",
        phone:"",
        msg:"",
    });

    const InputEvent = (event) => {
        const  {name, value} = event.target; 

        setData ((preVal)  => {
            return {
                ...preVal,
                [name]: value, 
            };

        });
    }

    const formSubmit = (e) => {
        e.preventDefault();
        alert (`${data.name}`);
    };  
    return (
    <>
        <div className="d-flex">
            <div className="container-fluid">
                <div className="row">      
                    <div className="col-12 col-md-12 col-lg-12 d-flex justify-content-center flex-column">
                        <h1 className="text-center mb-3">Contact us </h1>
                        <form className="contact-form px-3" onSubmit={formSubmit}>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group mb-3">
                                        <select className="form-control contact-form__input" id="" name="service" onChange={InputEvent}>
                                        <option>Select Service</option>
                                        <option value={data.service}> {data.service} </option>
                                        </select>
                                    </div>
                                    <div className="form-group mb-3">
                                        <input type="text" name="name" onChange={InputEvent} className="form-control contact-form__input" placeholder="Your Name *" value={data.name}/>
                                    </div>
                                    <div className="form-group mb-3">
                                        <input type="text" name="email" onChange={InputEvent} className="form-control contact-form__input" placeholder="Your Email *" value={data.email}/>
                                    </div>
                                    <div className="form-group mb-3">
                                        <input type="text" name="phone" onChange={InputEvent} className="form-control contact-form__input" placeholder="Your Phone Number *" value={data.phone}/>
                                    </div>
                                    
                                </div>
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <textarea name="msg" onChange={InputEvent} className="form-control contact-form__input" placeholder="Your Message *" value={data.msg}></textarea>
                                    </div>
                                </div> 
                                <div className="col-md-12 col-12">
                                    <div className="form-group">
                                        <input type="submit" name="btnSubmit" className="btn contact-form__btn" value="Send Message" />
                                    </div>
                                </div>
                            </div>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </>
    );
}

export default Contact;