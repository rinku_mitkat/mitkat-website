import React from 'react';
import 'react-alice-carousel/lib/alice-carousel.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/js/bootstrap.bundle';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import ComingSoon from './components/ComingSoon';
import NoPage from './components/NoPage';
import Home from './pages/Home';
import About from './pages/About';
import Career from './pages/Career';
import Contact from './pages/Contact';
import Pri from './pages/services/Pri';
import CasestudyPRI01 from './pages/casestudies/pri/Casestudy01';
import CasestudyPRI02 from './pages/casestudies/pri/Casestudy02';
import CasestudyPRI03 from './pages/casestudies/pri/Casestudy03';
import CasestudyPRI04 from './pages/casestudies/pri/Casestudy04';
import CasestudyPRI05 from './pages/casestudies/pri/Casestudy05';

import Pess from './pages/services/Pess';
import CasestudyTvra from './pages/casestudies/pess/CasestudyTvra';
import DataCenterCapability from './pages/services/DataCenterCapability';
import CasestudyPESS01 from './pages/casestudies/pess/Casestudy01';
import CasestudyPESS02 from './pages/casestudies/pess/Casestudy02';
import CasestudyPESS03 from './pages/casestudies/pess/Casestudy03';
import CasestudyPESS04 from './pages/casestudies/pess/Casestudy04';
import CasestudyPESS05 from './pages/casestudies/pess/Casestudy05';
import CasestudyPESS06 from './pages/casestudies/pess/Casestudy06';
import CasestudyPESS07 from './pages/casestudies/pess/Casestudy07';
import CasestudyPESS08 from './pages/casestudies/pess/Casestudy08';
import CasestudyPESS09 from './pages/casestudies/pess/Casestudy09';

import Sdcpm from './pages/services/Sdcpm';
import CasestudySDCPM01 from './pages/casestudies/sdcpm/Casestudy01';
import CasestudySDCPM02 from './pages/casestudies/sdcpm/Casestudy02';
import CasestudySDCPM03 from './pages/casestudies/sdcpm/Casestudy03';
import CasestudySDCPM04 from './pages/casestudies/sdcpm/Casestudy04';
import CasestudySDCPM05 from './pages/casestudies/sdcpm/Casestudy05';

import Igs from './pages/services/Igs';
import Csr from './pages/services/Csr';
import CasestudyCSR01 from './pages/casestudies/csr/Casestudy01';
import CasestudyCSR02 from './pages/casestudies/csr/Casestudy02';
import CasestudyCSR03 from './pages/casestudies/csr/Casestudy03';
import CasestudyCSR04 from './pages/casestudies/csr/Casestudy04';
import CasestudyCSR05 from './pages/casestudies/csr/Casestudy05';
import CasestudyCSR06 from './pages/casestudies/csr/Casestudy06';
import CasestudyCSR07 from './pages/casestudies/csr/Casestudy07';

import Irm from './pages/services/Irm';
import CasestudyIRM01 from './pages/casestudies/irm/Casestudy01';
import CasestudyIRM02 from './pages/casestudies/irm/Casestudy02';
import CasestudyIRM03 from './pages/casestudies/irm/Casestudy03';

import Twd from './pages/services/Twd';
import CasestudyTWD01 from './pages/casestudies/twd/Casestudy01';
import CasestudyTWD02 from './pages/casestudies/twd/Casestudy02';
import CasestudyTWD03 from './pages/casestudies/twd/Casestudy03';
import CasestudyTWD04 from './pages/casestudies/twd/Casestudy04';
import CasestudyTWD05 from './pages/casestudies/twd/Casestudy05';

import Mss from './pages/services/Mss';
import CasestudyMSS01 from './pages/casestudies/mss/Casestudy01';
import CasestudyMSS02 from './pages/casestudies/mss/Casestudy02';
import CasestudyMSS03 from './pages/casestudies/mss/Casestudy03';
import CasestudyMSS04 from './pages/casestudies/mss/Casestudy04';
import CasestudyMSS05 from './pages/casestudies/mss/Casestudy05';
import CasestudyMSS06 from './pages/casestudies/mss/Casestudy06';
import CasestudyMSS07 from './pages/casestudies/mss/Casestudy07';
import CasestudyMSS08 from './pages/casestudies/mss/Casestudy08';
import CasestudyMSS09 from './pages/casestudies/mss/Casestudy09';
import CasestudyMSS10 from './pages/casestudies/mss/Casestudy10';

import SudhirSharma from './pages/team/SudhirSharma';
import PremChand from './pages/team/PremChand';
import SamKumar from './pages/team/SamKumar';
import PawanDesai from './pages/team/PawanDesai';
import SushilPradhan from './pages/team/SushilPradhan';
import HimaBisht from './pages/team/HimaBisht';
import SandeepSinha from './pages/team/SandeepSinha';
import AparnaGuddad from './pages/team/AparnaGuddad';
import SohebKhan from './pages/team/SohebKhan';
import AmeyaPatkar from './pages/team/AmeyaPatkar';
import DhananjayBirwadkar from './pages/team/DhananjayBirwadkar';

import Rime from './pages/platforms/Rime';
import Covid from './pages/platforms/Covid';
import Sap from './pages/platforms/Sap';
import PlatformDashboard from './pages/login/PlatformDashboard';
import Dashboard from './pages/login/Dashboard';
import ScrollToTop from './components/ScrollToTop';
import CovidUsers from './pages/login/CovidUsers';
import CareerList from './pages/login/CareerList';
import Blogs from './pages/Blogs';
import BlogList from './pages/login/Blogs/BlogList';
import RecentNews from './pages/RecentNews';
import RanjeetSinha from './pages/team/RanjeetSinha';
import MiteshShah from './pages/team/MiteshShah';
import RecentNewsList from './pages/login/RecentNewsList';
import Advisories from './pages/Advisories';
import Monthly from './pages/Monthly';
import SpecialReports from './pages/SpecialReports';
import RiskReviews from './pages/RiskReviews';
import UpcomingEventList from './pages/login/UpcomingEventList';
import Podcast from './pages/Podcast';
import PodcastList from './pages/login/PodcastList';
// import EnquiryForm from './pages/enquiryForm';
import ThankYou from './pages/enquiryForm/thankYou';
import DemoList from './pages/login/DemoList';
import BookDemo from './pages/BookDemo';
import SagarBhanushali from './pages/team/SagarBhanushali';
import MalcolmCooper from './pages/team/MalcolmCooper';
import PrivacyPolicy from './pages/privacyPolicy';

const App = () => {
  return (
    <BrowserRouter onUpdate={() => window.scrollTo(0, 0)}>
      <ScrollToTop />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/about" component={About} />
        <Route exact path="/career" component={Career} />
        <Route exact path="/sudhir-sharma" component={SudhirSharma} />
        <Route exact path="/prem-chand" component={PremChand} />
        <Route exact path="/s-m-kumar" component={SamKumar} />
        <Route exact path="/pawan-desai" component={PawanDesai} />
        <Route exact path="/sushil-pradhan" component={SushilPradhan} />
        <Route exact path="/hima-bisht" component={HimaBisht} />
        <Route exact path="/sandeep-sinha" component={SandeepSinha} />
        <Route
          exact
          path="/dhananjay-birwadkar"
          component={DhananjayBirwadkar}
        />
        <Route exact path="/aparna-guddad" component={AparnaGuddad} />
        <Route exact path="/soheb-khan" component={SohebKhan} />
        <Route exact path="/ameya-patkar" component={AmeyaPatkar} />
        <Route exact path="/ranjeet-sinha" component={RanjeetSinha} />
        <Route exact path="/mitesh-shah" component={MiteshShah} />
        <Route exact path="/sagar-bhanushali" component={SagarBhanushali} />
        <Route exact path="/malcolm-cooper" component={MalcolmCooper} />
        <Route
          exact
          path="/risk-intelligence-monitoring-engine"
          component={Rime}
        />
        <Route exact path="/covid19-dashboard" component={Covid} />
        <Route exact path="/security-audit" component={Sap} />
        <Route exact path="/predictive-risk-intelligence" component={Pri} />

        <Route exact path="/casestudy_pri_01" component={CasestudyPRI01} />
        <Route exact path="/casestudy_pri_02" component={CasestudyPRI02} />
        <Route exact path="/casestudy_pri_03" component={CasestudyPRI03} />
        <Route exact path="/casestudy_pri_04" component={CasestudyPRI04} />
        <Route exact path="/casestudy_pri_05" component={CasestudyPRI05} />

        <Route
          exact
          path="/physical-environmental-security-and-safety-risk-consulting"
          component={Pess}
        />
        <Route
          exact
          path="/data-center-capability"
          component={DataCenterCapability}
        />
        <Route exact path="/casestudy_tvra" component={CasestudyTvra} />
        <Route exact path="/casestudy_pess_01" component={CasestudyPESS01} />
        <Route exact path="/casestudy_pess_02" component={CasestudyPESS02} />
        <Route exact path="/casestudy_pess_03" component={CasestudyPESS03} />
        <Route exact path="/casestudy_pess_04" component={CasestudyPESS04} />
        <Route exact path="/casestudy_pess_05" component={CasestudyPESS05} />
        <Route exact path="/casestudy_pess_06" component={CasestudyPESS06} />
        <Route exact path="/casestudy_pess_07" component={CasestudyPESS07} />
        <Route exact path="/casestudy_pess_08" component={CasestudyPESS08} />
        <Route exact path="/casestudy_pess_09" component={CasestudyPESS09} />

        <Route
          exact
          path="/security-design-consulting-and-project-management"
          component={Sdcpm}
        />
        <Route exact path="/casestudy_sdcpm_01" component={CasestudySDCPM01} />
        <Route exact path="/casestudy_sdcpm_02" component={CasestudySDCPM02} />
        <Route exact path="/casestudy_sdcpm_03" component={CasestudySDCPM03} />
        <Route exact path="/casestudy_sdcpm_04" component={CasestudySDCPM04} />
        <Route exact path="/casestudy_sdcpm_05" component={CasestudySDCPM05} />

        <Route
          exact
          path="/infrastructure-and-government-services"
          component={Igs}
        />
        <Route exact path="/cyber-security-and-resilience" component={Csr} />
        <Route exact path="/casestudy_csr_01" component={CasestudyCSR01} />
        <Route exact path="/casestudy_csr_02" component={CasestudyCSR02} />
        <Route exact path="/casestudy_csr_03" component={CasestudyCSR03} />
        <Route exact path="/casestudy_csr_04" component={CasestudyCSR04} />
        <Route exact path="/casestudy_csr_05" component={CasestudyCSR05} />
        <Route exact path="/casestudy_csr_06" component={CasestudyCSR06} />
        <Route exact path="/casestudy_csr_07" component={CasestudyCSR07} />

        <Route exact path="/integrity-risk-management" component={Irm} />
        <Route exact path="/casestudy_irm_01" component={CasestudyIRM01} />
        <Route exact path="/casestudy_irm_02" component={CasestudyIRM02} />
        <Route exact path="/casestudy_irm_03" component={CasestudyIRM03} />

        <Route
          exact
          path="/training-and-workforce-development"
          component={Twd}
        />
        <Route exact path="/casestudy_twd_01" component={CasestudyTWD01} />
        <Route exact path="/casestudy_twd_02" component={CasestudyTWD02} />
        <Route exact path="/casestudy_twd_03" component={CasestudyTWD03} />
        <Route exact path="/casestudy_twd_04" component={CasestudyTWD04} />
        <Route exact path="/casestudy_twd_05" component={CasestudyTWD05} />

        <Route exact path="/managed-security-services" component={Mss} />
        <Route exact path="/casestudy_mss_01" component={CasestudyMSS01} />
        <Route exact path="/casestudy_mss_02" component={CasestudyMSS02} />
        <Route exact path="/casestudy_mss_03" component={CasestudyMSS03} />
        <Route exact path="/casestudy_mss_04" component={CasestudyMSS04} />
        <Route exact path="/casestudy_mss_05" component={CasestudyMSS05} />
        <Route exact path="/casestudy_mss_06" component={CasestudyMSS06} />
        <Route exact path="/casestudy_mss_07" component={CasestudyMSS07} />
        <Route exact path="/casestudy_mss_08" component={CasestudyMSS09} />
        <Route exact path="/casestudy_mss_09" component={CasestudyMSS09} />
        <Route exact path="/casestudy_mss_10" component={CasestudyMSS10} />

        <Route exact path="/contact" component={Contact} />
        <Route exact path="/advisories" component={Advisories} />
        <Route exact path="/monthly" component={Monthly} />
        <Route exact path="/special-reports" component={SpecialReports} />
        <Route exact path="/risk-reviews" component={RiskReviews} />
        <Route exact path="/blogs" component={Blogs} />
        <Route exact path="/podcast" component={Podcast} />

        <Route exact path="/dashboard" component={Dashboard} />
        <Route exact path="/platform_dashboard" component={PlatformDashboard} />
        <Route exact path="/demo_dashboard" component={DemoList} />
        <Route exact path="/recent-news" component={RecentNews} />
        <Route exact path="/recentnews-list" component={RecentNewsList} />
        <Route exact path="/podcast-list" component={PodcastList} />
        <Route
          exact
          path="/upcomingevents-list"
          component={UpcomingEventList}
        />
        <Route exact path="/covid_user" component={CovidUsers} />
        <Route exact path="/career_list" component={CareerList} />
        <Route exact path="/blog_list" component={BlogList} />
        <Route exact path="/coming_soon" component={ComingSoon} />
        <Route
          exact
          path="/book_demo
"
          component={BookDemo}
        />
        <Route exact path="/response" component={ThankYou} />
        <Route exact path="/privacypolicy" component={PrivacyPolicy} />
        <Route exact path="*" component={NoPage} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
