import React, { useEffect, useState } from 'react';
import { getUpcomingEventsClient } from '../service/api';
import AliceCarousel from 'react-alice-carousel';
import { faClock } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const UpcomingEvents = () => {
  const responsiveN = {
    0: { items: 1 },
    568: { items: 1 },
    1024: { items: 1 },
  };
  const [eventList, setEventList] = useState([]);
  const monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'June',
    'July',
    'Aug',
    'Sept',
    'Oct',
    'Nov',
    'Dec',
  ];
  useEffect(() => {
    getUpcomingEventsClient().then((events) => {
      setEventList(events);
    });
  }, []);

  return (
    <>
      {eventList.length > 0 ? (
        <section className="py-5 upcoming-events">
          <div className="row ">
            <div className="col-lg-12 col-md-12 col-12">
              <AliceCarousel
                autoPlay
                autoPlayInterval="3000"
                mouseTracking
                responsive={responsiveN}
              >
                {eventList.map((item) => (
                  <div className="upcoming-events-box p-md-5 p-0">
                    <div
                      className="upcoming-events-img"
                      style={{
                        background: `linear-gradient(0deg, rgba(20, 33, 54, 0.75), rgba(20, 33, 54, 0.75)), url(${item.imageURL})`,
                      }}
                    >
                      <p className="big-date text-white">
                        {`${new Date(item.date).getDate()}`}
                      </p>
                      <p className="small text-white">
                        {`${monthNames[new Date(item.date).getMonth()]}`},
                        &nbsp;
                        {`${new Date(item.date).getFullYear()}`}
                      </p>
                    </div>
                    <div className="upcoming-events-summary p-md-5 p-2">
                      <h5 className="mb-3">{item.title}</h5>
                      <p>
                        <FontAwesomeIcon icon={faClock} /> {item.time}
                      </p>
                      <div className="mb-3"> {item.description}</div>
                      <a
                        href={item.imageURL}
                        target="_blank"
                        className="btn btn-secondary mr-3"
                      >
                        Know more
                      </a>
                      <a
                        href={item.registrationlink}
                        target="_blank"
                        className="btn btn-primary"
                      >
                        Register here
                      </a>
                    </div>
                  </div>
                ))}
              </AliceCarousel>
            </div>
          </div>
        </section>
      ) : null}
    </>
  );
};

export default UpcomingEvents;
