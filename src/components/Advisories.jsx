import React, { useEffect, useState } from 'react';
import { getAdvisories, setItemPlatform, getServiceList } from '../service/api';
// import BookDemo from '../pages/BookDemo';

const Advisories = () => {
  const [advisoryList, setAdvisoryList] = useState({
    success: '',
    message: '',
    output: { specialReports: [], monthly: [], riskReview: [], advisories: [] },
  });
  const [name, setName] = useState('');
  const [emailid, setEmailid] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [advisoryname, setAdvisoryname] = useState('');

  const [url, setUrl] = useState('');
  const [validate, setValidate] = useState(false);
  const [alert, setAlert] = useState(false);
  const [itemInputPlatform, setItemInputPlatform] = useState('');
  const [serviceList, setServiceList] = useState({
    success: '',
    message: '',
    output: [],
  });
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  function handleShow(url, advisoryname) {
    setShow(true);
    setUrl(url);
    setAdvisoryname(advisoryname);
  }
  useEffect(() => {
    // if(itemInputPlatform.length && !alert) {
    //     return;
    //   }

    getAdvisories().then((advisories) => {
      setAdvisoryList(advisories);
      //debugger
    });

    getServiceList().then((items) => {
      setServiceList(items);
    });
    return;
  }, [validate, itemInputPlatform]);

  const formSubmitPlatform = (e) => {
    e.preventDefault();
    setItemPlatform({ name, emailid, phoneNumber, advisoryname }).then(() => {
      setItemInputPlatform('');
      setValidate(true);
      setAlert(true);
      window.open(url, '_blank');
      window.location.reload(false);
    });
  };

  return (
    <>
      {/* <BookDemo /> */}
      <div className="col-12 ">
        <nav>
          <div
            className="nav mb-3 nav-tabs-location nav-fill  justify-content-center"
            id="nav-tab"
            role="tablist"
          >
            <a
              className="nav-item nav-link active"
              id="nav-adv-tab"
              data-toggle="tab"
              href="#nav-adv"
              role="tab"
              aria-controls="nav-adv"
              aria-selected="true"
            >
              Advisories
            </a>
            <a
              className="nav-item nav-link"
              id="nav-week-tab"
              data-toggle="tab"
              href="#nav-week"
              role="tab"
              aria-controls="nav-week"
              aria-selected="false"
            >
              Monthly
            </a>
            <a
              className="nav-item nav-link"
              id="nav-spl-tab"
              data-toggle="tab"
              href="#nav-spl"
              role="tab"
              aria-controls="nav-spl"
              aria-selected="false"
            >
              Special Reports
            </a>
            <a
              className="nav-item nav-link"
              id="nav-risk-tab"
              data-toggle="tab"
              href="#nav-risk"
              role="tab"
              aria-controls="nav-risk"
              aria-selected="false"
            >
              Risk Reviews
            </a>
          </div>
        </nav>
        <div className="tab-content " id="nav-tabContent-event">
          <div
            className="tab-pane fade show active"
            id="nav-adv"
            role="tabpanel"
            aria-labelledby="nav-adv-tab"
          >
            <div className="row">
              {advisoryList.output.advisories.map((advisories) => (
                <div className="col-md-4 p-4 ">
                  <div
                    className="advisory-Box"
                    style={{ background: `url(${advisories.displayURL})` }}
                  >
                    <div className="advisory-header">
                      <p className="small">{advisories.date} </p>
                      <p>{advisories.title} </p>
                      <div className="advisory-footer">
                        <a
                          className="btn-view"
                          onClick={() =>
                            handleShow(advisories.reportURL, advisories.title)
                          }
                        >
                          View
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
          <div
            className="tab-pane fade"
            id="nav-week"
            role="tabpanel"
            aria-labelledby="nav-week-tab"
          >
            <div className="row">
              {advisoryList.output.monthly.map((advisories) => (
                <div className="col-md-4 p-4">
                  <div
                    className="advisory-Box"
                    style={{ background: `url(${advisories.displayURL})` }}
                  >
                    <div className="advisory-header">
                      <p className="small">{advisories.date} </p>
                      <p>{advisories.title} </p>
                      <div className="advisory-footer">
                        <a
                          className="btn-view"
                          onClick={() =>
                            handleShow(advisories.reportURL, advisories.title)
                          }
                        >
                          View
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
          <div
            className="tab-pane fade"
            id="nav-spl"
            role="tabpanel"
            aria-labelledby="nav-spl-tab"
          >
            <div className="row">
              {advisoryList.output.specialReports.map((advisories) => (
                <div className="col-md-4 p-4">
                  <div
                    className="advisory-Box"
                    style={{ background: `url(${advisories.displayURL})` }}
                  >
                    <div className="advisory-header">
                      <p className="small">{advisories.date} </p>
                      <p>{advisories.title} </p>
                      <div className="advisory-footer">
                        <a
                          className="btn-view"
                          onClick={() =>
                            handleShow(advisories.reportURL, advisories.title)
                          }
                        >
                          View
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
          <div
            className="tab-pane fade"
            id="nav-risk"
            role="tabpanel"
            aria-labelledby="nav-risk-tab"
          >
            <div className="row">
              {advisoryList.output.riskReview.map((advisories) => (
                <div className="col-md-4 p-4">
                  <div
                    className="advisory-Box"
                    style={{ background: `url(${advisories.displayURL})` }}
                  >
                    <div className="advisory-header">
                      <p className="small">{advisories.date} </p>
                      <p>{advisories.title} </p>
                      <div className="advisory-footer">
                        <a
                          className="btn-view"
                          onClick={() =>
                            handleShow(advisories.reportURL, advisories.title)
                          }
                        >
                          View
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
      <div
        className={show ? ' modal fade show' : 'modal fade'}
        id="bookademoModal"
        tabindex="-1"
        aria-labelledby="bookademoModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header pb-0">
              <h5 className="modal-title" id="exampleModalLabel">
                {' '}
                Let us know you!{' '}
              </h5>
              <button type="button" className="close" onClick={handleClose}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body pt-0">
              <div className="d-flex bg-darker">
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-12 col-md-12 col-lg-12 m-a d-flex justify-content-center flex-column">
                      <p className="text-right text-white">
                        {' '}
                        <small> * required </small>{' '}
                      </p>
                      <form
                        className="contact-form px-3"
                        onSubmit={formSubmitPlatform}
                      >
                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group mb-3">
                              <label className="text-white">Name * </label>
                              <input
                                type="text"
                                value={name}
                                className="form-control contact-form__input"
                                onChange={(event) =>
                                  setName(event.target.value)
                                }
                                required
                              />
                            </div>
                            <div className="form-group mb-3">
                              <label className="text-white">Email Id * </label>
                              <input
                                type="email"
                                value={emailid}
                                className="form-control contact-form__input"
                                onChange={(event) =>
                                  setEmailid(event.target.value)
                                }
                                required
                              />
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group mb-3">
                              <label className="text-white">Phone * </label>
                              <input
                                type="text"
                                value={phoneNumber}
                                className="form-control contact-form__input"
                                onChange={(event) =>
                                  setPhoneNumber(event.target.value)
                                }
                                required
                              />
                            </div>
                            <div className="form-group mb-3">
                              <label className="text-white">
                                Advisory Name *{' '}
                              </label>
                              <input
                                type="text"
                                value={advisoryname}
                                className="form-control contact-form__input"
                                onChange={(event) =>
                                  setAdvisoryname(event.target.value)
                                }
                                readOnly
                              />
                            </div>
                          </div>

                          <div className="col-md-6 col-12">
                            <div className="form-group">
                              <input
                                type="submit"
                                name="btnSubmit"
                                className="btn contact-form__btn"
                                value="Submit"
                              />
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Advisories;
