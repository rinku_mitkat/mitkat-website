import React from 'react';

import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { faYoutube } from '@fortawesome/free-brands-svg-icons';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faViruses } from '@fortawesome/free-solid-svg-icons';

import isologo from '../assets/images/iso-1.webp';

const Footer = () => {
  return (
    <>
      <div className="floating-btn blinking">
        <NavLink
          exact
          to="/book_demo
"
          rel="noopener noreferrer"
          className="floating-btn__link"
        >
          Enquire Now
        </NavLink>
      </div>

      <footer className="bg-darker py-5 ">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 iso-certification ">
              <p className="smaller text-captilize">
                <img className="mr-2" src={isologo} alt="Canon" />
                ISO 27001:2013 Certified
              </p>

              {/* <ul className="social-links float-right">
                            <li>
                                <a href="https://www.linkedin.com/company/mitkat-advisory-services-pvt-ltd/" target="_blank"><FontAwesomeIcon icon={faLinkedinIn} /></a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/channel/UCvdOTlPvROlajddDXxGVU6A" target="_blank"><FontAwesomeIcon icon={faYoutube} /></a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/mitkatadvisoryservices" target="_blank"><FontAwesomeIcon icon={faFacebookF} /></a>
                            </li>            
                            <li>
                                <a href="https://twitter.com/MitKat_Advisory" target="_blank"><FontAwesomeIcon icon={faTwitter} /></a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/mitkatadvisory" target="_blank"><FontAwesomeIcon icon={faInstagram} /></a>
                            </li>
                        </ul> */}

              <ul className="social-links float-right">
                <li>
                  <a
                    href="https://www.linkedin.com/company/mitkat-advisory-services-pvt-ltd/"
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faLinkedinIn} />
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.youtube.com/channel/UCvdOTlPvROlajddDXxGVU6A"
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faYoutube} />
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.facebook.com/mitkatadvisoryservice/?ref=pages_you_manage"
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faFacebookF} />
                  </a>
                </li>
                <li>
                  <a href="https://twitter.com/MitKat_Advisory" target="_blank">
                    <FontAwesomeIcon icon={faTwitter} />
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.instagram.com/mitkatadvisory"
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faInstagram} />
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-12">
              <span className="float-left ">
                &copy; 2010-{new Date().getFullYear()} MitKat Advisory Services
                Pvt Ltd. All Rights Reserved{' '}
              </span>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
