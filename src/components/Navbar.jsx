import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import logo from '../assets/logo.png';
const Navbar = () => {
  const [navOpen, setnavOpen] = useState(false);

  useEffect(() => {
    document.body.classList.toggle('overflow-hidden', navOpen);
  }, [navOpen]);

  const [subMenuOpenRime, setsubMenuOpenRime] = useState(false);
  const [subMenuOpenCovid, setsubMenuOpenCovid] = useState(false);
  const [subMenuOpenSap, setsubMenuOpenSap] = useState(false);

  const [subMenuOpenPri, setsubMenuOpenPri] = useState(false);
  const [subMenuOpenPess, setsubMenuOpenPess] = useState(false);
  const [subMenuOpenSdcpm, setsubMenuOpenSdcpm] = useState(false);
  const [subMenuOpenIgs, setsubMenuOpenIgs] = useState(false);
  const [subMenuOpenCsr, setsubMenuOpenCsr] = useState(false);
  const [subMenuOpenIrm, setsubMenuOpenIrm] = useState(false);
  const [subMenuOpenTwd, setsubMenuOpenTwd] = useState(false);
  const [subMenuOpenMss, setsubMenuOpenMss] = useState(false);

  const [alert, setAlert] = useState(false);
  const [userid, setUserid] = useState('');
  const [password, setPassword] = useState('');
  const [itemInput, setItemInput] = useState('');
  useEffect(() => {
    if (itemInput.length && !alert) {
      return;
    }
  }, [alert, itemInput]);

  let history = useHistory();
  const formLoginSubmit = (e) => {
    e.preventDefault();
    if (
      itemInput.userid == 'admin@mitkat.com' &&
      itemInput.password == 'q123'
    ) {
      history.push('/dashboard');
      window.location.reload(false);
    }
    setAlert(true);
  };

  const InputEventLogin = (event) => {
    const { name, value } = event.target;

    setItemInput((preVal) => {
      return {
        ...preVal,
        [name]: value,
      };
    });
  };
  function refreshPage() {
    window.location.reload(false);
  }
  return (
    <>
      <nav className="navbar navbar-expand-lg">
        <div className="container">
          <div
            className={navOpen ? 'hamburger nav-open' : 'hamburger'}
            onClick={() => setnavOpen(!navOpen)}
          >
            <span></span>
            <span></span>
            <span></span>
          </div>
          <div className={navOpen ? 'closed open' : 'closed'}>
            <ul className={navOpen ? 'menu nav-open' : 'menu'}>
              <li className="menu_header my-5">
                <a
                  href="https://mitkatrisktracker.com/covid-19"
                  target="_blank"
                  className="text-white"
                >
                  Covid Dashboard
                </a>
              </li>
              <li className="menu_header">
                <a href="#" className="">
                  Our Platform
                </a>
              </li>

              <li
                className="menu-sports has-submenu"
                onClick={() => setsubMenuOpenRime(!subMenuOpenRime)}
              >
                <a href="#" className="">
                  Risk Intelligence Platform
                </a>
                <ul
                  className={subMenuOpenRime ? 'sub-menu active' : 'sub-menu'}
                >
                  <a className="back-to-menu">
                    <FontAwesomeIcon
                      icon={faArrowLeft}
                      className="main-header__link__arrow"
                    />
                    &nbsp; Back
                  </a>
                  <h3 className="title">Risk Intelligence Platform</h3>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/risk-intelligence-monitoring-engine"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Introduction
                    </NavLink>
                  </li>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/risk-intelligence-monitoring-engine"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Demo
                    </NavLink>
                  </li>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/risk-intelligence-monitoring-engine"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      API integration
                    </NavLink>
                  </li>
                </ul>
              </li>

              <li
                className="menu-sports has-submenu"
                onClick={() => setsubMenuOpenCovid(!subMenuOpenCovid)}
              >
                <a href="#" className="">
                  COVID-19 Platform
                </a>
                <ul
                  className={subMenuOpenCovid ? 'sub-menu active' : 'sub-menu'}
                >
                  <a className="back-to-menu">
                    <FontAwesomeIcon
                      icon={faArrowLeft}
                      className="main-header__link__arrow"
                    />
                    &nbsp; Back
                  </a>
                  <h3 className="title">COVID-19 Platform</h3>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/covid19-dashboard"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Introduction
                    </NavLink>
                  </li>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/covid19-dashboard"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Demo
                    </NavLink>
                  </li>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/covid19-dashboard"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      API integration
                    </NavLink>
                  </li>
                </ul>
              </li>

              <li
                className="menu-sports has-submenu"
                onClick={() => setsubMenuOpenSap(!subMenuOpenSap)}
              >
                <a href="#" className="">
                  Security Audit Platform
                </a>
                <ul className={subMenuOpenSap ? 'sub-menu active' : 'sub-menu'}>
                  <a className="back-to-menu">
                    <FontAwesomeIcon
                      icon={faArrowLeft}
                      className="main-header__link__arrow"
                    />
                    &nbsp; Back
                  </a>
                  <h3 className="title">Security Audit Platform</h3>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/security-audit"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Introduction
                    </NavLink>
                  </li>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/security-audit"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Demo
                    </NavLink>
                  </li>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/security-audit"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      API integration
                    </NavLink>
                  </li>
                </ul>
              </li>

              <li className="menu_header mt-5">
                <a href="#" className="">
                  Our Services
                </a>
              </li>

              <li
                className="menu-sports has-submenu"
                onClick={() => setsubMenuOpenPri(!subMenuOpenPri)}
              >
                <a href="#" className="">
                  Predictive Risk Intelligence
                </a>
                <ul className={subMenuOpenPri ? 'sub-menu active' : 'sub-menu'}>
                  <a className="back-to-menu">
                    <FontAwesomeIcon
                      icon={faArrowLeft}
                      className="main-header__link__arrow"
                    />
                    &nbsp; Back
                  </a>
                  <h3 className="title">Predictive Risk Intelligence</h3>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/predictive-risk-intelligence"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Introduction
                    </NavLink>
                  </li>
                  <li className="sub-title">
                    <p className="sub-title-header"> Service Offering </p>
                    <NavLink
                      exact
                      to="/predictive-risk-intelligence"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Risk Monitoring Platform (MitKat Risk Tracker) and Mobile
                      Application
                    </NavLink>
                    <NavLink
                      exact
                      to="/predictive-risk-intelligence"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Customised Risk Management Search Engine and Dashboards
                    </NavLink>
                    <NavLink
                      exact
                      to="/predictive-risk-intelligence"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Protective Intelligence
                    </NavLink>
                    <NavLink
                      exact
                      to="/predictive-risk-intelligence"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Alerts and Advisory Services
                    </NavLink>
                    <NavLink
                      exact
                      to="/predictive-risk-intelligence"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Command Centre Operations
                    </NavLink>
                    <NavLink
                      exact
                      to="/predictive-risk-intelligence"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Travel Risk Management
                    </NavLink>
                    <NavLink
                      exact
                      to="/predictive-risk-intelligence"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Country/ state/ city/ location Risk Assessments
                    </NavLink>
                    <NavLink
                      exact
                      to="/predictive-risk-intelligence"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Bespoke Research and Analysis
                    </NavLink>
                    <NavLink
                      exact
                      to="/predictive-risk-intelligence"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      24x7 Helpline
                    </NavLink>
                  </li>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/predictive-risk-intelligence"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Our Tools and Reports
                    </NavLink>
                  </li>

                  <li className="sub-title">
                    <p className="sub-title-header">Case Studies </p>
                    <NavLink
                      exact
                      to="/casestudy_pri_01"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Enabling rural electrification operations in challenging
                      regions
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_pri_02"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Comprehensive risk assessment for Oil & Gas operations in
                      North East India
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_pri_03"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Enabling business for a global multi-national Banking &
                      Financial organisation
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_pri_04"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Predictive Risk Intelligence for a multi-national Internet
                      Technology company to continue business across APAC
                      countries
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_pri_05"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Ensuring employee safety during COVID-19 pandemic and
                      enabling operational efficiency
                    </NavLink>
                  </li>
                </ul>
              </li>

              <li
                className="menu-sports has-submenu"
                onClick={() => setsubMenuOpenPess(!subMenuOpenPess)}
              >
                <a href="#" className="">
                  Physical, Environmental Security and Safety <br /> (PESS) Risk
                  Consulting
                </a>
                <ul
                  className={subMenuOpenPess ? 'sub-menu active' : 'sub-menu'}
                >
                  <a className="back-to-menu">
                    <FontAwesomeIcon
                      icon={faArrowLeft}
                      className="main-header__link__arrow"
                    />
                    &nbsp; Back
                  </a>
                  <h3 className="title">
                    Physical, Environmental Security and Safety (PESS) Risk
                    Consulting
                  </h3>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/physical-environmental-security-and-safety-risk-consulting"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Introduction
                    </NavLink>
                  </li>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/physical-environmental-security-and-safety-risk-consulting"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Data Center Capability
                    </NavLink>
                  </li>

                  <li className="sub-title">
                    <p className="sub-title-header"> Service Offering </p>
                    <NavLink
                      exact
                      to="/physical-environmental-security-and-safety-risk-consulting"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Enterprise Security Risk Management (ESRM)
                    </NavLink>
                    <NavLink
                      exact
                      to="/physical-environmental-security-and-safety-risk-consulting"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Assessment & Audit
                    </NavLink>
                    <NavLink
                      exact
                      to="/physical-environmental-security-and-safety-risk-consulting"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Operational Support Services
                    </NavLink>
                    <NavLink
                      exact
                      to="/physical-environmental-security-and-safety-risk-consulting"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Managed Security Services
                    </NavLink>
                    <NavLink
                      exact
                      to="/physical-environmental-security-and-safety-risk-consulting"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Training & Team management
                    </NavLink>
                  </li>

                  <li className="sub-title">
                    <p className="sub-title-header">Case Studies </p>
                    <NavLink
                      exact
                      to="/physical-environmental-security-and-safety-risk-consulting"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Enterprise-wide Security Assessment for ONGC
                    </NavLink>
                    <NavLink
                      exact
                      to="/physical-environmental-security-and-safety-risk-consulting"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Operational Support to Asian Oilfield Services Limited
                      (AOSL) in Iraqi Kurdistan
                    </NavLink>
                    <NavLink
                      exact
                      to="/physical-environmental-security-and-safety-risk-consulting"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Electrical Safety and Fire Safety Audit for a Leading
                      Retail Manufacturer in Mumbai
                    </NavLink>
                    <NavLink
                      exact
                      to="/physical-environmental-security-and-safety-risk-consulting"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Pan India EHS & Fire Safety Audit for a Leading Insurance
                      Provider
                    </NavLink>
                    <NavLink
                      exact
                      to="/physical-environmental-security-and-safety-risk-consulting"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Pan India Security & Fire Safety Audit for a Leading Bank
                    </NavLink>
                    <NavLink
                      exact
                      to="/physical-environmental-security-and-safety-risk-consulting"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Fire Safety Audit and Implementation Support for a Leading
                      MNC - World’s Largest Alcobev Company
                    </NavLink>
                    <NavLink
                      exact
                      to="/physical-environmental-security-and-safety-risk-consulting"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Fire Safety Audit and Implementation for one of the
                      World’s Largest IT Consulting, Outsourcing and
                      Professional Services Companies.
                    </NavLink>
                    <NavLink
                      exact
                      to="/physical-environmental-security-and-safety-risk-consulting"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Pan India Fire Safety, OHS & Electrical Thermography Audit
                      for a Leading Fashion Retails
                    </NavLink>
                  </li>
                </ul>
              </li>

              <li
                className="menu-sports has-submenu"
                onClick={() => setsubMenuOpenSdcpm(!subMenuOpenSdcpm)}
              >
                <a href="#" className="">
                  Security Design Consulting & Project Management
                </a>
                <ul
                  className={subMenuOpenSdcpm ? 'sub-menu active' : 'sub-menu'}
                >
                  <a className="back-to-menu">
                    <FontAwesomeIcon
                      icon={faArrowLeft}
                      className="main-header__link__arrow"
                    />
                    &nbsp; Back
                  </a>
                  <h3 className="title">
                    Security Design Consulting & Project Management
                  </h3>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/security-design-consulting-and-project-management"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Introduction
                    </NavLink>
                  </li>

                  <li className="sub-title">
                    <p className="sub-title-header"> Service Offering </p>
                    <NavLink
                      exact
                      to="/security-design-consulting-and-project-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Security Engineering & Design – greenfield & brownfield
                      projects
                    </NavLink>
                    <NavLink
                      exact
                      to="/security-design-consulting-and-project-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Critical Infrastructure Protection
                    </NavLink>
                    <NavLink
                      exact
                      to="/security-design-consulting-and-project-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Envision & Design of Security Operation Center (SOC)
                    </NavLink>
                    <NavLink
                      exact
                      to="/security-design-consulting-and-project-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Project Management
                    </NavLink>
                    <NavLink
                      exact
                      to="/security-design-consulting-and-project-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Security re-engineering & Cost Optimization
                    </NavLink>
                    <NavLink
                      exact
                      to="/security-design-consulting-and-project-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Blast Impact Analysis (BIA)
                    </NavLink>
                    <NavLink
                      exact
                      to="/security-design-consulting-and-project-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Other Security Consulting services
                    </NavLink>
                  </li>

                  <li className="sub-title">
                    <p className="sub-title-header">Case Studies </p>
                    <NavLink
                      exact
                      to="/casestudy_sdcpm_01"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Design of National Security Command Room (NSCR) for Indian
                      Multi-National conglomerate
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_sdcpm_02"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Security Technology Transformation of India's Largest IT &
                      Consulting Company
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_sdcpm_03"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Security Design Consulting Services of its Globally Second
                      Largest Campus for a British Baker
                    </NavLink>
                  </li>
                </ul>
              </li>

              <li
                className="menu-sports has-submenu"
                onClick={() => setsubMenuOpenIgs(!subMenuOpenIgs)}
              >
                <a href="#" className="">
                  Infrastructure & Government Services
                </a>
                <ul className={subMenuOpenIgs ? 'sub-menu active' : 'sub-menu'}>
                  <a className="back-to-menu">
                    <FontAwesomeIcon
                      icon={faArrowLeft}
                      className="main-header__link__arrow"
                    />
                    &nbsp; Back
                  </a>
                  <h3 className="title">
                    Infrastructure & Government Services
                  </h3>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/infrastructure-and-government-services"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Introduction
                    </NavLink>
                  </li>

                  <li className="sub-title">
                    <p className="sub-title-header"> Service Offering </p>
                    <NavLink
                      exact
                      to="/infrastructure-and-government-services"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Government Sector Transformation Projects incl physical
                      and logical security enhancements
                    </NavLink>
                    <NavLink
                      exact
                      to="/infrastructure-and-government-services"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Aerospace & Defence Consulting Services
                    </NavLink>
                    <NavLink
                      exact
                      to="/infrastructure-and-government-services"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Smart City, Smart Cantt and Smart Campus projects
                    </NavLink>
                    <NavLink
                      exact
                      to="/infrastructure-and-government-services"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Specialized products and solutions
                    </NavLink>
                    <NavLink
                      exact
                      to="/infrastructure-and-government-services"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Project Management & Delivery Assistance
                    </NavLink>
                    <NavLink
                      exact
                      to="/infrastructure-and-government-services"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Digital Forensics
                    </NavLink>
                  </li>
                </ul>
              </li>

              <li
                className="menu-sports has-submenu"
                onClick={() => setsubMenuOpenCsr(!subMenuOpenCsr)}
              >
                <a href="#" className="">
                  Cyber Security and Resilience
                </a>
                <ul className={subMenuOpenCsr ? 'sub-menu active' : 'sub-menu'}>
                  <a className="back-to-menu">
                    <FontAwesomeIcon
                      icon={faArrowLeft}
                      className="main-header__link__arrow"
                    />
                    &nbsp; Back
                  </a>
                  <h3 className="title">Cyber Security and Resilience</h3>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/cyber-security-and-resilience"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Introduction
                    </NavLink>
                  </li>

                  <li className="sub-title">
                    <p className="sub-title-header"> Service Offering </p>
                    <NavLink
                      exact
                      to="/cyber-security-and-resilience"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Information Risk Assurance
                    </NavLink>
                    <NavLink
                      exact
                      to="/cyber-security-and-resilience"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Cyber Security
                    </NavLink>
                    <NavLink
                      exact
                      to="/cyber-security-and-resilience"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Business Continuity Management (BCM) – Advisory,
                      Implementation & Sustenance
                    </NavLink>
                    <NavLink
                      exact
                      to="/cyber-security-and-resilience"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Managed Security Services (MSS)
                    </NavLink>
                    <NavLink
                      exact
                      to="/cyber-security-and-resilience"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      NexGen SOC
                    </NavLink>
                  </li>

                  <li className="sub-title">
                    <p className="sub-title-header">Case Studies </p>
                    <NavLink
                      exact
                      to="/casestudy_csr_01"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      ISMS audit of an engineering service provider on behalf of
                      an Indian Bank
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_csr_02"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Cybersecurity assessment of IT infrastructure of a leading
                      media company
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_csr_03"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Remediation of a hacking incident and designing
                      cybersecurity policies to prevent future attack for a
                      leading chain of healthcare Organisation
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_csr_04"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Cyber assurance for a cloud migration initiative of a
                      leading FMCG client
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_csr_05"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Implementation of DLP policies for a leading Financial
                      Organisation and its group companies
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_csr_06"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Designing of Business Continuity Management system for a
                      European Bank’s GIC centre
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_csr_07"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Crisis Management simulation exercise for a global
                      conglomerate GIC
                    </NavLink>
                  </li>
                </ul>
              </li>

              <li
                className="menu-sports has-submenu"
                onClick={() => setsubMenuOpenIrm(!subMenuOpenIrm)}
              >
                <a href="#" className="">
                  Integrity Risk Management
                </a>
                <ul className={subMenuOpenIrm ? 'sub-menu active' : 'sub-menu'}>
                  <a className="back-to-menu">
                    <FontAwesomeIcon
                      icon={faArrowLeft}
                      className="main-header__link__arrow"
                    />
                    &nbsp; Back
                  </a>
                  <h3 className="title">Integrity Risk Management</h3>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/integrity-risk-management"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Introduction
                    </NavLink>
                  </li>

                  <li className="sub-title">
                    <p className="sub-title-header"> Service Offering </p>
                    <NavLink
                      exact
                      to="/integrity-risk-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Brand Enforcement & Protection
                    </NavLink>
                    <NavLink
                      exact
                      to="/integrity-risk-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Intellectual Property Protection (IPP)
                    </NavLink>
                    <NavLink
                      exact
                      to="/integrity-risk-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Litigation Support
                    </NavLink>
                    <NavLink
                      exact
                      to="/integrity-risk-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Corporate Due Diligence Investigation
                    </NavLink>
                    <NavLink
                      exact
                      to="/integrity-risk-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Undercover Corporate Workplace Investigations
                    </NavLink>
                    <NavLink
                      exact
                      to="/integrity-risk-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Investigative Interviewing
                    </NavLink>
                    <NavLink
                      exact
                      to="/integrity-risk-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Forensic Accounting & Fraud Investigation Services
                    </NavLink>
                    <NavLink
                      exact
                      to="/integrity-risk-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Asset Tracing
                    </NavLink>
                    <NavLink
                      exact
                      to="/integrity-risk-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Skip Tracing
                    </NavLink>
                    <NavLink
                      exact
                      to="/integrity-risk-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Whistleblowers’ Claims
                    </NavLink>
                    <NavLink
                      exact
                      to="/integrity-risk-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Cyber Forensic And Review Of Evidence
                    </NavLink>
                    <NavLink
                      exact
                      to="/integrity-risk-management"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Mystery Shopping
                    </NavLink>
                  </li>

                  <li className="sub-title">
                    <p className="sub-title-header">Case Studies </p>
                    <NavLink
                      exact
                      to="/casestudy_irm_01"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Enabling Due Diligence Investigation on Procurement Fraud
                      in a Multinational Company in India
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_irm_02"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Comprehensive Due Diligence Investigation for leading
                      Japan-Headquartered MNC in India
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_irm_03"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Comprehensive Due Diligence Investigation for leading
                      Mauritius-based real estate MNC in India
                    </NavLink>
                  </li>
                </ul>
              </li>

              <li
                className="menu-sports has-submenu"
                onClick={() => setsubMenuOpenTwd(!subMenuOpenTwd)}
              >
                <a href="#" className="">
                  Training & Workforce Development
                </a>
                <ul className={subMenuOpenTwd ? 'sub-menu active' : 'sub-menu'}>
                  <a className="back-to-menu">
                    <FontAwesomeIcon
                      icon={faArrowLeft}
                      className="main-header__link__arrow"
                    />
                    &nbsp; Back
                  </a>
                  <h3 className="title">Training & Workforce Development</h3>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Introduction
                    </NavLink>
                  </li>

                  <li className="sub-title">
                    <p className="sub-title-header"> Service Offering </p>
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Women’s Safety Training
                    </NavLink>
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Cyber Security Training
                    </NavLink>
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Crisis Simulation & Emergency Response Training
                    </NavLink>
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Command Centre Operations Training
                    </NavLink>
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Risk Monitoring & Report Writing
                    </NavLink>
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Fire, Life Safety & First-Aid Training
                    </NavLink>
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Return to Office Guidelines
                    </NavLink>
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Corporate Security Training
                    </NavLink>
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Corporate Intel Training
                    </NavLink>
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      POSH & Gender Sensitization
                    </NavLink>
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Workplace Ergonomics
                    </NavLink>
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Road Safety/Safe Driving
                    </NavLink>
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Workplace Violence
                    </NavLink>
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Online Masterclass
                    </NavLink>
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Safety & Security Week Training
                    </NavLink>
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Business Continuity Management Training
                    </NavLink>
                  </li>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Our Approach
                    </NavLink>
                  </li>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Our Process
                    </NavLink>
                  </li>

                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Our USP
                    </NavLink>
                  </li>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Suggested Tools and Techniques
                    </NavLink>
                  </li>

                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/training-and-workforce-development"
                      onClick={() => setnavOpen(!navOpen)}
                      className="sub-title-header"
                    >
                      Our Collaterals
                    </NavLink>
                  </li>

                  <li className="sub-title">
                    <p className="sub-title-header">Case Studies </p>
                    <NavLink
                      exact
                      to="/casestudy_twd_01"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Addressing Women’s Safety Concerns
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_twd_02"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Bringing About Behavioural Change
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_twd_03"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Addressing Unconscious Bias Through e-Learning Modules
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_twd_04"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Online Masterclass Series
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_twd_05"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Inculcating a Security/Safety Culture Within an
                      Organisation
                    </NavLink>
                  </li>
                </ul>
              </li>

              <li
                className="menu-sports has-submenu"
                onClick={() => setsubMenuOpenMss(!subMenuOpenMss)}
              >
                <a href="#" className="">
                  Managed Security Services (MSS)
                </a>
                <ul className={subMenuOpenMss ? 'sub-menu active' : 'sub-menu'}>
                  <a className="back-to-menu">
                    <FontAwesomeIcon
                      icon={faArrowLeft}
                      className="main-header__link__arrow"
                    />
                    &nbsp; Back
                  </a>
                  <h3 className="title">Managed Security Services (MSS)</h3>
                  <li className="sub-title">
                    <NavLink
                      exact
                      to="/managed-security-services"
                      className="sub-title-header"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Introduction
                    </NavLink>
                  </li>

                  <li className="sub-title">
                    <p className="sub-title-header"> Service Offering </p>
                    <NavLink
                      exact
                      to="/managed-security-services"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Outsourced Security Leadership
                    </NavLink>
                    <NavLink
                      exact
                      to="/managed-security-services"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Outsourced Expertise
                    </NavLink>
                    <NavLink
                      exact
                      to="/managed-security-services"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Project Management
                    </NavLink>
                    <NavLink
                      exact
                      to="/managed-security-services"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Training & Sustenance
                    </NavLink>
                  </li>

                  <li className="sub-title">
                    <p className="sub-title-header">Case Studies </p>
                    <NavLink
                      exact
                      to="/casestudy_mss_01"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Staffing for South Asia Command Centre for a leading MNC
                      Bank based in Mumbai
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_mss_02"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Creation of Security Function of one of the India’s
                      Largest ITES Conglomerate and Managing Operations
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_mss_03"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Site Security Leadership for a leading Global FMCG Company
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_mss_04"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Provision of PMO for Risk Assessment, Integrated Security
                      Architecture & Security System; Project Implementation
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_mss_05"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Security Project Manager for a leading Global Financial
                      Sector Conglomerate
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_mss_06"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Provision of PMO for Risk Assessment, Designing
                      Architecture & Security System; Project Implementation at
                      Pune
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_mss_07"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Control Room Design & Operations for a global mining
                      company in Zambia, Africa
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_mss_08"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Operational Support to multiple oil projects in Iraqi
                      Kurdistan Region
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_mss_09"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Provision of Chief Security Officer (CSO) for a leading
                      Healthcare Company
                    </NavLink>
                    <NavLink
                      exact
                      to="/casestudy_mss_10"
                      onClick={() => setnavOpen(!navOpen)}
                    >
                      Provision of Corporate Security Manager for a Agri -
                      Business Company based out of Pune
                    </NavLink>
                  </li>
                </ul>
              </li>
              <li className="menu_header mt-5 ">
                <NavLink
                  exact
                  to="/career"
                  onClick={() => setnavOpen(!navOpen)}
                >
                  Careers
                </NavLink>
              </li>
              <li className="menu_header mt-5">
                <a href="#" className="">
                  Resources
                </a>
              </li>
              <li className="menu-sports has-submenu">
                <NavLink exact to="/advisories">
                  Advisories
                </NavLink>
              </li>
              <li className="menu-sports has-submenu">
                <NavLink exact to="/monthly">
                  Monthly
                </NavLink>
              </li>
              <li className="menu-sports has-submenu">
                <NavLink exact to="/special-reports">
                  Special Reports
                </NavLink>
              </li>
              <li className="menu-sports has-submenu">
                <NavLink exact to="/risk-reviews">
                  Risk Reviews
                </NavLink>
              </li>
              <li className="menu-sports has-submenu">
                <NavLink exact to="/blogs">
                  Blogs
                </NavLink>
              </li>
              <li className="menu-sports has-submenu">
                <NavLink exact to="/recent-news">
                  Events
                </NavLink>
              </li>
              <li className="menu-sports has-submenu">
                <NavLink exact to="/podcast">
                  Podcast
                </NavLink>
              </li>
              <li className="menu_header mt-5 ">
                <NavLink exact to="/about">
                  About Us
                </NavLink>
              </li>
              <li className="menu_header mt-5">
                <NavLink
                  exact
                  to="/contact"
                  onClick={() => setnavOpen(!navOpen)}
                >
                  Contact Us
                </NavLink>
              </li>
              <li className="menu_header mt-5">
                <a
                  className="text-white"
                  data-toggle="modal"
                  data-target="#loginModal"
                  onClick={() => setnavOpen(!navOpen)}
                >
                  Sign in
                </a>
              </li>
            </ul>
          </div>
          <NavLink className="navbar__brand" to="/">
            <img src={logo} alt="Logo" />
          </NavLink>
          <div className={navOpen ? ' closed ' : 'open'}>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarTogglerDemo03"
              aria-controls="navbarTogglerDemo03"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>

            <div id="navbarTogglerDemo03" className="collapse navbar-collapse">
              <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                <li className="nav-item">
                  <a
                    href="https://mitkatrisktracker.com/covid-19"
                    target="_blank"
                    className="nav-link nav-link__horizontal"
                  >
                    Covid Dashboard
                  </a>
                </li>
                {/* <li className="nav-item">
                                <NavLink exact activeClassName="navbar__link-active" className="nav-link nav-link__horizontal" to="/">Home </NavLink>
                            </li>  */}

                <li className="nav-item dropdown">
                  <a
                    className="nav-link nav-link__horizontal dropdown-toggle"
                    href="#"
                    id="platformDropdown"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Platforms
                  </a>
                  <div
                    className="dropdown-menu"
                    aria-labelledby="platformDropdown"
                  >
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/risk-intelligence-monitoring-engine"
                    >
                      Risk Intelligence Monitoring Engine
                    </NavLink>
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/security-audit"
                    >
                      Security Audit Platform
                    </NavLink>
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/covid19-dashboard"
                    >
                      Covid19 Platform
                    </NavLink>
                  </div>
                </li>
                <li className="nav-item dropdown">
                  <a
                    className="nav-link nav-link__horizontal dropdown-toggle"
                    href="#"
                    id="serviceDropdown"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Services
                  </a>
                  <div
                    className="dropdown-menu"
                    aria-labelledby="serviceDropdown"
                  >
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/predictive-risk-intelligence"
                    >
                      Predictive Risk Intelligence
                    </NavLink>
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/physical-environmental-security-and-safety-risk-consulting"
                    >
                      Physical, Environmental Security and Safety (PESS) Risk
                      Consulting
                    </NavLink>
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/security-design-consulting-and-project-management"
                    >
                      Security Design Consulting & Project Management
                    </NavLink>
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/infrastructure-and-government-services"
                    >
                      Infrastructure & Government Services
                    </NavLink>
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/cyber-security-and-resilience"
                    >
                      Cyber Security and Resilience
                    </NavLink>
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/integrity-risk-management"
                    >
                      Integrity Risk Management
                    </NavLink>
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/training-and-workforce-development"
                    >
                      Training & Workforce Development
                    </NavLink>
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/managed-security-services"
                    >
                      Managed Security Services (MSS)
                    </NavLink>
                  </div>
                </li>
                {/* <li className="nav-item">
                                <NavLink exact activeClassName="navbar__link-active" className="nav-link nav-link__horizontal" to="/contact">Blog </NavLink>
                            </li>*/}
                <li className="nav-item">
                  <NavLink
                    exact
                    activeClassName="navbar__link-active"
                    className="nav-link nav-link__horizontal"
                    to="/career"
                  >
                    Careers
                  </NavLink>
                </li>
                <li className="nav-item dropdown">
                  <a
                    className="nav-link nav-link__horizontal dropdown-toggle"
                    href="#"
                    id="resourcesDropdown"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Resources
                  </a>
                  <div
                    className="dropdown-menu"
                    aria-labelledby="resourcesDropdown"
                  >
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/advisories"
                    >
                      Advisories
                    </NavLink>
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/monthly"
                    >
                      Monthly
                    </NavLink>
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/special-reports"
                    >
                      Special Reports
                    </NavLink>
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/risk-reviews"
                    >
                      Risk Reviews
                    </NavLink>
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/blogs"
                    >
                      Blogs
                    </NavLink>
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/recent-news"
                    >
                      Events
                    </NavLink>
                    <NavLink
                      className="dropdown-item"
                      exact
                      activeClassName="navbar__link-active"
                      to="/podcast"
                    >
                      Podcast
                    </NavLink>
                  </div>
                </li>
                <li className="nav-item">
                  <NavLink
                    exact
                    activeClassName="navbar__link-active"
                    className="nav-link nav-link__horizontal"
                    to="/about"
                  >
                    About Us
                  </NavLink>
                </li>
              </ul>

              <ul className="navbar-nav ml-auto mt-2 mt-lg-0 right-list">
                <li className="nav-item">
                  <NavLink
                    exact
                    activeClassName="navbar__link-active"
                    className="nav-link py-0"
                    to="/contact"
                  >
                    Contact
                  </NavLink>
                </li>
                <li className="nav-item">
                  <a
                    exact
                    activeClassName="navbar__link-active"
                    className="nav-link py-0"
                    data-toggle="modal"
                    data-target="#loginModal"
                  >
                    Sign in
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>

      <div
        className="modal fade"
        id="loginModal"
        tabindex="-1"
        aria-labelledby="loginModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Sign in
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="d-flex bg-darker py-5">
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-12 col-md-12 col-lg-12 m-a d-flex justify-content-center flex-column">
                      <form
                        className="contact-form px-3"
                        onSubmit={formLoginSubmit}
                      >
                        <div className="row">
                          <div className="col-md-12 col-12">
                            <div className="form-group mb-3">
                              <label className="text-white">
                                Email address
                              </label>
                              <input
                                type="email"
                                name="userid"
                                onChange={InputEventLogin}
                                className="form-control contact-form__input"
                                placeholder="Your Email *"
                                value={itemInput.userid}
                              />
                            </div>
                            <div className="form-group mb-3">
                              <label className="text-white">Password </label>
                              <input
                                type="password"
                                name="password"
                                onChange={InputEventLogin}
                                className="form-control contact-form__input"
                                placeholder="Your Password *"
                                value={itemInput.password}
                              />
                            </div>
                          </div>
                          {alert && (
                            <p className="text-danger">
                              Please Provide valid data
                            </p>
                          )}
                          <div className="col-md-12 col-12">
                            <div className="form-group">
                              <input
                                type="submit"
                                name="btnSubmit"
                                className="btn contact-form__btn"
                                value="Sign In"
                              />
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Navbar;
