import React from 'react';
import Navbar from './Navbar';
import Footer from './Footer';

const NoPage = () => {
  return (
    <div>
      <Navbar />
      <div id="noPage"></div>
      <Footer />
    </div>
  );
};

export default NoPage;
