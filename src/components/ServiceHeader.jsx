import React from 'react';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

const ServiceHeader = (props) => {
  return (
    <>
      <div className="service-header">
        <div className="row justify-content-center m-0">
          <div className="col-12 col-md-12 col-lg-12 p-0">
            <img src={props.imgsrc} width="100%" />
          </div>
          <div className="col-12 col-md-9 col-lg-9">
            <div className="service-description">
              <div className="display-block m-a">
                <div className="content">
                  <h1 className="mb-3 service-description__header text-center">
                    {props.title}
                  </h1>
                  <p className="content__subheader py-3 m-0">
                    {props.description}
                    <br /> <br />
                    <NavLink
                      exact
                      to="/book_demo
"
                      rel="noopener noreferrer"
                      className="main-header__btn"
                    >
                      {props.linkTitle} &nbsp; &nbsp;
                      <FontAwesomeIcon
                        icon={faArrowRight}
                        className="main-header__link__arrow"
                      />
                    </NavLink>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ServiceHeader;
