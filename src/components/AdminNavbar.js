import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import logo from '../assets/logo_white.png';
const AdminNavbar = () => {
  const [navOpen, setnavOpen] = useState(false);

  useEffect(() => {
    document.body.classList.toggle('overflow-hidden', navOpen);
  }, [navOpen]);

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-darker row">
        <div className="container px-4">
          <NavLink className="navbar__brand" to="/">
            {' '}
            <img src={logo} alt="Logo" />{' '}
          </NavLink>
          <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
            <li className="nav-item">
              <NavLink
                exact
                activeClassName="navbar__link-active"
                className="nav-link text-white"
                to="/"
              >
                Sign out{' '}
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
    </>
  );
};

export default AdminNavbar;
