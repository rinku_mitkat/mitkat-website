import React from 'react';
import ShowMoreText from 'react-show-more-text';

const ServiceOffering = (props) => { 
    

    return(
        <> 
            <div className="block-box p-0">               
                <img className="block-box__img service-offering__img" src={props.imgsrc} width="100%" />       
                <h5 className="block-box__title px-3">
                    {props.title}
                </h5>
                <ShowMoreText 
                    lines={3}
                    more='Show more'
                    less='Show less'
                    className='content-css block-box__description p-3'
                    anchorClassName='my-anchor-css-class'
                    
                    expanded={false}
                    width={280}
                    keepNewLines={true} >
                    {props.description}
                </ShowMoreText>
            </div>
        </>
    )
};

export default ServiceOffering;