import React from 'react';  

const Copyright = () => {
    return (
        <>     
        <footer className="bg-transparent text-white">
            <div className="container-fluid">             
                <div className="row"> 
                    <div className="col-12">
                        <span className="float-left ">&copy; 2010-{(new Date().getFullYear())} MitKat Advisory Services Pvt Ltd. All Rights Reserved </span>
                    </div> 
                </div>
            </div>
        </footer>   
        

        </>
    );
}

export default Copyright;