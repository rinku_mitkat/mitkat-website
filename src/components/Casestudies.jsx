import React from 'react'; 
import AliceCarousel from 'react-alice-carousel';

const responsiveC = {
    0: { items: 1 },
    568: { items: 2 },
    1024: { items: 3 },
};

const Casestudies = (props) => {
    return (
        <>            
           <AliceCarousel mouseTracking items={props.item} responsive={responsiveC} />
        </>
    );
} 

export default Casestudies ; 