import React from 'react';
import quoteIcon from '../assets/images/arrow/quote.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons';

import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

const AboutSection = (props) => {
  return (
    <>
      <div className="section d-flex">
        <div className="container-fluid">
          <div className="row section__row">
            <div className="col-lg-6 col-md-6 col-12">
              <img src={props.imgsrc} width="100%" />
            </div>
            <div className="col-lg-6 col-md-6 col-12 my-auto">
              <div className="content">
                <NavLink exact to="/about" className="main-header__link">
                  Back to team &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <h1 className="my-3 main-header__header-font text-capitalize">
                  {' '}
                  {props.name}
                  <br />
                  <small className="smaller"> {props.award} </small>
                </h1>

                <p className="about__designation py-3">{props.designation}</p>
                <blockquote className="leader-quote" cite={props.linkedInLink}>
                  <img src={quoteIcon} height="35px;" />
                  {props.quote}
                </blockquote>
                <p className="sub-header  py-3">
                  <a
                    className="badge badge-pill badge-dark text-white"
                    href={props.linkedInLink}
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faLinkedinIn} />
                  </a>
                  {/* <a className="badge badge-pill badge-dark text-white" href={props.twitterLink} target="_blank">
                                        <FontAwesomeIcon icon={faTwitter} />  
                                    </a>            */}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AboutSection;
