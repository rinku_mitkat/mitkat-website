import React, { useEffect, useState } from 'react';
import { getTopEvent } from '../service/api';
import { faBell } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const TopEvents = () => {
  const [eventList, setEventList] = useState({
    success: '',
    message: '',
    output: [],
  });
  const [visible, setVisible] = useState(true);
  const RISK_LEVEL_COLORS = {
    1: '#EBF7C9', //verylow
    2: '#b8e994', //low
    3: '#FEEA81', //medium
    4: '#FCD293', //high
    5: '#E48F6F', //veryhigh
  };
  function hideMe() {
    setVisible(false);
  }

  useEffect(() => {
    let mounted = true;
    getTopEvent().then((items) => {
      if (mounted) {
        setEventList(items);
      }
    });
    return () => (mounted = false);
  }, []);

  return (
    <>
      {eventList.output.length > 0 ? (
        <div
          className={visible ? 'visibleBox topEvents' : 'hiddenBox topEvents'}
        >
          <p onClick={() => setVisible(!visible)} className="rw-header-btn">
            <FontAwesomeIcon icon={faBell} />
          </p>
          <ul>
            <marquee scrollamount="5" direction="left" height="50px" loop="">
              {eventList.output.map((item) => (
                <li>
                  <b
                    style={{
                      backgroundColor: `${RISK_LEVEL_COLORS[item.riskLevelid]}`,
                      border: 'none',
                    }}
                  >
                    &nbsp; &nbsp; &nbsp;
                  </b>
                  <span> {item.country} : </span>
                  {item.title}
                </li>
              ))}
            </marquee>
          </ul>
          {/* <section className="rw-wrapper">
            <h2 className="rw-sentence">
              <div className="rw-words rw-words-1">
                {eventList.output.map((item) => ( 
                    
                  <span className="">
                    <p className="country-name">
                      {item.country}
                      {item.title}
                      <b
                        style={{
                          backgroundColor: `${
                            RISK_LEVEL_COLORS[item.riskLevelid]
                          }`,
                          border: 'none',
                        }}
                      >
                        
                        &nbsp;
                      </b>
                      {item.risklevel}
                    </p>
                    <img src={item.imageURL} />
                  </span>
                ))}
              </div>
            </h2>
          </section> */}
        </div>
      ) : null}
    </>
  );
};

export default TopEvents;
