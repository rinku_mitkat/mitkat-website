import React, { useState, useEffect } from 'react';
import AliceCarousel from 'react-alice-carousel';
import { getTopNews } from '../service/api';
import { NavLink } from 'react-router-dom';

const handleDragStart = (e) => e.preventDefault();
const responsiveN = {
  0: { items: 1 },
  568: { items: 2 },
  1024: { items: 3 },
};

const News = () => {
  const [topNewsList, setTopNewsList] = useState([]);
  const monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'June',
    'July',
    'Aug',
    'Sept',
    'Oct',
    'Nov',
    'Dec',
  ];
  useEffect(() => {
    getTopNews().then((advisories) => {
      setTopNewsList(advisories);
    });
  }, []);
  return (
    <>
      <AliceCarousel
        autoPlay
        autoPlayInterval="3000"
        mouseTracking
        responsive={responsiveN}
      >
        {topNewsList.map((item) => (
          <div className="block-box ">
            <img
              src={item.coverpic}
              onDragStart={handleDragStart}
              className="block-box__img"
              alt={item.title}
            />
            <h5 className="block-box__title">{item.title}</h5>
            <div className="block-box__by">
              <em>
                {`${new Date(item.date).getDate()}`} &nbsp;
                {`${monthNames[new Date(item.date).getMonth()]}`} &nbsp;
                {`${new Date(item.date).getFullYear()}`} &nbsp;
              </em>
              - {item.summary}
              <p className="mt-3">
                <a href={item.link} target="_blank">
                  View Video
                </a>
              </p>
            </div>
          </div>
        ))}
      </AliceCarousel>
      <div className="row">
        <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 my-3 text-center">
          <NavLink exact className="btn btn-primary" to="/recent-news">
            View all
          </NavLink>
        </div>
      </div>
    </>
  );
};

export default News;
