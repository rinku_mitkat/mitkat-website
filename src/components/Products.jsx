import React from 'react'; 
import AliceCarousel from 'react-alice-carousel';

const responsiveP = {
    0: { items: 1 },
    568: { items: 1 },
    1024: { items: 2 },
};

const Products = (props) => {
    return (
        <>            
           <AliceCarousel className="helloclass" mouseTracking items={props.item} responsive={responsiveP} />
        </>
    );
} 

export default Products ; 