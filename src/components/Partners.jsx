import React from 'react'; 
import AliceCarousel from 'react-alice-carousel';

const responsivePa = {
    0: { items: 1 },
    568: { items: 1 },
    1024: { items: 5 },
};

const Partners = (props) => {
    return (
        <>            
           <AliceCarousel mouseTracking items={props.item} responsive={responsivePa} />
        </>
    );
} 

export default Partners ; 