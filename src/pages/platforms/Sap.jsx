import React from 'react';
import { NavLink } from 'react-router-dom';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import bannerImg from '../../assets/images/platform/Security Audit Platform.webp';
import sap01 from '../../assets/images/platform/Desktop-Mockup_2.webp';
import sap02 from '../../assets/images/platform/Desktop-Mockup_1.webp';
import mobileApp from '../../assets/images/platform/Fire-Safety-App.webp';
import { Helmet } from 'react-helmet';

const Sap = () => {
  return (
    <>
      <Helmet>
        <title>Security Audit Platform | MitKat Advisory</title>
      </Helmet>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" />
        </div>
        <div className="col-12 col-md-9 col-lg-9">
          <div className=" service-description">
            <div className="display-block m-a">
              <div className="content">
                <h1 className="mb-3 main-header__header-font font-weight-bold text-capitalize">
                  Security Audit Platform
                </h1>
                <p className="content__subheader  py-3">
                  Physical and environmental threats to organisations are
                  rising. A robust physical and environmental ‘Security Risk
                  Assessment (SRA)’ is an integral part of an enterprise
                  security programme.
                  <br />
                  MitKat’s unique methodology for SRA involves mobile App-based
                  Assessment and automatic generation of Reports, as well as
                  updating of Dashboards in real time. This covers all areas of
                  a facility’s Security and Safety, and includes a comprehensive
                  security and safety assessment checklist (customisable). The
                  app also covers emergency policies, procedures for employees
                  in emergency scenarios, additional security and safety
                  measures and considerations.
                  <br />
                  This App and Dashboard can be easily accessed by stakeholders
                  via smartphone / tablet / laptop / desktop, and be customised
                  to meet your organisation’s unique context and security needs.
                </p>
                <NavLink
                  exact
                  to="/book_demo
"
                  rel="noopener noreferrer"
                  className="main-header__btn"
                >
                  Book a Demo &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br /> <br />
                <a
                  href="https://mitkatrisktracker.com"
                  target="_blank"
                  className="main-header__link"
                >
                  Get lastest update &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section className="py-5 bg-gray">
        <div className="row m-0">
          <div className="col-12 col-md-6 col-lg-6 p-0 text-center">
            <img className="section-img width-80" src={sap02} width="100%" />
          </div>
          <div className="col-12 col-md-6 col-lg-6">
            <div className="d-flex center">
              <div className="d-block m-a width-80">
                <div className="content">
                  <p className="content__subheader py-3 ">
                    Assessing and auditing security systems for efficacy,
                    compliance and cost-optimization can be a less laborious
                    process now, with our platform-based Security Risk
                    Assessment tool. Configurable on desktop computers, laptop
                    or tablets, can be easily Customisedfor each type of
                    location with user-friendly drop-down menus. The inputs can
                    include yes/no or multiple-choice type observations,
                    quantifiable ratings, gap analysis and variation from best
                    practices. Graphical analysis and colour coded
                    recommendation lists can provide various ‘CXO snapshot’
                    views.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="service py-5">
        <div className="container">
          <div className="row">
            <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <div className="block-box">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-12">
                    <h5 className="px-3">
                      <small className="text-gray text-light-100">
                        Mobile Application
                      </small>
                      <br />
                      <br />
                      Safety and Security Audit Mobile Application
                    </h5>
                    <div className="block-box__by p-3">
                      Safety and Security Audit are safety and compliance
                      driven, based on local building and facility regulations,
                      and quantifiable parameters. Our Safety and Security Audit
                      App makes it easy to conduct Safety and Security Audit
                      audits on smartphones or tablets. Can be easily used by
                      auditors to benchmark against standards and best
                      practices, capture photographic evidence of discrepancies,
                      track the resolution of gaps and provide a ‘at-a-glance
                      health-check’ of Safety and Security Audit standards of a
                      facility; and quantifiable risk ratings with priority-wise
                      actionable recommendations.
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6 col-12">
                    <div className="d-flex center">
                      <div className="d-block m-a width-80">
                        <div className="content">
                          <img
                            className=""
                            src={mobileApp}
                            width="100%"
                            height="auto"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div className="container">
        <hr className="divider" />
      </div>

      <section className="service py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-capitalize">
                  API integration
                </h2>
                <p className="content__subheader  py-3">
                  Integrate the Security Risk Assessment platform and the Safety
                  and Security Audit Audits app with your safety and security
                  dashboards using our APIs. You can then have real-time updates
                  on your audit schedules and reporting; using your own or
                  in-house safety/security management solution.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Sap;
