import React from 'react';
import { NavLink } from 'react-router-dom';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import bannerImg from '../../assets/images/platform/Risk Intelligence Monitoring Engine.webp';
import rime01 from '../../assets/images/platform/Mockup_3.webp';
import rime02 from '../../assets/images/platform/RIME_02.webp';
import mobileApp from '../../assets/images/platform/Mobile-App.webp';
import { Helmet } from 'react-helmet';

const Rime = () => {
  return (
    <>
      <Helmet>
        <title>Risk Intelligence Platform | MitKat Advisory</title>
      </Helmet>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" />
        </div>
        <div className="col-12 col-md-9 col-lg-9">
          <div className=" service-description">
            <div className="display-block m-a">
              <div className="content">
                <h1 className="mb-3 main-header__header-font font-weight-bold text-capitalize">
                  Risk Intelligence Platform{' '}
                </h1>
                <p className="content__subheader  py-3">
                  Integrated Risk Intelligence Monitoring Engine (RIME) and Risk
                  Tracker Portal is an AI assisted, real-time event monitoring
                  platform driven by a team of analysts empowering users to
                  track and understand current events across South Asia and Asia
                  Pacific region.
                </p>
                <NavLink
                  exact
                  to="/book_demo
"
                  rel="noopener noreferrer"
                  className="main-header__btn"
                >
                  Book a Demo &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br /> <br />
                <a
                  href="https://mitkatrisktracker.com"
                  target="_blank"
                  className="main-header__link"
                >
                  Get lastest update &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section className="py-5 bg-gray">
        <div className="row m-0">
          <div className="col-12 col-md-6 col-lg-6  bg-gray">
            <div className="d-flex center">
              <div className="d-block m-a width-80">
                <div className="content">
                  <h2 className="mb-3 header-font text-capitalize text-left">
                    Risk Tracker Portal
                  </h2>

                  <p className="content__subheader py-3 ">
                    Risk Tracker Portal is an interactive platform that is
                    updated on real-time basis with curated and analysed
                    information on events of critical importance to businesses.
                    The portal is customer-friendly as it enables query response
                    communication as well as maps office locations vis-à-vis
                    ongoing and upcoming risk events. It also enables clients to
                    search the archived risk events as per multiple criteria.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6 col-lg-6 p-0 bg-gray text-center">
            <img className="section-img width-80" src={rime01} width="100%" />
          </div>
        </div>
      </section>

      <section className="py-5 bg-gray">
        <div className="row m-0">
          <div className="col-12 col-md-6 col-lg-6 p-0 text-center">
            <img className="section-img width-80" src={rime02} width="100%" />
          </div>
          <div className="col-12 col-md-6 col-lg-6">
            <div className="d-flex center">
              <div className="d-block m-a width-80">
                <div className="content">
                  <h2 className="mb-3 header-font text-capitalize text-left">
                    Risk Intelligence Monitoring Engine (RIME)
                  </h2>
                  <p className="content__subheader py-3 ">
                    Risk Intelligence Monitoring Engine (RIME) is an AI-powered
                    platform providing live feed of risk events across India,
                    APAC and South Asia regions. It is an open source
                    intelligence gathering tool that enables real time
                    monitoring, for production of actionable risk intelligence
                    for further dissemination. The platform allows users to
                    customize results to display location and time specific
                    events; and can also be provided as API for external
                    integration.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="service py-5">
        <div className="container">
          <div className="row">
            <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <div className="block-box">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-12">
                    <h5 className="px-3">
                      <small className="text-gray text-light-100">
                        Mobile Application
                      </small>
                      <br />
                      <br />
                      Risk Tracker Mobile Application
                    </h5>
                    <div className="block-box__by p-3">
                      The Risk Tracker application allows users to access
                      critical updates through their mobile phones, enabling
                      quick reference. It also allows information sharing onto
                      other communication platforms thereby making intelligence
                      dissemination easier.
                      <br />
                      <br />
                      Features of mobile app:
                      <br />
                      <br />
                      <ul className="timeline data-pipeline content__subheader">
                        <li>
                          {' '}
                          Quick reference to important access captured in the
                          daily risk tracker and option of forward transmission
                          of event information on WhatsApp.{' '}
                        </li>
                        <li>
                          {' '}
                          User can search for specific key words to filter event
                          results.
                        </li>
                        <li>
                          {' '}
                          Allows user to check the event calendar for upcoming
                          planned events of significance to businesses.{' '}
                        </li>
                        <li>
                          {' '}
                          COVID19 dashboard can be accessed on the mobile phone
                          through this application, from where daily statistic
                          reports, special reports and advisories can be
                          downloaded.{' '}
                        </li>
                        <li>
                          {' '}
                          COVID19 map can also be accessed through this mobile
                          application, to refer to location data on containment
                          zones, COVID19 hospitals across India.{' '}
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6 col-12">
                    <div className="d-flex center">
                      <div className="d-block m-a width-80">
                        <div className="content">
                          <img
                            className=""
                            src={mobileApp}
                            width="100%"
                            height="auto"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div className="container">
        <hr className="divider" />
      </div>

      <section className="service py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-capitalize">
                  API integration
                </h2>
                <p className="content__subheader  py-3">
                  MitKat provides the option of integration of customised
                  security updates, travel alerts and COVID-19 regulatory
                  changes from our Risk Tracker Portal, Application and RIME
                  through our Application Programming Interface (APIs).
                  Super-charge your in-house dashboard and mobile application by
                  integrating our APIs.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Rime;
