import React from 'react';
import { NavLink } from 'react-router-dom';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import bannerImg from '../../assets/images/platform/Covid-19 Dashboard-home.webp';
import covid01 from '../../assets/images/platform/Covid-19-Tracker-2.webp';
import covid02 from '../../assets/images/platform/Covid-19-map.webp';
import { Helmet } from 'react-helmet';

const Covid = () => {
  return (
    <>
      <Helmet>
        <title>COVID-19 Dashboard | MitKat Advisory</title>
      </Helmet>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" />
        </div>
        <div className="col-12 col-md-9 col-lg-9">
          <div className=" service-description">
            <div className="display-block m-a">
              <div className="content">
                <h1 className="mb-3 main-header__header-font font-weight-bold text-capitalize">
                  Covid19 Dashboard{' '}
                </h1>
                <p className="content__subheader  py-3">
                  The rapid spread of COVID-19 virus across the globe is
                  affecting millions of people and is at the same time resulting
                  in the spread of information, misinformation (false
                  information spread without malicious intent) and
                  disinformation (false information spread with the intent to
                  deceive). COVID-19 dashboard is a one-stop platform for access
                  to authenticated, real-time updates.
                </p>
                <NavLink
                  exact
                  to="/book_demo
"
                  rel="noopener noreferrer"
                  className="main-header__btn"
                >
                  Book a Demo &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>

                <br />
                <br />
                <a
                  href="https://mitkatrisktracker.com/covid-19"
                  target="_blank"
                  className="main-header__link"
                >
                  Click here for live update &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section className="py-5 bg-gray">
        <div className="row m-0">
          <div className="col-12 col-md-6 col-lg-6  bg-gray">
            <div className="d-flex center">
              <div className="d-block m-a width-80">
                <div className="content">
                  <ul className="timeline data-pipeline content__subheader">
                    <li>
                      {' '}
                      COVID19 dashboard presents live data of nationwide and
                      state-wise statistics in terms of infections, deaths and
                      recovered case counts, along with comprehensive graphical
                      trend analyses
                    </li>
                    <li>
                      {' '}
                      The dashboard contains central and individual state
                      government measures - on travel, quarantine, return to
                      work related guidelines - on a single click, all compiled
                      from authentic and verified sources
                    </li>
                    <li>
                      {' '}
                      Special reports on containment zones, hospital and bed
                      capacity in metro cities, etc. are available in quick view
                      as well as downloadable format{' '}
                    </li>
                    <li>
                      {' '}
                      Presence of all COVID19 related information on a single
                      platform makes it an ideal source of quick reference of
                      validated information with user-friendly interface
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6 col-lg-6 p-0 bg-gray text-center">
            <img className="section-img width-80" src={covid01} width="100%" />
          </div>
        </div>
      </section>

      <section className="py-5 bg-gray">
        <div className="row m-0">
          <div className="col-12 col-md-6 col-lg-6 p-0 text-center">
            <img className="section-img width-80" src={covid02} width="100%" />
          </div>
          <div className="col-12 col-md-6 col-lg-6">
            <div className="d-flex center">
              <div className="d-block m-a width-80">
                <div className="content">
                  <p className="content__subheader py-3 ">COVID19 map:</p>
                  <ul className="timeline data-pipeline content__subheader">
                    <li>
                      {' '}
                      The COVID19 map provides situational awareness through
                      virtual depiction of Containment zones in specific regions
                      of interest across India, as per the viewer’s requirements{' '}
                    </li>
                    <li>
                      {' '}
                      The map also includes employee residence mapping to
                      understand the proximity of employees to containment
                      zones, hence assisting organisations with phased return to
                      work strategies{' '}
                    </li>
                    <li>
                      {' '}
                      The map also integrates office location mapping vis-à-vis
                      containment zones, that facilitates seamless planning for
                      resumption of operations in low risk regions{' '}
                    </li>
                    <li>
                      {' '}
                      Locations of COVID19 treating hospitals across the country
                      are also mapped on this platform{' '}
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="service py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-capitalize">
                  API integration
                </h2>
                <p className="content__subheader  py-3">
                  COVID-19 related data that can be provided via APIs:
                </p>
                <ul className="content__subheader">
                  <li>Containment zone data </li>
                  <li>State/district/metro wise infection rates </li>
                  <li>Any other data that may be required</li>
                </ul>
                <p className="content__subheader pb-3">Salient features:</p>
                <ul className="content__subheader">
                  <li>
                    Information sharing through APIs can be customised as per
                    client requirements{' '}
                  </li>
                  <li>
                    APIs can directly be integrated to proprietary dashboards
                    for smooth flow of customised inputs{' '}
                  </li>
                  <li>
                    Contact us now to power your internal platforms through our
                    APIs{' '}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Covid;
