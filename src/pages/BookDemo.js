import React, { useEffect, useState } from 'react';
import contact from '../assets/images/background/Demo banner image.webp';
import { setItemDemo, getServiceList } from '../service/api';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import { Helmet } from 'react-helmet';

const BookDemo = () => {
  const [alert, setAlert] = useState(false);
  const [itemInputPlatform, setItemInputPlatform] = useState('');
  const [serviceList, setServiceList] = useState({
    success: '',
    message: '',
    output: [],
  });

  useEffect(() => {
    if (itemInputPlatform.length && !alert) {
      return;
    }
    let mounted = true;
    getServiceList().then((items) => {
      if (mounted) {
        setServiceList(items);
      }
    });
  }, [alert, itemInputPlatform]);

  const formSubmitPlatform = (e) => {
    e.preventDefault();

    setItemDemo(itemInputPlatform).then(() => {
      setItemInputPlatform('');
      debugger;
      setAlert(true);
    });
  };

  const InputEventPlatform = (event) => {
    const { name, value } = event.target;

    setItemInputPlatform((preVal) => {
      return {
        ...preVal,
        [name]: value,
      };
    });
  };

  function refreshPage() {
    window.location.reload(false);
  }

  return (
    <>
      <Helmet>
        <title>Book a Demo| MitKat Advisory</title>
      </Helmet>
      <Navbar />
      <div className="service-header">
        <div className="row justify-content-center m-0">
          <div className="col-12 col-md-12 col-lg-12 p-0">
            <img src={contact} width="100%" />
          </div>
          <div className="col-12 col-md-9 col-lg-9">
            <div className="service-description">
              <div className="display-block m-a">
                <div className="content">
                  <h1 className="mb-3 service-description__header text-capitalize text-center">
                    See What MitKat Can Do For You
                  </h1>
                  <p className="content__subheader py-3 m-0">
                    Request a Demo with One of our Experts to Discover the Power
                    of MitKat.
                  </p>
                  <p className="text-right">
                    <small className="text-danger">
                      Required fields are marked with an asterisk (*).
                    </small>
                  </p>
                </div>
              </div>

              <div className="row ">
                <div className="col-12 col-md-12 col-lg-12 d-flex justify-content-center flex-column"></div>
                <form
                  className="contact-form px-3"
                  onSubmit={formSubmitPlatform}
                >
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group mb-3">
                        <select
                          onChange={InputEventPlatform}
                          name="serviceid"
                          className="form-control contact-form__input"
                          value={itemInputPlatform.serviceid}
                        >
                          <option disabled selected>
                            Select Service
                          </option>

                          {serviceList.output.map((item) => (
                            <option value={item.id}> {item.name}</option>
                          ))}
                        </select>
                      </div>
                      <div className="form-group mb-3">
                        <input
                          type="text"
                          name="name"
                          onChange={InputEventPlatform}
                          className="form-control contact-form__input"
                          placeholder="Your Name *"
                          value={itemInputPlatform.name}
                          required
                        />
                      </div>
                      <div className="form-group mb-3">
                        <input
                          type="text"
                          name="emailid"
                          onChange={InputEventPlatform}
                          className="form-control contact-form__input"
                          placeholder="Your Email *"
                          value={itemInputPlatform.emailid}
                        />
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group mb-3">
                        <input
                          type="text"
                          name="phoneNumber"
                          onChange={InputEventPlatform}
                          className="form-control contact-form__input"
                          placeholder="Your Phone Number *"
                          value={itemInputPlatform.phoneNumber}
                        />
                      </div>
                      <div className="form-group mb-3">
                        <input
                          type="text"
                          name="companyname"
                          onChange={InputEventPlatform}
                          className="form-control contact-form__input"
                          placeholder="Your Company Name *"
                          value={itemInputPlatform.companyname}
                        />
                      </div>
                    </div>
                    <div className="col-12">
                      <div className="form-group mb-3">
                        <input
                          type="text"
                          name="subject"
                          onChange={InputEventPlatform}
                          className="form-control contact-form__input"
                          placeholder="Subject *"
                          value={itemInputPlatform.subject}
                        />
                      </div>
                      <div className="form-group mb-3">
                        <textarea
                          name="message"
                          onChange={InputEventPlatform}
                          className="form-control contact-form__input"
                          placeholder="Your Message *"
                          value={itemInputPlatform.message}
                        ></textarea>
                      </div>
                    </div>
                    <div className="col-md-12 col-12">
                      <div className="form-group">
                        <input
                          type="submit"
                          name="btnSubmit"
                          className="btn contact-form__btn"
                          value="Request Demo"
                        />
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

      {alert && (
        <div id="overlay" className="d-flex justify-content-center flex-column">
          <div className="success-checkmark">
            <div className="check-icon">
              <span className="icon-line line-tip"></span>
              <span className="icon-line line-long"></span>
              <div className="icon-circle"></div>
              <div className="icon-fix"></div>
            </div>
          </div>
          <center>
            <p className="text-success mb-4">
              Thank you! Your request has been successfully sent!
            </p>
            <button className="btn btn-success" onClick={refreshPage}>
              {' '}
              ok{' '}
            </button>
          </center>
        </div>
      )}
      <Footer />
    </>
  );
};

export default BookDemo;
