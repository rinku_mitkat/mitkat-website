import React from 'react';
import { NavLink } from 'react-router-dom';
import './style.css';

const ThankYou = () => {
  return (
    <>
      <div className="thankyou--box">
        <div className="thankyou--wrap">
          <h1> Thank you !</h1>
          <p>
            Thank you for your enquiry. You will received an email message
            shortly.
          </p>
          <div className="success-checkmark">
            <div className="check-icon">
              <span className="icon-line line-tip"></span>
              <span className="icon-line line-long"></span>
              <div className="icon-circle"></div>
              <div className="icon-fix"></div>
            </div>
          </div>
          <p> Check your Email</p>
          <p>
            <small>
              If you didn’t receive any mail contact
              <u> contact@mitkatadvisory.com </u>
            </small>
          </p>
          <NavLink exact to="/">
            Back to Home
          </NavLink>
        </div>
      </div>
    </>
  );
};

export default ThankYou;
