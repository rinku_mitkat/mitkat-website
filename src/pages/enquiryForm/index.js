import React from 'react';
import Footer from '../../components/Footer';
import Navbar from '../../components/Navbar';
import banner from '../../assets/images/background/Contact Us banner image.webp';

const EnquiryForm = () => {
  return (
    <>
      <Navbar />
      <div className="service-header">
        <div className="row justify-content-center m-0">
          <div className="col-12 col-md-12 col-lg-12 p-0">
            <img src={banner} width="100%" />
          </div>
          <div className="col-12 col-md-9 col-lg-9">
            <div className="service-description">
              <div className="display-block m-a">
                <div className="content">
                  <div className="text-center">
                    <iframe
                      width="610px"
                      height="630px"
                      src="https://crm.zoho.com/crm/WebFormServeServlet?rid=80287af3371e14e436816e3f70daf403b9016d7492460750687f8cd638225be1gid090fa678f4ebf88ecdc9bb8edcc01b44ab42769526c6ef0ed598c449e46597aa"
                    ></iframe>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </>
  );
};

export default EnquiryForm;
