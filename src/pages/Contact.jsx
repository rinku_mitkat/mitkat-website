import React, { useEffect, useState } from 'react';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import { getList, setItem, getCountryList } from '../service/api';
import contact from '../assets/images/background/Contact Us banner image.webp';
import mumbai from '../assets/images/location/Mumbai.webp';
import delhi from '../assets/images/location/Delhi.webp';
import bengaluru from '../assets/images/location/Bengaluru.webp';
import singapore from '../assets/images/location/Singapore.webp';
import { Helmet } from 'react-helmet';

const Contact = () => {
  const [alert, setAlert] = useState(false);
  const [commentList, setCommentList] = useState({
    success: '',
    message: '',
    output: [],
  });
  const [countryList, setCountryList] = useState({
    success: '',
    message: '',
    output: [],
  });

  const [itemInput, setItemInput] = useState('');

  useEffect(() => {
    if (itemInput.length && !alert) {
      return;
    }

    let mounted = true;
    getList().then((items) => {
      if (mounted) {
        setCommentList(items);
      }
    });

    getCountryList().then((items) => {
      if (mounted) {
        setCountryList(items);
      }
    });
    return () => (mounted = false);
  }, [alert, itemInput]);

  const formSubmit = (e) => {
    e.preventDefault();
    setItem(itemInput).then(() => {
      setItemInput('');
      setAlert(true);
    });
  };

  const InputEvent = (event) => {
    const { name, value } = event.target;

    setItemInput((preVal) => {
      return {
        ...preVal,
        [name]: value,
      };
    });
  };
  function refreshPage() {
    window.location.reload(false);
  }

  return (
    <>
      <Helmet>
        <title>Contact us| MitKat Advisory</title>
      </Helmet>
      <Navbar />
      <div className="service-header">
        <div className="row justify-content-center m-0">
          <div className="col-12 col-md-12 col-lg-12 p-0">
            <img src={contact} width="100%" />
          </div>
          <div className="col-12 col-md-9 col-lg-9">
            <div className="service-description">
              <div className="display-block m-a">
                <div className="content">
                  <h1 className="mb-3 service-description__header text-capitalize text-center">
                    Contact Us
                  </h1>
                  <p className="content__subheader py-3 m-0">
                    Thank you for your interest in <stong>MitKat</stong>. <br />{' '}
                    Please fill out the form below to ask a question.
                  </p>
                  <small className="text-gray">
                    Required fields are marked with an asterisk (*).
                  </small>
                </div>
              </div>

              <div className="row ">
                <div className="col-12 col-md-12 col-lg-12 d-flex justify-content-center flex-column">
                  <form className="contact-form mt-3" onSubmit={formSubmit}>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group mb-3">
                          <input
                            type="text"
                            name="emailid"
                            onChange={InputEvent}
                            className="form-control contact-form__input"
                            placeholder=" Email *"
                            value={itemInput.emailid}
                          />
                        </div>
                        <div className="form-group mb-3">
                          <input
                            type="text"
                            name="name"
                            onChange={InputEvent}
                            className="form-control contact-form__input"
                            placeholder=" Name"
                            value={itemInput.name}
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="input-group mb-3">
                          <div className="input-group-prepend">
                            <select
                              onChange={InputEvent}
                              name="countryid"
                              className="form-control contact-form__input countryInput"
                              value={itemInput.countryid}
                            >
                              {countryList.output.map((item) => (
                                <option value={item.id}>
                                  {item.dialCode} | {item.countryCode}-
                                  {item.countryName}
                                </option>
                              ))}
                            </select>
                          </div>
                          <input
                            type="text"
                            name="phoneNumber"
                            onChange={InputEvent}
                            className="form-control contact-form__input"
                            placeholder=" Phone Number *"
                            value={itemInput.phoneNumber}
                          />
                        </div>

                        <div className="form-group mb-3">
                          <input
                            type="text"
                            name="companyname"
                            onChange={InputEvent}
                            className="form-control contact-form__input"
                            placeholder=" Company Name"
                            value={itemInput.companyname}
                          />
                        </div>
                      </div>
                      <div className="col-12 ">
                        <div className="form-group mb-3">
                          <textarea
                            name="message"
                            row="5"
                            onChange={InputEvent}
                            className="form-control contact-form__input"
                            placeholder="Your Message"
                            value={itemInput.message}
                          ></textarea>
                        </div>
                      </div>
                      <div className="col-md-12 col-12">
                        <div className="form-group">
                          <input
                            type="submit"
                            name="btnSubmit"
                            className="btn contact-form__btn"
                            value="Send Message"
                          />
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section id="tabs" className="py-5">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-12 ">
              <nav>
                <div
                  className="nav nav-tabs nav-tabs-location nav-fill"
                  id="nav-tab"
                  role="tablist"
                >
                  <a
                    className="nav-item nav-link active"
                    id="nav-delhi-tab"
                    data-toggle="tab"
                    href="#nav-delhi"
                    role="tab"
                    aria-controls="nav-delhi"
                    aria-selected="true"
                  >
                    Delhi/Gurgaon
                  </a>
                  <a
                    className="nav-item nav-link"
                    id="nav-mumbai-tab"
                    data-toggle="tab"
                    href="#nav-mumbai"
                    role="tab"
                    aria-controls="nav-mumbai"
                    aria-selected="false"
                  >
                    Mumbai
                  </a>
                  <a
                    className="nav-item nav-link"
                    id="nav-benglore-tab"
                    data-toggle="tab"
                    href="#nav-benglore"
                    role="tab"
                    aria-controls="nav-benglore"
                    aria-selected="false"
                  >
                    Bengaluru
                  </a>
                  <a
                    className="nav-item nav-link"
                    id="nav-singapore-tab"
                    data-toggle="tab"
                    href="#nav-singapore"
                    role="tab"
                    aria-controls="nav-singapore"
                    aria-selected="false"
                  >
                    Singapore
                  </a>
                </div>
              </nav>
              <div className="tab-content " id="nav-tabContent-location">
                <div
                  className="tab-pane fade show active"
                  id="nav-delhi"
                  role="tabpanel"
                  aria-labelledby="nav-delhi-tab"
                >
                  <div className="row">
                    <div className="col-md-4 text-white bg-darker position-relative">
                      <div className="location-address py-3">
                        <strong> Delhi/Gurgaon </strong>
                        <br />
                        <br />
                        <p>
                          9, 4th Floor, Time Square, Sushant Lok – 1
                          <br />
                          Gurgaon – 122 002
                          <br />
                          <br />
                          contact@mitkatadvisory.com
                          <br />
                          <br />
                          +91 011-412-36710
                        </p>
                        <img src={delhi} />
                      </div>
                    </div>

                    <div className="col-md-8 pr-0">
                      <iframe
                        className="locationIframe"
                        src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7015.187767066962!2d77.08061808626098!3d28.46165630824162!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2e2e55589b4bb07e!2sMitKat%20Advisory%20Services%20Pvt%20Ltd!5e0!3m2!1sen!2sin!4v1596563331585!5m2!1sen!2sin"
                        width="725"
                        height="365"
                        frameborder="0"
                        allowfullscreen=""
                        aria-hidden="false"
                        tabindex="0"
                      ></iframe>
                      <div className="location-frame"></div>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="nav-mumbai"
                  role="tabpanel"
                  aria-labelledby="nav-mumbai-tab"
                >
                  <div className="row">
                    <div className="col-md-4 text-white bg-darker position-relative">
                      <div className="location-address py-3">
                        <strong> Mumbai </strong>
                        <br />
                        <br />
                        <p>
                          511, Ascot Centre, Off Sahar Road, Near Hyatt Regency,
                          Andheri (East)
                          <br />
                          Mumbai – 400 099
                          <br />
                          <br />
                          contact@mitkatadvisory.com
                          <br />
                          <br />
                          +91 011-412-36710
                        </p>
                        <img src={mumbai} />
                      </div>
                    </div>

                    <div className="col-md-8">
                      <div className="location-frame">
                        <iframe
                          className="locationIframe"
                          src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d49277.68519875877!2d72.87088941732196!3d19.11170042419934!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x37013eee929634f9!2sMitKat%20Advisory%20Services%20Pvt%20Ltd!5e0!3m2!1sen!2sin!4v1595403860505!5m2!1sen!2sin"
                          width="725"
                          height="365"
                          frameborder="0"
                          allowfullscreen=""
                          aria-hidden="false"
                          tabindex="0"
                        ></iframe>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="nav-benglore"
                  role="tabpanel"
                  aria-labelledby="nav-benglore-tab"
                >
                  <div className="row">
                    <div className="col-md-4 text-white bg-darker position-relative">
                      <div className="location-address py-3">
                        <strong> Bengaluru </strong>
                        <br />
                        <br />
                        <p>
                          Esteem Gardenia, Sahakar Nagar
                          <br />
                          Bangalore - 560092
                          <br />
                          <br />
                          contact@mitkatadvisory.com
                          <br />
                          <br />
                          +91 011-412-36710
                        </p>
                        <img src={bengaluru} />
                      </div>
                    </div>

                    <div className="col-md-8">
                      <div className="location-frame">
                        <iframe
                          className="locationIframe"
                          src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15080.251457327846!2d72.8706655!3d19.1048979!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x37013eee929634f9!2sMitKat%20Advisory%20Services%20Pvt%20Ltd!5e0!3m2!1sen!2sin!4v1598439350774!5m2!1sen!2sin"
                          width="725"
                          height="365"
                          frameborder="0"
                          allowfullscreen=""
                          aria-hidden="false"
                          tabindex="0"
                        ></iframe>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="nav-singapore"
                  role="tabpanel"
                  aria-labelledby="nav-singapore-tab"
                >
                  <div className="row">
                    <div className="col-md-4 text-white bg-darker position-relative">
                      <div className="location-address py-3">
                        <strong> Singapore </strong>
                        <br />
                        <br />
                        <p>
                          101 Cecil Street, #23-12
                          <br />
                          Tong Eng Building,
                          <br />
                          Singapore 069 533
                          <br />
                          <br />
                          contact@mitkatadvisory.com
                          <br />
                          <br />
                          +65 9452 1622
                        </p>
                        <img src={singapore} />
                      </div>
                    </div>

                    <div className="col-md-8">
                      <div className="location-frame">
                        <iframe
                          className="locationIframe"
                          src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15955.284871518494!2d103.8494496!3d1.2809731!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe011b77cc1240d3c!2sTong%20Eng%20Building!5e0!3m2!1sen!2sin!4v1607771111774!5m2!1sen!2sin"
                          width="725"
                          height="365"
                          frameborder="0"
                          allowfullscreen=""
                          aria-hidden="false"
                          tabindex="0"
                        ></iframe>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      {alert && (
        <div id="overlay" className="d-flex justify-content-center flex-column">
          <div className="success-checkmark">
            <div className="check-icon">
              <span className="icon-line line-tip"></span>
              <span className="icon-line line-long"></span>
              <div className="icon-circle"></div>
              <div className="icon-fix"></div>
            </div>
          </div>
          <center>
            <p className="text-success mb-4">
              Thank you! Your message has been successfully sent!{' '}
            </p>
            <button className="btn btn-success" onClick={refreshPage}>
              {' '}
              ok{' '}
            </button>
          </center>
        </div>
      )}
      <Footer />
    </>
  );
};

export default Contact;
