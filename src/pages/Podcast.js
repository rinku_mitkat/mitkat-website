import React, { useEffect, useState } from 'react';
import Footer from '../components/Footer';
import Navbar from '../components/Navbar';
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getPodcastClient } from '../service/api';
import { MONTH_NAME } from '../helper/month';
import { Helmet } from 'react-helmet';

const Podcast = () => {
  const [video, setVideo] = useState([]);
  const [podcastList, setPodcastList] = useState([]);
  useEffect(() => {
    getPodcastClient().then((items) => {
      setPodcastList(items);
      setVideo(items[0]);
    });
  }, []);
  return (
    <>
      <Helmet>
        <title>Podcast | MitKat Advisory</title>
      </Helmet>
      <Navbar />
      <section className="py-5">
        <div className="container px-md-5 px-3">
          <div className="row px-md-5  px-3">
            <div className="service-description p-2 mt-0">
              <div className="display-block m-a">
                <div className="content">
                  <h1 className="mb-3 service-description__header text-capitalize text-center">
                    Podcast
                  </h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="row px-3 px-md-5 mb-5 bg-gray py-5">
        <div className="row px-3 px-md-5 ">
          <div className="col-12 col-md-8 col-lg-8">
            <div className="embed-responsive embed-responsive-4by3 px-5">
              <iframe
                width="560"
                height="315"
                src={video.linkURL}
                frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
              ></iframe>
            </div>
          </div>
          <div className="col-12 col-md-4 col-lg-4 pt-5 pt-md-0">
            <div className="podcast-box bg-gray">
              {podcastList.map((item) => (
                <div className="podcast-list" onClick={() => setVideo(item)}>
                  <p className="podcast-title"> {item.title}</p>
                  <p className="podcast-date">
                    {' '}
                    <span>
                      {`${new Date(item.date).getDate()}`}&nbsp;
                      {`${MONTH_NAME[new Date(item.date).getMonth()]}`}&nbsp;
                      {`${new Date(item.date).getFullYear()}`}
                    </span>
                  </p>
                </div>
              ))}
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Podcast;
