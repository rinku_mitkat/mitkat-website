import React from 'react';
import { NavLink } from 'react-router-dom';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import AliceCarousel from 'react-alice-carousel';
import aboutBannerImg from '../assets/images/background/About Us 1.webp';
import customerVideo01 from '../assets/video/VID-20200826-WA0003.mp4';
import customerVideo02 from '../assets/video/VID-20200826-WA0005.mp4';
import customerVideo03 from '../assets/video/VID-20200826-WA0006.mp4';
import customerVideo04 from '../assets/video/VID-20200826-WA0007.mp4';
import customerVideo05 from '../assets/video/VID-20200826-WA0008.mp4';

import genSharma from '../assets/images/team/about-page/Gen Sharma.webp';
import premChand from '../assets/images/team/about-page/Premchand.webp';
import samKumar from '../assets/images/team/about-page/Sam Sir.webp';
import pawanDesai from '../assets/images/team/about-page/Pawan Sir.webp';
import sushilPradhan from '../assets/images/team/about-page/Sushil Sir.webp';
import sandeepSinha from '../assets/images/team/about-page/Sandeep Sinha.webp';
import dhananjayBirwadkar from '../assets/images/team/about-page/Dhananjay.webp';
import aparnaGuddad from '../assets/images/team/about-page/Aparna.webp';
import sohebKhan from '../assets/images/team/about-page/Soheb.webp';
import ameya from '../assets/images/team/about-page/Ameya.webp';
import hima from '../assets/images/team/about-page/Hima.webp';
import ranjeet from '../assets/images/team/about-page/Ranjeet.jpg';
import mitesh from '../assets/images/team/about-page/Mitesh.webp';
import sagar from '../assets/images/team/about-page/Sagar B._350_210.png';
import malcolm from '../assets/images/team/about-page/Malcolm-350_210px.webp';
import { Helmet } from 'react-helmet';
import certificate from '../assets/images/0001.webp';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';

const handleDragStart = (e) => e.preventDefault();
const responsive = {
  0: { items: 1 },
  568: { items: 2 },
  1024: { items: 4 },
};

const videoSlider = [
  <div className="video-box">
    <div className="embed-responsive embed-responsive-16by9">
      <video
        controls
        muted
        className="embed-responsive-item"
        onDragStart={handleDragStart}
      >
        <source src={customerVideo01} type="video/mp4" />
      </video>
    </div>
    <p className="video-box__title  m-0 ">Suresh Nair </p>
    <p className="video-box__subtitle ">
      Senior Risk Professional (ex Siemens){' '}
    </p>
  </div>,
  <div className="video-box">
    <div className="embed-responsive embed-responsive-16by9">
      <video controls muted className="embed-responsive-item">
        <source src={customerVideo02} type="video/mp4" />
      </video>
    </div>
    <p className="video-box__title  m-0 ">Ratna Pawan </p>
    <p className="video-box__subtitle ">
      Senior Security, Risk and Resilience Specialist (ex HSBC){' '}
    </p>
  </div>,
  <div className="video-box">
    <div className="embed-responsive embed-responsive-16by9">
      <video controls muted className="embed-responsive-item">
        <source src={customerVideo03} type="video/mp4" />
      </video>
    </div>
    <p className="video-box__title  m-0 ">Preeti Dubey </p>
    <p className="video-box__subtitle ">
      Founder, Strive High Pte Ltd, Singapore{' '}
    </p>
  </div>,
  <div className="video-box">
    <div className="embed-responsive embed-responsive-16by9">
      <video controls muted className="embed-responsive-item">
        <source src={customerVideo04} type="video/mp4" />
      </video>
    </div>
    <p className="video-box__title  m-0 ">Iain Findlay</p>
    <p className="video-box__subtitle ">Black & Veatch </p>
  </div>,
  <div className="video-box">
    <div className="embed-responsive embed-responsive-16by9">
      <video controls muted className="embed-responsive-item">
        <source src={customerVideo05} type="video/mp4" />
      </video>
    </div>
    <p className="video-box__title  m-0 ">Shakil Ahmed </p>
    <p className="video-box__subtitle ">OYO </p>
  </div>,
];
const About = () => {
  return (
    <>
      <Helmet>
        <title>About us | MitKat Advisory</title>
      </Helmet>
      <Navbar />
      <div className="main-header__about">
        <div className="">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <img src={aboutBannerImg} width="100%" alt="About Banner" />
            </div>
          </div>
        </div>
      </div>
      <section className=" py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="content__header text-center">
                  what our customers say{' '}
                </h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12 col-12">
              <AliceCarousel
                mouseTracking
                items={videoSlider}
                responsive={responsive}
              />
            </div>
          </div>
        </div>
      </section>

      <section className="py-5">
        <div className="container">
          <div className="row px-3">
            <div className="col-12 col-md-12 col-lg-12">
              <div className="content">
                <h2 className="mb-3 content__header text-center">Our Story</h2>
                <p className="content__subheader py-3 ">
                  The end of the first decade of the 21st century saw the global
                  economy reeling under the Lehman Brothers aftermath, while
                  India experienced its own 9/11 moment. Simultaneously,
                  geo-political, security (physical & logical) and integrity
                  risks were on the rise. The security design and risk
                  mitigation landscape in India was then the exclusive preserve
                  of few international companies with limited understanding of
                  India or Asia. This approach was akin to missing the wood for
                  the trees.
                  <br />
                  <br />
                  It was with our desire and vision to see the intel, security
                  and risk domain from a pragmatic, indigenous and hands on
                  perspective, that the idea of a homegrown and niche
                  entrepreneurial venture germinated. We believed that the new
                  age geo-political, socio-economic, environmental,
                  technological and regulatory risks merited a fresh paradigm to
                  de-risking - a business-focused, tech-enabled, predictive,
                  process-based and cost-conscious approach, bringing to fore
                  military precision in planning and execution, with continual
                  improvement as the fulcrum. The missionary zeal of Sam and
                  down-to-earth approach of Pawan saw the fruition of this dream
                  into reality with the formation of MitKat in 2010.
                  <br />
                  <br />
                  <div className="embed-responsive embed-responsive-16by9 px-5">
                    <iframe
                      width="560"
                      height="315"
                      src="https://www.youtube.com/embed/oXbpNVGZVRo"
                      frameborder="0"
                      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                      allowfullscreen
                    ></iframe>
                  </div>
                  <br />
                  <br />
                  We genuinely believed that the twin engines of Customers and
                  Colleagues would drive our growth and progress; technology
                  would be needed to achieve scale; intimate knowledge of
                  segments and geographies we operate in, would add real and
                  lasting value, and agility would be required to remain
                  relevant in a rapidly changing world with disruptive business
                  models, and a complex, ambiguous and intensifying threat
                  landscape.
                  <br />
                  <br />
                  MitKat was raised with the avowed aim of being an ethical,
                  credible and dependable risk mitigation company where our
                  client would not only be the king or queen, but family. We
                  vowed to be honest, upright and devoted to our vision of being
                  a leading South Asian Risk Consultancy by 2020, a leader in
                  Indo-Pacific by 2025, and globally by 2030. Our credo “do less
                  to do more" symbolizes our desire to remain focused to deliver
                  the best-in-class de-risking services and customer experience.
                  Having worked with more than 50 of the top 100 global
                  corporations, among others, in over 25 countries across
                  Asia-Pacific, Japan, China, Australia, Europe and Americas,
                  and many challenging environments in SE Asia, South Asia,
                  Middle East and Africa, we now feel confident of bringing
                  similar value to other customers – in business, government and
                  non-government sectors.
                  <br />
                  <br />
                  The twin shock of the Pandemic and the resulting Global
                  Economic Dislocation has given us an opportunity to accelerate
                  our digital transformation and sharpen our technological edge.
                  Happy and proud of our colleagues who have helped us
                  accelerate and innovate, and of our customers who have
                  remained steadfast and loyal. We now step into the new decade
                  with renewed vigour and resolve to touch the sky and live our
                  shared dreams through our work.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="py-5 bg-gray" id="our_team">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 content__header text-center"> Our Team </h2>
              </div>
            </div>
          </div>
          <div className="row mt-5 ">
            <div className="col-lg-4 col-md-4 col-12 mb-5 ">
              <NavLink exact to="/sudhir-sharma">
                <div className="card team-card">
                  <img
                    className="card-img-top team-card__img"
                    src={genSharma}
                    alt="Gen Sudhir Shamra"
                  />
                  <div className="card-block team-card__block">
                    <h4 className="card-title team-card__title mt-3">
                      Lt Gen Sudhir Sharma
                      <br /> <small>PVSM, AVSM, YSM, VSM </small>{' '}
                    </h4>
                    <p>Chairman </p>
                  </div>
                </div>
              </NavLink>
            </div>
            <div className="col-lg-4 col-md-4 col-12 mb-5 ">
              <NavLink exact to="/prem-chand">
                <div className="card team-card">
                  <img
                    className="card-img-top team-card__img"
                    src={premChand}
                    alt="Dr Prem Chand"
                  />
                  <div className="card-block team-card__block">
                    <h4 className="card-title team-card__title mt-3">
                      {' '}
                      Dr Prem Chand{' '}
                    </h4>
                    <p>Executive Vice Chairman </p>
                  </div>
                </div>
              </NavLink>
            </div>
            <div className="col-lg-4 col-md-4 col-12 mb-5 ">
              <NavLink exact to="/s-m-kumar">
                <div className="card team-card">
                  <img
                    className="card-img-top team-card__img"
                    src={samKumar}
                    alt="S M Kumar"
                  />
                  <div className="card-block team-card__block">
                    <h4 className="card-title team-card__title mt-3">
                      S M Kumar (Sam)
                    </h4>
                    <p>Co-founder & MD </p>
                  </div>
                </div>
              </NavLink>
            </div>
            <div className="col-lg-4 col-md-4 col-12 mb-5 ">
              <NavLink exact to="/pawan-desai">
                <div className="card team-card">
                  <img
                    className="card-img-top team-card__img"
                    src={pawanDesai}
                    alt=""
                  />
                  <div className="card-block team-card__block">
                    <h4 className="card-title team-card__title mt-3">
                      Pawan Desai
                      <br /> <small> CISA, CISSP, CBCP </small>
                    </h4>
                    <p>Co-founder & CEO </p>
                  </div>
                </div>
              </NavLink>
            </div>
            <div className="col-lg-4 col-md-4 col-12 mb-5 ">
              <NavLink exact to="/sushil-pradhan">
                <div className="card team-card">
                  <img
                    className="card-img-top team-card__img"
                    src={sushilPradhan}
                    alt=" Sushil Pradhan"
                  />
                  <div className="card-block team-card__block">
                    <h4 className="card-title team-card__title mt-3">
                      {' '}
                      Sushil Pradhan{' '}
                    </h4>
                    <p>Executive Director & COO </p>
                  </div>
                </div>
              </NavLink>
            </div>

            <div className="col-lg-4 col-md-4 col-12 mb-5 ">
              <NavLink exact to="/sandeep-sinha">
                <div className="card team-card">
                  <img
                    className="card-img-top team-card__img"
                    src={sandeepSinha}
                    alt="Sandeep Sinha"
                  />
                  <div className="card-block team-card__block">
                    <h4 className="card-title team-card__title mt-3">
                      Sandeep Sinha{' '}
                    </h4>
                    <p> Director - Risk Consulting & Managed Services </p>
                  </div>
                </div>
              </NavLink>
            </div>

            <div className="col-lg-4 col-md-4 col-12 mb-5 ">
              <NavLink exact to="/ranjeet-sinha">
                <div className="card team-card">
                  <img
                    className="card-img-top team-card__img"
                    src={ranjeet}
                    alt="Ranjeet Sinha"
                    alt="Soheb Khan "
                  />
                  <div className="card-block team-card__block">
                    <h4 className="card-title team-card__title mt-3">
                      Ranjeet Sinha{' '}
                    </h4>
                    <p>Director - Strategic Initiatives</p>
                  </div>
                </div>
              </NavLink>
            </div>

            <div className="col-lg-4 col-md-4 col-12 mb-5 ">
              <NavLink exact to="/dhananjay-birwadkar">
                <div className="card team-card">
                  <img
                    className="card-img-top team-card__img"
                    src={dhananjayBirwadkar}
                    alt="Dhananjay Birwadkar"
                  />
                  <div className="card-block team-card__block">
                    <h4 className="card-title team-card__title mt-3">
                      Dhananjay Birwadkar{' '}
                    </h4>
                    <p> Director – Design Consulting & Project Management </p>
                  </div>
                </div>
              </NavLink>
            </div>

            <div className="col-lg-4 col-md-4 col-12 mb-5 ">
              <NavLink exact to="/mitesh-shah">
                <div className="card team-card">
                  <img
                    className="card-img-top team-card__img"
                    src={mitesh}
                    alt="Mitesh Shah"
                    alt="Soheb Khan "
                  />
                  <div className="card-block team-card__block">
                    <h4 className="card-title team-card__title mt-3">
                      Mitesh Shah{' '}
                    </h4>
                    <p>
                      Regional Director
                      <br />
                      MitKat Global Consulting Pte Ltd
                    </p>
                  </div>
                </div>
              </NavLink>
            </div>

            <div className="col-lg-4 col-md-4 col-12 mb-5 ">
              <NavLink exact to="/malcolm-cooper">
                <div className="card team-card">
                  <img
                    className="card-img-top team-card__img"
                    src={malcolm}
                    alt="Malcolm Cooper"
                  />
                  <div className="card-block team-card__block">
                    <h4 className="card-title team-card__title mt-3">
                      Malcolm Cooper
                    </h4>
                    <p>Regional Director </p>
                  </div>
                </div>
              </NavLink>
            </div>

            <div className="col-lg-4 col-md-4 col-12 mb-5 ">
              <NavLink exact to="/aparna-guddad">
                <div className="card team-card">
                  <img
                    className="card-img-top team-card__img"
                    src={aparnaGuddad}
                    alt="Aparna Guddad"
                  />
                  <div className="card-block team-card__block">
                    <h4 className="card-title team-card__title mt-3">
                      Aparna Guddad{' '}
                    </h4>
                    <p>Associate Director - Predictive Risk Intelligence </p>
                  </div>
                </div>
              </NavLink>
            </div>

            <div className="col-lg-4 col-md-4 col-12 mb-5 ">
              <NavLink exact to="/ameya-patkar">
                <div className="card team-card">
                  <img
                    className="card-img-top team-card__img"
                    src={ameya}
                    alt="Ameya Patkar"
                  />
                  <div className="card-block team-card__block">
                    <h4 className="card-title team-card__title mt-3">
                      Ameya Patkar{' '}
                    </h4>
                    <p> Digital Transformation Leader </p>
                  </div>
                </div>
              </NavLink>
            </div>

            <div className="col-lg-4 col-md-4 col-12 mb-5 ">
              <NavLink exact to="/sagar-bhanushali">
                <div className="card team-card">
                  <img
                    className="card-img-top team-card__img"
                    src={sagar}
                    alt="Sagar Bhanushali"
                  />
                  <div className="card-block team-card__block">
                    <h4 className="card-title team-card__title mt-3">
                      Sagar Bhanushali{' '}
                    </h4>
                    <p>
                      {' '}
                      Business Head – Predictive Risk Intelligence Platform{' '}
                    </p>
                  </div>
                </div>
              </NavLink>
            </div>

            <div className="col-lg-4 col-md-4 col-12 mb-5 ">
              <NavLink exact to="/hima-bisht">
                <div className="card team-card">
                  <img
                    className="card-img-top team-card__img"
                    src={hima}
                    alt="Hima Bisht"
                  />
                  <div className="card-block team-card__block">
                    <h4 className="card-title team-card__title mt-3">
                      Hima Bisht{' '}
                    </h4>
                    <p>Head – Training & Workforce Development </p>
                  </div>
                </div>
              </NavLink>
            </div>

            <div className="col-lg-4 col-md-4 col-12 mb-5 ">
              <NavLink exact to="/soheb-khan">
                <div className="card team-card">
                  <img
                    className="card-img-top team-card__img"
                    src={sohebKhan}
                    alt="Soheb Khan"
                    alt="Soheb Khan "
                  />
                  <div className="card-block team-card__block">
                    <h4 className="card-title team-card__title mt-3">
                      Soheb Khan{' '}
                    </h4>
                    <p>Creative Head</p>
                  </div>
                </div>
              </NavLink>
            </div>
          </div>
        </div>
      </section>
      <section className="py-5 ">
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-12">
              <div className="recentnews-Box">
                <div className="recentnews-img">
                  <a
                    href="https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/Mitkat+Cert+2021.pdf"
                    target="_blank"
                  >
                    <img src={certificate} alt="" />{' '}
                  </a>
                </div>
                <div className="recentnews-header pl-3">
                  <span className="recentnews-title">
                    ISO 27001 certification{' '}
                  </span>

                  <p className="recentnews-summary mb-5">
                    Our ISO 27001 certification demonstrates that MitKat
                    Advisory has invested in the people, processes, and
                    technology to protect our and our clients’ valuable data. It
                    provides evidence to our customers, investors, and other
                    interested parties that we are managing information security
                    according to international best practice. Having a ISO
                    27001-compliant ISMS helps us manage the confidentiality,
                    integrity, and availability of all corporate data in an
                    optimized and cost-effective way.
                    <br />
                    <a
                      href="https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/Mitkat+Cert+2021.pdf"
                      target="_blank"
                    >
                      <FontAwesomeIcon icon={faExternalLinkAlt} /> &nbsp;view
                      certificate
                    </a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default About;
