import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import AboutSection from '../../components/AboutSection';
import leaderImg from '../../assets/images/team/Sagar B._960_1139.png';

const SagarBhanushali = () => {
  return (
    <>
      <Navbar />
      <AboutSection
        imgsrc={leaderImg}
        name="Sagar Bhanushali"
        award=""
        designation="Business Head – Predictive Risk Intelligence Platform  "
        quote="Life is a Journey - Think Big. Start Small. Grow Consistently"
        linkedInLink="https://www.linkedin.com/in/sagar-bhanushali-%F0%9F%87%AE%F0%9F%87%B3-5aa36b29/"
      />

      <section className="about-us py-5">
        <div className="container">
          <div className="row px-3">
            <div className="col-12 col-md-8 col-lg-8">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase">
                  About Sagar Bhanushali
                </h2>
                <p className="content__subheader py-3 ">
                  Sagar Bhanushali is a seasoned professional with 14 years of
                  business acumen across the fields like e-security, home
                  automation, frontier technologies like AR/VR and education. At
                  work, he enjoys networking and interested in promoting
                  businesses that solve real world problems using Data analytics
                  and Technology. He values people, processes, and systems to
                  ensure long term connections & associations. He is a founder
                  of 2 start-up companies in the past at early age and learned
                  from hands-on entrepreneurial and OJT experiences. Sagar has
                  also been instrumental in setting up strategic initiatives,
                  digital transformation besides generating revenue while
                  working with Reliance Jio Tesseract Imaging Ltd, CP Goenka
                  international Schools group and Technocrat Security Systems in
                  multi-functional roles.
                  <br />
                  At MitKat Advisory services Pvt Ltd, he oversees new business
                  development for the PRI platform services globally. His focus
                  is to help the business continuity managers, corporate
                  security leaders, risk management & resiliency professionals,
                  facility heads to de-risk their assets with real time
                  actionable intelligence. Our team of risk analyst &
                  AI-assisted portal services will compliment these functional
                  heads to focus on their core business operations, while we do
                  the heavy weightlifting of corroborating relevant information
                  and recommending remedies to mitigate risks. As a co-chair for
                  global organization VRAR Association in India and certified
                  Microsoft Educator, Sagar is passionate towards teaching STEM
                  concepts to the Gen Z and helping them shape up their future
                  careers.
                  <br />
                  He is proud alumnus of NMIMS University and KJ Somaiya College
                  of Engineering at Mumbai. In personal life, he is passionate
                  about singing, biking, traveling, investing, mentoring
                  start-ups and teaching.
                </p>
              </div>
            </div>

            <div className="col-12 col-md-4 col-lg-4">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase ">
                  Specialties
                </h2>
                <ul className="sub-header  py-3 ">
                  <li>Leadership</li>
                  <li>Strategy and Execution</li>
                  <li>Team building</li>
                  <li>Consultative Business Development</li>
                  <li>Hunter approach</li>
                  <li>Partner ecosystem creation</li>
                  <li>Account nurturing </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default SagarBhanushali;
