import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import AboutSection from '../../components/AboutSection';
import leaderImg from '../../assets/images/team/Malcolm-568_611px.webp';

const MalcolmCooper = () => {
  return (
    <>
      <Navbar />
      <AboutSection
        imgsrc={leaderImg}
        name="Malcolm Cooper"
        award=""
        designation="Regional Director"
        quote="If Learning never stops – No reason why “unlearning” should never stop as well"
        linkedInLink="https://www.linkedin.com/in/coopermalcolm/"
      />

      <section className="about-us py-5">
        <div className="container">
          <div className="row px-3">
            <div className="col-12 col-md-8 col-lg-8">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase">
                  About Malcolm Cooper
                </h2>
                <p className="content__subheader py-3 ">
                  Malcolm has a decade of experience in various roles including
                  Military, People Consulting, Security Risk Management,
                  Facilities Management, Corporate Real Estate and Business
                  Continuity.
                  <br />
                  He has worked in various roles within the security domain
                  including geo-political risk intelligence, security risk
                  assessments, security operations support, project management,
                  executive and event protection, and Enterprise level Crisis
                  Management. He has also worked for high-risk assignments
                  helping organizations and people manage crisis as well
                  contribute to disaster recovery. He has successfully delivered
                  projects and assignments across the industry spectrum and in
                  foreign geographies as well. He is also an effective trainer
                  delivering trainings on various aspects of Security, Safety
                  and Crisis Management to first response teams and employee
                  groups alike.
                  <br />
                  His certifications include ISO 27001 and ISO 22301. He has
                  also received his CPP (Certified Protection Professional) and
                  is currently pursuing MCR (Master of Corporate Real Estate).
                  Malcolm is an active member of the ASIS Bangalore Chapter and
                  the CoreNet Global India chapter.
                </p>
              </div>
            </div>

            <div className="col-12 col-md-4 col-lg-4">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase ">
                  Specialties
                </h2>
                <ul className="sub-header  py-3 ">
                  <li>Physical Security Management </li>
                  <li>Security Risk Assessments</li>
                  <li>Crisis Management</li>
                  <li>
                    Business Continuity Management and capability development
                  </li>
                  <li>Operations support and consulting</li>
                  <li>Executive Protection and Event Protection</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default MalcolmCooper;
