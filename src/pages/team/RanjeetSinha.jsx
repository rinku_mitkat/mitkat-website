import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import AboutSection from '../../components/AboutSection';
import leaderImg from '../../assets/images/team/Ranjeet banner image.jpg';

const RanjeetSinha = () => {
  return (
    <>
      <Navbar />
      <AboutSection
        imgsrc={leaderImg}
        name="Ranjeet Sinha"
        award=""
        designation="Director - Strategic Initiatives "
        quote="No rewards without risk "
        linkedInLink="https://www.linkedin.com/in/ranjeet-sinha-44091711"
      />

      <section className="about-us py-5">
        <div className="container">
          <div className="row px-3">
            <div className="col-12 col-md-8 col-lg-8">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase">
                  About Ranjeet Sinha{' '}
                </h2>
                <p className="content__subheader py-3 ">
                  Ranjeet is a military veteran with more than 22 years of
                  experience in the uniform and corporate world. He specialises
                  in Physical security strategy development, creation of
                  mitigation framework and policies, integrated security project
                  management, crisis management, counter-terrorism strategy, and
                  training. He has executed projects across industries including
                  critical infrastructure, oil & gas, transport, energy, SEZ,
                  port operations, manufacturing, hospitality, retail, real
                  estate, and others both in India and globally.
                  <br />
                  He has been a speaker at various forums on the effective use
                  of new-age technologies for the benefits of security and
                  crisis management operations. His certifications include ISO
                  27001 LA and CASM (Aviation).
                  <br />
                  He is based out of Gurgaon.
                </p>
              </div>
            </div>

            <div className="col-12 col-md-4 col-lg-4">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase ">
                  Specialties{' '}
                </h2>
                <ul className="sub-header  py-3 ">
                  <li>Strategy and Concept Development for Risk Management </li>
                  <li>Crisis management and Capability Development </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default RanjeetSinha;
