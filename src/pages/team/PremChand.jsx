import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import AboutSection from '../../components/AboutSection';
import leaderImg from '../../assets/images/team/Dr Prem Chand.webp';

const PremChand = () => {
  return (
    <>
      <Navbar />
      <AboutSection
        imgsrc={leaderImg}
        name="Dr Prem Chand "
        award=""
        designation=" Executive Vice Chairman"
        quote="Our entrepreneurial DNA lies in our ability to build smaller teams from ground up and sustained focus on blending emerging technologies with ideas, innovation and implementation – truly a boutique culture."
        linkedInLink="https://www.linkedin.com/in/dr-prem-chand-47669960/"
      />

      <section className="about-us py-5">
        <div className="container">
          <div className="row px-3">
            <div className="col-12 col-md-8 col-lg-8">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase">
                  About Dr Prem Chand{' '}
                </h2>
                <p className="content__subheader py-3 ">
                  Dr. Prem Chand is an Indian Navy Veteran officer with an
                  outstanding profile. He was responsible for Information
                  Infrastructure, IT Security, Information Warfare Program,
                  development of Real Time Decision Support and Combat Systems
                  for the Navy.
                  <br />
                  After his tenure with the Indian Navy, Dr. Prem Chand was CIO
                  and CISO in Tech Mahindra. He was heading different business
                  lines of Tech Mahindra such as Security Consulting, Security
                  Technology Group, ERP (SAP & MS Practices) and Telecom
                  Business Consulting. He also served as the CEO of iPolicy
                  Networks. Additionally, Dr. Prem Chand has served on the Board
                  of several Government and Non-Government Organisations such as
                  :
                  <br />
                  <ul>
                    <li>
                      Prime Minister’s National Task Force on IT & Software
                      Development{' '}
                    </li>
                    <li>
                      New Generation Operations Software & Systems Architecture
                      Board at TMF
                    </li>
                    <li>
                      Information Systems Audit & Control Association (ISACA)
                    </li>
                    <li>
                      NASSCOM National Advisory Board on Security & Assurance
                    </li>
                    <li>Regional Security Institute</li>
                    <li>Asia Pacific Telecommunication Council</li>
                    <li>Design & Development Working Group on IT</li>
                  </ul>
                  <br />
                  Dr. Prem Chand was also instrumental in heading the World Tele
                  Management Forum’s (TMF) Security Committee as the Chairman.
                  Having seen the evolution of the IT industry not only in India
                  but across the world, Dr. Prem Chand has also authored several
                  research papers and whitepapers. Dr. Prem Chand has been
                  awarded several times for his stellar achievements.
                </p>
              </div>
            </div>

            <div className="col-12 col-md-4 col-lg-4">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase ">
                  Specialties{' '}
                </h2>
                <ul className="sub-header  py-3 "></ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default PremChand;
