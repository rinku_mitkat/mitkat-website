import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import AboutSection from '../../components/AboutSection';
import leaderImg from '../../assets/images/team/Ameya banner image.webp';

const AmeyaPatkar = () => {
  return (
    <>
      <Navbar />
      <AboutSection
        imgsrc={leaderImg}
        name="Ameya Patkar"
        award=""
        designation="Digital Transformation Leader "
        quote="For any organisation to sustain and thrive in this fast-paced world, it is essential to make technology adoption a part of their business ethos! "
        linkedInLink="https://www.linkedin.com/in/ameya-patkar-89580987/"
      />

      <section className="about-us py-5">
        <div className="container">
          <div className="row px-3">
            <div className="col-12 col-md-8 col-lg-8">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase">
                  About Ameya Patkar{' '}
                </h2>
                <p className="content__subheader py-3 ">
                  Alumnus of Indian Institute of Technology, Roorkee
                  (IIT-Roorkee) and a qualified finance graduate, Ameya excels
                  in conceptualising and creating scalable solutions that
                  increase productivity and improve user experience.
                  <br />
                  Ameya’s love for problem solving and technology help him
                  create state of the art software for hitherto unexplored
                  industries. His rich and diverse experience, having worked
                  with the financial, manufacturing, eCommerce and Information
                  Technology sectors previously, aids in creating solutions that
                  are practical while addressing all problem areas.
                  <br />
                  At MitKat Advisory Services, Ameya heads the technology
                  vertical and has been pivotal in launching several technology
                  based solutions such as Risk Tracker Portal and the Mobile
                  App, Safety Audit platforms, COVID-19 Dashboard and
                  Containment Zone map and Risk and Intelligence Monitoring
                  Engine (RIME).
                  <br />A Software Engineer by profession, Ameya is an
                  adventurer at heart. A trait that allows him to take risks
                  while making a statement along the way.
                </p>
              </div>
            </div>

            <div className="col-12 col-md-4 col-lg-4">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase ">
                  Specialties{' '}
                </h2>
                {/* <ul className="sub-header  py-3 ">
                            <li>Leadership </li>
                            <li>Strategy </li>              
                            <li>Business Management</li>
                            <li>Business Development</li> 
                            <li>Key Account Management </li>
                            <li>Operations </li>
                            <li>Managed Services </li>
                            <li>Geo-politics </li>
                            <li>Corporate Security </li>    
                        </ul>  */}
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default AmeyaPatkar;
