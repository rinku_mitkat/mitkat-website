import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import quoteIcon from '../../assets/images/arrow/quote.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';
import leaderImg from '../../assets/images/team/Sudhir Sir banner image.webp';
import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import video from '../../assets/video/sudhir sharma.mp4';

const SudhirSharma = () => {
  return (
    <>
      <Navbar />
      <div className="section d-flex">
        <div className="container-fluid">
          <div className="row section__row">
            <div className="col-lg-6 col-md-6 col-12">
              <img src={leaderImg} width="100%" />
            </div>
            <div className="col-lg-6 col-md-6 col-12 my-auto">
              <div className="content">
                <NavLink exact to="/about" className="main-header__link">
                  Back to team &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <h1 className="my-3 main-header__header-font text-capitalize">
                  {' '}
                  Lt Gen Sudhir Sharma
                  <br />
                  <small className="smaller leader-addition ">
                    PVSM, AVSM, YSM, VSM
                  </small>
                </h1>

                <p className="about__designation py-3">Chairman</p>
                <blockquote
                  className="leader-quote"
                  cite="https://www.linkedin.com/in/lt-gen-sudhir-sharma-b9083525/"
                >
                  <img src={quoteIcon} height="35px;" />
                  Culture is the habitat in which strategy thrives or withers.
                  While culture defines engagement, passion and execution, it
                  also includes the core values of the company and its
                  employees. MitKat gives primacy to culture as the bedrock and
                  inherent founding pillar of the company.
                </blockquote>
                <p className="sub-header  py-3">
                  <a
                    className="badge badge-pill badge-dark text-white"
                    href="https://www.linkedin.com/in/lt-gen-sudhir-sharma-b9083525/"
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faLinkedinIn} />
                  </a>
                  <a
                    className="badge badge-pill badge-dark text-white"
                    href="https://twitter.com/gensudhir"
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faTwitter} />
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section className="about-us py-5">
        <div className="container">
          <div className="row px-3">
            <div className="col-12 col-md-8 col-lg-8">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase">
                  About Lt Gen Sudhir Sharma{' '}
                </h2>
                <p className="content__subheader py-3 ">
                  Lt. Gen Sudhir Sharma has handled distinguished leadership,
                  staff, instructional and diplomatic assignments, including
                  command of the largest operational force in the world. He has
                  lead and modernized the logistics of the 1.3 million-strong
                  Indian Army, and was India’s Defence Attaché in London. He has
                  been on the Board/Panel of Advisors/Governors of large
                  Indian/global corporations.
                  <br />
                  Lt. Gen Sharma is an acknowledged thought leader and expert on
                  defence, homeland and corporate security. He advises
                  corporations, defence, diplomatic and homeland security (HLS)
                  think-tanks, and is a speaker and author of repute. He has
                  been providing his valuable insights on the most salient
                  economic and political risk factors hovering over the globe,
                  in major international forums like The Horasis Annual Meeting,
                  Switzerland & Ireland, Global Arab Summit, Secure Tech etc.
                  <br />
                  Lt Gen Sharma has been decorated for gallantry as well as
                  distinguished service. He has been felicitated on two
                  occasions by the President of India for leadership and
                  devotion to duty and country.
                  <br />A strong votary of safe, secure, responsible and
                  sustainable development, General Sharma is passionate about
                  ecology, environment and entrepreneurship. He is a
                  well-recognized leadership coach and has mentored startups,
                  and helped them realize their potential.
                </p>
              </div>
            </div>

            <div className="col-12 col-md-4 col-lg-4">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase ">
                  Specialties{' '}
                </h2>
                <ul className="sub-header  py-3 ">
                  <li> Thought Leadership </li>
                  <li> Strategic Analysis </li>
                  <li> Mentoring </li>
                  <li> Business Leadership </li>
                  <li> Public & Media Relations </li>
                </ul>
                <br /> <br />
                <div className="embed-responsive embed-responsive-16by9">
                  <video controls muted className="embed-responsive-item">
                    <source src={video} type="video/mp4" />
                  </video>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default SudhirSharma;
