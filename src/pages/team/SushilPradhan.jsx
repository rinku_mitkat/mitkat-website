import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import quoteIcon from '../../assets/images/arrow/quote.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';

import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import leaderImg from '../../assets/images/team/Sushil Sir banner image.webp';
import video from '../../assets/video/sushil.mp4';

const SushilPradhan = () => {
  return (
    <>
      <Navbar />
      <div className="section d-flex">
        <div className="container-fluid">
          <div className="row section__row">
            <div className="col-lg-6 col-md-6 col-12">
              <img src={leaderImg} width="100%" />
            </div>
            <div className="col-lg-6 col-md-6 col-12 my-auto">
              <div className="content">
                <NavLink exact to="/about" className="main-header__link">
                  Back to team &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <h1 className="my-3 main-header__header-font text-capitalize">
                  {' '}
                  Sushil Pradhan{' '}
                </h1>

                <p className="about__designation py-3">
                  Executive Director & COO
                </p>
                <blockquote
                  className="leader-quote"
                  cite="https://www.linkedin.com/in/pradhansushil/"
                >
                  <img src={quoteIcon} height="35px;" />
                  Find good people, give them a great atmosphere to work in, and
                  they’ll do a great job – that’s what we in MitKat believe in.
                  ”
                </blockquote>
                <p className="sub-header  py-3">
                  <a
                    className="badge badge-pill badge-dark text-white"
                    href="https://www.linkedin.com/in/pradhansushil/"
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faLinkedinIn} />
                  </a>
                  <a
                    className="badge badge-pill badge-dark text-white"
                    href="https://twitter.com/SushilMitkat"
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faTwitter} />
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section className="about-us py-5">
        <div className="container">
          <div className="row px-3">
            <div className="col-12 col-md-8 col-lg-8">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase">
                  About Sushil Pradhan{' '}
                </h2>
                <p className="content__subheader py-3 ">
                  Sushil is a seasoned professional with over 30 years of
                  experience in leadership and management roles within complex
                  operations, supervising diverse teams and managing challenging
                  programs/assignments within India and abroad.
                  <br />
                  Sushil has provided customised Risk Management solutions to a
                  variety of MNCs and Indian companies; in Myanmar, Iraq,
                  Bangladesh, China, Japan, Sri Lanka and difficult geographies
                  of India.
                  <br />
                  Sushil is a prolific speaker in several influential and high
                  profile industry forums not only in India but also at London,
                  Dubai, New York, Singapore, Beijing, Colombo and Dhaka; and is
                  a sought after keynote speaker and moderator in industry panel
                  discussions. To his credit various articles published in
                  industry and strategic journals; and has also made several
                  appearances as a security/defence expert on national
                  television.
                  <br />
                  Sushil has been conducting security audits and assessments
                  across the world for data centres, IT parks, schools,
                  manufacturing plants, offices and banks. He is also much in
                  demand for conducting crisis simulation exercises for
                  corporate management teams, and other forms of
                  risk/security/safety training.
                  <br />
                  In addition to an MBA from Pune University, he has an MSc (in
                  Defence and Strategic Studies) and MPhil (in National Security
                  Studies).
                  <br />
                  During his 22 years in the military (he retired as a
                  Lieutenant Colonel from the Indian Army), Sushil handled
                  combat, instructional, staff and multi-national assignments.
                  He was decorated for gallantry in Counter-Terrorist
                  operations; trained in commando operations, counter-insurgency
                  and jungle warfare; and has served extensively in the
                  Himalayan high-altitudes. He was also responsible for
                  preparation of policy documents, doctrines, geo-political
                  assessments and geo-strategic approach papers.
                  <br />
                  He has been a combat trainer in tactics, anti-tank missiles
                  and fighting vehicles; having trained officers and soldiers of
                  several countries. He was a UN Peacekeeper in Sierra Leone in
                  West Africa; and has been awarded the UN Medal.
                  <br />
                  He trained International UN peacekeepers in the United Nations
                  Training Centre of the Bundeswehr (Germany) on hostage
                  negotiation, mine awareness, cultural sensitisation, and
                  protection of civilians in war zones.
                </p>
              </div>
            </div>

            <div className="col-12 col-md-4 col-lg-4">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase ">
                  Specialties{' '}
                </h2>
                <ul className="sub-header  py-3 ">
                  <li>Strategic Analysis </li>
                  <li>Geo-political Risk Consulting </li>
                  <li>Security Assessments and Benchmarking </li>
                  <li>Crisis Simulations </li>
                  <li>Predictive Risk Intelligence </li>
                </ul>
                <br /> <br />
                <div className="embed-responsive embed-responsive-16by9">
                  <video controls muted className="embed-responsive-item">
                    <source src={video} type="video/mp4" />
                  </video>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default SushilPradhan;
