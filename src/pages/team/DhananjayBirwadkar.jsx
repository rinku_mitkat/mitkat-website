import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import quoteIcon from '../../assets/images/arrow/quote.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';

import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import leaderImg from '../../assets/images/team/Dhananjay banner image.webp';

const DhananjayBirwadkar = () => {
  return (
    <>
      <Navbar />
      <div className="section d-flex">
        <div className="container-fluid">
          <div className="row section__row">
            <div className="col-lg-6 col-md-6 col-12">
              <img src={leaderImg} width="100%" />
            </div>
            <div className="col-lg-6 col-md-6 col-12 my-auto">
              <div className="content">
                <NavLink exact to="/about" className="main-header__link">
                  Back to team &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <h1 className="my-3 main-header__header-font text-capitalize">
                  {' '}
                  Dhananjay Birwadkar
                </h1>

                <p className="about__designation py-3">
                  Director – Design Consulting & Project Management
                </p>
                <blockquote
                  className="leader-quote"
                  cite="https://www.linkedin.com/in/dhananjay-birwadkar/"
                >
                  <img src={quoteIcon} height="35px;" />
                  In the new diverse, diffused and complex risk environment, it
                  is imperative that security be designed and built in at the
                  planning stage itself, and not be bolted on as an
                  afterthought!
                </blockquote>
                <p className="sub-header  py-3">
                  <a
                    className="badge badge-pill badge-dark text-white"
                    href="https://www.linkedin.com/in/dhananjay-birwadkar/"
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faLinkedinIn} />
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section className="about-us py-5">
        <div className="container">
          <div className="row px-3">
            <div className="col-12 col-md-8 col-lg-8">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase">
                  About Dhananjay Birwadkar{' '}
                </h2>
                <p className="content__subheader py-3 ">
                  Dhananjay has around 11 years of wide and deep industrial
                  experience of handling varieties of critical projects. He has
                  worked in various domains viz. Physical security system
                  design, Automation, Technology transformation/migrations, Safe
                  and smart city, rolling out E-governance, IOT, Networking &
                  communication system design projects etc.
                  <br />
                  He has worked in various roles and functions as BD, Pre-Sales,
                  System designer, Solution Architect, Consultant, On-ground
                  system implementer, Tendering & bid manager, Vendor
                  management, Project management in Turnkey projects deliveries.
                  He is a dynamic team leader with consistently proven track
                  record of leading large size projects and operations in
                  adverse conditions. He has ability to set up clear objectives
                  and execute strategies in a highly demanding environment with
                  limited resources.
                  <br />
                  In his consulting career, he has successfully delivered
                  various greenfield, brownfield security engineering, SOC
                  designing and ad-hoc consulting design requirement in various
                  sectors including Education institutes, Banks, IT/ITES, Data
                  Centers, Mix civil development (office-retail shopping
                  complex/malls-multiplex in same campus), Housings/township,
                  Defense, manufacturing plants, conglomerates etc.
                  <br />
                  His specific areas of expertise include:
                  <br />
                  <br />
                  <strong> Homeland Securities: </strong>
                  <br />
                  Video Surveillance, Access Control Systems, Intrusion
                  Detection Systems (IDS), PAS, FAS, BMS, Integrated Security
                  Systems and Security operation center (SOC) designing.
                  <br />
                  <br />
                  <strong> Safe & Smart Cities and IOT: </strong>
                  <br />
                  City Surveillance, Information Communication Technologies
                  (ICT), E-governance systems, Intelligent traffic Management
                  Systems (ITMS), Automatic Fare Collection Systems, Municipal
                  Solid Waste (MSW) Management Solutions, Smart metering system,
                  Digital classroom system, Intelligent Street Light Systems,
                  Parking Management Systems, Public distribution system,
                  Residential Automation Systems and Sensor Networks etc.
                </p>
              </div>
            </div>

            <div className="col-12 col-md-4 col-lg-4">
              {/* <div className="content">
                <h2 className="mb-3 header-font text-uppercase ">
                  Specialties{' '}
                </h2>
                <ul className="sub-header  py-3 "></ul>
              </div> */}
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default DhananjayBirwadkar;
