import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import quoteIcon from '../../assets/images/arrow/quote.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';

import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import leaderImg from '../../assets/images/team/Pawan Sir.webp';
import video from '../../assets/video/pawan.mp4';

const PawanDesai = () => {
  return (
    <>
      <Navbar />
      <div className="section d-flex">
        <div className="container-fluid">
          <div className="row section__row">
            <div className="col-lg-6 col-md-6 col-12">
              <img src={leaderImg} width="100%" />
            </div>
            <div className="col-lg-6 col-md-6 col-12 my-auto">
              <div className="content">
                <NavLink exact to="/about" className="main-header__link">
                  Back to team &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <h1 className="my-3 main-header__header-font text-capitalize">
                  {' '}
                  Pawan Desai
                  <br />
                  <small className="smaller leader-addition">
                    {' '}
                    CISA, CISSP, CBCP
                  </small>
                </h1>

                <p className="about__designation py-3">Co-founder & CEO</p>
                <blockquote
                  className="leader-quote"
                  cite="https://www.linkedin.com/in/pawan-desai-5455562/"
                >
                  <img src={quoteIcon} height="35px;" />
                  Creating brand is a long journey. It needs passionate leaders,
                  colleagues and supportive clients. Magic happens when this is
                  achieved - and we are here to create this Magic.
                </blockquote>
                <p className="sub-header  py-3">
                  <a
                    className="badge badge-pill badge-dark text-white"
                    href="https://www.linkedin.com/in/pawan-desai-5455562/"
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faLinkedinIn} />
                  </a>
                  <a
                    className="badge badge-pill badge-dark text-white"
                    href="https://twitter.com/pawandesai"
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faTwitter} />
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section className="about-us py-5">
        <div className="container">
          <div className="row px-3">
            <div className="col-12 col-md-8 col-lg-8">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase">
                  About Pawan Desai{' '}
                </h2>
                <p className="content__subheader py-3 ">
                  An alumnus of Jamnalal Bajaj Institute of management studies
                  and IVLP Fellow (US State Department) Pawan has 25+ years of
                  experience in the security risk management domain.
                  <br />
                  He has been involved with initial set up and growth of a
                  Mahindra Group consulting company in the domain of Cyber
                  Security before co-founding MitKat Advisory Services.
                  <br />
                  Currently he is the CEO and responsible for the MitKat’s P&L
                  and future global aspirations.
                  <br />
                  He has led several high profile complex and multidimensional
                  risk consultancy assignments with industry leaders across
                  verticals for more than 100+ blue chip companies.
                  <br />
                  A distinguished Ex- Indian Navy officer with specialization in
                  Submarine operations.
                  <br />
                  He is an acknowledged thought leader in security risk
                  management and a staunch supporter of convergency of Cyber and
                  Physical security domains in future.
                  <br />
                  He speaks regularly at prestigious forums like OSAC, ACSG,
                  ASIS, BCI, Duty of Care, etc. He has authored several articles
                  in professional journals and has been selected as one of the
                  few security professionals in the country to be part of
                  delegation representing India in the India-US security
                  professionals meet in US in 2014.
                  <br />
                  He has contributed to security fraternity by holding a post of
                  Secretary of Mumbai chapter of American Society for Industrial
                  Security (ASIS).
                  <br />
                  He likes to give back to next generation by being a regular
                  visiting faculty at Symbiosis International University, NMIMS
                  and many other premium management institutes. As a part of
                  Digital India initiative he has been deeply involved with
                  Industry body in training of more than 1000+ people on Cyber
                  safety awareness and certification.
                  <br />
                  His professional qualifications includes CISA, CISSP, CBCP,
                  among others.
                </p>
              </div>
            </div>

            <div className="col-12 col-md-4 col-lg-4">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase ">
                  Specialties{' '}
                </h2>
                <ul className="sub-header  py-3 ">
                  <li>Entrepreneurship</li>
                  <li>Strategy</li>
                  <li>Leadership</li>
                  <li>Information Security</li>
                  <li>Business Continuity </li>
                </ul>
                <br /> <br />
                <div className="embed-responsive embed-responsive-16by9">
                  <video controls muted className="embed-responsive-item">
                    <source src={video} type="video/mp4" />
                  </video>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default PawanDesai;
