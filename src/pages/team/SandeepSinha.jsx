import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import quoteIcon from '../../assets/images/arrow/quote.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';

import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import leaderImg from '../../assets/images/team/Sandeepl Sir banner image.webp';

const SandeepSinha = () => {
  return (
    <>
      <Navbar />
      <div className="section d-flex">
        <div className="container-fluid">
          <div className="row section__row">
            <div className="col-lg-6 col-md-6 col-12">
              <img src={leaderImg} width="100%" />
            </div>
            <div className="col-lg-6 col-md-6 col-12 my-auto">
              <div className="content">
                <NavLink exact to="/about" className="main-header__link">
                  Back to team &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <h1 className="my-3 main-header__header-font text-capitalize">
                  {' '}
                  Sandeep Sinha
                </h1>

                <p className="about__designation py-3">
                  Director - Risk Consulting & Managed Services
                </p>
                <blockquote
                  className="leader-quote"
                  cite="https://www.linkedin.com/in/sandeep-sinha-05/"
                >
                  <img src={quoteIcon} height="35px;" />
                  Threats are increasingly multi-dimensional and
                  multi-directional. Lines between physical security, IT and
                  Cybersecurity, BCM, Resilience have blurred permanently.
                  Collaboration, integration, and consolidation are the key to a
                  secure business environment.
                </blockquote>
                <p className="sub-header  py-3">
                  <a
                    className="badge badge-pill badge-dark text-white"
                    href="https://www.linkedin.com/in/sandeep-sinha-05/"
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faLinkedinIn} />
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section className="about-us py-5">
        <div className="container">
          <div className="row px-3">
            <div className="col-12 col-md-8 col-lg-8">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase">
                  About Sandeep Sinha{' '}
                </h2>
                <p className="content__subheader py-3 ">
                  An alumnus of the Indian Institute of Management, Ahmedabad
                  and Jamnalal Bajaj Institute of Management Studies, Sandeep
                  comes with wide experience of strategic and tactical planning,
                  operations & maritime security in high-risk environment in
                  India and abroad.
                  <br />
                  Formerly an Indian Navy Submarine Commanding Officer, and now
                  heading the Risk Consulting and Managed Services verticals of
                  MitKat, Sandeep has provided stellar thought leadership and
                  services to a large number of global Organisations in India
                  and overseas. Prominent areas of consultancy have been
                  Enterprise Risk focused on security, Threat Vulnerability Risk
                  Assessment (TVRA), Security & Safety Auditing, Emergency &
                  Crisis Management, Process and Cost Optimization, Technical
                  Surveillance and Countermeasures (TSCM) etc. across industry
                  for critical infrastructure (including government/military
                  mega-projects of strategic importance), refineries,
                  manufacturing facilities, data centres, commercial complexes,
                  IT/ ITES, Supply chain, hospitality etc. in South Asia.
                  <br />
                  Sandeep has spoken at reputed Industry events as Subject
                  Matter Expert on Corporate Security, Drones & Counter-drone
                  technology, Homeland Security, Command and Control Centres for
                  Smart Cities etc. His articles have been published in leading
                  international Security-related magazines. He also conducts
                  webinars on Security Risk Assessment frameworks for domestic
                  and overseas audiences.
                </p>
              </div>
            </div>

            <div className="col-12 col-md-4 col-lg-4">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase ">
                  Specialties{' '}
                </h2>
                <ul className="sub-header  py-3 ">
                  <li>Leadership </li>
                  <li>Strategy </li>
                  <li>Business Management</li>
                  <li>Business Development</li>
                  <li>Key Account Management </li>
                  <li>Operations </li>
                  <li>Managed Services </li>
                  <li>Geo-politics </li>
                  <li>Corporate Security </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default SandeepSinha;
