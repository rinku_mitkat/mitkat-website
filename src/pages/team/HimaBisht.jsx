import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import AboutSection from '../../components/AboutSection';
import leaderImg from '../../assets/images/team/Hima banner image.webp';

const HimaBisht = () => {
  return (
    <>
      <Navbar />
      <AboutSection
        imgsrc={leaderImg}
        name="Hima Bisht "
        award=""
        designation="Head – Training & Workforce Development"
        quote="Training needs to have a learning approach that addresses mindsets and attitudes. We change Organisations, one mind at a time, through training” "
        linkedInLink="https://www.linkedin.com/in/hima-bisht-98525817b/"
      />

      <section className="about-us py-5">
        <div className="container">
          <div className="row px-3">
            <div className="col-12 col-md-8 col-lg-8">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase">
                  About Hima Bisht{' '}
                </h2>
                <p className="content__subheader py-3 ">
                  Hima is a training professional with extensive leadership
                  experience in workforce development & skill upgradation. She
                  understands how to keep a company competitive and efficient
                  through training. This includes finding the best ways to
                  enhance a company’s education, while remaining within the
                  business’ means.
                  <br />
                  She has over 20 years of experience conducting training
                  programmes for Women’s Safety Awareness and Women’s
                  Empowerment as well as conducting Communication skills,
                  Motivational speaking and Soft Skill/Behavioural Training
                  Programmes for the Government.
                  <br />
                  Hima has carried out Training Need Assessment (TNA) designed
                  training curriculum, prepared lesson plans, imparted training
                  in security, safety and emergency response, soft/life skills,
                  carried out tests/assessments and soft skill training for
                  government security professionals.
                  <br />
                  She has successfully spearheaded and extensively conducted
                  MitKat’s Training Programmes for various corporate entities.
                  She is an effective communicator and has hosted many talks and
                  panel discussions on Women Empowerment at events such as IFSEC
                  India, PSP(Pune Security Professionals), SSW (Safe & Secure
                  Workplace) etc.
                  <br />
                  Her forte lies in development of communication skills and
                  personality enhancement. She has led the outreach initiative
                  for various NGO’s to provide training for skill upgradation
                  for children from under-privileged backgrounds in fields as
                  varied as Photography, Painting and Theatre Direction. She
                  herself has directed and acted in plays and being a keen
                  enthusiast of drama created a unique programme/workshop for
                  personality development using theatre. She helped to create
                  programmes for training healthcare professionals and creating
                  healthcare ecosystems in Africa to help improve the quality of
                  life.
                </p>
              </div>
            </div>

            <div className="col-12 col-md-4 col-lg-4">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase ">
                  Specialties{' '}
                </h2>
                <ul className="sub-header  py-3 ">
                  <li>Communication Skills </li>
                  <li>Training Content Development </li>
                  <li>Program Management </li>
                  <li>Leadership Training </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default HimaBisht;
