import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import AboutSection from '../../components/AboutSection';
import leaderImg from '../../assets/images/team/Mitesh banner image.jpg';

const MiteshShah = () => {
  return (
    <>
      <Navbar />
      <AboutSection
        imgsrc={leaderImg}
        name="Mitesh Shah"
        award=""
        designation="Regional Director,
        MitKat Global Consulting Pte Ltd "
        quote="Stay hungry and stay humble. "
        linkedInLink="https://www.linkedin.com/in/mitesh-shah-587826b/"
      />

      <section className="about-us py-5">
        <div className="container">
          <div className="row px-3">
            <div className="col-12 col-md-8 col-lg-8">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase">
                  About Mitesh Shah
                </h2>
                <p className="content__subheader py-3 ">
                  Mitesh is a ‘’Subject Matter Expert’’ in the field of
                  Organizational Resilience, Business Continuity Management and
                  Enterprise Risk Management. He has over 12 years of experience
                  in consulting organizations on how they can de-risk their
                  business and improve their resiliency posture through risk
                  assessments and creation of frameworks in physical security,
                  business continuity and information security. He has worked
                  extensively in the APAC region and assisted clients across
                  sectors of BFSI, Retail, Telecom and global conglomerates.
                  <br />
                  In terms of certifications, Mitesh is CBCI Certified. He is
                  also a CISA & CISA from ISACA and a certified ISO 27001 Lead
                  Auditor.
                  <br />
                  On a personal note, Mitesh has been staying in Singapore since
                  2011 and is blessed with two lovely daughters.
                </p>
              </div>
            </div>

            <div className="col-12 col-md-4 col-lg-4">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase ">
                  Specialties
                </h2>
                <ul className="sub-header  py-3 ">
                  <li>Organizational Resilience, </li>
                  <li>Enterprise Security </li>
                  <li>Business Continuity Management</li>
                  <li>Crisis Management & Cyber Security</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default MiteshShah;
