import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import AboutSection from '../../components/AboutSection';
import leaderImg from '../../assets/images/team/Soheb banner image.webp';

const SohebKhan = () => {
  return (
    <>
      <Navbar />
      <AboutSection
        imgsrc={leaderImg}
        name="Soheb Khan "
        award=""
        designation="Creative Head "
        quote="The idea is to grow together, capitalise on each-other's strengths, enjoy the journey and have remarkable achievements along the way!"
        linkedInLink="https://www.linkedin.com/in/soheb-khan-94a9aa143/"
      />

      <section className="about-us py-5">
        <div className="container">
          <div className="row px-3">
            <div className="col-12 col-md-8 col-lg-8">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase">
                  About Soheb Khan{' '}
                </h2>
                <p className="content__subheader py-3 ">
                  Soheb Khan is an exceptionally talented creative designer who
                  is independent, self-motivated and comes with a strong
                  background in both print and digital media. One of the first
                  few employees, Soheb has played a pivotal part in MitKat’s
                  journey from a start-up to being industry leaders in Safety
                  and Security consultancy for the South Asian market.
                  <br />
                  A qualified graphic and web designed from Arena Animation,
                  Soheb has an impeccable understanding of Photoshop,
                  Illustrator, CorelDraw, Microsoft Word, Microsoft Powerpoint
                  and other latest web-based technologies. Soheb’s quest for
                  perfection and innovative conceptualisation help create high
                  impact material that communicate the message in the most
                  appropriate manner.
                  <br />
                  At MitKat, Soheb ably heads the Design vertical. Over the
                  years, his understanding of pulse of the market have ensured
                  that MitKat’s products and designs have evolved in keeping up
                  with the latest industry requirements as well as market
                  trends.
                </p>
              </div>
            </div>

            <div className="col-12 col-md-4 col-lg-4">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase ">
                  Specialties{' '}
                </h2>
                <ul className="sub-header  py-3 ">
                  <li>Brand Identity </li>
                  <li>Visualization </li>
                  <li>Digital Design </li>
                  <li>Product Design </li>
                  <li>Print Media </li>
                  <li>Infographics </li>
                  <li>Typography </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default SohebKhan;
