import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import AboutSection from '../../components/AboutSection';
import leaderImg from '../../assets/images/team/Aparna banner image.webp';

const AparnaGuddad = () => {
  return (
    <>
      <Navbar />
      <AboutSection
        imgsrc={leaderImg}
        name="Aparna Guddad "
        award=""
        designation="Associate Director - Predictive Risk Intelligence"
        quote="Identify opportunity, provide innovative solutions, build a great team and surge ahead!"
        linkedInLink="https://www.linkedin.com/in/aparna-guddad-aa392294/"
      />

      <section className="about-us py-5">
        <div className="container">
          <div className="row px-3">
            <div className="col-12 col-md-8 col-lg-8">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase">
                  About Aparna Guddad{' '}
                </h2>
                <p className="content__subheader py-3 ">
                  Aparna Guddad has carved a niche for herself in the field of
                  corporate risk intelligence by keeping a keen eye on the
                  geo-political developments across Asia Pacific with specific
                  focus on South Asia.
                  <br />
                  An integral part of MitKat’s leadership, she along with her
                  team of intelligence analysts, have been instrumental in
                  making the Predictive Risk Intel (PRI) vertical a brand to
                  reckon with. As an individual who believes in leading by
                  example, Aparna invests in building a strong and able team by
                  mentoring, motivating, and training young individuals to
                  derive the best out of a geographically dispersed team.
                  <br />
                  Aparna has previously worked with Organisations such as ISS
                  Security Services, India Electronics & Semiconductor
                  Association (IESA) and the Citigroup. Her rich and diverse
                  experience aids in providing comprehensive assessment of the
                  security scenario and tactful recommendations for continued
                  corporate business operations.
                  <br />
                  At MitKat, she heads the flagship Predictive Threat Intel
                  service line. Under her leadership, the vertical has grown by
                  leaps and bounds within a short span of time and is on the
                  right trajectory to scale new heights.
                </p>
              </div>
            </div>

            <div className="col-12 col-md-4 col-lg-4">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase ">
                  Specialties{' '}
                </h2>
                <ul className="sub-header  py-3 ">
                  <li>Leadership </li>
                  <li>Team Building </li>
                  <li>Geopolitical Risk Analysis</li>
                  <li>Research and Trend Analysis</li>
                  <li>Key Account Management </li>
                  <li>Business Development </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default AparnaGuddad;
