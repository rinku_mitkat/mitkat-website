import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import AboutSection from '../../components/AboutSection';
import leaderImg from '../../assets/images/team/Sam Sir banner image.webp';
import video from '../../assets/video/Sam begin.mp4';

const SamKumar = () => {
  return (
    <>
      <Navbar />
      <AboutSection
        imgsrc={leaderImg}
        name="S M Kumar (Sam) "
        award=""
        designation="Co-founder & MD  "
        quote="Mitigating Knowledge Age Risks merits a business-focused, tech-enabled, predictive, intelligence-led, standards & process-based, and cost-conscious approach, with military precision in planning and execution. ” "
        linkedInLink="https://www.linkedin.com/in/samrendra-mohan-kumar-91614311/"
      />

      <section className="about-us py-5">
        <div className="container">
          <div className="row px-3">
            <div className="col-12 col-md-8 col-lg-8">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase">
                  About S M Kumar (Sam){' '}
                </h2>
                <p className="content__subheader py-3 ">
                  Alumnus of IIM Calcutta, King’s College, London and Joint
                  Services Command & Staff College, UK, Sam is an experienced
                  Security and Risk professional, with a demonstrated history of
                  building and driving business to profitability & sustained
                  growth.
                  <br />
                  Founder CEO of MitKat Advisory, a leading Asian risk
                  consultancy, and currently its Managing Director, Samrendra
                  leads the development of products to achieve its mission -
                  ‘de-risk client business and protect vital assets’. Sam has
                  been pivotal to MitKat’s regional and global ambitions – the
                  company counts over 50 of the top 100 global corporations as
                  its customers, and has executed projects successfully in 25
                  countries across 5 continents.
                  <br />
                  A distinguished Veteran (winner of Sword of Honour &
                  President’s Gold Medal at Indian Military Academy), Col S M
                  Kumar has held distinguished leadership, staff, instructional,
                  technical and multi-national assignments, commanded a combat
                  unit, and was decorated for distinguished service. Sam raised
                  a Consulting sub-unit for a leading Conglomerate, and oversaw
                  its rapid growth.
                  <br />
                  Sam is an acknowledged thought leader, speaker and author. He
                  appears on primetime TV, Radio, security and leadership
                  conclaves, and supports veterans and entrepreneurs.
                </p>
              </div>
            </div>

            <div className="col-12 col-md-4 col-lg-4">
              <div className="content">
                <h2 className="mb-3 header-font text-uppercase ">
                  Specialties{' '}
                </h2>
                <ul className="sub-header  py-3 ">
                  <li>Leadership </li>
                  <li>Entrepreneurship </li>
                  <li>Strategy </li>
                  <li>Business Management</li>
                  <li>Revenue & Business Development</li>
                  <li>Brand and Marketing Strategy </li>
                  <li>Geo-politics </li>
                  <li>Security </li>
                  <li>Motivational Speaking and Talent Development </li>
                </ul>
                <br /> <br />
                <div className="embed-responsive embed-responsive-16by9">
                  <video controls muted className="embed-responsive-item">
                    <source src={video} type="video/mp4" />
                  </video>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default SamKumar;
