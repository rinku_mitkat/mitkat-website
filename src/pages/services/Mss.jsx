import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import { NavLink } from 'react-router-dom';
import ServiceHeader from '../../components/ServiceHeader';
import ShowMoreText from 'react-show-more-text';
import mssBannerImg from '../../assets/images/service/Managed Security Services (MSS).webp';
import osl from '../../assets/images/service/MSS/Outsourced Security Leadership.webp';
import oe from '../../assets/images/service/MSS/Outsourced Expertise.webp';
import pm from '../../assets/images/service/MSS/Project Management.webp';
import ts from '../../assets/images/service/MSS/Training & Sustenance.webp';
import Casestudies from '../../components/Casestudies';

import casestudy01 from '../../assets/images/casestudies/MSS/Staffing for South Asia Command Centre for a leading MNC Bank based in Mumbai.webp';
import casestudy02 from '../../assets/images/casestudies/MSS/Creation of Security Function of one of the India’s Largest ITES Conglomerate and Managing Operations.webp';
import casestudy03 from '../../assets/images/casestudies/MSS/Site Security Leadership  for a leading Global FMCG Company.webp';
import casestudy04 from '../../assets/images/casestudies/MSS/Provision of PMO for Risk Assessment, Integrated Security Architecture & Security System_ Project Implementation.webp';
import casestudy05 from '../../assets/images/casestudies/MSS/Security Project Manager for a leading Global Financial Sector Conglomerate.webp';
import casestudy06 from '../../assets/images/casestudies/MSS/Provision of PMO for Risk Assessment, Designing Architecture & Security System_ Project Implementation at Pune.webp';
import casestudy07 from '../../assets/images/casestudies/MSS/Control Room Design & Operations for a global mining company in Zambia, Africa.webp';
import casestudy08 from '../../assets/images/casestudies/MSS/Operational Support to multiple oil projects in Iraqi Kurdistan Region.webp';
import casestudy09 from '../../assets/images/casestudies/MSS/Provision of Chief Security Officer (CSO) for a leading Healthcare Company thumbnail.webp';
import casestudy10 from '../../assets/images/casestudies/MSS/Provision of Corporate Security Manager for a Agri - Business Company based out of Pune.webp';
import { Helmet } from 'react-helmet';

const handleDragStart = (e) => e.preventDefault();

const casestudiesItem = [
  <div className="block-box ">
    <img
      src={casestudy01}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_mss_01" className="block-box__title">
        Staffing for South Asia Command Centre for a leading MNC Bank based in
        Mumbai
      </NavLink>
    </h5>
    <div className="block-box__by"></div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy02}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_mss_02" className="block-box__title">
        Creation of Security Function of one of the India’s Largest ITES
        Conglomerate and Managing Operations
      </NavLink>
    </h5>
    <div className="block-box__by"></div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy03}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_mss_03" className="block-box__title">
        Site Security Leadership for a leading Global FMCG Company
      </NavLink>
    </h5>
    <div className="block-box__by"></div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy04}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_mss_04" className="block-box__title">
        Provision of PMO for Risk Assessment, Integrated Security Architecture &
        Security System; Project Implementation
      </NavLink>
    </h5>
    <div className="block-box__by"></div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy05}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_mss_05" className="block-box__title">
        Security Project Manager for a leading Global Financial Sector
        Conglomerate
      </NavLink>
    </h5>
    <div className="block-box__by"></div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy06}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_mss_06" className="block-box__title">
        Provision of PMO for Risk Assessment, Designing Architecture & Security
        System; Project Implementation at Pune
      </NavLink>
    </h5>
    <div className="block-box__by"></div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy07}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_mss_07" className="block-box__title">
        Control Room Design & Operations for a global mining company in Zambia,
        Africa
      </NavLink>
    </h5>
    <div className="block-box__by"></div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy08}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_mss_08" className="block-box__title">
        Operational Support to multiple oil projects in Iraqi Kurdistan Region
      </NavLink>
    </h5>
    <div className="block-box__by"></div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy09}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_mss_09" className="block-box__title">
        Provision of Chief Security Officer (CSO) for a leading Healthcare
        Company
      </NavLink>
    </h5>
    <div className="block-box__by"></div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy10}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_mss_10" className="block-box__title">
        Provision of Corporate Security Manager for a Agri - Business Company
        based out of Pune
      </NavLink>
    </h5>
    <div className="block-box__by"></div>
  </div>,

  ,
];

const Mss = () => {
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Managed Security Services | MitKat Advisory</title>
        <meta
          id="description"
          name="description"
          content="Building a world-class Security team is a complex task. While you focus on your core business, MitKat’s curated Managed Security Services"
        />
        <meta
          id="keywords"
          name="keywords"
          content="hiring assistance in security, project management support"
        />
      </Helmet>
      <Navbar />
      <ServiceHeader
        imgsrc={mssBannerImg}
        linkTitle="To know more"
        title="Managed Security Services (MSS)"
        description="Building a world-class Security team is a complex task. While you focus on your core business, MitKat’s curated Managed Security Services enables you to build, operate and upskill, and sustain a proactive, world-class security posture to keep your assets secure and reduce business risk, thereby contributing to profitability."
      />

      <section className="service bg-gray py-5" id="pri_services">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <p className="mb-5 content__header text-center">
                  Service offering
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={osl}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Outsourced Security Leadership
                </h5>
                <div className="block-box__description p-3">
                  <ul className="unorder-list">
                    <li>
                      Embedded Security Leadership - Global to site-level{' '}
                    </li>
                    <li>Talent Identification and Selection </li>
                    <li>Hiring Assistance </li>
                    <li>Performance Support, Oversight, Mentoring </li>
                    <li>Security Vendor Management </li>
                    <li>Attrition Management </li>
                    <li>Pay-rolling </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={oe}
                  width="100%"
                />
                <h5 className="block-box__title px-3">Outsourced Expertise</h5>
                <div className="block-box__description p-3">
                  <ul className="unorder-list">
                    <li>Command & Control Centre Operations </li>
                    <li>Embedded Intelligence specialists </li>
                    <li>Embedded HSE professionals </li>
                    <li>
                      Embedded Resilience and Cyber security experts (incl
                      vCISO){' '}
                    </li>
                    <li>Crisis & Emergency Response assistance </li>
                    <li>Operational Support in challenging environment </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={pm}
                  width="100%"
                />
                <h5 className="block-box__title px-3">Project Management</h5>
                <div className="block-box__description p-3">
                  <ul className="unorder-list">
                    <li>Technical system specification formulation </li>
                    <li>
                      Security project procurement support – RFP formulation to
                      Vendor selection{' '}
                    </li>
                    <li>Security Project Monitoring and Oversight </li>
                    <li>Security Vendor/ OEM/ SI/ MSI Management support </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={ts}
                  width="100%"
                />
                <h5 className="block-box__title px-3">Training & Sustenance</h5>
                <div className="block-box__description p-3">
                  <ul className="unorder-list">
                    <li>Curated training delivered over multiple channels </li>
                    <li>Periodic upskilling </li>
                    <li>Performance Improvement and Audit </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-center">Case Studies</h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <Casestudies item={casestudiesItem} />
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Mss;
