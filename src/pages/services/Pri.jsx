import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import ShowMoreText from 'react-show-more-text';
import priBannerImg from '../../assets/images/Threat Intelligence.webp';
import riskMonitoringPlatform from '../../assets/images/service/PRI/Risk Monitoring Platform.webp';
import customisedRiskManagementSearchEngineandDashboards from '../../assets/images/service/PRI/Customised Risk Management.webp';
import protectiveIntelligence from '../../assets/images/service/PRI/Protective Intelligence.webp';
import alertsandAdvisoryServices from '../../assets/images/service/PRI/Alerts and Advisory Services.webp';
import commandCentreOperations from '../../assets/images/service/PRI/Command Centre Operations.webp';
import travelRiskManagement from '../../assets/images/service/PRI/Travel Risk Management.webp';
import locationRiskAssessments from '../../assets/images/service/PRI/Country location Risk Assessments.webp';
import bespokeResearchandAnalysis from '../../assets/images/service/PRI/Bespoke Research and Analysis.webp';
import helpline from '../../assets/images/24x7 Helpline.webp';
import Casestudies from '../../components/Casestudies';
import Products from '../../components/Products';
import casestudy01 from '../../assets/images/casestudies/PRI/Enabling rural electrification operations in challenging regions.webp';
import casestudy02 from '../../assets/images/casestudies/PRI/Comprehensive risk assessment for Oil & Gas operations in North East India.webp';
import casestudy03 from '../../assets/images/casestudies/PRI/Enabling business for a global multi-national Banking & Financial organisation.webp';
import casestudy04 from '../../assets/images/casestudies/PRI/Predictive Risk Intelligence for a multi-national Internet Technology company to continue business across APAC countries.webp';
import casestudy05 from '../../assets/images/casestudies/PRI/Ensuring employee safety during COVID-19 pandemic and enabling operational efficiency.webp';

import tool01 from '../../assets/images/products/PRI/Weekly Forecast.webp';
import tool02 from '../../assets/images/products/PRI/Monthly Forecast.webp';
import tool03 from '../../assets/images/products/PRI/Daily Risk Tracker.webp';
import tool04 from '../../assets/images/products/PRI/India Risk Review.webp';
import tool05 from '../../assets/images/products/PRI/Asia Risk Review.webp';
import tool06 from '../../assets/images/products/PRI/Real-time-Risk-Alerts.webp';
import tool07 from '../../assets/images/products/PRI/Risk-Tracker-Portal.webp';
import tool08 from '../../assets/images/products/PRI/Risk and Intelligence Monitoring Engine (RIME).webp';
import tool09 from '../../assets/images/products/PRI/Risk-Tracker-Mobile-App.webp';
import tool10 from '../../assets/images/products/PRI/India-COVID-19-Dashboard.webp';
import tool11 from '../../assets/images/products/PRI/India-COVID-19-Map.webp';
import tool12 from '../../assets/images/products/PRI/Special Reports.webp';
import tool13 from '../../assets/images/products/PRI/Advisories.webp';

import Advisories from '../../components/Advisories';
import { Helmet } from 'react-helmet';
const handleDragStart = (e) => e.preventDefault();

const casestudiesItem = [
  <div className="block-box ">
    <img
      src={casestudy01}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_pri_01" className="block-box__title">
        Enabling rural electrification operations in challenging regions{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      Information support for facilitating rural electrification operations in
      high risk regions.
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy02}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_pri_02" className="block-box__title">
        Comprehensive risk assessment for Oil & Gas operations in North East
        India{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      Geo-political risk assessment of potential oil exploration areas in North
      East India.
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy03}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_pri_03" className="block-box__title">
        Enabling business for a global multi-national Banking & Financial
        organisation{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      Industry specific customised threat intelligence for the South Asian
      region for a global multi-national Banking & Financial organisation.
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy04}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_pri_04" className="block-box__title">
        Predictive Risk Intelligence for a multi-national Internet Technology
        company to continue business across APAC countries{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      Predictive Risk Intelligence for a multi-national Internet Technology and
      Applications development company across APAC region.
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy05}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_pri_05" className="block-box__title">
        Ensuring employee safety during COVID-19 pandemic and enabling
        operational efficiency{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      Ensuring employee safety during COVID-19 pandemic and enabling operational
      efficiency for a multinational technology and consulting company.
    </div>
  </div>,

  ,
];

const toolsItem = [
  <div className="block-box">
    <img
      src={tool01}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="Weekly Forecast"
    />
    <h5>
      <span className="block-box__title">Weekly Forecast </span>
    </h5>
    <div className="block-box__by">
      An aggregation of forthcoming events, the Weekly-Forecast keeps
      organisations aware of the latest developments in various South Asian
      countries.
    </div>
  </div>,
  <div className="block-box">
    <img
      src={tool02}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="Monthly Forecast "
    />
    <h5>
      <span className="block-box__title">Monthly Forecast </span>
    </h5>
    <div className="block-box__by">
      An overarching collection of risks to the South and Southeast Asian
      countries, the Monthly Forecast rounds up events of the previous month and
      presents a forecast of those coming up.
    </div>
  </div>,
  <div className="block-box">
    <img
      src={tool03}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="Daily Risk Tracker "
    />
    <h5>
      <span className="block-box__title">Daily Risk Tracker </span>
    </h5>
    <div className="block-box__by">
      Encapsulating predictive and post-incident risk based analysis, the Daily
      Risk Tracker incorporates events happening on a day-to-day basis covering
      specific geographies.
    </div>
  </div>,
  <div className="block-box">
    <img
      src={tool04}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="India Risk Review"
    />
    <h5>
      <span className="block-box__title">India Risk Review </span>
    </h5>
    <div className="block-box__by">
      Annually published document, the Risk Review of India brings into its
      ambit, an analysis of risks exclusive to India in the present year with an
      overall forecast for the upcoming year.
    </div>
  </div>,
  <div className="block-box">
    <img
      src={tool05}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="Asia Risk Review"
    />
    <h5>
      <span className="block-box__title">Asia Risk Review </span>
    </h5>
    <div className="block-box__by">
      A broader arc of annual evaluation, the Risk Review of Asia covers 50
      Asian countries including India, providing a comprehensive country level
      risk profile of each.
    </div>
  </div>,
  <div className="block-box">
    <img
      src={tool06}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="Real-time Risk Alerts"
    />
    <h5>
      <span className="block-box__title">Real-time Risk Alerts </span>
    </h5>
    <div className="block-box__by">
      Monitoring of risks associated with ongoing events, these alerts provide
      location specific actionable intelligence with an impact assessment and
      quick recommendations through WhatsApp, Email and SMS services.
    </div>
  </div>,
  <div className="block-box">
    <img
      src={tool07}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="Risk Tracker Portal"
    />
    <h5>
      <span className="block-box__title">Risk Tracker Portal </span>
    </h5>
    <div className="block-box__by">
      An AI-assisted platform, the portal functions as the focal point of stored
      data providing multiple insights to risk analysis through alerts/ updates,
      mapping events/live events and graphical representations.
    </div>
  </div>,
  <div className="block-box">
    <img
      src={tool08}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="Risk and Intelligence Monitoring Engine (RIME)"
    />
    <h5>
      <span className="block-box__title">
        Risk and Intelligence Monitoring Engine (RIME){' '}
      </span>
    </h5>
    <div className="block-box__by">
      A highly customised risk focused search engine which mines data and event
      notifications from media, online sources and social media platforms.
    </div>
  </div>,
  <div className="block-box">
    <img
      src={tool09}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="Risk Tracker Mobile App "
    />
    <h5>
      <span className="block-box__title">Risk Tracker Mobile App </span>
    </h5>
    <div className="block-box__by">
      A smartphone application that is convenient to access, the Risk Tracker
      app provides risk associated inputs throughout the day, on your
      fingertips, as per specified geographies.
    </div>
  </div>,
  <div className="block-box">
    <img
      src={tool10}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="India COVID-19 Dashboard"
    />
    <h5>
      <span className="block-box__title">India COVID-19 Dashboard </span>
    </h5>
    <div className="block-box__by">
      An all-in-one weblink to gather comprehensive COVID-19 relevant data, the
      Dashboard provides in-depth trend analysis at the State/Metro city/Union
      territory level.
    </div>
  </div>,
  <div className="block-box">
    <img
      src={tool11}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="India COVID-19 Map"
    />
    <h5>
      <span className="block-box__title">India COVID-19 Map </span>
    </h5>
    <div className="block-box__by">
      An AI-assisted map of India, depicting information regarding containment
      zones, hospitals vis-à-vis office and employee locations, to facilitate
      seamless resumption of operations in low risk regions.
    </div>
  </div>,
  <div className="block-box">
    <img
      src={tool12}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="Special Reports"
    />
    <h5>
      <span className="block-box__title">Special Reports </span>
    </h5>
    <div className="block-box__by">
      Detailed reports aimed at analysing and comprehending the long term
      aspects of risks impacting businesses in multiple sectors.
    </div>
  </div>,
  <div className="block-box">
    <img
      src={tool13}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="Advisories"
    />
    <h5>
      <span className="block-box__title">Advisories </span>
    </h5>
    <div className="block-box__by">
      Time bound in nature, advisories cover covers specific issues and
      communicates actionable inputs in a visually appealing manner.
    </div>
  </div>,
];

const Pri = () => {
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Predictive Risk Intelligence | MitKat Advisory</title>
        <meta
          id="description"
          name="description"
          content="MitKat is the trusted risk intelligence partner to world’s most respected organisations. MitKat’s AI-enabled ‘Risk Tracker’ platform enables us to monitor"
        />
        <meta
          id="keywords"
          name="keywords"
          content="risk management intelligence, risk management ai, ai risk assessment, world check risk intelligence, predictive risk intelligence, business risk intelligence, artificial intelligence risk management, third party risk intelligence, intelligence risk asssessment, ai risk management, predictive intelligence services, business intelligence predictions, predictive risk analysis, predictive risk management"
        />
      </Helmet>
      <Navbar />
      <div className="service-header">
        <div className="row justify-content-center m-0">
          <div className="col-12 col-md-12 col-lg-12 p-0">
            <img src={priBannerImg} width="100%" />
          </div>
          <div className="col-12 col-md-9 col-lg-9">
            <div className="service-description">
              <div className="display-block m-a">
                <div className="content">
                  <h1 className="mb-3 service-description__header text-capitalize text-center">
                    Predictive Risk Intelligence
                  </h1>
                  <p className="content__subheader py-3 m-0">
                    MitKat is the trusted risk intelligence partner to world’s
                    most respected organisations. MitKat’s AI-enabled ‘Risk
                    Tracker’ platform enables us to monitor and analyse an array
                    of geo-political, socio-economic, environmental, health,
                    safety, technology and regulatory risks impacting your
                    business, and our customised alerts and advisories help you
                    stay ahead of the events. As business owners, intel,
                    security and resiliency leaders, our technology - based risk
                    monitoring and expert human analysis allows you to
                    understand global, regional and local incidents and trends,
                    and factor in appropriate de-risking and mitigation measures
                    into your decision-support system. In addition, as a MitKat
                    customer your dedicated analyst will respond urgently and
                    effectively to your queries.
                    <br />
                    To know more about PRI subscription services or of our Intel
                    Platform (GRIT) - Global Risk Intelligence Tracker
                    <br /> <br />
                    <NavLink
                      exact
                      to="/book_demo
"
                      rel="noopener noreferrer"
                      className="main-header__btn"
                    >
                      Book a Demo &nbsp; &nbsp;
                      <FontAwesomeIcon
                        icon={faArrowRight}
                        className="main-header__link__arrow"
                      />
                    </NavLink>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section className="service bg-gray py-5" id="pri_services">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <p className="mb-5 content__header text-center">
                  Service offering
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={riskMonitoringPlatform}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Risk Monitoring Platform (MitKat Risk Tracker) and Mobile
                  Application
                </h5>
                <ShowMoreText
                  lines={3}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={240}
                  keepNewLines={true}
                >
                  MitKat’s Risk Monitoring Platform provides you exclusive
                  information about current and relevant events that could
                  impact business operations, employee movement or corporate
                  security/safety. Customised event notifications through the
                  mobile application version provide live monitoring of relevant
                  events for seamless operations.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={customisedRiskManagementSearchEngineandDashboards}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Customised Risk Management Search Engine and Dashboards
                </h5>
                <ShowMoreText
                  lines={3}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={241}
                  keepNewLines={true}
                >
                  Risk and Intelligence Monitoring Engine (RIME) is an
                  AI-powered search engine for round the clock threat
                  intelligence monitoring, with the capability of customisation
                  for specific interests and locations. Bespoke situation
                  specific dashboards can provide comprehensive coverage of
                  complex scenarios with a user-friendly interface.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={protectiveIntelligence}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Protective Intelligence
                </h5>
                <ShowMoreText
                  lines={4}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={270}
                  keepNewLines={true}
                >
                  MitKat’s Protective Intelligence Services provide specific and
                  actionable intelligence to ensure safety of organisations,
                  employees, brands and assets.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={alertsandAdvisoryServices}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Alerts and Advisory Services
                </h5>
                <ShowMoreText
                  lines={4}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={280}
                  keepNewLines={true}
                >
                  MitKat’s Alerts and Advisory Services assist business heads,
                  corporate security professionals and business resilience
                  managers to plan for operational and security risks, work out
                  mitigation measures and implement security/safety plans for
                  corporates.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={commandCentreOperations}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Command Centre Operations
                </h5>
                <ShowMoreText
                  lines={5}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={287}
                  keepNewLines={true}
                >
                  MitKat’s team of security analysts and professionals can help
                  you set up, operate and manage your Command Centre Operations.
                  This could be in location at your site or remotely.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={travelRiskManagement}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Travel Risk Management
                </h5>
                <ShowMoreText
                  lines={5}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={275}
                  keepNewLines={true}
                >
                  Business travellers are often exposed to multiple safety and
                  security risks, especially in India and South Asia. MitKat’s
                  Travel Risk Management Services can assist expatriates, local
                  employees, and especially women employees to travel safely.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={locationRiskAssessments}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Country/ State/ City/ Location Risk Assessments
                </h5>
                <ShowMoreText
                  lines={4}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={295}
                  keepNewLines={true}
                >
                  Operations in new or high risk environments necessitate high
                  quality risk assessments to enable organisations to plan and
                  conduct secure business operations. MitKat’s Risk Assessments
                  provide organisations with country/state/city/location
                  specific risk reports aligned to their business objectives.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={bespokeResearchandAnalysis}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Bespoke Research and Analysis
                </h5>
                <ShowMoreText
                  lines={4}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={230}
                  keepNewLines={true}
                >
                  MitKat’s Bespoke Research and Analysis Services provide
                  client-specific reports with comprehensive and strategic risk
                  assessment covering all variables that may impact operations
                  in complex environments. The analysis is customised for better
                  risk mitigation and to enable decision-making.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={helpline}
                  width="100%"
                />
                <h5 className="block-box__title px-3">24x7 Helpline</h5>
                <ShowMoreText
                  lines={4}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={273}
                  keepNewLines={true}
                >
                  ‘Follow the Sun’ operational environment for multi-national
                  and trans-national companies implies that they need to have a
                  central point of contact, where their employees and business
                  heads can fall back upon, for information and assistance.
                  MitKat’s 24x7 helpline provides support through multi-pronged
                  accessibility.
                </ShowMoreText>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-center">
                  Our Tools and Reports
                </h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <Products item={toolsItem} />
            </div>
          </div>
        </div>
      </section>

      <section className="py-5 bg-darker">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-center text-white">
                  Advisories
                </h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <Advisories />
            </div>
          </div>
        </div>
      </section>

      <section className="py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-center">Case Studies</h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <Casestudies item={casestudiesItem} />
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Pri;
