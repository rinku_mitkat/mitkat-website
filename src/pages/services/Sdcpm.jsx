import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import { NavLink } from 'react-router-dom';
import ShowMoreText from 'react-show-more-text';
import ServiceHeader from '../../components/ServiceHeader';
import sdcpmBannerImg from '../../assets/images/service/Security Design Consulting & Project Management.webp';

import Casestudies from '../../components/Casestudies';
import sedgbp from '../../assets/images/service/SDCPM/Security Engineering & Design - greenfield & brownfield projects.webp';
import cip from '../../assets/images/service/SDCPM/Critical Infrastructure Protection.webp';
import soc from '../../assets/images/service/SDCPM/Envision & Design of Security Operation Center (SOC).webp';
import pm from '../../assets/images/service/SDCPM/Project Management.webp';
import srco from '../../assets/images/service/SDCPM/Security re-engineering & Cost Optimization.webp';
import bia from '../../assets/images/service/SDCPM/Blast Impact Analysis (BIA).webp';
import oscs from '../../assets/images/service/SDCPM/Other Security Consulting services.webp';
import smartcity from '../../assets/images/service/SDCPM/Safe city consulting services.webp';

import casestudy01 from '../../assets/images/casestudies/SDCPM/Design of National Security Command Room (NSCR) for Indian Multi-National conglomerate.webp';
import casestudy02 from '../../assets/images/casestudies/SDCPM/Security Technology Transformation of Indias Largest IT & Consulting Company.webp';
import casestudy03 from '../../assets/images/casestudies/SDCPM/Security Design Consulting Services of its Globally Second Largest Campus for a British Baker.webp';
import casestudy04 from '../../assets/images/casestudies/SDCPM/Security Design Consulting Services of its Globally Second Largest Campus for a British Baker Thumbnail.webp';
import casestudy05 from '../../assets/images/casestudies/SDCPM/Security Design Consulting Services for Greenfield Corporate Park Project Thumbnail.webp';
import { Helmet } from 'react-helmet';

const handleDragStart = (e) => e.preventDefault();

const casestudiesItem = [
  <div className="block-box ">
    <img
      src={casestudy01}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt="Design of National Security Control Room (NSCR) for Indian Multi-National conglomerate"
    />
    <h5>
      <NavLink exact to="/casestudy_sdcpm_01" className="block-box__title">
        Envision and Design of National Security Control Room (NSCR) for a
        Leading Indian Conglomerate{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      MitKat worked with a leading Indian multi-national conglomerate to envison
      and design an NSCR, with a view to enhancing real time situational
      awareness and operational visibility across the enterprise.
      <br />
      The complex project, which involved sourcing data from millions of sensors
      and humans, delivered over multiple media, analysed using complex
      algorithms, providing real-time, actionable intel, high situational
      awareness, and better decision-making capability, was delivered on
      schedule, as per mutually agreed quality parameters.
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy02}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_sdcpm_02" className="block-box__title">
        Security Technology Transformation for a Leading Indian IT MNC{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      MitKat worked with the client (presence across 44 countries and over 55
      locations in India) to re-engineer security architecture, optimise
      surveillance and access management, leading to robust security posture,
      cost optimisation (nearly 20% savings) and significantly improved user
      experience.
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy03}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_sdcpm_03" className="block-box__title">
        Security Design Consultancy for a Leading Global Bank{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      MitKat’s Security Consulting, Design and Project Management team worked
      collaboratively with our client, a London headquartered, leading
      multi-national bank, in setting up its second largest global campus, in
      India.
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy04}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_sdcpm_04" className="block-box__title">
        Security Design Consulting & Project management Services for a
        Multi-National Bank{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      MitKat's Client is a German multinational investment bank and financial
      services company.
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy05}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_sdcpm_05" className="block-box__title">
        {' '}
        Security Design Consulting Services for Greenfield Corporate Park
        Project{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      MitKat's Client in Bangalore, India is a developer of prestigious business
      parks. The Group is the name behind premier Grade A development of
      next-generation workplaces.
    </div>
  </div>,
];

const Sdcpm = () => {
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>
          Security Design Consulting & Project Management | MitKat Advisory
        </title>
        <meta
          id="description"
          name="description"
          content="Organisations are continuously exposed to a host of evolving threats, which create a multitude of security risks. Security Design consulting is a principle "
        />
        <meta
          id="keywords"
          name="keywords"
          content="what is critical infrastructure protection for, infrastructure protection specialist, infrastructure protection plan, security operations center framework, command control center, security engineering, security architecture, soc engineer, infrastructure security, which element of a command and control center focus, how can a security framework assist in the design, secure design, project management services, project management professional, project management tools, blast impact analysis, blast impact assessment, how to strengthen security posture, blast impact testing, pmo, agile project management, blast vulnerability assessment service, smart city consulting, safe city consultants"
        />
      </Helmet>
      <Navbar />
      <ServiceHeader
        imgsrc={sdcpmBannerImg}
        linkTitle="Book a Demo"
        title="Security Design Consulting & Project Management"
        description="Organisations are continuously exposed to a host of evolving threats, which create a multitude of security risks. Security Design consulting is a principle and process driven approach to how enterprises can manage such risks. 
            Once the vulnerabilities are identified, we work with Organisations to help in identifying what can be done, what options are available and what are the associated trade-offs in terms of costs, benefits and risks. We then design the solution, by finding the right balance between security and convenience, which respects the unique culture, mission & values of each Organisation.
            "
      />

      <section className="service bg-gray py-5" id="pri_services">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <p className="mb-5 content__header text-center">
                  Service offering
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={sedgbp}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Security Engineering & Design – greenfield & brownfield
                  projects
                </h5>
                <ShowMoreText
                  lines={4}
                  more="Show more"
                  less="Show less"
                  className="content-css"
                  anchorClassNAmeclassName="my-anchor-css-class"
                  expanded={false}
                  width={307}
                  className="block-box__description p-3"
                >
                  When designing a new building or planning the renovation of an
                  existing one, it is crucial that security be considered as a
                  part of the design and planning process. Having a qualified
                  security consultant as a part of the design team can allow
                  organisations to construct a facility that provides maximum
                  protection to its people, information and other assets, while
                  at the same time, greatly reducing security operating costs.
                  <br />
                  At MitKat with our in-house professional team and experience,
                  we assist owners, architects, engineers, and property managers
                  with the planning of security for new construction and
                  renovation projects.
                </ShowMoreText>
              </div>
            </div>

            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={cip}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Critical Infrastructure Protection
                </h5>
                <ShowMoreText
                  lines={4}
                  more="Show more"
                  less="Show less"
                  className="content-css"
                  anchorClassNAmeclassName="my-anchor-css-class"
                  expanded={false}
                  width={270}
                  className="block-box__description p-3"
                >
                  The areas to be protected and extent of controls to be
                  designed and deployed need in-depth study and understanding of
                  the perceived threat, the vulnerability of the facility, and
                  the cost of the controls necessary to reduce that
                  vulnerability.
                  <br />
                  At MitKat, we carry out an in-depth analysis to, understand
                  the purpose and goals of security system design or upgrade
                  programs, in order, to meet appropriate user requirements.
                </ShowMoreText>
              </div>
            </div>

            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={soc}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Envisioning & Designing a Security Operation Centre (SOC)
                </h5>
                <ShowMoreText
                  lines={3}
                  more="Show more"
                  less="Show less"
                  className="content-css"
                  anchorClassNAmeclassName="my-anchor-css-class"
                  expanded={false}
                  width={265}
                  className="block-box__description p-3"
                >
                  A Security Operation Centre (SOC) or Command and Control
                  Centre (CCC) is a centralised facility created for the
                  security team for monitoring and analysing an Organisation’s
                  security posture on a 24/7 basis. A SOC will detect, analyse,
                  and respond or prevent incidents using a combination of
                  technology solutions and a strong set of processes.
                  <br />
                  The development of SOC involves a logical sequence of
                  activities, careful vetting and training of personnel,
                  resulting in a state-of-the-art, robust, resilient and
                  pro-active core unit. A well-functioning SOC reduces
                  vulnerabilities and incidences, improves security and
                  emergency response, assures stakeholders, curtails losses and
                  boosts productivity.
                  <br />
                  MitKat has vast experience of envisioning, designing and
                  operating state-of-the-art SOCs for leading global
                  corporations and strategic entities.
                </ShowMoreText>
              </div>
            </div>

            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={pm}
                  width="100%"
                />
                <h5 className="block-box__title px-3">Project Management</h5>
                <ShowMoreText
                  lines={5}
                  more="Show more"
                  less="Show less"
                  className="content-css"
                  anchorClassNAmeclassName="my-anchor-css-class"
                  expanded={false}
                  width={260}
                  className="block-box__description p-3"
                >
                  The development of a security system cannot be an isolated
                  project, in most cases, it is a part of a larger effort, and
                  impacts many other activities. It is essential in the project,
                  to have a good project management team for coordination with
                  related programs, conflict avoidance, and incorporation of
                  opportunities for collaboration with other projects or actions
                  to meet agreed timelines.
                  <br />
                  MitKat has experience of managing complex, multi-location and
                  multi-stakeholder projects. We understand and appreciate the
                  varying requirements and priorities of various stakeholders,
                  and help align them to project objectives and organisational
                  goals.
                </ShowMoreText>
              </div>
            </div>

            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={srco}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Security Re-Engineering & Cost Optimization
                </h5>
                <ShowMoreText
                  lines={4}
                  more="Show more"
                  less="Show less"
                  className="content-css"
                  anchorClassNAmeclassName="my-anchor-css-class"
                  expanded={false}
                  width={245}
                  className="block-box__description p-3"
                >
                  Organisations are looking to move from guns, gates and guards
                  to tech-enabled, process-oriented, risk-based, cost-conscious
                  security models.
                  <br />
                  MitKat works with organisations to reduce human-dependence,
                  re-engineer an optimum balance among people, process and
                  technology controls, thereby leading to more
                  business-relevant, user-friendly, efficient, consistent,
                  robust and cost-effective security architecture.
                </ShowMoreText>
              </div>
            </div>

            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={bia}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Blast Impact Analysis (BIA)
                </h5>
                <ShowMoreText
                  lines={5}
                  more="Show more"
                  less="Show less"
                  className="content-css"
                  anchorClassNAmeclassName="my-anchor-css-class"
                  expanded={false}
                  width={280}
                  className="block-box__description p-3"
                >
                  All critical facilities and establishments suffer from a
                  fundamental vulnerability to terrorist attacks during
                  heightened levels of security warnings.
                  <br /> MitKat helps it’s clients to strengthen their security
                  posture by undertaking Blast Impact Assessment (BIA) while
                  undertaking construction of a new facility or Blast
                  Vulnerability Assessment (BVA) to suggest retrofitting
                  measures for an existing facility of critical importance.
                </ShowMoreText>
              </div>
            </div>

            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={oscs}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Other Security Consulting Services
                </h5>
                <ShowMoreText
                  lines={3}
                  more="Show more"
                  less="Show less"
                  className="content-css"
                  anchorClassNAmeclassName="my-anchor-css-class"
                  expanded={false}
                  width={280}
                  className="block-box__description p-3"
                >
                  Creation and operation of security operation procedures
                  <br />
                  <br />
                  Providing advice concerning a specific security problem or
                  situation
                  <br />
                  <br />
                  Providing a second opinion on security-related issues
                  <br />
                  <br />
                  Conducting cost/benefit analysis of proposed security
                  solutions
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={smartcity}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Smart city, Safe city consulting services
                </h5>
                <ShowMoreText
                  lines={3}
                  more="Show more"
                  less="Show less"
                  className="content-css"
                  anchorClassNAmeclassName="my-anchor-css-class"
                  expanded={false}
                  width={310}
                  className="block-box__description p-3"
                >
                  <strong> Planning, assessment and DPR preparation: </strong>
                  <br />
                  Survey and data collection, preparation of Concept of
                  operations (CONOPs), Organisation design to support entire
                  program with competencies, roles and responsibilities. Drawing
                  operation policies, procedures and processes.
                  <br /> <br />
                  <strong> Designing intelligent technologies: </strong>
                  <br /> Current state assessment (Gap analysis), Analyse needs
                  and opportunities, identify right set of system, data and
                  technology required (future state, To-be), System design and
                  integration planning.
                  <br />
                  <br />
                  RFP, tendering, vendor onboarding and project management
                  support
                </ShowMoreText>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-center">Case Studies</h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <Casestudies item={casestudiesItem} />
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Sdcpm;
