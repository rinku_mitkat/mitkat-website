import React, { useState } from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import { NavLink } from 'react-router-dom';
import ServiceHeader from '../../components/ServiceHeader';
import twdBannerImg from '../../assets/images/service/Training & Workforce Development.webp';
import womenSafety from '../../assets/images/service/TWD/Womens Safety Training.webp';
import cyberSecurity from '../../assets/images/service/TWD/Cyber Security Training.webp';
import crisisSimulation from '../../assets/images/service/TWD/Crisis Simulation & Emergency Response Training.webp';
import commandCentre from '../../assets/images/service/TWD/Command Centre Operations Training.webp';
import riskMonitoring from '../../assets/images/service/TWD/Risk Monitoring & Report Writing.webp';
import fireLifeSafety from '../../assets/images/service/TWD/Fire, Life Safety & First-Aid Training.webp';
import returntoOffice from '../../assets/images/service/TWD/Return to Office Guidelines.webp';
import corporateSecurity from '../../assets/images/service/TWD/Corporate Security Training.webp';
import corporateIntel from '../../assets/images/service/TWD/Corporate Intel Training.webp';
import posh from '../../assets/images/service/TWD/POSH & Gender Sensitization.webp';
import workplaceErgonomics from '../../assets/images/service/TWD/Workplace Ergonomics.webp';
import roadSafety from '../../assets/images/service/TWD/Road Safety Safe Driving.webp';
import workplaceViolence from '../../assets/images/service/TWD/Workplace Violence.webp';
import onlineMasterclass from '../../assets/images/service/TWD/Online Masterclass.webp';
import safetyAndSecurityWeek from '../../assets/images/service/TWD/Safety & Security Week Training.webp';
import businessContinuity from '../../assets/images/service/TWD/Business Continuity Management Training.webp';
import suggestedTool from '../../assets/images/service/TWD/STT/Suggested tools and technique blue background.webp';

import responsiveDesign from '../../assets/images/service/TWD/USP/Responsive-Design.webp';
import safetySecurityWeek from '../../assets/images/service/TWD/USP/Safety-Security-Weeks.webp';
import assessmentTools from '../../assets/images/service/TWD/USP/Assessment-Tools-to-measure-participation.webp';
import sme from '../../assets/images/service/TWD/USP/SME--Security.webp';
import inhouseTeam from '../../assets/images/service/TWD/USP/In-House-Development-&-Design-Team.webp';
import onlineClasses from '../../assets/images/service/TWD/USP/Online-Masterclasses.webp';
import highlycustomizable from '../../assets/images/service/TWD/USP/Highly-customizable White.webp';
import militaryVeterans from '../../assets/images/service/TWD/USP/Military-veterans-with-expertise.webp';

import processBenchamrkD from '../../assets/images/service/TWD/Process/Benchmark 1.webp';
import processBenchamrkM from '../../assets/images/service/TWD/Process/Benchmark 2.webp';
import processDesignD from '../../assets/images/service/TWD/Process/Design 1.webp';
import processDesignM from '../../assets/images/service/TWD/Process/Design 2.webp';
import processTrainingD from '../../assets/images/service/TWD/Process/Training 1.webp';
import processTrainingM from '../../assets/images/service/TWD/Process/Training 2.webp';
import processImplimentaionD from '../../assets/images/service/TWD/Process/Implementation 1.webp';
import processImplimentaionM from '../../assets/images/service/TWD/Process/Implementation 2.webp';

import Casestudies from '../../components/Casestudies';

import casestudy01 from '../../assets/images/casestudies/TWD/Addressing Womens Safety Concerns.webp';
import casestudy02 from '../../assets/images/casestudies/TWD/Bringing About Behavioural Change.webp';
import casestudy03 from '../../assets/images/casestudies/TWD/Addressing Unconscious Bias Through e-Learning Modules.webp';
import casestudy04 from '../../assets/images/casestudies/TWD/Online Masterclass Series.webp';
import casestudy05 from '../../assets/images/casestudies/TWD/Inculcating a Security Safety Culture Within an Organisation.webp';

import Products from '../../components/Products';

import collateral01 from '../../assets/images/service/TWD/OC/Infographics.webp';
import collateral02 from '../../assets/images/service/TWD/OC/Standees.webp';
import collateral03 from '../../assets/images/service/TWD/OC/Handbooks.webp';
import collateral04 from '../../assets/images/service/TWD/OC/Animated-Videos.webp';
import collateral05 from '../../assets/images/service/TWD/OC/Gamification.webp';
import collateral06 from '../../assets/images/service/TWD/OC/e-Learning-Training-Modules.webp';
import collateral07 from '../../assets/images/service/TWD/OC/Safety-Quiz-&-Street-Plays.webp';
import { Helmet } from 'react-helmet';

const handleDragStart = (e) => e.preventDefault();

const collateralsItem = [
  <div className="block-box">
    <img
      src={collateral01}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="Weekly Forecast"
    />
    <h5>
      <span className="block-box__title">Infographics </span>
    </h5>
    <div className="block-box__by"></div>
  </div>,
  <div className="block-box">
    <img
      src={collateral02}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="Weekly Forecast"
    />
    <h5>
      <span className="block-box__title"> Standees </span>
    </h5>
    <div className="block-box__by"></div>
  </div>,
  <div className="block-box">
    <img
      src={collateral03}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="Weekly Forecast"
    />
    <h5>
      <span className="block-box__title">Animated Videos </span>
    </h5>
    <div className="block-box__by"></div>
  </div>,

  <div className="block-box">
    <img
      src={collateral04}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="Weekly Forecast"
    />
    <h5>
      <span className="block-box__title"> </span>
    </h5>
    <div className="block-box__by"></div>
  </div>,
  <div className="block-box">
    <img
      src={collateral05}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="Weekly Forecast"
    />
    <h5>
      <span className="block-box__title">Gamification </span>
    </h5>
    <div className="block-box__by"></div>
  </div>,
  <div className="block-box">
    <img
      src={collateral07}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="Weekly Forecast"
    />
    <h5>
      <span className="block-box__title">e-Learning Training Modules </span>
    </h5>
    <div className="block-box__by"></div>
  </div>,
  <div className="block-box">
    <img
      src={collateral06}
      onDragStart={handleDragStart}
      className="block-box__img product__img"
      alt="Weekly Forecast"
    />
    <h5>
      <span className="block-box__title">Safety Quiz & Street Plays </span>
    </h5>
    <div className="block-box__by"></div>
  </div>,
];
const casestudiesItem = [
  <div className="block-box ">
    <img
      src={casestudy01}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_twd_01" className="block-box__title">
        {' '}
        Addressing Women’s Safety Concerns{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      Bringing about women empowerment for an Indian multinational bank and
      financial services company with over 3000 branches pan-India
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy02}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_twd_02" className="block-box__title">
        {' '}
        Bringing About Behavioural Change{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      Training government security personnel for emergency response &
      communication skills
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy03}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_twd_03" className="block-box__title">
        Addressing Unconscious Bias Through e-Learning Modules{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      Understanding that unconscious bias affects how we make decisions, engage
      with others, and respond to various situations and circumstances, often
      limiting potential.
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy04}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_twd_04" className="block-box__title">
        {' '}
        Online Masterclass Series{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      Enabling security professionals to navigate the Security, Intel, Business
      Continuity Management eco-system
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy05}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_twd_05" className="block-box__title">
        Inculcating a Security/Safety Culture Within an Organisation{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      Inculcating & Ensuring a Security/Safety First attitude amongst employees
    </div>
  </div>,

  ,
];

const Twd = () => {
  const [btnclick, setbtnclick] = useState(false);

  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Training & Workforce Development | MitKat Advisory</title>
        <meta
          id="description"
          name="description"
          content="Companies often invest substantial amount of time and money in training and skilling their workforce. These activities are usually business focussed."
        />
        <meta
          id="keywords"
          name="keywords"
          content="physical security training topics, physical security certifications, cyber security training, infosec training, women's safety training course, emergency response training, command center operations training, risk monitoring training, how to write reports, first aid training, corporate security training, corporate intel training, how to use corporate intel for business decisions, POSH awareness workshops, defence driving techniques, BCM online class"
        />
      </Helmet>
      <Navbar />
      <ServiceHeader
        imgsrc={twdBannerImg}
        linkTitle="Book a Demo"
        title="Training & Workforce Development"
        description="Companies often invest substantial amount of time and money in training and skilling their workforce. These activities are usually business focussed. However, the complex external environment often exposes organisations to unpredictable disruption and employees to safety/security risks. MitKat’s training intends to upskill intel, security and BCM professionals and strengthen women empowerment. As one of the industry leaders, MitKat’s basic and advanced training covers all facets of Intel, Physical Security & Safety, Cyber Security, Fraud Prevention, Women Security and Empowerment, Defensive Driving,.. etc. We have empowered over 200,000 women across corporate India with our customised Women’s Safety Training."
      />

      <section className="service bg-gray py-5" id="pri_services">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <p className="mb-5 content__header text-center">
                  Service offering
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <div className="block-box">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-12">
                    <img className="block-img" src={womenSafety} width="100%" />
                  </div>
                  <div className="col-lg-6 col-md-6 col-12">
                    <h5 className="px-3">Women’s Safety Training</h5>
                    <nav className="mt-3 px-3">
                      <div
                        className="nav nav-tabs nav-fill text-left"
                        id="nav-tab"
                        role="tablist"
                      >
                        <a
                          className="nav-item nav-link text-left active"
                          id="nav-wst-overview-tab"
                          data-toggle="tab"
                          href="#nav-wst-overview"
                          role="tab"
                          aria-controls="nav-wst-overview"
                          aria-selected="true"
                        >
                          Overview
                        </a>
                        <a
                          className="nav-item nav-link text-left"
                          id="nav-wst-video-tab"
                          data-toggle="tab"
                          href="#nav-wst-video"
                          role="tab"
                          aria-controls="nav-wst-video"
                          aria-selected="false"
                        >
                          Watch a Video
                        </a>
                        <a
                          className="nav-item nav-link text-left"
                          id="nav-wst-outcome-tab"
                          data-toggle="tab"
                          href="#nav-wst-outcome"
                          role="tab"
                          aria-controls="nav-wst-outcome"
                          aria-selected="false"
                        >
                          Outcome
                        </a>
                      </div>
                    </nav>
                    <div className="tab-content p-3" id="nav-tabContent">
                      <div
                        className="tab-pane block-box__by fade show active"
                        id="nav-wst-overview"
                        role="tabpanel"
                        aria-labelledby="nav-wst-overview-tab"
                      >
                        Women’s Safety Training is a curated and customised
                        training programme that empowers women with safety
                        awareness tips and self-defence techniques that makes
                        them more assertive and helps reduce violence against
                        women. This flagship training has empowered more than
                        1.5 lakh women across corporate India.
                      </div>
                      <div
                        className="tab-pane block-box__by fade"
                        id="nav-wst-video"
                        role="tabpanel"
                        aria-labelledby="nav-wst-video-tab"
                      >
                        <div className="embed-responsive embed-responsive-16by9 px-5">
                          <iframe
                            width="560"
                            height="315"
                            src="https://www.youtube.com/embed/FpPB8Luo0Ao"
                            frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen
                          ></iframe>
                        </div>
                      </div>
                      <div
                        className="tab-pane block-box__by fade"
                        id="nav-wst-outcome"
                        role="tabpanel"
                        aria-labelledby="nav-wst-outcome-tab"
                      >
                        <ul>
                          <li>
                            {' '}
                            Makes women more aware of how to keep away from
                            potentially dangerous situations{' '}
                          </li>
                          <li> Makes them more assertive </li>
                          <li> Helps to reduce violence against women </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
              <div className="block-box">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-12">
                    <h5 className="px-3">Cyber Security Training</h5>
                    <nav className="mt-3 px-3">
                      <div
                        className="nav nav-tabs nav-fill text-left"
                        id="nav-tab"
                        role="tablist"
                      >
                        <a
                          className="nav-item nav-link text-left active"
                          id="nav-cst-overview-tab"
                          data-toggle="tab"
                          href="#nav-cst-overview"
                          role="tab"
                          aria-controls="nav-cst-overview"
                          aria-selected="true"
                        >
                          Overview
                        </a>
                        <a
                          className="nav-item nav-link text-left"
                          id="nav-cst-video-tab"
                          data-toggle="tab"
                          href="#nav-cst-video"
                          role="tab"
                          aria-controls="nav-cst-video"
                          aria-selected="false"
                        >
                          Watch a Video
                        </a>
                        <a
                          className="nav-item nav-link text-left"
                          id="nav-cst-outcome-tab"
                          data-toggle="tab"
                          href="#nav-cst-outcome"
                          role="tab"
                          aria-controls="nav-cst-outcome"
                          aria-selected="false"
                        >
                          Outcome
                        </a>
                      </div>
                    </nav>
                    <div className="tab-content p-3" id="nav-tabContent">
                      <div
                        className="tab-pane block-box__by fade show active"
                        id="nav-cst-overview"
                        role="tabpanel"
                        aria-labelledby="nav-cst-overview-tab"
                      >
                        The Cyber Security Training is conducted in
                        collaboration with CII-CDT (Confederation of Indian
                        Industry - Centre for Digital Transformation) and is a
                        certification programme which covers cyber resilience,
                        cyber attack vectors, cyber technologies, cyber policy &
                        frameworks and cyber laws along with BCP & Crisis
                        Management by industry experts.
                      </div>
                      <div
                        className="tab-pane block-box__by fade"
                        id="nav-cst-video"
                        role="tabpanel"
                        aria-labelledby="nav-cst-video-tab"
                      >
                        <div className="embed-responsive embed-responsive-16by9 px-5">
                          <iframe
                            width="560"
                            height="315"
                            src="https://www.youtube.com/embed/TxEgDVIuEzA"
                            frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen
                          ></iframe>
                        </div>
                      </div>
                      <div
                        className="tab-pane block-box__by fade"
                        id="nav-cst-outcome"
                        role="tabpanel"
                        aria-labelledby="nav-cst-outcome-tab"
                      >
                        <ul>
                          <li> Bring about cyber security awareness </li>
                          <li> Help prevent breaches & attacks </li>
                          <li>
                            {' '}
                            Influence company culture and inculcate a
                            safety/security attitude{' '}
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6 col-12">
                    <img
                      className="block-img"
                      src={cyberSecurity}
                      width="100%"
                    />
                  </div>
                </div>
              </div>
            </div>

            <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
              <div className="block-box">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-12">
                    <img
                      className="block-img"
                      src={crisisSimulation}
                      width="100%"
                    />
                  </div>
                  <div className="col-lg-6 col-md-6 col-12">
                    <h5 className="px-3">
                      Crisis Simulation & Emergency Response Training
                    </h5>
                    <nav className="mt-3 px-3">
                      <div
                        className="nav nav-tabs nav-fill text-left"
                        id="nav-tab"
                        role="tablist"
                      >
                        <a
                          className="nav-item nav-link text-left active"
                          id="nav-csert-overview-tab"
                          data-toggle="tab"
                          href="#nav-csert-overview"
                          role="tab"
                          aria-controls="nav-csert-overview"
                          aria-selected="true"
                        >
                          Overview
                        </a>
                        <a
                          className="nav-item nav-link text-left"
                          id="nav-csert-video-tab"
                          data-toggle="tab"
                          href="#nav-csert-video"
                          role="tab"
                          aria-controls="nav-csert-video"
                          aria-selected="false"
                        >
                          Watch a Video
                        </a>
                        <a
                          className="nav-item nav-link text-left"
                          id="nav-csert-outcome-tab"
                          data-toggle="tab"
                          href="#nav-csert-outcome"
                          role="tab"
                          aria-controls="nav-csert-outcome"
                          aria-selected="false"
                        >
                          Outcome
                        </a>
                      </div>
                    </nav>
                    <div className="tab-content p-3" id="nav-tabContent">
                      <div
                        className="tab-pane block-box__by fade show active"
                        id="nav-csert-overview"
                        role="tabpanel"
                        aria-labelledby="nav-csert-overview-tab"
                      >
                        Crisis Simulation Exercises provide an interactive forum
                        to key stakeholders & Emergency Response Teams, to make
                        them aware about their roles & responsibilities during a
                        crisis by simulating a close to real-life crisis
                        scenario to elicit a collective and coordinated
                        response.
                      </div>
                      <div
                        className="tab-pane block-box__by fade"
                        id="nav-csert-video"
                        role="tabpanel"
                        aria-labelledby="nav-csert-video-tab"
                      >
                        <div className="embed-responsive embed-responsive-16by9 px-5">
                          <iframe
                            width="560"
                            height="315"
                            src="https://www.youtube.com/embed/GksNLcjjQbk"
                            title="YouTube video player"
                            frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen
                          ></iframe>
                        </div>
                      </div>
                      <div
                        className="tab-pane block-box__by fade"
                        id="nav-csert-outcome"
                        role="tabpanel"
                        aria-labelledby="nav-csert-outcome-tab"
                      >
                        <ul>
                          <li>
                            {' '}
                            Helps teams prepare for a crisis by giving them a
                            thorough understanding of roles and responsibilities{' '}
                          </li>
                          <li>
                            {' '}
                            Results in a coordinated response in a crisis{' '}
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
              <div className="block-box">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-12">
                    <h5 className="px-3">Command Centre Operations Training</h5>
                    <nav className="mt-3 px-3">
                      <div
                        className="nav nav-tabs nav-fill text-left"
                        id="nav-tab"
                        role="tablist"
                      >
                        <a
                          className="nav-item nav-link text-left active"
                          id="nav-ccot-overview-tab"
                          data-toggle="tab"
                          href="#nav-ccot-overview"
                          role="tab"
                          aria-controls="nav-ccot-overview"
                          aria-selected="true"
                        >
                          Overview
                        </a>
                        <a
                          className="nav-item nav-link text-left"
                          id="nav-ccot-video-tab"
                          data-toggle="tab"
                          href="#nav-ccot-video"
                          role="tab"
                          aria-controls="nav-ccot-video"
                          aria-selected="false"
                        >
                          Watch a Video
                        </a>
                        <a
                          className="nav-item nav-link text-left"
                          id="nav-ccot-outcome-tab"
                          data-toggle="tab"
                          href="#nav-ccot-outcome"
                          role="tab"
                          aria-controls="nav-ccot-outcome"
                          aria-selected="false"
                        >
                          Outcome
                        </a>
                      </div>
                    </nav>
                    <div className="tab-content p-3" id="nav-tabContent">
                      <div
                        className="tab-pane block-box__by fade show active"
                        id="nav-ccot-overview"
                        role="tabpanel"
                        aria-labelledby="nav-ccot-overview-tab"
                      >
                        Command Centre Operations Training trains teams on
                        security operations, external environment monitoring,
                        incident management, crisis communications and report
                        generation.
                      </div>
                      <div
                        className="tab-pane block-box__by fade"
                        id="nav-ccot-video"
                        role="tabpanel"
                        aria-labelledby="nav-ccot-video-tab"
                      >
                        <div className="embed-responsive embed-responsive-16by9 px-5">
                          <iframe
                            width="560"
                            height="315"
                            src="https://www.youtube.com/embed/Td5CIO0Hu_M"
                            title="YouTube video player"
                            frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen
                          ></iframe>
                        </div>
                      </div>
                      <div
                        className="tab-pane block-box__by fade"
                        id="nav-ccot-outcome"
                        role="tabpanel"
                        aria-labelledby="nav-ccot-outcome-tab"
                      >
                        Command Centre Operations Training prepares command
                        centre teams on all command centre operations
                        requirements from understanding various risks, garnering
                        and filtering information relevant to organisational
                        needs to writing reports that convey exact information
                        which is unbiased.
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6 col-12">
                    <img
                      className="block-img"
                      src={commandCentre}
                      width="100%"
                    />
                  </div>
                </div>
              </div>
            </div>

            <div className={btnclick ? 'div open' : 'div closed'}>
              <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
                <div className="block-box">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-12">
                      <img
                        className="block-img"
                        src={riskMonitoring}
                        width="100%"
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-12">
                      <h5 className="px-3">Risk Monitoring & Report Writing</h5>
                      <nav className="mt-3 px-3">
                        <div
                          className="nav nav-tabs nav-fill text-left"
                          id="nav-tab"
                          role="tablist"
                        >
                          <a
                            className="nav-item nav-link text-left active"
                            id="nav-rmrw-overview-tab"
                            data-toggle="tab"
                            href="#nav-rmrw-overview"
                            role="tab"
                            aria-controls="nav-rmrw-overview"
                            aria-selected="true"
                          >
                            Overview
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-rmrw-video-tab"
                            data-toggle="tab"
                            href="#nav-rmrw-video"
                            role="tab"
                            aria-controls="nav-rmrw-video"
                            aria-selected="false"
                          >
                            Watch a Video
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-rmrw-outcome-tab"
                            data-toggle="tab"
                            href="#nav-rmrw-outcome"
                            role="tab"
                            aria-controls="nav-rmrw-outcome"
                            aria-selected="false"
                          >
                            Outcome
                          </a>
                        </div>
                      </nav>
                      <div className="tab-content p-3" id="nav-tabContent">
                        <div
                          className="tab-pane block-box__by fade show active"
                          id="nav-rmrw-overview"
                          role="tabpanel"
                          aria-labelledby="nav-rmrw-overview-tab"
                        >
                          Risk Monitoring & Report Writing Training will help
                          your analysts and security professionals to understand
                          various risks, collect, collate and filter information
                          relevant to organisational needs and write reports for
                          decision support by multiple stakeholders.
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-rmrw-video"
                          role="tabpanel"
                          aria-labelledby="nav-rmrw-video-tab"
                        >
                          <div className="embed-responsive embed-responsive-16by9 px-5">
                            <iframe
                              width="560"
                              height="315"
                              src="https://www.youtube.com/embed/uLP3O0kVHLs"
                              title="YouTube video player"
                              frameborder="0"
                              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                              allowfullscreen
                            ></iframe>
                          </div>
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-rmrw-outcome"
                          role="tabpanel"
                          aria-labelledby="nav-rmrw-outcome-tab"
                        >
                          <ul>
                            <li> In understanding various risks </li>
                            <li>
                              {' '}
                              Garnering and filtering information relevant to
                              organisational needs{' '}
                            </li>
                            <li>
                              {' '}
                              Writing reports that convey exact information
                              which is unbiased{' '}
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
                <div className="block-box">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-12">
                      <h5 className="px-3">
                        Fire, Life Safety & First-Aid Training
                      </h5>
                      <nav className="mt-3 px-3">
                        <div
                          className="nav nav-tabs nav-fill text-left"
                          id="nav-tab"
                          role="tablist"
                        >
                          <a
                            className="nav-item nav-link text-left active"
                            id="nav-flsft-overview-tab"
                            data-toggle="tab"
                            href="#nav-flsft-overview"
                            role="tab"
                            aria-controls="nav-flsft-overview"
                            aria-selected="true"
                          >
                            Overview
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-flsft-video-tab"
                            data-toggle="tab"
                            href="#nav-flsft-video"
                            role="tab"
                            aria-controls="nav-flsft-video"
                            aria-selected="false"
                          >
                            Watch a Video
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-flsft-outcome-tab"
                            data-toggle="tab"
                            href="#nav-flsft-outcome"
                            role="tab"
                            aria-controls="nav-flsft-outcome"
                            aria-selected="false"
                          >
                            Outcome
                          </a>
                        </div>
                      </nav>
                      <div className="tab-content p-3" id="nav-tabContent">
                        <div
                          className="tab-pane block-box__by fade show active"
                          id="nav-flsft-overview"
                          role="tabpanel"
                          aria-labelledby="nav-flsft-overview-tab"
                        >
                          Fire Safety & First Aid Training will prepare the
                          workforce to reduce number of accidents in the
                          workplace , keep employees safe and hugely improve
                          your workplace productivity. It gives your employees
                          confidence and clarity during a fire or medical
                          emergency.
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-flsft-video"
                          role="tabpanel"
                          aria-labelledby="nav-flsft-video-tab"
                        >
                          <div className="embed-responsive embed-responsive-16by9 px-5">
                            <iframe
                              width="560"
                              height="315"
                              src="https://www.youtube.com/embed/Q2pu400_Vdw"
                              title="YouTube video player"
                              frameborder="0"
                              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                              allowfullscreen
                            ></iframe>
                          </div>
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-flsft-outcome"
                          role="tabpanel"
                          aria-labelledby="nav-flsft-outcome-tab"
                        >
                          <ul>
                            <li> It can save lives </li>
                            <li>
                              {' '}
                              It can reduce the number of accidents in the
                              workplace{' '}
                            </li>
                            <li>
                              {' '}
                              It gives your employees confidence and clarity
                              during an emergency{' '}
                            </li>
                            <li> First aid kits are used properly </li>
                            <li> It can reduce recovery time </li>
                            <li>
                              {' '}
                              It can keep your employees safe outside of the
                              workplace{' '}
                            </li>
                            <li>
                              {' '}
                              Help to inculcate peace of mind in the workplace{' '}
                            </li>
                            <li>
                              {' '}
                              Hugely improve your workplace productivity{' '}
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-12">
                      <img
                        className="block-img"
                        src={fireLifeSafety}
                        width="100%"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
                <div className="block-box">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-12">
                      <img
                        className="block-img"
                        src={returntoOffice}
                        width="100%"
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-12">
                      <h5 className="px-3">Return to Office Guidelines</h5>
                      <nav className="mt-3 px-3">
                        <div
                          className="nav nav-tabs nav-fill text-left"
                          id="nav-tab"
                          role="tablist"
                        >
                          <a
                            className="nav-item nav-link text-left active"
                            id="nav-rog-overview-tab"
                            data-toggle="tab"
                            href="#nav-rog-overview"
                            role="tab"
                            aria-controls="nav-rog-overview"
                            aria-selected="true"
                          >
                            Overview
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-rog-video-tab"
                            data-toggle="tab"
                            href="#nav-rog-video"
                            role="tab"
                            aria-controls="nav-rog-video"
                            aria-selected="false"
                          >
                            Watch a Video
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-rog-outcome-tab"
                            data-toggle="tab"
                            href="#nav-rog-outcome"
                            role="tab"
                            aria-controls="nav-rog-outcome"
                            aria-selected="false"
                          >
                            Outcome
                          </a>
                        </div>
                      </nav>
                      <div className="tab-content p-3" id="nav-tabContent">
                        <div
                          className="tab-pane block-box__by fade show active"
                          id="nav-rog-overview"
                          role="tabpanel"
                          aria-labelledby="nav-rog-overview-tab"
                        >
                          Working through the pandemic, ‘return to the office’
                          in a multi-tenant building or in a corporate campus,
                          will entail different strategic and tactical planning
                          guidelines for each location and city. We can train
                          your key stakeholders, security and safety teams to
                          enable a safe workplace.
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-rog-video"
                          role="tabpanel"
                          aria-labelledby="nav-rog-video-tab"
                        >
                          Coming Soon
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-rog-outcome"
                          role="tabpanel"
                          aria-labelledby="nav-rog-outcome-tab"
                        >
                          Ensure employee safety at the workplace
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
                <div className="block-box">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-12">
                      <h5 className="px-3">Corporate Security Training</h5>
                      <nav className="mt-3 px-3">
                        <div
                          className="nav nav-tabs nav-fill text-left"
                          id="nav-tab"
                          role="tablist"
                        >
                          <a
                            className="nav-item nav-link text-left active"
                            id="nav-corst-overview-tab"
                            data-toggle="tab"
                            href="#nav-corst-overview"
                            role="tab"
                            aria-controls="nav-corst-overview"
                            aria-selected="true"
                          >
                            Overview
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-corst-video-tab"
                            data-toggle="tab"
                            href="#nav-corst-video"
                            role="tab"
                            aria-controls="nav-corst-video"
                            aria-selected="false"
                          >
                            Watch a Video
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-corst-outcome-tab"
                            data-toggle="tab"
                            href="#nav-corst-outcome"
                            role="tab"
                            aria-controls="nav-corst-outcome"
                            aria-selected="false"
                          >
                            Outcome
                          </a>
                        </div>
                      </nav>
                      <div className="tab-content p-3" id="nav-tabContent">
                        <div
                          className="tab-pane block-box__by fade show active"
                          id="nav-corst-overview"
                          role="tabpanel"
                          aria-labelledby="nav-corst-overview-tab"
                        >
                          Corporate Security Training will give an overview of
                          the role of security and an understanding of Security
                          Project Management, Trends in Security Technology,
                          Security Assessments and an understanding of
                          Crisis/Emergency Responses.
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-corst-video"
                          role="tabpanel"
                          aria-labelledby="nav-corst-video-tab"
                        >
                          <div className="embed-responsive embed-responsive-16by9 px-5">
                            <iframe
                              width="560"
                              height="315"
                              src="https://www.youtube.com/embed/UFYwvSrEz28"
                              title="YouTube video player"
                              frameborder="0"
                              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                              allowfullscreen
                            ></iframe>
                          </div>
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-corst-outcome"
                          role="tabpanel"
                          aria-labelledby="nav-corst-outcome-tab"
                        >
                          Give security teams an in-depth understanding of
                          security roles in entirety
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-12">
                      <img
                        className="block-img"
                        src={corporateSecurity}
                        width="100%"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
                <div className="block-box">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-12">
                      <img
                        className="block-img"
                        src={corporateIntel}
                        width="100%"
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-12">
                      <h5 className="px-3">Corporate Intel Training</h5>
                      <nav className="mt-3 px-3">
                        <div
                          className="nav nav-tabs nav-fill text-left"
                          id="nav-tab"
                          role="tablist"
                        >
                          <a
                            className="nav-item nav-link text-left active"
                            id="nav-cit-overview-tab"
                            data-toggle="tab"
                            href="#nav-cit-overview"
                            role="tab"
                            aria-controls="nav-cit-overview"
                            aria-selected="true"
                          >
                            Overview
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-cit-video-tab"
                            data-toggle="tab"
                            href="#nav-cit-video"
                            role="tab"
                            aria-controls="nav-cit-video"
                            aria-selected="false"
                          >
                            Watch a Video
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-cit-outcome-tab"
                            data-toggle="tab"
                            href="#nav-cit-outcome"
                            role="tab"
                            aria-controls="nav-cit-outcome"
                            aria-selected="false"
                          >
                            Outcome
                          </a>
                        </div>
                      </nav>
                      <div className="tab-content p-3" id="nav-tabContent">
                        <div
                          className="tab-pane block-box__by fade show active"
                          id="nav-cit-overview"
                          role="tabpanel"
                          aria-labelledby="nav-cit-overview-tab"
                        >
                          Corporate Intel Training will introduce security
                          analysts, professionals and Command Centre operatives
                          to the Corporate Intel ecosystem and gives an
                          understanding of external threat environment and in
                          synthesising Corporate Intelligence to support a wide
                          range of business decisions ranging from operational
                          to strategic.
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-cit-video"
                          role="tabpanel"
                          aria-labelledby="nav-cit-video-tab"
                        >
                          Coming Soon
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-cit-outcome"
                          role="tabpanel"
                          aria-labelledby="nav-cit-outcome-tab"
                        >
                          Gives professional & intel analysts an in-depth
                          understanding of the corporate intel ecosystem
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
                <div className="block-box">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-12">
                      <h5 className="px-3">POSH & Gender Sensitization</h5>
                      <nav className="mt-3 px-3">
                        <div
                          className="nav nav-tabs nav-fill text-left"
                          id="nav-tab"
                          role="tablist"
                        >
                          <a
                            className="nav-item nav-link text-left active"
                            id="nav-pgs-overview-tab"
                            data-toggle="tab"
                            href="#nav-pgs-overview"
                            role="tab"
                            aria-controls="nav-pgs-overview"
                            aria-selected="true"
                          >
                            Overview
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-pgs-video-tab"
                            data-toggle="tab"
                            href="#nav-pgs-video"
                            role="tab"
                            aria-controls="nav-pgs-video"
                            aria-selected="false"
                          >
                            Watch a Video
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-pgs-outcome-tab"
                            data-toggle="tab"
                            href="#nav-pgs-outcome"
                            role="tab"
                            aria-controls="nav-pgs-outcome"
                            aria-selected="false"
                          >
                            Outcome
                          </a>
                        </div>
                      </nav>
                      <div className="tab-content p-3" id="nav-tabContent">
                        <div
                          className="tab-pane block-box__by fade show active"
                          id="nav-pgs-overview"
                          role="tabpanel"
                          aria-labelledby="nav-pgs-overview-tab"
                        >
                          POSH Awareness Workshops & Gender Sensitisation
                          Training will sensitise employees to an understanding
                          of what constitutes sexual harassment, legal rights
                          and company policy and help understand unconscious
                          bias.
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-pgs-video"
                          role="tabpanel"
                          aria-labelledby="nav-pgs-video-tab"
                        >
                          <div className="embed-responsive embed-responsive-16by9 px-5">
                            <iframe
                              width="560"
                              height="315"
                              src="https://www.youtube.com/embed/-kbyozsHVpg"
                              title="YouTube video player"
                              frameborder="0"
                              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                              allowfullscreen
                            ></iframe>
                          </div>
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-pgs-outcome"
                          role="tabpanel"
                          aria-labelledby="nav-pgs-outcome-tab"
                        >
                          <ul>
                            <li>
                              {' '}
                              Will promote awareness and propensity to behave in
                              a manner which is sensitive to gender justice and
                              gender equality issues{' '}
                            </li>
                            <li>
                              {' '}
                              Recognize the impact of bias on behaviours,
                              decisions, and performance{' '}
                            </li>
                            <li>
                              {' '}
                              Help create a space where everyone is respected,
                              included, and valued{' '}
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-12">
                      <img className="block-img" src={posh} width="100%" />
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
                <div className="block-box">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-12">
                      <img
                        className="block-img"
                        src={workplaceErgonomics}
                        width="100%"
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-12">
                      <h5 className="px-3">Workplace Ergonomics</h5>
                      <nav className="mt-3 px-3">
                        <div
                          className="nav nav-tabs nav-fill text-left"
                          id="nav-tab"
                          role="tablist"
                        >
                          <a
                            className="nav-item nav-link text-left active"
                            id="nav-we-overview-tab"
                            data-toggle="tab"
                            href="#nav-we-overview"
                            role="tab"
                            aria-controls="nav-we-overview"
                            aria-selected="true"
                          >
                            Overview
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-we-video-tab"
                            data-toggle="tab"
                            href="#nav-we-video"
                            role="tab"
                            aria-controls="nav-we-video"
                            aria-selected="false"
                          >
                            Watch a Video
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-we-outcome-tab"
                            data-toggle="tab"
                            href="#nav-we-outcome"
                            role="tab"
                            aria-controls="nav-we-outcome"
                            aria-selected="false"
                          >
                            Outcome
                          </a>
                        </div>
                      </nav>
                      <div className="tab-content p-3" id="nav-tabContent">
                        <div
                          className="tab-pane block-box__by fade show active"
                          id="nav-we-overview"
                          role="tabpanel"
                          aria-labelledby="nav-we-overview-tab"
                        >
                          The Workplace Ergonomics Training will discuss
                          physical & environmental ergonomic factors affecting
                          workstation ergonomics & help to recognise & prevent
                          musculoskeletal disorders (MSD).
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-we-video"
                          role="tabpanel"
                          aria-labelledby="nav-we-video-tab"
                        >
                          Coming Soon
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-we-outcome"
                          role="tabpanel"
                          aria-labelledby="nav-we-outcome-tab"
                        >
                          <ul>
                            <li>
                              {' '}
                              Understanding ergonomics makes employees aware of
                              the benefits of good posture and what makes a
                              workstation more efficient{' '}
                            </li>
                            <li>
                              {' '}
                              Improves productivity, quality and employee
                              engagement{' '}
                            </li>
                            <li>
                              {' '}
                              Use of ergonomic workstations reduces costs by
                              keeping employees injury free{' '}
                            </li>
                            <li>
                              {' '}
                              Ergonomics shows a company’s commitment to safety
                              and health as a core value and creates a better
                              safety culture{' '}
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
                <div className="block-box">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-12">
                      <h5 className="px-3">Road Safety/Safe Driving</h5>
                      <nav className="mt-3 px-3">
                        <div
                          className="nav nav-tabs nav-fill text-left"
                          id="nav-tab"
                          role="tablist"
                        >
                          <a
                            className="nav-item nav-link text-left active"
                            id="nav-rssd-overview-tab"
                            data-toggle="tab"
                            href="#nav-rssd-overview"
                            role="tab"
                            aria-controls="nav-rssd-overview"
                            aria-selected="true"
                          >
                            Overview
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-rssd-video-tab"
                            data-toggle="tab"
                            href="#nav-rssd-video"
                            role="tab"
                            aria-controls="nav-rssd-video"
                            aria-selected="false"
                          >
                            Watch a Video
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-rssd-outcome-tab"
                            data-toggle="tab"
                            href="#nav-rssd-outcome"
                            role="tab"
                            aria-controls="nav-rssd-outcome"
                            aria-selected="false"
                          >
                            Outcome
                          </a>
                        </div>
                      </nav>
                      <div className="tab-content p-3" id="nav-tabContent">
                        <div
                          className="tab-pane block-box__by fade show active"
                          id="nav-rssd-overview"
                          role="tabpanel"
                          aria-labelledby="nav-rssd-overview-tab"
                        >
                          The Safe Driving Training shall cover safe driving
                          mantras, defensive driving, discuss case studies of
                          avoidable accidents and give tips on driving in bad
                          weather conditions and dealing with road rage.
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-rssd-video"
                          role="tabpanel"
                          aria-labelledby="nav-rssd-video-tab"
                        >
                          Coming Soon
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-rssd-outcome"
                          role="tabpanel"
                          aria-labelledby="nav-rssd-outcome-tab"
                        >
                          <ul>
                            <li> Reinforces defensive driving techniques </li>
                            <li> Reduces At-Risk behaviours </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-12">
                      <img
                        className="block-img"
                        src={roadSafety}
                        width="100%"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
                <div className="block-box">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-12">
                      <img
                        className="block-img"
                        src={workplaceViolence}
                        width="100%"
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-12">
                      <h5 className="px-3">Workplace Violence</h5>
                      <nav className="mt-3 px-3">
                        <div
                          className="nav nav-tabs nav-fill text-left"
                          id="nav-tab"
                          role="tablist"
                        >
                          <a
                            className="nav-item nav-link text-left active"
                            id="nav-wv-overview-tab"
                            data-toggle="tab"
                            href="#nav-wv-overview"
                            role="tab"
                            aria-controls="nav-wv-overview"
                            aria-selected="true"
                          >
                            Overview
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-wv-video-tab"
                            data-toggle="tab"
                            href="#nav-wv-video"
                            role="tab"
                            aria-controls="nav-wv-video"
                            aria-selected="false"
                          >
                            Watch a Video
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-wv-outcome-tab"
                            data-toggle="tab"
                            href="#nav-wv-outcome"
                            role="tab"
                            aria-controls="nav-wv-outcome"
                            aria-selected="false"
                          >
                            Outcome
                          </a>
                        </div>
                      </nav>
                      <div className="tab-content p-3" id="nav-tabContent">
                        <div
                          className="tab-pane block-box__by fade show active"
                          id="nav-wv-overview"
                          role="tabpanel"
                          aria-labelledby="nav-wv-overview-tab"
                        >
                          The Workplace Violence Training will empower employees
                          to prevent/deal with workplace violence keeping in
                          view the necessity to have a safe workplace
                          environment. The aim is to create enterprise-wide
                          awareness by way of conducting training /
                          sensitisation workshops.
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-wv-video"
                          role="tabpanel"
                          aria-labelledby="nav-wv-video-tab"
                        >
                          Coming Soon
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-wv-outcome"
                          role="tabpanel"
                          aria-labelledby="nav-wv-outcome-tab"
                        >
                          This training will empower the individual to:
                          <ul>
                            <li> Understand and reduce the risks they face </li>
                            <li>
                              {' '}
                              Respond appropriately when confronted with a
                              potentially dangerous situation{' '}
                            </li>
                            <li>
                              {' '}
                              Maximise control over their own environment{' '}
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
                <div className="block-box">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-12">
                      <h5 className="px-3">Online Masterclass</h5>
                      <nav className="mt-3 px-3">
                        <div
                          className="nav nav-tabs nav-fill text-left"
                          id="nav-tab"
                          role="tablist"
                        >
                          <a
                            className="nav-item nav-link text-left active"
                            id="nav-om-overview-tab"
                            data-toggle="tab"
                            href="#nav-om-overview"
                            role="tab"
                            aria-controls="nav-om-overview"
                            aria-selected="true"
                          >
                            Overview
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-om-video-tab"
                            data-toggle="tab"
                            href="#nav-om-video"
                            role="tab"
                            aria-controls="nav-om-video"
                            aria-selected="false"
                          >
                            Watch a Video
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-om-outcome-tab"
                            data-toggle="tab"
                            href="#nav-om-outcome"
                            role="tab"
                            aria-controls="nav-om-outcome"
                            aria-selected="false"
                          >
                            Outcome
                          </a>
                        </div>
                      </nav>
                      <div className="tab-content p-3" id="nav-tabContent">
                        <div
                          className="tab-pane block-box__by fade show active"
                          id="nav-om-overview"
                          role="tabpanel"
                          aria-labelledby="nav-om-overview-tab"
                        >
                          Online Masterclass Series conducted in Intel,
                          Security, BCM (Business Continuity Management will
                          give an overview of the role of security, intel and
                          business continuity & resilience
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-om-video"
                          role="tabpanel"
                          aria-labelledby="nav-om-video-tab"
                        >
                          Coming Soon
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-om-outcome"
                          role="tabpanel"
                          aria-labelledby="nav-om-outcome-tab"
                        >
                          Gives an in-depth understanding of the Security, Intel
                          & Business Resiliency and trains on how to handle the
                          ecosystem of risk functions
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-12">
                      <img
                        className="block-img"
                        src={onlineMasterclass}
                        width="100%"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
                <div className="block-box">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-12">
                      <img
                        className="block-img"
                        src={safetyAndSecurityWeek}
                        width="100%"
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-12">
                      <h5 className="px-3">Safety & Security Week Training</h5>
                      <nav className="mt-3 px-3">
                        <div
                          className="nav nav-tabs nav-fill text-left"
                          id="nav-tab"
                          role="tablist"
                        >
                          <a
                            className="nav-item nav-link text-left active"
                            id="nav-sswt-overview-tab"
                            data-toggle="tab"
                            href="#nav-sswt-overview"
                            role="tab"
                            aria-controls="nav-sswt-overview"
                            aria-selected="true"
                          >
                            Overview
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-sswt-video-tab"
                            data-toggle="tab"
                            href="#nav-sswt-video"
                            role="tab"
                            aria-controls="nav-sswt-video"
                            aria-selected="false"
                          >
                            Watch a Video
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-sswt-outcome-tab"
                            data-toggle="tab"
                            href="#nav-sswt-outcome"
                            role="tab"
                            aria-controls="nav-sswt-outcome"
                            aria-selected="false"
                          >
                            Outcome
                          </a>
                        </div>
                      </nav>
                      <div className="tab-content p-3" id="nav-tabContent">
                        <div
                          className="tab-pane block-box__by fade show active"
                          id="nav-sswt-overview"
                          role="tabpanel"
                          aria-labelledby="nav-sswt-overview-tab"
                        >
                          Conducting a week-long safety & security training and
                          developing specific activities customised to create
                          widespread awareness on varied topics such as employee
                          safety, cyber safety, first aid, fire safety,
                          workplace safety along with handing out of collaterals
                          and handbooks reinforcing the same learnings.
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-sswt-video"
                          role="tabpanel"
                          aria-labelledby="nav-sswt-video-tab"
                        >
                          Coming Soon
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-sswt-outcome"
                          role="tabpanel"
                          aria-labelledby="nav-sswt-outcome-tab"
                        >
                          Inculcating a culture of safety & security
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
                <div className="block-box">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-12">
                      <h5 className="px-3">
                        Business Continuity Management Training
                      </h5>
                      <nav className="mt-3 px-3">
                        <div
                          className="nav nav-tabs nav-fill text-left"
                          id="nav-tab"
                          role="tablist"
                        >
                          <a
                            className="nav-item nav-link text-left active"
                            id="nav-bcmt-overview-tab"
                            data-toggle="tab"
                            href="#nav-bcmt-overview"
                            role="tab"
                            aria-controls="nav-bcmt-overview"
                            aria-selected="true"
                          >
                            Overview
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-bcmt-video-tab"
                            data-toggle="tab"
                            href="#nav-bcmt-video"
                            role="tab"
                            aria-controls="nav-bcmt-video"
                            aria-selected="false"
                          >
                            Watch a Video
                          </a>
                          <a
                            className="nav-item nav-link text-left"
                            id="nav-bcmt-outcome-tab"
                            data-toggle="tab"
                            href="#nav-bcmt-outcome"
                            role="tab"
                            aria-controls="nav-bcmt-outcome"
                            aria-selected="false"
                          >
                            Outcome
                          </a>
                        </div>
                      </nav>
                      <div className="tab-content p-3" id="nav-tabContent">
                        <div
                          className="tab-pane block-box__by fade show active"
                          id="nav-bcmt-overview"
                          role="tabpanel"
                          aria-labelledby="nav-bcmt-overview-tab"
                        >
                          The Business Continuity Management Training is
                          conducted in collaboration with GWFM (Global Workforce
                          Management) and is a certification programme which
                          will cover Business Continuity Planning & Crisis
                          Management by industry experts.
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-bcmt-video"
                          role="tabpanel"
                          aria-labelledby="nav-bcmt-video-tab"
                        >
                          Coming Soon
                        </div>
                        <div
                          className="tab-pane block-box__by fade"
                          id="nav-bcmt-outcome"
                          role="tabpanel"
                          aria-labelledby="nav-bcmt-outcome-tab"
                        >
                          Help teams to:
                          <ul>
                            <li>
                              {' '}
                              Minimise the effect of a disruption on an
                              organisation{' '}
                            </li>
                            <li> Reduce the risk of financial loss</li>
                            <li>
                              {' '}
                              Retain company brand and image and give staff,
                              clients and suppliers confidence in the
                              organisation’s services{' '}
                            </li>
                            <li>
                              {' '}
                              Enable the recovery of critical systems within an
                              agreed timeframe{' '}
                            </li>
                            <li> Meet legal and statutory obligations </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-12">
                      <img
                        className="block-img"
                        src={businessContinuity}
                        width="100%"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 my-3 text-center">
              <button
                className={
                  btnclick
                    ? 'btn btn-primary show-service-btn closed'
                    : 'btn btn-primary show-service-btn open'
                }
                onClick={() => setbtnclick(!btnclick)}
              >
                Show all Services{' '}
              </button>
            </div>
          </div>
        </div>
      </section>

      <section className="py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-center">
                  Our Collaterals
                </h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <Products item={collateralsItem} />
            </div>
          </div>
        </div>
      </section>

      <section className="py-5 bg-darker" id="twd_stt">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-center text-white">
                  Suggested Tools and Techniques
                </h2>
              </div>
            </div>
          </div>
        </div>
        <img
          src={suggestedTool}
          width="100%"
          alt="Suggested tools and techniques"
        />
      </section>
      <section className="service py-5" id="twd_approach">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-center">Our Approach</h2>
                <p className="content__subheader  pt-3">
                  At MitKat, we provide content, tools, methodology, training
                  and thought leadership that helps in bringing about change at
                  the core of any Organisation- the people.
                  <br />
                  We utilize adaptive learning paths, gamification, assessments
                  & more to bring about change in the workforce’s habits and way
                  of looking at assignments.
                  <br />
                  MitKat provides training for professional development,
                  advancing the skills of individuals to drive business success.
                  It helps people develop the skills needed to progress in the
                  professional field. MitKat helps Organisations to achieve
                  competitive advantages through superior enterprise-wide
                  knowledge and skills.
                  <br />
                  We provide e-learning modules and online learning content that
                  is available on any device and help bring employees up to
                  speed with engaging content and powerful technology in a time
                  of digital transformation. Its various learning modules’
                  intuitive design engages modern learners and its consumer-led
                  experience assists in accelerating learning.
                  <br />
                  MitKat provides training and development in Corporate Intel &
                  Security, POSH & Gender Sensitisation, Cyber Security and
                  Business Continuity / Resiliency..
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="our-process py-5 bg-darker" id="twd_process">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-center text-white">
                  Our Process
                </h2>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row mt-5">
            <div className="col-lg-3 col-md-3 col-12 text-center">
              <div className="process-box">
                <img
                  src={processBenchamrkD}
                  className="process-desktop display-big"
                  alt=""
                />
                <img
                  src={processBenchamrkM}
                  className="process-mobile display-small"
                  alt=""
                />
                <h2 className="process-box__header text-white mt-5">
                  BENCHMARK{' '}
                </h2>
                <p className="process-box__description text-white">
                  Conduct TNA (Training Need Analysis). Uncover gaps and
                  opportunities to optimize employee performance. We benchmark
                  against best practices and peer companies.{' '}
                </p>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-12 text-center">
              <div className="process-box">
                <img
                  src={processDesignD}
                  className="process-desktop display-big"
                  alt=""
                />
                <img
                  src={processDesignM}
                  className="process-mobile display-small"
                  alt=""
                />
                <h2 className="process-box__header text-white mt-5">DESIGN </h2>
                <p className="process-box__description text-white">
                  Give your team playbooks with an exact process to follow.{' '}
                </p>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-12 text-center">
              <div className="process-box">
                <img
                  src={processTrainingD}
                  className="process-desktop display-big"
                  alt=""
                />
                <img
                  src={processTrainingM}
                  className="process-mobile display-small"
                  alt=""
                />
                <h2 className="process-box__header text-white mt-5">
                  TRAINING{' '}
                </h2>
                <p className="process-box__description text-white">
                  Train your teams on the fundamental skills, roles and
                  responsibilities to optimise performance.{' '}
                </p>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-12 text-center">
              <div className="process-box">
                <img
                  src={processImplimentaionD}
                  className="process-desktop display-big"
                  alt=""
                />
                <img
                  src={processImplimentaionM}
                  className="process-mobile display-small "
                  alt=""
                />
                <h2 className="process-box__header text-white mt-5">
                  IMPLEMENTATION{' '}
                </h2>
                <p className="process-box__description text-white">
                  Build improved frameworks to ensure business continuity &
                  resiliency.{' '}
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="py-5" id="twd_usp">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-center">Our USP</h2>
              </div>
            </div>
          </div>
          <div className="row">
            <ul className="honeycomb" lang="es">
              <li className="honeycomb-cell">
                <img className="honeycomb-cell__image" src={responsiveDesign} />
              </li>
              <li className="honeycomb-cell">
                <div className="honeycomb-cell__title">
                  Responsive Design - Seamless LMS platform
                </div>
              </li>
              <li className="honeycomb-cell">
                <img
                  className="honeycomb-cell__image"
                  src={safetySecurityWeek}
                />
              </li>
              <li className="honeycomb-cell">
                <div className="honeycomb-cell__title">
                  Conduct of Safety Security Weeks- Inculcating an attitude of
                  Security & Safety
                </div>
              </li>
              <li className="honeycomb-cell">
                <img className="honeycomb-cell__image" src={assessmentTools} />
              </li>
              <li className="honeycomb-cell">
                <div className="honeycomb-cell__title">
                  Assessment Tools to measure participation, scorecards,
                  differential analysis on performance
                </div>
              </li>
              <li className="honeycomb-cell order-011">
                <div className="honeycomb-cell__title">
                  <img src={sme} /> <br /> SME- Security{' '}
                </div>
              </li>
              <li className="honeycomb-cell">
                <div className="honeycomb-cell__title">
                  In-House Development & Design Team for creation of collaterals
                </div>
              </li>
              <li className="honeycomb-cell">
                <img className="honeycomb-cell__image" src={inhouseTeam} />
              </li>
              <li className="honeycomb-cell">
                <div className="honeycomb-cell__title">
                  Online Masterclasses for Skill Upgradation
                </div>
              </li>
              <li className="honeycomb-cell">
                <img className="honeycomb-cell__image" src={onlineClasses} />
              </li>

              <li className="honeycomb-cell order-011">
                <div className="honeycomb-cell__title">
                  <img src={highlycustomizable} /> <br /> Highly customizable
                </div>
              </li>
              <li className="honeycomb-cell">
                <img className="honeycomb-cell__image" src={militaryVeterans} />
              </li>
              <li className="honeycomb-cell">
                <div className="honeycomb-cell__title">
                  Military veterans with expertise in Security & Crisis
                  Response, External Environment & Threat Monitoring
                </div>
              </li>
              <li className="honeycomb-cell honeycomb__placeholder"></li>
            </ul>
          </div>
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
        </div>
      </section>

      <section className="py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-center">Case Studies</h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <Casestudies item={casestudiesItem} />
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Twd;
