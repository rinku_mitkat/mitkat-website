import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import { NavLink } from 'react-router-dom';
import ServiceHeader from '../../components/ServiceHeader';
import ShowMoreText from 'react-show-more-text';
import irmBannerImg from '../../assets/images/service/Integrity Risk Management.webp';

import bep from '../../assets/images/service/IRM/BRAND ENFORCEMENT & PROTECTION.webp';
import ipp from '../../assets/images/service/IRM/INTELLECTUAL PROPERTY PROTECTION (IPP).webp';
import ls from '../../assets/images/service/IRM/LITIGATION SUPPORT.webp';
import cddi from '../../assets/images/service/IRM/CORPORATE DUE DILIGENCE INVESTIGATION.webp';
import ucwi from '../../assets/images/service/IRM/UNDERCOVER CORPORATE WORKPLACE INVESTIGATIONS.webp';
import investigativeInterviewing from '../../assets/images/service/IRM/INVESTIGATIVE INTERVIEWING.webp';
import fafis from '../../assets/images/service/IRM/FORENSIC ACCOUNTING & FRAUD INVESTIGATION SERVICES.webp';
import assettracing from '../../assets/images/service/IRM/Asset Tracing.webp';
import skipTracing from '../../assets/images/service/IRM/SKIP TRACING.webp';
import whistleblowers from '../../assets/images/service/IRM/Whistleblower Claims.webp';
import cfre from '../../assets/images/service/IRM/CYBER FORENSIC AND REVIEW OF EVIDENCE.webp';
import ms from '../../assets/images/service/IRM/MYSTERY SHOPPING.webp';

import Casestudies from '../../components/Casestudies';

import casestudy01 from '../../assets/images/casestudies/IRM/Enabling Due Diligence Investigation on Procurement Fraud in a Multinational Company in India.webp';
import casestudy02 from '../../assets/images/casestudies/IRM/Comprehensive Due Diligence Investigation for leading Japan-Headquartered MNC in India.webp';
import casestudy03 from '../../assets/images/casestudies/IRM/Comprehensive Due Diligence Investigation for leading Mauritius-based real estate MNC in India.webp';
import { Helmet } from 'react-helmet';

const handleDragStart = (e) => e.preventDefault();

const casestudiesItem = [
  <div className="block-box ">
    <img
      src={casestudy01}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_irm_01" className="block-box__title">
        Enabling Due Diligence Investigation on Procurement Fraud in a
        Multinational Company in India{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      MitKat was engaged by a client in the food industry (with export
      operations in many countries) to conduct due diligence investigation on
      procurement fraud in the company’s locations in India.
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy02}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_irm_02" className="block-box__title">
        Comprehensive Due Diligence Investigation for leading
        Japan-Headquartered MNC in India{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      MitKat was engaged by a leading Japan-headquartered MNC to conduct due
      diligence investigation in one of its subsidiary companies and its
      management based in India.
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy03}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <NavLink exact to="/casestudy_irm_03" className="block-box__title">
        Comprehensive Due Diligence Investigation for leading Mauritius-based
        real estate MNC in India{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      MitKat was engaged by a leading Mauritius-based real estate MNC to conduct
      due diligence investigation on an India headquartered real estate company.
      The client had planned for the acquisition of India based real estate
      Company.
    </div>
  </div>,

  ,
];

const Irm = () => {
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Integrity Risk Management | MitKat Advisory</title>
        <meta
          id="description"
          name="description"
          content="Frauds are on the rise - across industries and geographies. Frauds can have a devastating impact on an organisation’s profitability, brand/reputation"
        />
        <meta
          id="keywords"
          name="keywords"
          content="brand and reputation enhacement, trademark law protection, intellectual property rights protection, ipr protect the use of information, types of ipr, intellectual property laws, kinds of trademarks, need for ipr, suppy chain security audit, financial fraud investigation, mystery shopper services"
        />
      </Helmet>
      <Navbar />
      <ServiceHeader
        imgsrc={irmBannerImg}
        title="Integrity Risk Management"
        linkTitle="Book a Demo"
        description="Frauds are on the rise - across industries and geographies. Frauds can have a devastating impact on an organisation’s profitability,  brand/reputation and even continuity. Fraudsters flourish when controls are weak; conversely, frauds can be prevented/minimised if an appropriate fraud risk management framework is implemented. 
            MitKat’s fraud risk management framework helps organisations prevent, detect and investigate frauds. 
            MitKat provides full spectrum of advisory and investigative services, including basic and enhanced due diligence, corporate investigations, anti-counterfeit operations and litigation support. We use technology extensively in support of our Security and Integrity Risk Management operations."
      />

      <section className="service bg-gray py-5" id="pri_services">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <p className="mb-5 content__header text-center">
                  Service offering
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={bep}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Brand Enforcement & Protection
                </h5>
                <ShowMoreText
                  lines={4}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={270}
                  keepNewLines={true}
                >
                  Brand Enforcement protects your reputation and increases the
                  value of your brand thereby contributing to profits. We have
                  10 years of experience identifying counterfeiters, tracking
                  their channels of trade, and locating goods. Fortune 500
                  companies rely on us to help safeguard their brand and
                  reputation. We have the ability to operate in remote locations
                  across South Asia, and to deliver such services with our
                  partner organisations in other geographies.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={ipp}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Intellectual Property Protection (IPP)
                </h5>
                <ShowMoreText
                  lines={3}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={260}
                  keepNewLines={true}
                >
                  Intellectual Property Protection offers protection of your
                  products, patents, and profits. We work with brand owners,
                  manufacturers, law firms, in-house counsel, and other
                  stakeholders to implement incremental solutions and tactical
                  actions to combat increasingly sophisticated intellectual
                  property crime. Services: Market Surveys, Criminal /
                  Enforcement Actions, Online Monitoring, Investigations and
                  Supply Chain Audits. Some of the world’s largest global
                  corporations turn to our experts for investigating and
                  resolving IPP issues.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={ls}
                  width="100%"
                />
                <h5 className="block-box__title px-3">Litigation Support</h5>
                <ShowMoreText
                  lines={4}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={265}
                  keepNewLines={true}
                >
                  Our Litigation Support Services empower your legal team by
                  taking on the heavy lifting. We’ve helped lawyers arrange and
                  perform expert witness testimony, perform due diligence checks
                  and procure a chain of evidence. Our team of investigative
                  professionals work with you at each phase of litigation
                  helping your legal team focus on building a successful case.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={cddi}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Corporate Due Diligence Investigation
                </h5>
                <ShowMoreText
                  lines={5}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={265}
                  keepNewLines={true}
                >
                  Due diligence checks are invaluable in assessing potential
                  investor relations, business partners, senior management,
                  personnel involved in mergers and acquisitions etc. This would
                  help you know the ethical behaviour, financials, criminal
                  background, court cases, media news, etc. MitKat’s has
                  valuable experience of carrying out basic as well as advanced
                  due diligence. We use desktop searches, publicly available
                  information, paid databases, as well as deploy ground sources
                  and field operatives to confirm, deny or verify findings.
                  MitKat’s services include Third Part / Vendor Due Diligence,
                  Counter Party Due Diligence, Integrity Due Diligence and
                  Senior Management Due Diligence. These can be customised to
                  meet your unique requirements.
                </ShowMoreText>
              </div>
            </div>

            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={ucwi}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Undercover Corporate Workplace Investigations
                </h5>
                <ShowMoreText
                  lines={4}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={261}
                  keepNewLines={true}
                >
                  Situations where undercover work has helped include: theft,
                  sexual harassment, kickback schemes, industrial espionage,
                  substance abuse, workplace violations, fraud, and compliance
                  issues. Our undercover security agents are trained to catch
                  nuances, build trust, and develop leads. Covert operatives can
                  reveal more information than most security systems or CCTVs
                  could ever hope to capture.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={investigativeInterviewing}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Investigative Interviewing
                </h5>
                <ShowMoreText
                  lines={4}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={275}
                  keepNewLines={true}
                >
                  Investigative Interviewing is often employed in cases of
                  corporate theft, fraud, kickbacks, conflicts of interest,
                  extortion and embezzlement. Our team of professional
                  interviewers act as true investigators. We strive to help you
                  learn the truth fast, without sacrificing accuracy.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={fafis}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Forensic Accounting & Fraud Investigation Services
                </h5>
                <ShowMoreText
                  lines={3}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={270}
                  keepNewLines={true}
                >
                  Know and understand where money flows and prevent white-collar
                  crime. An effective forensic investigation will help determine
                  the source of financial fraud in areas such as payroll,
                  expense reporting, contract bidding, and invoicing, and act as
                  a foundation for future fraud prevention.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={assettracing}
                  width="100%"
                />
                <h5 className="block-box__title px-3">Asset Tracing</h5>
                <ShowMoreText
                  lines={5}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={275}
                  keepNewLines={true}
                >
                  Banks, financial institutions, investors, venture capitalists
                  need help in tracing assets when their investment/loans go bad
                  and they need to find assets to recover their capital
                  investments. It may be against individuals/companies. MitKat
                  has the requisite expertise to assist clients in tracing their
                  assets directly or through advisors.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={skipTracing}
                  width="100%"
                />
                <h5 className="block-box__title px-3">Skip Tracing</h5>
                <ShowMoreText
                  lines={5}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={275}
                  keepNewLines={true}
                >
                  Keep track of promoters and key personnel of companies, who
                  are considered strong competition in key markets. They could
                  be challenging your own brand and business. As the market gets
                  more competitive, several companies feel the need for such
                  services.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={whistleblowers}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Whistle-blowers’ Claims
                </h5>
                <ShowMoreText
                  lines={5}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={271}
                  keepNewLines={true}
                >
                  Whistle-blowers’ claims can bring to light wrongdoings in your
                  Organisation. Such claims could also be frivolous allegations
                  intended to project your company in a bad light. MitKat can
                  help you get to the truth of allegations, or absence thereof,
                  so that you can decide appropriate next steps, while acting on
                  a whistle-blower’s claim.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={cfre}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Cyber Forensics & Review Of Evidence
                </h5>
                <ShowMoreText
                  lines={3}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={260}
                  keepNewLines={true}
                >
                  E-discovery is an important process of gathering the evidence
                  that may be used in support of forensic investigations and
                  disputes. MitKat has the necessary experience and expertise
                  for the same.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={ms}
                  width="100%"
                />
                <h5 className="block-box__title px-3">Mystery Shopping</h5>
                <ShowMoreText
                  lines={5}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={255}
                  keepNewLines={true}
                >
                  Each company wants to be known for a high level of service,
                  and many businesses make substantial investments to provide
                  excellent customer care and support. Nevertheless, such
                  initiatives need to be monitored and verified on ground.
                  Mystery Shopping Services will show just what kind of service
                  customers are actually receiving or what they can expect, so
                  that requisite corrective actions, if needed, can be put in
                  place. MitKat’s operatives have vast experience of
                  demonstrating to organisation’s owners/CXOs/managers, with
                  tangible evidence, if there is any significant mismatch
                  between their assumptions/expectations and prevailing on
                  ground realities.
                </ShowMoreText>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-center">Case Studies</h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <Casestudies item={casestudiesItem} />
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Irm;
