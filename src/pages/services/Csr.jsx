import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import ServiceHeader from '../../components/ServiceHeader';
import csrBannerImg from '../../assets/images/service/Cyber Security and Resilience.webp';
import ira from '../../assets/images/service/CSR/Information Risk Assurance.webp';
import cs from '../../assets/images/service/CSR/Cyber Security.webp';
import bcm from '../../assets/images/service/CSR/Business Continuity Management (BCM) - Advisory, Implementation & Sustenance.webp';
import mss from '../../assets/images/service/CSR/Managed Security Services.webp';
import nexgen from '../../assets/images/service/CSR/NexGen SOC.webp';
import grc from '../../assets/images/service/CSR/GRC.webp';
import techCon from '../../assets/images/service/CSR/Technology Consulting.webp';
import { NavLink } from 'react-router-dom';
import Partners from '../../components/Partners';
import Casestudies from '../../components/Casestudies';
import casestudy01 from '../../assets/images/casestudies/CSR/ISMS-audit-of-an-engineering-service-provider-on-behalf-of-an-Indian-Bank-T.webp';
import casestudy02 from '../../assets/images/casestudies/CSR/Cybersecurity-assessment-of-IT-infrastructure-of-a-leading-media-company-T.webp';
import casestudy03 from '../../assets/images/casestudies/CSR/Remediation-of-a-hacking-incident-and-designing-cybersecurity-policies-to-prevent-future-attack-for-a-leading-chain-of-healthcare-organization-T.webp';
import casestudy04 from '../../assets/images/casestudies/CSR/Cyber-assurance-for-a-cloud-migration-initiative-of-a-leading-FMCG-client-T.webp';
import casestudy05 from '../../assets/images/casestudies/CSR/Implementation-of-DLP-policies-for-a-leading-Financial-organization-and-its-group-companies-T.webp';
import casestudy06 from '../../assets/images/casestudies/CSR/Designing-of-Business-Continuity-Management-system-for-a-European-Banks-GIC-centre-T.webp';
import casestudy07 from '../../assets/images/casestudies/CSR/Crisis-Management-simulation-exercise-for-a-global-conglomerate-GIC-T.webp';

import partners01 from '../../assets/images/partners/Forcepoint.webp';
import partners02 from '../../assets/images/partners/fireeye.webp';
import partners03 from '../../assets/images/partners/Sophos_logo.webp';
import partners04 from '../../assets/images/partners/tripwire-logo.webp';
import partners05 from '../../assets/images/partners/rapid7.webp';
import partners06 from '../../assets/images/partners/seqrite-end-point-security-eps-500x500.webp';
import partners07 from '../../assets/images/partners/regus.webp';
import partners08 from '../../assets/images/partners/integrum.webp';
import { Helmet } from 'react-helmet';

const handleDragStart = (e) => e.preventDefault();

const partnersItem = [
  <div className="block-box ">
    <img
      src={partners01}
      onDragStart={handleDragStart}
      className="block-box__img-partners"
      alt=""
    />
  </div>,
  <div className="block-box ">
    <img
      src={partners02}
      onDragStart={handleDragStart}
      className="block-box__img-partners"
      alt=""
    />
  </div>,
  <div className="block-box ">
    <img
      src={partners03}
      onDragStart={handleDragStart}
      className="block-box__img-partners"
      alt=""
    />
  </div>,
  <div className="block-box ">
    <img
      src={partners04}
      onDragStart={handleDragStart}
      className="block-box__img-partners"
      alt=""
    />
  </div>,
  <div className="block-box ">
    <img
      src={partners05}
      onDragStart={handleDragStart}
      className="block-box__img-partners"
      alt=""
    />
  </div>,
  <div className="block-box ">
    <img
      src={partners06}
      onDragStart={handleDragStart}
      className="block-box__img-partners"
      alt=""
    />
  </div>,
  <div className="block-box ">
    <img
      src={partners07}
      onDragStart={handleDragStart}
      className="block-box__img-partners"
      alt=""
    />
  </div>,
  <div className="block-box ">
    <img
      src={partners08}
      onDragStart={handleDragStart}
      className="block-box__img-partners"
      alt=""
    />
  </div>,

  ,
];

const casestudiesItem = [
  <div className="block-box ">
    <img
      src={casestudy01}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <small className="text-gray text-light-100"> Cyber Security </small>
      <br />
      <NavLink exact to="/casestudy_csr_01" className="block-box__title">
        ISMS audit of an engineering service provider on behalf of an Indian
        Bank{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      ISMS audit of an ODC infrastructure and cybersecurity assessment of the
      banking applications
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy02}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <small className="text-gray text-light-100"> Cyber Security </small>
      <br />
      <NavLink exact to="/casestudy_csr_02" className="block-box__title">
        {' '}
        Cybersecurity assessment of IT infrastructure of a leading media company{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      Cybersecurity policy and controls’ assessment to protect corporate IT
      infrastructure from insider threat and external hacking efforts
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy03}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <small className="text-gray text-light-100"> Cyber Security </small>
      <br />
      <NavLink exact to="/casestudy_csr_03" className="block-box__title">
        Remediation of a hacking incident and designing cybersecurity policies
        to prevent future attack for a leading Healthcare chain{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      A Virtual CISO services are being provided to the Client, to help recover
      rapidly from a recent hacking and data theft incident, and to design
      policies to prevent such incidents in future
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy04}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <small className="text-gray text-light-100"> Cyber Security </small>
      <br />
      <NavLink exact to="/casestudy_csr_04" className="block-box__title">
        Cyber assurance for a cloud migration initiative of a leading FMCG
        client{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      Cybersecurity assessment & policy review for a cloud migration project
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy05}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <small className="text-gray text-light-100"> Cyber Security </small>
      <br />
      <NavLink exact to="/casestudy_csr_05" className="block-box__title">
        Implementation of DLP policies for a leading Financial organization and
        its group companies{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      Enterprise wide rollout of a new DLP technology and relevant policies
    </div>
  </div>,

  <div className="block-box ">
    <img
      src={casestudy06}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <small className="text-gray text-light-100"> Resilience </small>
      <br />
      <NavLink exact to="/casestudy_csr_06" className="block-box__title">
        {' '}
        Designing of Business Continuity Management system for a European Bank’s
        GIC centre
      </NavLink>
    </h5>
    <div className="block-box__by">
      Building a BCM framework and Crisis Management system
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy07}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <small className="text-gray text-light-100"> Resilience </small>
      <br />
      <NavLink exact to="/casestudy_csr_07" className="block-box__title">
        Crisis Management simulation exercise for a global conglomerate GIC{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      A readiness check via table top simulation for crisis management system
      involving senior management, crisis team and common employees
    </div>
  </div>,

  ,
];

const Csr = () => {
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Cyber Security and Resilience | MitKat Advisory </title>
        <meta
          id="description"
          name="description"
          content="Cyberspace has become an incredibly complex environment. The threat landscape is growing and evolving quickly, and defense and control mechanisms"
        />
        <meta
          id="keywords"
          name="keywords"
          content="why is cybersecurity important?, cybersecurity types, cybersecurity threats, how cybersecurity works, bca with cyber, cyber security organizations in India, which cyber security certification, types of security, what is information security, ISO 27001 certification, data encryption standard, infomation security policy, cyber security consulting, operating system security,  data security in cloud computing, types of active attacks, protection and security in operating systems "
        />
      </Helmet>
      <Navbar />
      <ServiceHeader
        imgsrc={csrBannerImg}
        linkTitle="Book a Demo"
        title="Cyber Security and Resilience"
        description="Cyberspace has become an incredibly complex environment. The threat landscape is growing and evolving quickly, and defense and control mechanisms falter in keeping pace with the disruptive trends. The explosion of data analytics, the digitization of business, and technology mergers, are creating new risks in a VUCA (Volatile, Uncertain, Complex, Ambiguous) world.
           
                  It is imperative for organizations to gear up for the security challenge by formulating relevant strategies and implementing technology solutions to monitor and manage risks. The right consulting partner can help you gauge evident and unforeseen perils, and assist you in compliance with industry standards and best practices.  
                   
                  We, at MitKat, armed with the arsenal of diverse experience and up-to-date knowledge of the latest technology, emerging risks and industry practices, can implement solutions to keep your security backbone abreast with the dynamic threat landscape. Your organizational needs are unique, and we believe solutions cannot be designed in a ‘One size fits all’ manner. We can develop the methodology and approach, tailored to your organization’s specific needs and interests."
      />

      <section className="service bg-gray py-5" id="pri_services">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <p className="mb-5 content__header text-center">
                  Service offering
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={ira}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Information Risk Assurance
                </h5>
                <div className="block-box__description p-3">
                  Being a seasoned player in the security domain with vast
                  exposure to several organizations and the associated risks,
                  MitKat comprises of a team of expert consultants. They will
                  research, develop solutions and work with you to solve your
                  specific security problems, as well as for the industry at
                  large.
                  <ul className="unorder-list">
                    <li>ISMS Design & Implementation </li>
                    <li>Information Security Risk Management </li>
                    <li>Internal Audits & Third Party Audits </li>
                    <li>ISO 27001 Certification Assistance </li>
                    <li>Sustenance Support, Review and Awareness </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={cs}
                  width="100%"
                />
                <h5 className="block-box__title px-3">Cyber Security</h5>
                <div className="block-box__description p-3">
                  The ever-evolving dynamics of the threat landscape render
                  organizations vulnerable to a host of security hazards and
                  cyber-attacks. At MitKat, we offer end-to-end cyber security
                  resilience solutions, equipped with the best technology and
                  industry practices, to address such concerns that can enable
                  your enterprise to develop a smart and secure infrastructure.
                  <ul className="unorder-list">
                    <li>Vulnerability Assessment & Penetration Testing </li>
                    <li>Security Auditing & Digital Forensics Services </li>
                    <li>Embedded Cyber Security Managers </li>
                    <li>Cyber Security Tracker </li>
                    <li>Security Benchmarking </li>
                    <li>Monitoring & Incidence Response</li>
                  </ul>
                </div>
              </div>
            </div>

            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={bcm}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Business Continuity Management (BCM) – Advisory,
                  Implementation & Sustenance
                </h5>
                <div className="block-box__description p-3">
                  The challenge enterprises face today primarily involve
                  protecting business critical functions and data against
                  various downtime. MitKat provides a comprehensive, integrated
                  portfolio of Business Continuity Plans (BCP) and Disaster
                  Recovery (DR) services to decrease risk, control expenses,
                  increase resilience and thus reliability.
                  <ul className="unorder-list">
                    <li> BCMS Design & Implementation </li>
                    <li>
                      {' '}
                      Emergency Response Procedures & Crisis Simulation
                      Exercises{' '}
                    </li>
                    <li> Awareness Programs & E-learning </li>
                    <li> ISO 22301 Certification Assistance </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={techCon}
                  width="100%"
                />
                <h5 className="block-box__title px-3">Technology consulting</h5>
                <div className="block-box__description p-3">
                  MitKat specializes in developing flexible turnkey solutions
                  that work the way the client wants, to augment their existing
                  security program, infrastructure and personnel, while
                  relieving your in-house resources from the information
                  security and compliance burden.
                  <br />
                  <br />
                  Our robust alliances with top security vendors testify our
                  commitment to meet international standards of enterprise
                  security: McAfee, WebSense, etc.
                  <ul className="unorder-list">
                    <li>Cloud & Virtualization Security </li>
                    <li>Datacenter Security </li>
                    <li>
                      Identity Access Management – Privileged Password
                      Management, Privileged Session Management{' '}
                    </li>
                    <li>
                      Data Security & Protection Strategy – DLP, Data Security
                      Suite Implementation & Configuration{' '}
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={mss}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Managed Security Services (MSS)
                </h5>
                <div className="block-box__description p-3">
                  <ul className="unorder-list">
                    <li>
                      vCIO
                      <br />
                      Our seasoned CIO partners with key stakeholders to design
                      and deliver best of the bread IT Solutions and Services
                    </li>
                    <li>
                      vCISO
                      <br />
                      Our seasoned CISO will help you secure your business
                      technology and crucial Information by providing 360 degree
                      view using Industry best practices
                    </li>
                    <li>
                      vDPO
                      <br />
                      Engage our highly skilled Data Privacy Officer for your
                      regulatory and compliance requirements
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={grc}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Governance, Risk & Compliance
                </h5>
                <div className="block-box__description p-3">
                  <ul className="unorder-list">
                    <li>
                      Review business processes for overall effectiveness as
                      well as risks associated with the internal controls
                      system.
                    </li>
                    <li>
                      Identify processes and security controls to secure the
                      cloud infrastructure and make it more resilient.
                    </li>
                    <li>
                      Assurance for compliance to data protection regulations,
                      where required. Review of agreements with
                      customer/partners.
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            {/* <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                            <div className="block-box p-0">               
                                <img className="block-box__img service-offering__img" src={nexgen} width="100%" />       
                                <h5 className="block-box__title px-3">
                                NexGen SOC
                                </h5>
                                <div
                                    className="block-box__description p-3">
                                MitKat can customize a dedicated service portfolio to help enterprises better utilize their current monitoring and analytics technology, and provide on-site data control to design and deploy security intelligence solutions that help clients to optimize and enhance their existing deployments.
              
                                <ul className="unorder-list">
                                    <li>Security Information & Event Management (SIEM) & Security Operations Centre (SOC)  </li>
                                    <li>Security Device Management – firewalls, IDS, IPS, secure web gateways, WAF </li>
                                    <li>Security Analytics & Visualization Platform – Deep Packet Inspection </li>
                                    <li>Cyber Threat Intelligence & Advisory </li>
                                </ul>
                                </div>
                            </div>
                        </div> */}
          </div>
        </div>
      </section>

      <section className="py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-center">
                  Our Product Partners
                </h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <Partners item={partnersItem} />
            </div>
          </div>
        </div>
      </section>

      <section className="py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-center">Case Studies</h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <Casestudies item={casestudiesItem} />
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Csr;
