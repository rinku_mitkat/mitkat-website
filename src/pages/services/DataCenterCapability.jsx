import React from 'react';
import { NavLink } from 'react-router-dom';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import ShowMoreText from 'react-show-more-text';
import bannerImg from '../../assets/images/service/PESS/Data Center Capability banner image.webp';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import greenfields from '../../assets/images/service/PESS/GREENDFIELD PHASE.webp';
import underConstruction from '../../assets/images/service/PESS/UNDER CONSTRUCTION PHASE.webp';
import sapt from '../../assets/images/service/PESS/SUSTENANCE AND PENETRATION TESTING.webp';
import tvra from '../../assets/images/casestudies/PESS/TVRA of An Upcoming Data Center Thumbnail.webp';
const DataCenterCapability = () => {
  return (
    <>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" />
        </div>
        <div className="col-12 col-md-9 col-lg-9">
          <div className="service-description">
            <div className="display-block m-a">
              <div className="content">
                <h1 className="my-3 main-header__header-font text-capitalize">
                  Data Centre Capability
                </h1>
                <p className="content__subheader py-3 m-0">
                  With over a decade of consulting experience, MitKat is able to
                  address robust needs of any Organisation dealing with data
                  centres. Whether your data centre is in the pre-construction
                  phase, operational phase or needs to gauge the efficiency of
                  countermeasures deployed for security, we have the apt
                  resources and experience you can count on.
                  <br />
                  The methodologies we adhere to for threat assessments,
                  designing the physical security or improving upon the existing
                  facilities of a data centre, are aligned with the latest
                  globally accepted and recognized security standards as well as
                  legal requirements like ISO 31000 (2018), ANSI/TIA 942, SSAE
                  16, ISO 27001 and PCI DSS (Payment Card Industry Data Security
                  Standard).
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container">
        <hr className="divider" />
      </div>

      <section className="py-5">
        <div className="container">
          <div className="row">
            <div className="block-box">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-12">
                  <img className="" src={greenfields} width="100%" />
                </div>
                <div className="col-lg-6 col-md-6 col-12">
                  <h2 className="mb-3 header-font text-capitalize text-left">
                    Greenfield Phase{' '}
                  </h2>
                  <ShowMoreText
                    lines={7}
                    more="Show more"
                    less="Show less"
                    className="content-css"
                    anchorClassName="my-anchor-css-class"
                    expanded={false}
                    width={480}
                    className="content__subheader  py-3 show-read-more-paragraph"
                  >
                    MitKat conducts a threat-vulnerability risk assessment
                    (TVRA) of land parcels and greenfield site(s) with a view to
                    determining the suitability of such site(s) for data center,
                    from a TVR or security perspective. TVRA helps to develop a
                    clear understanding of the operational environment, existing
                    threats, vulnerabilities and recommend mitigation measures.
                    MitKat has carried out TVRA of some of the largest
                    greenfield data center sites in India, and our
                    recommendations have been well received by our customers.
                  </ShowMoreText>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="block-box">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-12">
                    <h2 className="mb-3 header-font text-capitalize text-left">
                      Under Construction Phase{' '}
                    </h2>
                    <ShowMoreText
                      lines={7}
                      more="Show more"
                      less="Show less"
                      className="content-css"
                      anchorClassName="my-anchor-css-class"
                      expanded={false}
                      width={490}
                      className="content__subheader  py-3 show-read-more-paragraph"
                    >
                      Emerging dynamic threats require state-of-the-art
                      solutions.While undergoing construction, it becomes
                      extremely significant to secure Data Centres with
                      leading-edge technology and manpower. Therefore, it
                      demands a security framework comprising of both physical
                      security and technology design, which is both
                      cost-effective and easy to maintain – without compromising
                      the security. MitKat helps in providing User Acceptance
                      Testing and documentation along with training the security
                      manpower.
                    </ShowMoreText>
                  </div>
                  <div className="col-lg-6 col-md-6 col-12">
                    <img className="" src={underConstruction} width="100%" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="py-5">
        <div className="container">
          <div className="row">
            <div className="block-box">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-12">
                  <img className="" src={sapt} width="100%" />
                </div>
                <div className="col-lg-6 col-md-6 col-12">
                  <h2 className="mb-3 header-font text-capitalize text-left">
                    Sustenance And Penetration Testing{' '}
                  </h2>
                  <ShowMoreText
                    lines={7}
                    more="Show more"
                    less="Show less"
                    className="content-css"
                    anchorClassName="my-anchor-css-class"
                    expanded={false}
                    width={470}
                    className="content__subheader  py-3 show-read-more-paragraph"
                  >
                    Once the construction of a data centre has been completed
                    and it is in the operational state, the emerging and dynamic
                    threats decide whether the existing security controls can
                    sustain amid those threats or not. A periodic Security Risk
                    Assessment of the site including review of existing policies
                    and implementation on ground becomes imperative. MitKat
                    helps in finding the gaps and loopholes which can be
                    leveraged by any adversary and recommend if the security
                    posture needs to be augmented.
                    <br />
                    In addition to the above, a physical penetration test can
                    help reveal security vulnerabilities that might otherwise be
                    discovered and exploited by malicious actors, giving
                    organisations a valuable insight into the existing security
                    framework of physical assets. We provide highly trained
                    physical pen testers who have experience in infiltrating
                    some of the most secure environments. Subsequently, they
                    leverage the loopholes on critical issues and provide
                    implementable, effective and cost-conscious recommendations
                    and technology upgrades to mitigate the risks.
                  </ShowMoreText>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div className="container">
        <hr className="divider" />
      </div>

      <section className="py-5">
        <div className="container" id="pess_data_center_casestudies">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-center">Case Studies</h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <div className="block-box">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-12">
                    <img className="" src={tvra} width="100%" />
                  </div>
                  <div className="col-lg-6 col-md-6 col-12">
                    <h5 className="px-3 pt-5">
                      <NavLink exact to="/casestudy_tvra">
                        {' '}
                        TVRA of An Upcoming Data Centre{' '}
                      </NavLink>
                    </h5>
                    <div className="block-box__by p-3">
                      TVRA of an upcoming data center of London based
                      multinational company.
                      <br /> <br />
                      <NavLink
                        exact
                        to="/casestudy_tvra"
                        className="main-header__link"
                      >
                        Read More &nbsp; &nbsp;
                        <FontAwesomeIcon
                          icon={faArrowRight}
                          className="main-header__link__arrow"
                        />
                      </NavLink>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default DataCenterCapability;
