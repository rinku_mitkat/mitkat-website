import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import ServiceHeader from '../../components/ServiceHeader';
// import ShowMoreText from 'react-show-more-text';
import pessBannerImg from '../../assets/images/service/Physical, Environmental Security And Safety (PESS) Risk Consulting.webp';
import dcc from '../../assets/images/service/PESS/Data Center Capability Thumbnail image.webp';
import Casestudies from '../../components/Casestudies';
import esrm from '../../assets/images/service/PESS/Enterprise Security Risk Management (ESRM).webp';
import assessmentAudit from '../../assets/images/service/PESS/Assessment & Audit.webp';
import operationalSupportServices from '../../assets/images/service/PESS/Operational Support Services.webp';
import managedSecurityServices from '../../assets/images/service/PESS/Managed Security Services.webp';
import trainingTeammanagement from '../../assets/images/service/PESS/Training & Team management.webp';
import sap02 from '../../assets/images/platform/Desktop-Mockup_2.webp';
import casestudy01 from '../../assets/images/casestudies/PESS/Safety Assessments of Leading Educational Institutes - Pan India Thumbnail.webp';
import casestudy02 from '../../assets/images/casestudies/PESS/Pan India EHS & Fire Safety Audit for a Leading Insurance Provider Thumbnail.webp';
import casestudy03 from '../../assets/images/casestudies/PESS/Pan India Fire Safety, OHS & Electrical Thermography Audit for a Leading Fashion Retails Thumbnail.webp';
import casestudy04 from '../../assets/images/casestudies/PESS/Security Vulnerability Assessment of Indias largest Alumina Manufacturing Plant Thumbnail.webp';
import casestudy05 from '../../assets/images/casestudies/PESS/Security Risk Assessments for the Manufacturing Plants of one of Indias largest Cement Manufacturer Banner.webp';
import casestudy06 from '../../assets/images/casestudies/PESS/Risk Assessment & Operational Security Support in Zambia for a Coloured Gem Mining Facility Thumbnail.webp';
import casestudy07 from '../../assets/images/casestudies/PESS/Formulation of a Security Management Policy and SOPs for one of Indias leading Commercial Real Estate Developer in Bengaluru Thumbnail.webp';
import casestudy08 from '../../assets/images/casestudies/PESS/Operational Support to Asian Oilfield Services Limited (AOSL) in Iraqi Kurdistan.webp';
import casestudy09 from '../../assets/images/casestudies/PESS/Enterprise-wide Security Assessment for ONGC.webp';
import { Helmet } from 'react-helmet';

const handleDragStart = (e) => e.preventDefault();

const casestudiesItem = [
  <div className="block-box ">
    <img
      src={casestudy09}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <small className="text-gray text-light-100">Risk Assessments </small>
      <br />
      <NavLink exact to="/casestudy_pess_09" className="block-box__title">
        {' '}
        Enterprise-wide Security Assessment for ONGC{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      MitKat alongwith its global partner carried out, for ONGC, an
      enterprise-wide assessment of perimeter surveillance, security and access
      control systems and recommended frameworks for its integration with ERP,
      HR and fire safety systems.
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy08}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <small className="text-gray text-light-100">Risk Assessments </small>
      <br />
      <NavLink exact to="/casestudy_pess_08" className="block-box__title">
        Operational Support to Asian Oilfield Services Limited (AOSL) in Iraqi
        Kurdistan{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      MitKat is vendor and technology agnostic and is able to offer impartial
      and unbiased advice to its clients to design ‘fit for purpose’ and best
      value solutions to suit their business and operational needs.
    </div>
  </div>,

  <div className="block-box ">
    <img
      src={casestudy04}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <small className="text-gray text-light-100">Risk Assessments </small>
      <br />
      <NavLink exact to="/casestudy_pess_04" className="block-box__title">
        {' '}
        Security Vulnerability Assessment of India’s largest Alumina
        Manufacturing Plant{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      MitKat was engaged by India’s leading conglomerate to conduct a Security
      Vulnerability Assessment of its Alumina manufacturing plant in the state
      of Odisha.
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy05}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <small className="text-gray text-light-100">Risk Assessments </small>
      <br />
      <NavLink exact to="/casestudy_pess_05" className="block-box__title">
        {' '}
        Multi-location Security Risk Assessment (SRA) for a Leading Cement Brand{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      MitKat was engaged by one of India’s leading conglomerates to conduct a
      Security Risk Assessment of its Cement manufacturing plants in the states
      of Karnataka, Assam, and Meghalaya.
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy06}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <small className="text-gray text-light-100">Risk Assessments </small>
      <br />
      <NavLink exact to="/casestudy_pess_06" className="block-box__title">
        {' '}
        Operational Support in Challenging Environment (Zambia, Africa){' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      A leading global natural resources company, headquartered in London,
      supplier of responsibly-sourced, coloured gemstones, partnered with
      MitKat, to carry out Security Risk Assessment (SRA) and provide
      operational support at its mining facility in a challenging environment in
      Zambia. MitKat also deployed skilled Control Room Operators and
      Surveillance Specialists - they significantly curtailed loss of precious
      gemstones, uncovered thefts, made recoveries and enhanced deterrence
      levels.
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy07}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <small className="text-gray text-light-100">Risk Assessments </small>
      <br />
      <NavLink exact to="/casestudy_pess_07" className="block-box__title">
        {' '}
        Formulation of Security Policy and Procedures for a Leading Real Estate
        Developer{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      MitKat was engaged by one of India’s leading Commercial Real Estate
      Development companies to streamline the multiple campus-wide security by
      way of a robust security management policy and customised Standard
      Operating Procedures (SOPs).
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy01}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <small className="text-gray text-light-100">
        School Safety Assessment
      </small>
      <br />
      <NavLink exact to="/casestudy_pess_01" className="block-box__title">
        {' '}
        Safety Assessments of Leading Educational Institutes (pan-India){' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      Engagement with more than 90 educational institutions to assess current
      security and safety mechanism against those outlined in the relevant
      school safety manuals and help build a robust security posture which is
      proactive, preventive, and responsive.
    </div>
  </div>,

  <div className="block-box ">
    <img
      src={casestudy02}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <small className="text-gray text-light-100">Fire Safety Assessment</small>
      <br />
      <NavLink exact to="/casestudy_pess_02" className="block-box__title">
        {' '}
        Pan-India environment, health and safety (EHS) & Fire Safety Audit for a
        Leading Insurance Provider{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      Evaluation of base infrastructure against EHS norms, significant fire
      hazards and assessment of existing fire safety equipment including control
      measures.
    </div>
  </div>,
  <div className="block-box ">
    <img
      src={casestudy03}
      onDragStart={handleDragStart}
      className="block-box__img"
      alt=""
    />
    <h5>
      <small className="text-gray text-light-100">Fire Safety Assessment</small>
      <br />
      <NavLink exact to="/casestudy_pess_03" className="block-box__title">
        {' '}
        Pan India Fire Safety, OHS & Electrical Thermography Audit For A Leading
        Fashion Retail Company{' '}
      </NavLink>
    </h5>
    <div className="block-box__by">
      Evaluation of base infrastructure against EHS norms, significant fire
      hazards, electrical hygiene and assessment of existing fire safety
      equipment and relevant documents.
    </div>
  </div>,
];

const Pess = () => {
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>
          Physical, Environmental Security and Safety Risk Consulting | MitKat
          Advisory
        </title>
        <meta
          id="description"
          name="description"
          content="The Security and Safety risk landscape is constantly evolving, multi-directional, increasingly sophisticated, and of increasing velocity and intensity."
        />
        <meta
          id="keywords"
          name="keywords"
          content="risk analysis framework, risk analysis plan, risk analysis process, security risk analysis, risk analysis and security countermeasures,  integration planning, system analysis and design, what is system design, [digital system design], physical security risks"
        />
      </Helmet>
      <Navbar />
      <ServiceHeader
        imgsrc={pessBannerImg}
        linkTitle="Book a Demo"
        title="Physical, Environmental Security and Safety Risk Consulting"
        description="The Security and Safety risk landscape is constantly evolving, multi-directional, increasingly sophisticated, and of increasing velocity and intensity. 
            
            MitKat’s customised risk-analysis based approach to Security and Safety supported by global standards, expertise and experience, and meticulous conduct helps Organisations comprehensively monitor their threat landscape, proactively identify risks, and pre-emptively deploy robust mitigation measures to keep businesses secure."
      />

      <section className="service py-5">
        <div className="container">
          <div className="row">
            <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <div className="block-box">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-12">
                    <img
                      className="block-box__img height-auto"
                      src={dcc}
                      width="100%"
                      alt="Data Center Capability "
                    />
                  </div>
                  <div className="col-lg-6 col-md-6 col-12">
                    <h5 className="px-3">
                      <small className="text-gray text-light-100">
                        Our Special Service{' '}
                      </small>
                      <br />
                      <br />
                      <NavLink exact to="/data-center-capability">
                        Data Center Capability{' '}
                      </NavLink>
                    </h5>
                    <div className="block-box__by p-3">
                      With over a decade of consulting experience, MitKat is
                      able to address robust needs of any Organisation dealing
                      with data centers. Whether your data center is in
                      pre-construction phase, existing phase or needs to gauge
                      the efficiency of countermeasures deployed for security,
                      we have the apt resources and experience you can count on.
                      <br />
                      <NavLink
                        exact
                        to="/data-center-capability"
                        className="main-header__link pt-3"
                      >
                        Read More &nbsp; &nbsp;
                        <FontAwesomeIcon
                          icon={faArrowRight}
                          className="main-header__link__arrow"
                        />
                      </NavLink>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="service bg-gray py-5" id="pri_services">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <p className="mb-5 content__header text-center">
                  Service offering
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={esrm}
                  width="100%"
                  alt="Enterprise Security Risk Management (ESRM)"
                />
                <h5 className="block-box__title px-3">
                  Enterprise Security Risk Management (ESRM)
                </h5>
                <div className="block-box__description p-3">
                  <ul className="unorder-list">
                    <li>Build-Operate-Transfer </li>
                    <li>Policy and Procedure creation </li>
                    <li>
                      Safety and Security manuals, playbooks, and handbooks{' '}
                    </li>
                    <li>Bespoke Security & Safety dashboards </li>
                    <li>Security training and change management </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={assessmentAudit}
                  width="100%"
                  alt="Assessment & Audit "
                />
                <h5 className="block-box__title px-3">Assessment & Audit</h5>
                <div className="block-box__description p-3">
                  <ul className="unorder-list">
                    <li>Threat Vulnerability Risk Assessment (TVRA) </li>
                    <li>Physical & Environmental Security Risk Assessment </li>
                    <li>Security Assessment, Audit and Advisory </li>
                    <li>Fire and Life-Safety Audits </li>
                    <li>
                      App-based Security and Safety assessment tools and
                      dashboards{' '}
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={operationalSupportServices}
                  width="100%"
                  alt="Operational Support Services"
                />
                <h5 className="block-box__title px-3">
                  Operational Support Services
                </h5>
                <div className="block-box__description p-3">
                  <ul className="unorder-list">
                    <li>Data Centre Security </li>
                    <li>Cost optimization and performance improvement </li>
                    <li>Technical Surveillance Countermeasures (TSCM) </li>
                    <li>Executive Protection </li>
                    <li>Event Security </li>
                    <li>Supply Chain Security </li>
                    <li>Bespoke India-entry support </li>
                    <li>Operational support in challenging environment </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={managedSecurityServices}
                  width="100%"
                  alt="Managed Security Services"
                />
                <h5 className="block-box__title px-3">
                  Managed Security Services
                </h5>
                <div className="block-box__description p-3">
                  <ul className="unorder-list">
                    <li>Embedded Security Leadership</li>
                    <li>Security Project Management</li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={trainingTeammanagement}
                  width="100%"
                  alt="Training & Team management"
                />
                <h5 className="block-box__title px-3">
                  Training & Team Management
                </h5>
                <div className="block-box__description p-3">
                  <ul className="unorder-list">
                    <li>Command & Control Centre operators </li>
                    <li>
                      Crisis & Incident management – Training & Simulation{' '}
                    </li>
                    <li>Bespoke Security & Safety training</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="service py-5">
        <div className="container">
          <div className="row">
            <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <div className="block-box">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-12">
                    <h5 className="px-3">
                      <br />
                      <br />
                      Security and safety audit platform
                    </h5>
                    <div className="block-box__by p-3">
                      Assessing and auditing security systems for efficacy,
                      compliance and cost-optimization can be a less laborious
                      process now, with our platform-based Security Risk
                      Assessment tool. Configurable on desktop computers, laptop
                      or tablets, can be easily Customisedfor each type of
                      location with user-friendly drop-down menus. The inputs
                      can include yes/no or multiple-choice type observations,
                      quantifiable ratings, gap analysis and variation from best
                      practices. Graphical analysis and colour coded
                      recommendation lists can provide various ‘CXO snapshot’
                      views.
                      <br /> <br />
                      <NavLink
                        exact
                        to="/book_demo
"
                        rel="noopener noreferrer"
                        className="main-header__link"
                      >
                        Book a Demo &nbsp; &nbsp;
                        <FontAwesomeIcon
                          icon={faArrowRight}
                          className="main-header__link__arrow"
                        />
                      </NavLink>
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6 col-12">
                    <div className="d-flex center">
                      <div className="d-block m-a width-80">
                        <div className="content">
                          <img
                            className=""
                            src={sap02}
                            width="100%"
                            height="auto"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="py-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-center">Case Studies</h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <Casestudies item={casestudiesItem} />
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Pess;
