import React from 'react';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import ServiceHeader from '../../components/ServiceHeader';
import ShowMoreText from 'react-show-more-text';
import igsBannerImg from '../../assets/images/service/Infrastructure & Government Services.webp';
import gstp from '../../assets/images/service/IGS/Government Sector Transformation Projects incl physical and logical security enhancements.webp';
import adcs from '../../assets/images/service/IGS/Aerospace & Defence Consulting Services.webp';
import smartCity from '../../assets/images/service/IGS/Smart City, Smart Cantt and Smart Campus projects.webp';
import sps from '../../assets/images/service/IGS/Specialized products and solutions.webp';
import pmda from '../../assets/images/service/IGS/Project Management & Delivery Assistance.webp';
import digitalForensics from '../../assets/images/service/IGS/Digital Forensics.webp';
import { Helmet } from 'react-helmet';

const Igs = () => {
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Infrastructure & Government Services | MitKat Advisory</title>
        <meta
          id="description"
          name="description"
          content="MitKat's team of professionals deliver advisory services to infrastructure and government clients to improve and de-risk public services, and protect critical"
        />
        <meta
          id="keywords"
          name="keywords"
          content="customized advisory services, how to improve physical and logical security, market entry strategies in India, defence strategy execution, security and safety engineering, security project management and programs, how to set up forensic laboratories,physical security controls, physical security policy, physical security of IT assests, what is physical security and why is it important, physical security measures, physical security audit checklist, what are the security measures, physical security threats and vulnerabilities,physical security levels"
        />
      </Helmet>
      <Navbar />
      <ServiceHeader
        imgsrc={igsBannerImg}
        linkTitle="Book a Demo"
        title="Infrastructure & Government Services"
        description="The operating requirements in the government and defence sectors present unique challenges, and mandate a Customisedapproach to address them.
            
            MitKat’s team of professionals deliver advisory services to infrastructure and government clients to improve and de-risk public services, and protect critical infrastructure. MitKat also advises companies in defence & aerospace, smart city, digital transformation and cyber security, and delivers transformation and process re-engineering projects, including physical and logical security enhancements."
      />

      <section className="service bg-gray py-5" id="pri_services">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <p className="mb-5 content__header text-center">
                  Service offering
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={gstp}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Government Sector Transformation Projects Including Physical &
                  Logical Security Enhancements
                </h5>
                <ShowMoreText
                  lines={3}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={220}
                  keepNewLines={true}
                >
                  The operating requirements in the Government sector present
                  unique challenges, and mandate a Customisedapproach to address
                  them. MitKat’s subject matter experts bring their unique blend
                  of experience with the Government and Defence, and undertake
                  delivery of transformation and process re-engineering
                  projects, including physical and logical security
                  enhancements.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={adcs}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Aerospace & Defence Consulting Services
                </h5>
                <ShowMoreText
                  lines={3}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={277}
                  keepNewLines={true}
                >
                  Aerospace and Defence sector is a focus area for 'Make in
                  India’ and includes establishment of two defence corridors.
                  MitKat can assist clients with a complete solution – from
                  strategy to execution, including but not limited to, market
                  research & India entry strategy, offset advisory and support
                  on specific projects.
                </ShowMoreText>
              </div>
            </div>

            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={smartCity}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Smart City, Smart Cantonment & Smart Campus Projects
                </h5>
                <ShowMoreText
                  lines={3}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={240}
                  keepNewLines={true}
                >
                  MitKat specialises in the security and safety engineering &
                  design (physical and logical) of smart cities, cantonments and
                  campuses, including design & operation of command & control
                  centres.
                </ShowMoreText>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={sps}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Specialized Products & Solutions
                </h5>
                <ShowMoreText
                  lines={4}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={270}
                  keepNewLines={true}
                >
                  MitKat’s team of experts assist in identification, selection
                  and deployment of specialist intel, security and defence
                  solutions for Indian markets and operating conditions.
                </ShowMoreText>
              </div>
            </div>

            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={pmda}
                  width="100%"
                />
                <h5 className="block-box__title px-3">
                  Project Management & Delivery Assistance
                </h5>
                <ShowMoreText
                  lines={3}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={280}
                  keepNewLines={true}
                >
                  MitKat can undertake project management of security projects &
                  programs, or any of the work streams within the project, to
                  facilitate achievement of project objectives.
                </ShowMoreText>
              </div>
            </div>

            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
              <div className="block-box p-0">
                <img
                  className="block-box__img service-offering__img"
                  src={digitalForensics}
                  width="100%"
                />
                <h5 className="block-box__title px-3">Digital Forensics</h5>
                <ShowMoreText
                  lines={4}
                  more="Show more"
                  less="Show less"
                  className="content-css block-box__description p-3"
                  anchorclassName="my-anchor-css-class"
                  expanded={false}
                  width={281}
                  keepNewLines={true}
                >
                  MitKat provides expertise in computer forensics, cyber
                  forensics, handheld device forensics, database forensics and
                  hardware forensics. Key services include assistance in setting
                  up forensic laboratories, forensic data analysis, computer
                  forensics to include disk mapping, data recovery, keyword
                  search & incident response, and expertise & tooling support
                  for investigations.
                </ShowMoreText>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Igs;
