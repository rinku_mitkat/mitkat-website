import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/TWD/Bringing About Behavioural Change Banner.webp';

const Casestudy02 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/training-and-workforce-development"
        title="Bringing About Behavioural Change"
        step1Title="Learning Objective"
        step1Description="Bring about attitudinal and behavioural change among law enforcement professionals while performing citizen services."
        step2Title="Support Extended"
        step2Description="MitKat’s Training Team set up a training programme that provided personalised multi-pronged training content that prepared each individual member of the law enforcement team with communication skills, empathy while while dealing with citizens, particularly the women, the children and the elderly,  stress handling and self-management skills which equipped them mentally to deal with all challenges that come up when on duty for long hours and being first response teams which required multiple skills."
        step3Title="Outcome"
        step3Description="A seamless and well-integrated training programme that has created professional security personnel capable of handling a multi-dimensional role created healthy, diligent professional security and law enforcement officials,  with better attitude and empathy, capable of handling citizen services, incident and emergency response roles more efficiently and effectively."
      />
      <Footer />
    </>
  );
};

export default Casestudy02;
