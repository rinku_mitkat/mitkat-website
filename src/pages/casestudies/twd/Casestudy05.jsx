import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/TWD/Inculcating a Security Safety Culture Within an Organisation banner.webp';

const Casestudy05 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/training-and-workforce-development"
        title="Inculcating a Security Safety Culture Within an Organisation"
        step1Title="Learning Objective"
        step1Description="Bringing about behavioural and attitudinal changes that ensure employees understand the importance of security/safety protocols and company policies that will ensure their own safety and further company productivity."
        step2Title="Support Extended"
        step2Description="MitKat’s Training Team developed specific activities customised to create widespread awareness on varied topics such as employee safety, cyber safety, first aid, fire safety, workplace safety etc. They created fun activities like CPR Marathon, Quiz & Street plays which highlighted importance of security/safety protocols. They also created handbooks, standees, infographics reinforcing the same learnings."
        step3Title="Outcome"
        step3Description="Informed employees who understood the reasons for the security/safety protocols and how it impacts them as well as the organisation. An attitudinal change within the organisation that puts security/safety first."
      />
      <Footer />
    </>
  );
};

export default Casestudy05;
