import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/TWD/Addressing Womens Safety Concerns banner.webp';

const Casestudy01 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/training-and-workforce-development"
        title="Addressing Women’s Safety Concerns"
        step1Title="Learning Objective"
        step1Description="To empower women employees by teaching them practical methods of personal safety and self-defence."
        step2Title="Support Extended"
        step2Description="MitKat’s Pan-India Women’s Team conducted a highly curated & customised training session across all geographies focussing on local conditions to make women aware of the various threats they faced and giving tips on safety awareness along with training on physical safety and self-defence."
        step3Title="Outcome"
        step3Description="Creation of seasoned & trained women employees, well able to take care of and protect themselves. They imbibe the concept that for every Individual “Our Safety Lies  In Our Hands”. "
      />
      <Footer />
    </>
  );
};

export default Casestudy01;
