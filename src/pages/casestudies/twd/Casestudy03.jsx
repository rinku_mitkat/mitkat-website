import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/TWD/Addressing Unconscious Bias Through e-Learning Modules Banner.webp';

const Casestudy05 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/training-and-workforce-development"
        title="Addressing Unconscious Bias Through e-Learning Modules"
        step1Title="Learning Objective"
        step1Description="To help understand underlying beliefs that are the foundation of stereotypes, prejudice and discrimination."
        step2Title="Support Extended"
        step2Description="MitKat’s Training Team created an e-Learning module incorporating an approach to help participants in identifying and addressing unconscious bias."
        step3Title="Outcome"
        step3Description="Employees who are able to identify unconscious bias, can make changes to ensure that bias does not impact decisions and performance."
      />
      <Footer />
    </>
  );
};

export default Casestudy05;
