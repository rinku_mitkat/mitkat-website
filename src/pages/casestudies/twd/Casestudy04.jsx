import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/TWD/Online Masterclass Series banner.webp';

const Casestudy04 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/training-and-workforce-development"
        title="Online Masterclass Series"
        step1Title="Learning Objective"
        step1Description="Creating seasoned and trained security professionals with an in-depth understanding of the roles and responsibilities of key risk functions."
        step2Title="Support Extended"
        step2Description="MitKat’s Training Experts have created a curated, interactive, role specific series of knowledge sessions that address all aspects of Corporate Security, Intel and Business Continuity / Resiliency. Leaders and subject matter experts from MitKat and other industry experts deliver these programmes on line (usually over two two days). These programs have been very popular during Covid-induced lockdown and ‘remote working’ times. "
        step3Title="Outcome"
        step3Description="With the customised Online Masterclass Series, a team of work-ready security professionals have been created who can handle a variety of Corporate Security, Intel, Cyber Security, BCM and Crisis Management roles effectively. MitKat trained professionals do well as intel analysts, and are well-equipped to handling Command & Control Centre operations."
      />
      <Footer />
    </>
  );
};

export default Casestudy04;
