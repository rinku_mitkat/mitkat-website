import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import step1Icon from '../../../assets/images/casestudies/client.webp';
import step2Icon from '../../../assets/images/casestudies/description.webp';
import step3Icon from '../../../assets/images/casestudies/result.webp';
import bannerImg from '../../../assets/images/casestudies/SDCPM/Security Design Consulting Services for Greenfield Corporate Park Project Banner.webp';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
const Casestudy05 = () => {
  return (
    <>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" alt="Casestudy Image" />
        </div>
        <div className="col-12 col-md-10 col-lg-10">
          <div className="service-description">
            <div className="display-block m-a">
              <div className="content">
                <NavLink
                  exact
                  to="/security-design-consulting-and-project-management"
                  className="main-header__link"
                >
                  Back to Casestudies &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br />
                <h1 className="my-3 main-header__header-font text-capitalize">
                  Security Design Consulting Services for Greenfield Corporate
                  Park Project
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="py-5">
        <div className="container-fluid blue-bg">
          <div className="">
            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center bottom">
                <div className="circle">
                  {' '}
                  <img src={step1Icon} />{' '}
                </div>
              </div>
              <div className="col-8">
                <h5>Business challenges</h5>

                <ul>
                  <li>
                    {' '}
                    Client has appointed MitKat as Security Consultant for its
                    two greenfield/ fit out projects in SEZ areas in Bangalore{' '}
                  </li>
                </ul>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner top-right"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner left-bottom"></div>
              </div>
            </div>

            <div className="row align-items-center justify-content-end how-it-works">
              <div className="col-8">
                <h5>Detailed scope of work / solution proposed:</h5>
                <p>MitKat has executed the project in following stages:</p>
                <ul>
                  <li>
                    {' '}
                    Threat, Risk and Vulnerability Assessment (TRVA) – The TRVA
                    is divided into categories such as, crime, safety, public
                    disorder, natural disaster and terror. The assessment aims
                    to evaluate threats (likelihood) and risks (consequences)
                    for each of these categories by surveying the geographical
                    areas of the site In each geographical area, MitKat experts
                    assessed the people, assets and technological security
                    domains.{' '}
                  </li>
                  <li>
                    {' '}
                    Concept Design Recommendations – Based o the TRVA outcomes
                    and in agreement with the Client, MitKat prepared and
                    practical recommendations to mitigate the security gaps
                    identified to ensured that in each geographical area the
                    security objectives of detection, deterrence, evaluation and
                    protection are achieved.{' '}
                  </li>
                  <li>
                    {' '}
                    Master Security Plan (MSP) –Master security plan has
                    prepared focusing on how the concept will integrate
                    efficiently within the environment, personnel and property.
                    The detailed report included how the full infrastructure
                    would work as an integrated security management system
                    taking into account all the relevant threats and risks to
                    design a holistic cost-effective yet efficient security
                    plan. It also included a technical design and layout of a
                    control room that is organized to gather, process, analyze,
                    display and disseminate planning and operational data.
                  </li>
                  <li>
                    {' '}
                    Preparation of Tender – MitKat then developed RFPs
                    comprising tender documents, bill of quantities and
                    technical specifications for each security requirement,
                    involving the client in all aspects of the tender
                    preparations to ensure that the RFPs and documents match the
                    client’s official policies.
                  </li>
                  <li>
                    {' '}
                    Selection of the vendor – Once all the technical
                    specifications and tender documents were completed, MitKat
                    assisted the client in the evaluation process of proposals
                    received by the participating vendors. Following the
                    presentations, MitKat presented a comparative analysis and
                    assisted the client in the selection of the vendor for
                    implementation.{' '}
                  </li>
                  <li>
                    {' '}
                    Supervision, testing and commissioning – MitKat controlled
                    and supervised the chosen vendor in overseeing the
                    installation and testing of security equipment while
                    representing the client to ensure that all aspects of the
                    security system have been installed as per the
                    specifications and MSP.
                  </li>
                  <li>
                    {' '}
                    Training – After taking everything into consideration,
                    MitKat prepared Standard Security Operating Procedures (SOP)
                    with clear instructions on required actions during routine
                    and emergency. MitKat also provided periodic training to
                    security teams, management and full staff on physical
                    security concepts, security awareness, evacuation plan and
                    drills.{' '}
                  </li>
                </ul>
              </div>
              <div className="col-2 text-center full">
                <div className="circle">
                  <img src={step2Icon} />
                </div>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner right-bottom"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner top-left"></div>
              </div>
            </div>

            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center top">
                <div className="circle">
                  <img src={step3Icon} />
                </div>
              </div>
              <div className="col-8">
                <h5>Benefits</h5>

                <ul>
                  <li> Both the projects completed in the specified time </li>{' '}
                  <li>
                    {' '}
                    In the recent interaction with Client for some new
                    opportunities Client appraise nd acknowledge MitKat for the
                    work delivered and is interested to consider MitKat for
                    further association.{' '}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Casestudy05;
