import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import step1Icon from '../../../assets/images/casestudies/client.webp';
import step2Icon from '../../../assets/images/casestudies/description.webp';
import step3Icon from '../../../assets/images/casestudies/result.webp';
import bannerImg from '../../../assets/images/casestudies/SDCPM/Design of National Security Command Room (NSCR) for Indian Multi-National conglomerate Banner.webp';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
const Casestudy01 = () => {
  return (
    <>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" alt="Casestudy Image" />
        </div>
        <div className="col-12 col-md-10 col-lg-10">
          <div className="service-description">
            <div className="display-block m-a">
              <div className="content">
                <NavLink
                  exact
                  to="/security-design-consulting-and-project-management"
                  className="main-header__link"
                >
                  Back to Casestudies &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br />
                <h1 className="my-3 main-header__header-font text-capitalize">
                  Envision and Design of National Security Control Room (NSCR)
                  for a Leading Indian Conglomerate
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="py-5">
        <div className="container-fluid blue-bg">
          <div className="">
            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center bottom">
                <div className="circle">
                  {' '}
                  <img src={step1Icon} />{' '}
                </div>
              </div>
              <div className="col-8">
                <h5>Business challenges</h5>
                <ul>
                  <li>
                    {' '}
                    With continuously advancing business and a large number of
                    facilities, the client felt the requirement of building a
                    National Security Control Room for better visibility in
                    operations and security{' '}
                  </li>
                  <li>
                    {' '}
                    Client required the NSCR to deliver video surveillance
                    monitoring & control, various advanced video analytics,
                    access control management, intrusion alarm management, logs
                    & reporting, unified dash boarding, PIDS, SOP driven process
                    management and a coordinated & calibrated response to
                    business{' '}
                  </li>
                </ul>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner top-right"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner left-bottom"></div>
              </div>
            </div>

            <div className="row align-items-center justify-content-end how-it-works">
              <div className="col-8">
                <h5>Detailed scope of work / solution proposed:</h5>
                <ul>
                  <li>
                    {' '}
                    As the business and its operations were varying in its form
                    and spread, the group-wide standardization of the people,
                    processes and technology was the first step of the
                    assignment undertaken by MitKat{' '}
                  </li>
                  <li>
                    {' '}
                    On a sampling basis, the MitKat team along with the Client's
                    team physically surveyed some of the sites based on its
                    vastness{' '}
                  </li>
                  <li>
                    {' '}
                    Based on the inputs received during site survey and
                    interface with various stakeholders, MitKat prepared a
                    security domain maturity model to define the as-is state of
                    the security posture of the Client's business{' '}
                  </li>
                  <li>
                    {' '}
                    Once as-is state was defined, MitKat extensively worked with
                    the Client’s project, user and management team to finalise
                    the to-be state of the group security posture{' '}
                  </li>
                  <li>
                    {' '}
                    List of processes for collaboration, technology gaps and
                    initiatives required to close the gaps, models and
                    parameters to sizing the solution (hardware) and deployment
                    options with pros & cons were defined subsequently{' '}
                  </li>
                  <li>
                    {' '}
                    Various architectures defining technology, deployment,
                    Organisation & skill matrix and implementation roadmap were
                    prepared and submitted to the Client{' '}
                  </li>
                  <li>
                    {' '}
                    On the basis of the whole exercise, a business case was then
                    prepared and presented to the Client's Chairing Committee
                    and the stakeholders for approval{' '}
                  </li>
                  <li>
                    {' '}
                    A RFP with detailed techno-functional specifications, BOQ,
                    drawings and estimates was prepared and submitted to the
                    Client to go to market and initiate the tendering process to
                    identify a suitable works contractor{' '}
                  </li>
                </ul>
              </div>
              <div className="col-2 text-center full">
                <div className="circle">
                  <img src={step2Icon} />
                </div>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner right-bottom"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner top-left"></div>
              </div>
            </div>

            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center top">
                <div className="circle">
                  <img src={step3Icon} />
                </div>
              </div>
              <div className="col-8">
                <h5>Benefits</h5>

                <p>
                  Since the deployment of the project, the Client can enjoy the
                  convenience of a unified security management system. The
                  system was designed keeping a simple structure, user friendly
                  interface, and reliable reporting to ensure greater
                  productivity and a decrease in labour. The functionalities
                  were defined in a manner that allowed the Client to easily
                  pinpoint security incidents. All the aspects of a smooth,
                  stable, easy-to-use system to produce reliable results were
                  taken into consideration in designing.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Casestudy01;
