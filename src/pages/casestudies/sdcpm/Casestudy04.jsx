import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import step1Icon from '../../../assets/images/casestudies/client.webp';
import step2Icon from '../../../assets/images/casestudies/description.webp';
import step3Icon from '../../../assets/images/casestudies/result.webp';
import bannerImg from '../../../assets/images/casestudies/SDCPM/Security Design Consulting Services of its Globally Second Largest Campus for a British Baker Banner.webp';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
const Casestudy04 = () => {
  return (
    <>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" alt="Casestudy Image" />
        </div>
        <div className="col-12 col-md-10 col-lg-10">
          <div className="service-description">
            <div className="display-block m-a">
              <div className="content">
                <NavLink
                  exact
                  to="/security-design-consulting-and-project-management"
                  className="main-header__link"
                >
                  Back to Casestudies &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br />
                <h1 className="my-3 main-header__header-font text-capitalize">
                  Security Design Consulting & Project management Services for a
                  Multi-National Bank
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="py-5">
        <div className="container-fluid blue-bg">
          <div className="">
            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center bottom">
                <div className="circle">
                  {' '}
                  <img src={step1Icon} />{' '}
                </div>
              </div>
              <div className="col-8">
                <h5>Business challenges</h5>
                <p> Client appointed MitKat as its security consultant for: </p>

                <ul>
                  <li>
                    {' '}
                    Assist Client in multiple projects initiatives in the domain
                    of security systems and controls.{' '}
                  </li>
                  <li>
                    {' '}
                    Co-ordination and delivery of security systems projects with
                    the responsibility for monitoring project execution, project
                    reporting, governance and controls and working closely with
                    the Corporate Security teams{' '}
                  </li>
                </ul>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner top-right"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner left-bottom"></div>
              </div>
            </div>

            <div className="row align-items-center justify-content-end how-it-works">
              <div className="col-8">
                <h5>Detailed scope of work / solution proposed:</h5>
                <ul>
                  <li>
                    {' '}
                    Risk assessment: Advise Client on the security plan that
                    will include recommendations on design solutions and advise
                    on risk mitigation strategies through physical/ electronic
                    security and controls.
                  </li>
                  <li>
                    {' '}
                    Concept design: Provide inputs towards the design of
                    security systems along with budgetary estimates{' '}
                  </li>

                  <li>
                    {' '}
                    Detail designing: Detailing of the technical specifications,
                    preparation of BOQ, optimization of facility layouts with
                    proposed security system, preparation of approximate
                    estimates.{' '}
                  </li>
                  <li>
                    {' '}
                    Tendering: Assist Client for onboarding of vendor for
                    security system implementation, review and summaries the
                    submittals of bidders, bidders query handling during the
                    tendering process{' '}
                  </li>
                  <li>
                    {' '}
                    Project management support: A. Review and confirm the
                    technical solution/ drawings provided by security vendors/
                    OEMs. <br />
                    B. Acting as the key interface between the Project and
                    Corporate Security team. <br />
                    C. During the testing phase, manage and coordinate with the
                    other associated work streams and ensure that any
                    inter-dependencies are pre-empted and provisioned for ahead
                    of time <br />
                    D. Successfully handing over the system to the business and
                    periodical review & support
                  </li>
                </ul>
              </div>
              <div className="col-2 text-center full">
                <div className="circle">
                  <img src={step2Icon} />
                </div>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner right-bottom"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner top-left"></div>
              </div>
            </div>

            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center top">
                <div className="circle">
                  <img src={step3Icon} />
                </div>
              </div>
              <div className="col-8">
                <h5>Benefits</h5>
                <p>
                  {' '}
                  During MitKat's engagement with the Client for nearly two
                  years clearly Client has fetch following benefits :{' '}
                </p>
                <ul>
                  <li>
                    {' '}
                    Project completion and handover within the specified time{' '}
                  </li>{' '}
                  <li> Ensuring no cost over runs </li>{' '}
                  <li> Periodical overview and support from experts </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Casestudy04;
