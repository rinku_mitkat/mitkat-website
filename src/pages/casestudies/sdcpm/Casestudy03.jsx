import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import step1Icon from '../../../assets/images/casestudies/client.webp';
import step2Icon from '../../../assets/images/casestudies/description.webp';
import step3Icon from '../../../assets/images/casestudies/result.webp';
import bannerImg from '../../../assets/images/casestudies/SDCPM/Security Design Consulting Services of its Globally Second Largest Campus for a British Baker banner image.webp';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
const Casestudy03 = () => {
  return (
    <>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" alt="Casestudy Image" />
        </div>
        <div className="col-12 col-md-10 col-lg-10">
          <div className="service-description">
            <div className="display-block m-a">
              <div className="content">
                <NavLink
                  exact
                  to="/security-design-consulting-and-project-management"
                  className="main-header__link"
                >
                  Back to Casestudies &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br />
                <h1 className="my-3 main-header__header-font text-capitalize">
                  Security Design Consulting for a Leading Global Bank
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="py-5">
        <div className="container-fluid blue-bg">
          <div className="">
            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center bottom">
                <div className="circle">
                  {' '}
                  <img src={step1Icon} />{' '}
                </div>
              </div>
              <div className="col-8">
                <h5>Business challenges</h5>
                <ul>
                  <li>
                    {' '}
                    Client has decided to build a greenfield office of
                    comprising towers having 1.1 Million Sq ft of carpet area at
                    Pune, Maharashtra, India{' '}
                  </li>
                  <li>
                    {' '}
                    Client wishes the campus environment should be user
                    friendly, flexible, open, non-privacy intrusive but secured{' '}
                  </li>

                  <li>
                    {' '}
                    The client is developing a greenfield office complex in
                    Pune, India - its second largest campus globally (a green,
                    user and environment-friendly, secure) The project has
                    stringent timelines and strict quality parameters{' '}
                  </li>
                </ul>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner top-right"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner left-bottom"></div>
              </div>
            </div>

            <div className="row align-items-center justify-content-end how-it-works">
              <div className="col-8">
                <h5>Detailed Scope of work / solution proposed:</h5>
                <ul>
                  <li>
                    {' '}
                    MitKat undertook a study of the Client's standard corporate
                    security guidelines & processes along with “a requirement
                    and expectation gathering and understanding of constraints
                    exercise” with the Client and other project stakeholders
                    involved{' '}
                  </li>
                  <li>
                    {' '}
                    A Blast Impact Assessment (BIA) study was done based on the
                    structural drawings to verify the impact in case of any
                    blast/ high-impact in the stand-off, pick-up/ drop-point,
                    main entry/ exit zones on structural members and facade{' '}
                  </li>
                  <li>
                    {' '}
                    A Concept Report/ Preliminary Design Report (PDR) was
                    prepared covering various threats, vulnerabilities and risk
                    scenarios and proposing mitigation measures to counter such
                    scenarios and submitted to the Client for approval{' '}
                  </li>
                  <li>
                    {' '}
                    Based on the approval and guidance received from the Client
                    on PDR, the detailed physical/ electronic security
                    infrastructure design comprising video surveillance, access
                    control, intrusion detection, mass communication, alarm
                    management and system integration for effective command and
                    control was undertaken and delivered in the form of an RFP
                    for vendor selection. This RFP had technical specifications,
                    BOQ, tender drawings and budgetary estimates{' '}
                  </li>
                  <li>
                    {' '}
                    MitKat then helped the Client in the tendering and vendor
                    onboarding process by participating in handling bidder’s
                    queries in the RFP, review of bidders technical submittals,
                    preparing and submitting the findings/observations in the
                    form of a summary to the Client to enable further
                    decision-making{' '}
                  </li>
                  <li>
                    {' '}
                    Post onboarding of the bidder, MitKat was engaged in
                    reviewing and signing-off, of the vendor designs and
                    drawings submitted, reviewing of the implementation work
                    on-site, helping with vendor-queries and participating in
                    the regular update meeting with the Client by acting as an
                    interface between the vendor and the Client{' '}
                  </li>
                  <li>
                    {' '}
                    When the project was in it’s handing-over phase from
                    vendors, MitKat engaged with the Client for handing-over
                    support, documentation and signing off with the vendor{' '}
                  </li>
                </ul>
              </div>
              <div className="col-2 text-center full">
                <div className="circle">
                  <img src={step2Icon} />
                </div>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner right-bottom"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner top-left"></div>
              </div>
            </div>

            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center top">
                <div className="circle">
                  <img src={step3Icon} />
                </div>
              </div>
              <div className="col-8">
                <h5>Benefits</h5>
                <ul>
                  <li>
                    {' '}
                    Two of the four phases of the project have been successfully
                    commissioned, and the premises are being used by the client
                  </li>
                  <li>
                    {' '}
                    MitKat worked collaboratively and closely with the customer
                    to meet stringent delivery timelines and ensure adherence to
                    quality parameters
                  </li>
                  <li>
                    {' '}
                    The Client is happy, has renewed the contract and we now
                    have multiple engagements{' '}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Casestudy03;
