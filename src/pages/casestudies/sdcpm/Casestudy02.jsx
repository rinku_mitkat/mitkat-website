import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import step1Icon from '../../../assets/images/casestudies/client.webp';
import step2Icon from '../../../assets/images/casestudies/description.webp';
import step3Icon from '../../../assets/images/casestudies/result.webp';
import bannerImg from '../../../assets/images/casestudies/SDCPM/Security Technology Transformation of Indias Largest IT & Consulting Company banner image.webp';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
const Casestudy02 = () => {
  return (
    <>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" alt="Casestudy Image" />
        </div>
        <div className="col-12 col-md-10 col-lg-10">
          <div className="service-description">
            <div className="display-block m-a">
              <div className="content">
                <NavLink
                  exact
                  to="/security-design-consulting-and-project-management"
                  className="main-header__link"
                >
                  Back to Casestudies &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br />
                <h1 className="my-3 main-header__header-font text-capitalize">
                  Security Technology Transformation for a Leading Indian IT MNC
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="py-5">
        <div className="container-fluid blue-bg">
          <div className="">
            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center bottom">
                <div className="circle">
                  {' '}
                  <img src={step1Icon} />{' '}
                </div>
              </div>
              <div className="col-8">
                <h5>Business challenges</h5>
                <ul>
                  <li>
                    {' '}
                    The Client engaged MitKat to carry out a strategic
                    transformation of its security function across the
                    enterprise with a view to improving performance and
                    productivity, streamlining processes, carrying out
                    techno-infra interventions, optimising costs, improving user
                    experience and documenting security, technology and
                    emergency processes.{' '}
                  </li>
                  <li>
                    {' '}
                    The security technology transformation assignment comprised
                    of 80% of the Client's existing sites and remaining
                    bare-shell/ fitout designs at new locations.{' '}
                  </li>
                  <li>
                    {' '}
                    Before MitKat got onboarded as Client's Global Security
                    Consultant, the Client had systems of varying make & models
                    placed at various sites. Based on the demonstrated success
                    of a pilot study conducted, the Client hired MitKat to
                    deliver the larger package of the Project comprising three
                    parts:
                    <br />
                    i. Enterprise wide review of physical/ electronic security
                    system comprising video surveillance and access control
                    technologies and integration of both the systems in command
                    & control room (techno-infra interventions and project
                    management).
                    <br />
                    ii. Preparation of blueprint and provide lean design for
                    physical security vendor performance optimisation resulting
                    in better compliance and savings.
                    <br />
                    iii. Security process improvements (eight key processes were
                    identified and redefined which resulted in cost optimisation
                    as well as performance and productivity improvement).{' '}
                  </li>
                </ul>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner top-right"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner left-bottom"></div>
              </div>
            </div>

            <div className="row align-items-center justify-content-end how-it-works">
              <div className="col-8">
                <h5>Detailed scope of work / solution proposed:</h5>
                <ul>
                  <li>
                    {' '}
                    MitKat started this project by undertaking a need assessment
                    exercise, which enabled us to gain a thorough understanding
                    of the Client's business, operations and facilities
                    requirements . It also helped us to identify the Client's
                    security concerns, issues, threats and vulnerabilities. The
                    need assessment served as the basis for our recommendations
                    and ultimately resulted in a security system design that
                    addressed the Client's unique security requirements.{' '}
                  </li>
                  <li>
                    {' '}
                    MitKat carried out the sample preliminary site assessment of
                    the Client's mega sites. Based on this, a preliminary draft
                    report was created by grading the sites and finalising the
                    functional requirements of the systems.{' '}
                  </li>
                  <li>
                    {' '}
                    An exercise was undertaken to standardise the systems and to
                    freeze the technical specifications.{' '}
                  </li>
                  <li>
                    {' '}
                    The physical security system design was undertaken by the
                    MitKat team after physically visiting all the 55 locations
                    of the Client's campuses in India and a virtual walkthrough
                    of 22 sites abroad through respective site security
                    coordinators.{' '}
                  </li>
                  <li>
                    {' '}
                    The integration roadmap of both video surveillance and
                    access control at the centralised command and conntrol room
                    was prepared. The reviews were conducted for various COTS
                    products available in the market to identify the best and
                    most suitable system for the Client's business.{' '}
                  </li>
                  <li>
                    {' '}
                    The system requirement document was prepared comprising
                    site/ facility layouts, updated with physical security
                    systems superimposed with BOQs.{' '}
                  </li>
                  <li>
                    {' '}
                    MitKat then assisted the Client in the tendering process for
                    onboarding of a works contractor.{' '}
                  </li>

                  <li>
                    {' '}
                    Based on our project experience, product knowledge and
                    familiarity with industry standards and best practices,
                    MitKat helped the Client to develop standard Security
                    Operation Procedures (SOP) for security process
                    optimisation, management and governance.{' '}
                  </li>
                </ul>
              </div>
              <div className="col-2 text-center full">
                <div className="circle">
                  <img src={step2Icon} />
                </div>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner right-bottom"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner top-left"></div>
              </div>
            </div>

            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center top">
                <div className="circle">
                  <img src={step3Icon} />
                </div>
              </div>
              <div className="col-8">
                <h5>Benefits</h5>
                <ul>
                  <li>
                    {' '}
                    Physical security technology standardization and
                    up-gradation across the enterprise fetched better return on
                    investment for Client (optimization of ~ 20% as per our
                    estimates).{' '}
                  </li>
                  <li>
                    The Client had an enhanced user experience, site security
                    and improved productivity.{' '}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Casestudy02;
