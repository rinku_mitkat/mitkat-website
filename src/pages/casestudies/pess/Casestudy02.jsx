import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/PESS/Pan India EHS & Fire Safety Audit for a Leading Insurance Provider Banner.webp';

const Casestudy02 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/physical-environmental-security-and-safety-risk-consulting"
        title="Pan India EHS & Fire Safety Audit for a Leading Insurance Provider"
        step1Title="Client’s challenge"
        step1Description="environment, health and safety (EHS) and fire and life safety, with a view to optimising their emergency response capability and maintaining a higher standard and assurance of employee safety. "
        step2Title="Support extended"
        step2Description="MitKat’s Fire Safety Assessment team conducted a pan-India EHS and fire safety assessment, wherein all potential risks were highlighted along with mitigation measures and recommendations."
        step3Title="Result"
        step3Description="The pan-India assessment highlighted major EHS and fire vulnerabilities and risks, enabling the client to focus on implementing controls (people, process and technological) for mitigation of these risks. Implementation and oversight of control measures at each of the sites and a well-defined, documented and implemented emergency response plan (ERP) has led to improved levels of safety and assurance in the organisation."
      />
      <Footer />
    </>
  );
};

export default Casestudy02;
