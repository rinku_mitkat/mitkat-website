import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/PESS/Risk Assessment & Operational Security Support in Zambia for a Coloured Gem Mining Facility.webp';

const Casestudy06 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/physical-environmental-security-and-safety-risk-consulting"
        title="Operational Support in Challenging Environment (Zambia, Africa)  "
        step1Title="Client’s challenge"
        step1Description="The client had an operational Ruby mine which was perhaps the largest in the world. Security of mining staff, mined highly-valued items, mining waste, pilferage, theft, crime etc. were extant concerns of the client. "
        step2Title="Support extended"
        step2Description="MitKat’s team of expat consultants grasped the prevailing on ground realities, carried out Security Risk Assessment (SRA), highlighted gaps and made recommendations to the client. The team understood and outlined the need for surveillance and screening operations, and carried out training need analysis (TNA) for operators. Deployment of eight skilled expat operators was carried out and their performance continually monitored. "
        step3Title="Result"
        step3Description="The client’s Security Control Room’s functioning was significantly enhanced through training and deployment of specially skilled Control Room monitoring team (8 personnel). The security team made substantial recoveries and uncovered thefts of precious gemstones. Significant losses were prevented. The security posture became more robust and deterrence levels were enhanced significantly."
      />
      <Footer />
    </>
  );
};

export default Casestudy06;
