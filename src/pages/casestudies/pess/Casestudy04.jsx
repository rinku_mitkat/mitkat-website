import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/PESS/Security Vulnerability Assessment of Indias largest Alumina Manufacturing Plant.webp';

const Casestudy04 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/physical-environmental-security-and-safety-risk-consulting"
        title="Security Vulnerability Assessment of India’s largest Alumina Manufacturing Plant"
        step1Title="Client’s challenge"
        step1Description="The client needed to significantly enhance its security posture by way of a holistic assessment. A range of multi-faceted threats were being faced in the challenging geographic location ranging from social opposition, high and often negative external stakeholder interest, theft, crime, incitement citing environmental concerns, misleading of public etc. All these not only created adverse publicity, but also led to fear and low-morale among the staff, workers and their families. "
        step2Title="Support extended"
        step2Description="MitKat’s team of experienced consultants conducted an elaborate, end-to-end Security Vulnerability Assessment as per the ‘American Petroleum Institute’ (API) framework and methodology."
        step3Title="Result"
        step3Description="The client was given a detailed view covering a multitude of threat vectors and categories, against which readiness was to be ensured. Each asset of the manufacturing plant was mapped from the security perspective relative utility to the manufacturing process, impact on financials, environment, publicity, compliances etc. Further, the degree of adversary interest was quantified. Deployed security, including that by government and private agencies, was evaluated as per identified threat and gaps identified. A new Security Concept was created to substantially upgrade security of the Plant by way of process improvement, training need assessment (TNA) for staff, technology infusion etc. A roadmap of prioritized activities to obviate risks in a time-bound manner was drawn up in consultation with the client. Areas of optimisation of new surveillance and access control systems were identified to bolster security.MitKat’s recommendations led to improved deterrence and robust security, as also significantly raised the morale of the workforce."
      />
      <Footer />
    </>
  );
};

export default Casestudy04;
