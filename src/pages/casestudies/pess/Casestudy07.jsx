import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/PESS/Formulation of a Security Management Policy and SOPs for one of Indias leading Commercial Real Estate Developer in Bengaluru Banner.webp';

const Casestudy07 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/physical-environmental-security-and-safety-risk-consulting"
        title="Formulation of Security Policy and Procedures for a Leading Real Estate Developer"
        step1Title="Client’s challenge"
        step1Description="The client had engaged multiple security guard providing agencies and adhoc surveillance equipment installation firms to provide security. This format was observed to be disjointed, ineffective in the absence of a laid-down security structure."
        step2Title="Support extended"
        step2Description="MitKat conducted numerous safety and security assessments for multiple sites keeping crisis management as an end objective. Further, MitKat engaged with property management teams and building management teams to identify responders and protocols, keeping in mind a multi-tenant operation. An Organisational governance program was created which was built on SOPs and incident matrices, for each individual site, as well as for multiple sites, in the city of Bengaluru. A document highlighting governance, threat assessments, responsibility matrix, call trees, check lists, communication guidelines, key SOP’s and relevant emergency response plans, was created."
        step3Title="Result"
        step3Description="The exercise enabled the Organisation to establish key relevant risks. The client was able to communicate these plans to other tenants to achieve sync. MitKat delivered a framework which could be used by the client to replicate in other regions. Further, MitKat identified stakeholders that could lead and sustain the program year on year."
      />
      <Footer />
    </>
  );
};

export default Casestudy07;
