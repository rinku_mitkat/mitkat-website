import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import bannerImg from '../../../assets/images/casestudies/PESS/Enterprise-wide Security Assessment for ONGC banner.webp';
import step1Icon from '../../../assets/images/casestudies/client.webp';
import step2Icon from '../../../assets/images/casestudies/description.webp';
import step3Icon from '../../../assets/images/casestudies/result.webp';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
const Casestudy09 = () => {
  return (
    <>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" alt="Casestudy Image" />
        </div>
        <div className="col-12 col-md-10 col-lg-10">
          <div className="service-description">
            <div className="display-block m-a">
              <div className="content">
                <NavLink
                  exact
                  to="/physical-environmental-security-and-safety-risk-consulting"
                  className="main-header__link"
                >
                  Back to Casestudies &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br />
                <h1 className="my-3 main-header__header-font text-capitalize">
                  Enterprise-wide Security Assessment for ONGC
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="py-5">
        <div className="container-fluid blue-bg">
          <div className="">
            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center bottom">
                <div className="circle">
                  {' '}
                  <img src={step1Icon} />{' '}
                </div>
              </div>
              <div className="col-8">
                <h5>The mandate</h5>
                <p>
                  MitKat alongwith its global partner carried out, for ONGC, an
                  enterprise-wide assessment of perimeter surveillance, security
                  and access control systems and recommended frameworks for its
                  integration with ERP, HR and fire safety systems.
                </p>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner top-right"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner left-bottom"></div>
              </div>
            </div>

            <div className="row align-items-center justify-content-end how-it-works">
              <div className="col-8">
                <h5>Methodology adopted</h5>
                <p>
                  After a preliminary workshop for personnel with
                  multi-disciplinary expertise (security technologies, project
                  management, risk assessment etc.), MitKat put together three
                  teams working in parallel (one each for East, West and rest of
                  India) and carried out the security assessments (over 200
                  locations) and made recommendations (site-specific as well as
                  enterprise-wide) to strengthen the overall perimeter security
                  and access control. The team also made recommendations to
                  integrate various technologies, as well integrate with safety,
                  HR and ERP. Illustrative diagram of some of the
                  recommendations are given above.
                </p>
              </div>
              <div className="col-2 text-center full">
                <div className="circle">
                  <img src={step2Icon} />
                </div>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner right-bottom"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner top-left"></div>
              </div>
            </div>

            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center top">
                <div className="circle">
                  <img src={step3Icon} />
                </div>
              </div>
              <div className="col-8">
                <h5> Benefits to ONGC</h5>
                <ul className="unorder-list pt-1">
                  <li>
                    {' '}
                    Improved security posture at each of the sites as well as
                    across the enterprise{' '}
                  </li>
                  <li>
                    {' '}
                    Integration and standardization of security and safety
                    systems and technologies
                  </li>
                  <li> Standardization of security systems and processes</li>
                  <li>
                    {' '}
                    Optimal utilisation of existing assets – cost optimisation{' '}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Casestudy09;
