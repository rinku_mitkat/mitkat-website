import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/PESS/Pan India Fire Safety, OHS & Electrical Thermography Audit for a Leading Fashion Retails Banner.webp';

const Casestudy03 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/physical-environmental-security-and-safety-risk-consulting"
        title="Pan India Fire Safety, OHS & Electrical Thermography Audit For A Leading Fashion Retail Company   "
        step1Title="Client’s challenge"
        step1Description="Client required a Pan India Fire Safety, OHS & Electrical Thermography Audit  "
        step2Title="Support extended"
        step2Description="MitKat fire safety assessment team led the task as per the guidelines mentioned in the National Building Code of India (NBC), Bureau of Indian Standards (BIS) and Best Practices. The team identified the areas of risk with major non-compliance and testing of all installed fire safety equipment was conducted at respective sites to authenticate the safety standards."
        step3Title="Result"
        step3Description="The client was able to understand areas of improvement to safeguard brand reputation, people & assets. This  enabled safe business operations and requisite mitigation measures based on relevant regulations and standards. "
      />
      <Footer />
    </>
  );
};

export default Casestudy03;
