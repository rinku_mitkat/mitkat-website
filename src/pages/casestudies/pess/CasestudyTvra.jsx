import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/PESS/TVRA of An Upcoming Data Center Banner.webp';

const CasestudyTvra = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/data-center-capability"
        title="TVRA of An Upcoming Data Centre"
        step1Title="Client’s challenge"
        step1Description="A multinational telecommunication company based out of London needed Threat, Vulnearbility and Risk Assessment of the upcoming data centre site, pre-construction."
        step2Title="Support extended"
        step2Description="MitKat’s Risk Management team carried out the task using TVRA methodology based on ISO 31000, TIA 942 and other globally accepted frameworks. Extensive consultations and on ground observation of processes were carried out with multiple stakeholders."
        step3Title="Result"
        step3Description="MitKat identified major threats, vulnerabilities and risks to the site and conducted regional risk assessment and recommended effective measures to mitigate risks to acceptable levels in a cost-conscious manner."
      />
      <Footer />
    </>
  );
};

export default CasestudyTvra;
