import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/PESS/Operational Support to Asian Oilfield Services Limited (AOSL) in Iraqi Kurdistan Banner.webp';

const Casestudy08 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/physical-environmental-security-and-safety-risk-consulting"
        title="Operational Support to Asian Oilfield Services Limited (AOSL) in Iraqi Kurdistan  "
        step1Title="Client’s Objective"
        step1Description=" Our client, a Dubai-based oil services company, undertook a challenging 3D wireless seismic project in geographically difficult and socio-politically sensitive region in Middle East for a leading global oil company.  "
        step2Title="Description of the Mandate"
        step2Description="MitKat carried out a risk assessment, assisted in scouting, bid preparation (from security perspective) and represented the client during meetings at the highest level meetings. Post the award of contract, MitKat assisted the customer in induction and set up, provided security leadership at country and project levels and operational support to the client throughout the project (from bidding, scouting, security planning, mobilisation, operation and demobilisation phase). "
        step3Title="Result"
        step3Description="The team worked closely with the client through the project life cycle – from assisting in winning the bid to setting up, maximising production and minimising production. MitKat’s support helped the client to successfully complete the project, establish great reputation in the country and region, leading to award of another prestigious contract in quick succession by another leading global oil company. "
      />
      <Footer />
    </>
  );
};

export default Casestudy08;
