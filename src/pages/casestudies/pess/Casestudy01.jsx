import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import bannerImg from '../../../assets/images/casestudies/PESS/Safety Assessments of Leading Educational Institutes - Pan India Banner.webp';
import step1Icon from '../../../assets/images/casestudies/client.webp';
import step2Icon from '../../../assets/images/casestudies/description.webp';
import step3Icon from '../../../assets/images/casestudies/result.webp';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
const Casestudy01 = () => {
  return (
    <>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" alt="Casestudy Image" />
        </div>
        <div className="col-12 col-md-10 col-lg-10">
          <div className="service-description">
            <div className="display-block m-a">
              <div className="content">
                <NavLink
                  exact
                  to="/physical-environmental-security-and-safety-risk-consulting"
                  className="main-header__link"
                >
                  Back to Casestudies &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br />
                <h1 className="my-3 main-header__header-font text-capitalize">
                  Safety Assessments of Leading Educational Institutes
                  (pan-India)
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="py-5">
        <div className="container-fluid blue-bg">
          <div className="">
            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center bottom">
                <div className="circle">
                  {' '}
                  <img src={step1Icon} />{' '}
                </div>
              </div>
              <div className="col-8">
                <h5>Client’s challenge</h5>
                <p>
                  Insight/ Focus on the aspect of safety and security of
                  children in schools and implementation of stringent and
                  uniform norms across schools in India.
                </p>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner top-right"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner left-bottom"></div>
              </div>
            </div>

            <div className="row align-items-center justify-content-end how-it-works">
              <div className="col-8">
                <h5>Support extended</h5>
                <p>
                  A comprehensive assessment of the entire security systems,
                  infrastructure, technology, Organisational structure, and
                  processes by MitKat, was the first step in this regard. MitKat
                  deployed a team of consultants with vast experience and skills
                  in all aspects of security management and designing technology
                  solutions. The assessments were based on detailed research,
                  on-site surveys, discussions with multiple stakeholders,
                  reference to existing instructions and regulations at local
                  and national level, as well as global best practices (as
                  applicable to Indian and local conditions).
                  <br />
                  The School Safety Assessments encompassed
                  examination-cum-analysis of the following aspects:
                </p>
                <ul className="unorder-list pt-1">
                  <li>
                    {' '}
                    Threat environment (including neighbourhood analysis){' '}
                  </li>
                  <li> Prevention and intervention measures </li>
                  <li> Physical security measures </li>
                  <li> Transportation security arrangements </li>
                  <li>
                    {' '}
                    Support services including facilities, food services,
                    medical, sports and others{' '}
                  </li>
                  <li>
                    {' '}
                    Personal safety (handling angry parents, intervening in
                    fights etc.){' '}
                  </li>
                  <li> Security for special events </li>
                  <li>
                    {' '}
                    Emergency preparedness (including manmade and natural
                    disasters){' '}
                  </li>
                  <li> School security organisation </li>
                  <li> Training and awareness for staff and students </li>
                  <li> Cyber security </li>
                </ul>
              </div>
              <div className="col-2 text-center full">
                <div className="circle">
                  <img src={step2Icon} />
                </div>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner right-bottom"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner top-left"></div>
              </div>
            </div>

            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center top">
                <div className="circle">
                  <img src={step3Icon} />
                </div>
              </div>
              <div className="col-8">
                <h5>Result</h5>
                <p>
                  The assessment led to identification of major threats,
                  vulnerabilities and risks to students, teachers and staff,
                  highlighted the gaps and compliance requirements. The
                  implementation of recommender control measures (people,
                  process and techno-infra interventions) would lead to a more
                  robust, effective, tech-enabled, process-oriented and
                  cost-conscious security architecture and higher level of
                  assurance to stakeholders.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Casestudy01;
