import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/PESS/Security Risk Assessments for the Manufacturing Plants of one of Indias largest Cement Manufacturer.webp';

const Casestudy05 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/physical-environmental-security-and-safety-risk-consulting"
        title="Multi-location Security Risk Assessment (SRA) for a Leading Cement Brand"
        step1Title="Client’s challenge"
        step1Description="The client had three cement manufacturing plants in regions that were traditionally high-risk owing to sensitivities attached with the socio-economic and geo-political nuances. With this background, the client wanted fresh security assessments done, so as to streamline and update security policies and processes, and arrive at specific skills and training to be imparted to security staff. "
        step2Title="Support extended"
        step2Description="MitKat’s team of experienced consultants conducted simultaneous desktop and on-ground security threat assessments in accordance with ISO 310000 supported by tools and techniques of ISO 31010 for each of the three sites. Based on these, MitKat made recommendations on enterprise-wide security polices and processes, and organisation structure, with clear definition of skills and experience levels. Further, recommendations pertaining to security infrastructure, and technology deployment solutions to include CCTV and access management systems, were made to the client, with a view to making the security posture more robust."
        step3Title="Result"
        step3Description="MitKat made specific recommendations on enhancing the capability of the leadership element, as this emerged critical in the high threat environment. Detailed and cost-effective recommendations were made on improving security infrastructure with particular reference to areas where external violent threat was considered significantly potent.MitKat’s recommendations significantly improved deterrence levels, made security posture more robust, less human-centric, more tech-enabled, process-oriented, consistent and business-relevant."
      />
      <Footer />
    </>
  );
};

export default Casestudy05;
