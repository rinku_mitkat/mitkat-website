import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import bannerImg from '../../../assets/images/casestudies/MSS/Security Project Manager for a leading Global Financial Sector Conglomerate banner.webp';
import step1Icon from '../../../assets/images/casestudies/client.webp';
import step2Icon from '../../../assets/images/casestudies/description.webp';
import step3Icon from '../../../assets/images/casestudies/result.webp';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
const Casestudy05 = () => {
  return (
    <>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" alt="Casestudy Image" />
        </div>
        <div className="col-12 col-md-10 col-lg-10">
          <div className="service-description">
            <div className="display-block m-a">
              <div className="content">
                <NavLink
                  exact
                  to="/managed-security-services"
                  className="main-header__link"
                >
                  Back to Casestudies &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br />
                <h1 className="my-3 main-header__header-font text-capitalize">
                  Security Project Manager for a leading Global Financial Sector
                  Conglomerate
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="py-5">
        <div className="container-fluid blue-bg">
          <div className="">
            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center bottom">
                <div className="circle">
                  {' '}
                  <img src={step1Icon} />{' '}
                </div>
              </div>
              <div className="col-8">
                <h5>Nature of Engagement </h5>
                <p>
                  Designing, procurement and implementation of integrated
                  security systems as part of office fit out projects for
                  approx. 21,800 sq mtrs carpet area at Bangalore and approx.
                  10,300 sq mtrs at Pune
                </p>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner top-right"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner left-bottom"></div>
              </div>
            </div>

            <div className="row align-items-center justify-content-end how-it-works">
              <div className="col-8">
                <h5> Engagement Scope </h5>
                <p>
                  Provision of embed required to provide end to end support to
                  manage project as Corporate Security representative in the
                  project management team and expected to deliver the following:
                </p>
                <ul className="unorder-list pt-1">
                  <li> Project Assessment and Functional Analysis </li>
                  <li> Threat Assessment and Vulnerability Analysis </li>
                  <li> Conceptual Security Planning </li>
                  <li>
                    {' '}
                    Detailed Security Planning, including placement of systems
                    and functions together with security equipment{' '}
                  </li>
                  <li> Project Execution </li>
                  <li> Testing and Commissioning </li>
                  <li> Transition and Handing over to operations </li>
                  <li>
                    {' '}
                    Security deployment planning, selection and training{' '}
                  </li>
                </ul>
              </div>
              <div className="col-2 text-center full">
                <div className="circle">
                  <img src={step2Icon} />
                </div>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner right-bottom"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner top-left"></div>
              </div>
            </div>

            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center top">
                <div className="circle">
                  <img src={step3Icon} />
                </div>
              </div>
              <div className="col-8">
                <h5> The Outcome </h5>
                <p>
                  Successfully implementation security systems for approx. 4000
                  work pts at Bangalore and approx. 1100 work pts at Pune within
                  stipulated project timeline. Streamlining of security
                  operation controls and processes at both projects based on
                  business continuity and risk management protocols Reduction in
                  overall project cost due to minimum re-work and deployment of
                  latest technology.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Casestudy05;
