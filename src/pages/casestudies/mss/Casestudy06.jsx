import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import bannerImg from '../../../assets/images/casestudies/MSS/Provision of PMO for Risk Assessment, Designing Architecture & Security System_ Project Implementation at Pune Banner.webp';
import step1Icon from '../../../assets/images/casestudies/client.webp';
import step2Icon from '../../../assets/images/casestudies/description.webp';
import step3Icon from '../../../assets/images/casestudies/result.webp';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
const Casestudy06 = () => {
  return (
    <>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" alt="Casestudy Image" />
        </div>
        <div className="col-12 col-md-10 col-lg-10">
          <div className="service-description">
            <div className="display-block m-a">
              <div className="content">
                <NavLink
                  exact
                  to="/managed-security-services"
                  className="main-header__link"
                >
                  Back to Casestudies &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br />
                <h1 className="my-3 main-header__header-font text-capitalize">
                  Provision of PMO for Risk Assessment, Designing Architecture &
                  Security System; Project Implementation at Pune
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="py-5">
        <div className="container-fluid blue-bg">
          <div className="">
            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center bottom">
                <div className="circle">
                  {' '}
                  <img src={step1Icon} />{' '}
                </div>
              </div>
              <div className="col-8">
                <h5>Nature of Engagement </h5>
                <p>
                  Risk assessment, Designing, procurement and implementation of
                  integrated security systems as part of office fit out projects
                  at Pune
                </p>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner top-right"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner left-bottom"></div>
              </div>
            </div>

            <div className="row align-items-center justify-content-end how-it-works">
              <div className="col-8">
                <h5> Engagement Scope </h5>
                <p>
                  Provision of embed required to provide end to end support to
                  manage project as Corporate Security representative in the
                  project management team and expected to deliver the following:
                </p>
                <ul className="unorder-list pt-1">
                  <li> Project Assessment and Functional Analysis </li>
                  <li>
                    Threat Assessment and Vulnerability Analysis leading to
                    Conceptual Security Planning{' '}
                  </li>
                  <li>
                    Detailed Security Planning, including placement of systems
                    and functions together with security equipment
                  </li>
                  <li>Project Execution</li>
                  <li>
                    Testing – Commissioning, Transition and Handing over to
                    operations
                  </li>
                </ul>
              </div>
              <div className="col-2 text-center full">
                <div className="circle">
                  <img src={step2Icon} />
                </div>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner right-bottom"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner top-left"></div>
              </div>
            </div>

            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center top">
                <div className="circle">
                  <img src={step3Icon} />
                </div>
              </div>
              <div className="col-8">
                <h5> The Outcome </h5>
                <ul className="unorder-list pt-1">
                  <li>
                    Successfully conducted Risk assessment and created concept
                    design{' '}
                  </li>
                  <li>
                    Reduction in overall project cost due to minimum re-work and
                    deployment of latest technology.{' '}
                  </li>
                  <li>
                    The project is ongoing and will be conclude by Mar 2020{' '}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Casestudy06;
