import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import bannerImg from '../../../assets/images/casestudies/MSS/Provision of Corporate Security Manager for a Agri - Business Company based out of Pune Banner.webp';
import step1Icon from '../../../assets/images/casestudies/client.webp';
import step2Icon from '../../../assets/images/casestudies/description.webp';
import step3Icon from '../../../assets/images/casestudies/result.webp';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
const Casestudy10 = () => {
  return (
    <>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" alt="Casestudy Image" />
        </div>
        <div className="col-12 col-md-10 col-lg-10">
          <div className="service-description">
            <div className="display-block m-a">
              <div className="content">
                <NavLink
                  exact
                  to="/managed-security-services"
                  className="main-header__link"
                >
                  Back to Casestudies &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br />
                <h1 className="my-3 main-header__header-font text-capitalize">
                  Provision of Corporate Security Manager for a Agri - Business
                  Company based out of Pune
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="py-5">
        <div className="container-fluid blue-bg">
          <div className="">
            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center bottom">
                <div className="circle">
                  {' '}
                  <img src={step1Icon} />{' '}
                </div>
              </div>
              <div className="col-8">
                <h5>Nature of Engagement </h5>
                <p>
                  Deployment of Corporate Security Manager to ensure Protection
                  of Assets, People, Product & Information, in accordance with
                  Global Security global guidelines and strategy.
                </p>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner top-right"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner left-bottom"></div>
              </div>
            </div>

            <div className="row align-items-center justify-content-end how-it-works">
              <div className="col-8">
                <h5> Engagement Scope </h5>
                <ul className="unorder-list pt-1">
                  <li>
                    {' '}
                    Payroll Management to include Payroll Processing, Payroll
                    Statutory Compliance & Reporting, Employee Query Management,
                    Tax Calculation & Verification and Statutory & MIS Reporting{' '}
                  </li>
                  <li>
                    {' '}
                    Replacement Support by assisting in replacing the candidate
                    by leveraging our vast network to provide adequate resumes
                    of suitably experienced resource with proven credentials.{' '}
                  </li>
                  <li>
                    {' '}
                    Risk Advisory & Emergency Management Support to provide
                    timely inputs for enabling pro-active response to emerging
                    risk scenarios
                  </li>
                  <li>
                    {' '}
                    In case of a business demand, provide additional subject
                    matter support on assignment basis{' '}
                  </li>
                </ul>
              </div>
              <div className="col-2 text-center full">
                <div className="circle">
                  <img src={step2Icon} />
                </div>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner right-bottom"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner top-left"></div>
              </div>
            </div>

            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center top">
                <div className="circle">
                  <img src={step3Icon} />
                </div>
              </div>
              <div className="col-8">
                <h5> The Outcome </h5>
                <ul className="unorder-list pt-1">
                  <li>
                    {' '}
                    MitKat’s vast candidate database and network successfully
                    ensured continued deployment at all sites and timely
                    replacement, when required.{' '}
                  </li>
                  <li>
                    {' '}
                    By deploying an appropriate resource, client was able to
                    enhance the effectiveness of its brand protection structure
                    to combat the loss of revenue, reputation and customer trust{' '}
                  </li>
                  <li>
                    {' '}
                    Improvement in effective emergency response by providing
                    off-site advisory support using collective expertise and
                    experience of the entire MitKat Team{' '}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Casestudy10;
