import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import bannerImg from '../../../assets/images/casestudies/MSS/Control Room Design & Operations for a global mining company in Zambia, Africa Banner.webp';
import step1Icon from '../../../assets/images/casestudies/client.webp';
import step2Icon from '../../../assets/images/casestudies/description.webp';
import step3Icon from '../../../assets/images/casestudies/result.webp';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
const Casestudy07 = () => {
  return (
    <>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" alt="Casestudy Image" />
        </div>
        <div className="col-12 col-md-10 col-lg-10">
          <div className="service-description">
            <div className="display-block m-a">
              <div className="content">
                <NavLink
                  exact
                  to="/managed-security-services"
                  className="main-header__link"
                >
                  Back to Casestudies &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br />
                <h1 className="my-3 main-header__header-font text-capitalize">
                  Control Room Design & Operations for a global mining company
                  in Zambia, Africa
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="py-5">
        <div className="container-fluid blue-bg">
          <div className="">
            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center bottom">
                <div className="circle">
                  {' '}
                  <img src={step1Icon} />{' '}
                </div>
              </div>
              <div className="col-8">
                <h5>Nature of Engagement </h5>
                <p>
                  Design of security control room, operational and emergency
                  communications support; control room operations for one of
                  world’s largest emerald mines in Africa.
                </p>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner top-right"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner left-bottom"></div>
              </div>
            </div>

            <div className="row align-items-center justify-content-end how-it-works">
              <div className="col-8">
                <h5> Engagement Scope </h5>
                <ul className="unorder-list pt-1">
                  <li>
                    Manning & operation of control room, reporting exceptions &
                    incidents and enabling response; geo-political and social
                    risk analysis and advice to the client{' '}
                  </li>
                  <li>
                    Threat-vulnerability risk assessment and working out
                    surveillance, security & communication (including emergency
                    communications) requirements and plans
                  </li>
                  <li>
                    Design & implementation of Control Room; working out 24x7
                    ops SOPs
                  </li>
                  <li>
                    Deterrence, theft and pilferage prevention and precious
                    metal recoveries
                  </li>
                </ul>
              </div>
              <div className="col-2 text-center full">
                <div className="circle">
                  <img src={step2Icon} />
                </div>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner right-bottom"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner top-left"></div>
              </div>
            </div>

            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center top">
                <div className="circle">
                  <img src={step3Icon} />
                </div>
              </div>
              <div className="col-8">
                <h5> The Outcome </h5>
                <ul className="unorder-list pt-1">
                  <li>
                    State-of the art surveillance, field communications and
                    security control room
                  </li>
                  <li>
                    24x7 surveillance, drop in theft & pilferage, evidence
                    collection and appropriate response
                  </li>
                  <li>MitKat Team deployed 8 man team from 2015 -2017</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Casestudy07;
