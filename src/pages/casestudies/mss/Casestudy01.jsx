import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import bannerImg from '../../../assets/images/casestudies/MSS/Staffing for South Asia Command Centre for a leading MNC Bank based in Mumbai Banner.webp';
import step1Icon from '../../../assets/images/casestudies/client.webp';
import step2Icon from '../../../assets/images/casestudies/description.webp';
import step3Icon from '../../../assets/images/casestudies/result.webp';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
const Casestudy01 = () => {
  return (
    <>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" alt="Casestudy Image" />
        </div>
        <div className="col-12 col-md-10 col-lg-10">
          <div className="service-description">
            <div className="display-block m-a">
              <div className="content">
                <NavLink
                  exact
                  to="/managed-security-services"
                  className="main-header__link"
                >
                  Back to Casestudies &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br />
                <h1 className="my-3 main-header__header-font text-capitalize">
                  Staffing for South Asia Command Centre for a leading MNC Bank
                  based in Mumbai
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="py-5">
        <div className="container-fluid blue-bg">
          <div className="">
            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center bottom">
                <div className="circle">
                  {' '}
                  <img src={step1Icon} />{' '}
                </div>
              </div>
              <div className="col-8">
                <h5>Nature of Engagement </h5>
                <p>
                  Deployment of an outsourced team for the position of Team
                  Lead, Analyst and Operations Executive to monitor threats for
                  its businesses across PAN India region and provide their
                  expertise to Corporate Security team to mitigate the risks.
                </p>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner top-right"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner left-bottom"></div>
              </div>
            </div>

            <div className="row align-items-center justify-content-end how-it-works">
              <div className="col-8">
                <h5> Engagement Scope </h5>

                <ul className="unorder-list pt-1">
                  <li>
                    {' '}
                    Outsourced resources study the operating environment and
                    identify/evaluate the threats covering but not limited to
                    terrorism, crime and public unrest
                  </li>
                  <li>
                    {' '}
                    Outsourced resources strategize & manage the overall
                    security systems of its location{' '}
                  </li>
                  <li>
                    {' '}
                    MitKat conducts initial orientation and periodic refresher
                    training for the resources deployed.
                  </li>
                </ul>
              </div>
              <div className="col-2 text-center full">
                <div className="circle">
                  <img src={step2Icon} />
                </div>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner right-bottom"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner top-left"></div>
              </div>
            </div>

            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center top">
                <div className="circle">
                  <img src={step3Icon} />
                </div>
              </div>
              <div className="col-8">
                <h5> The Outcome </h5>
                <ul className="unorder-list pt-1">
                  <li>
                    {' '}
                    Monitoring the risk incidents and provide risk advisories to
                    enhance business continuity{' '}
                  </li>
                  <li>
                    {' '}
                    Optimized the security spending and reduction in security
                    budget on year-on-year basis{' '}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Casestudy01;
