import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/CSR/Implementation-of-DLP-policies-for-a-leading-Financial-Organization-and-its-group-companies.webp';
const Casestudy05 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/cyber-security-and-resilience"
        title="Implementation of DLP policies for a leading Financial Organisation and its group companies"
        step1Title="Client’s challenge"
        step1Description="A BFSI Organisation decided to roll out new cybersecurity policy and migrate the on premise data centre to cloud based infrastructure."
        step2Title="Support Extended"
        step2Description="MitKat team   planned and implemented a DLP solution for client including 5 of the group companies. Team also collaborated with client’s cloud service provider and devised a plan to migrate the data centres to cloud. MitKat team managed the project very meticulously and completed it within 12 weeks’ time. "
        step3Title="Result"
        step3Description="MitKat team’s efficient project managed skills has saved 20% of time and resources for client on this project."
      />
      <Footer />
    </>
  );
};

export default Casestudy05;
