import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/CSR/Designing-of-Business-Continuity-Management-system-for-a-European-Bank’s-GIC-centre.webp';

const Casestudy06 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/cyber-security-and-resilience"
        title="Designing of Business Continuity Management system for a European Bank’s GIC centre"
        step1Title="Client’s challenge"
        step1Description="A multinational bank’s Indian GIC centre needs support to design Business Continuity framework and Crisis Management System"
        step2Title="Support Extended"
        step2Description="MitKat team carried out series of discussions with global BCM team of the Organisation and understood corporate policies and process. Considering the local parameters and variables, MitKat team designed the BCM framework for India GIC centres and delivered a comprehensive document which got great amount of appreciation from client. Moreover, team also identified crisis scenarios and designed relevant contact trees, process matrix. To deliver 360 degree consulting support, ERT team of the client was also trained for Crisis Management"
        step3Title="Result"
        step3Description="A corporate BCM framework was design and delivered along with identified Crisis Management scenarios."
      />
      <Footer />
    </>
  );
};

export default Casestudy06;
