import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/CSR/Crisis-Management-simulation-exercise-for-a-global-conglomerate-GIC.webp';

const Casestudy07 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/cyber-security-and-resilience"
        title="Crisis Management simulation exercise for a global conglomerate GIC"
        step1Title="Client’s challenge"
        step1Description="Recent attrition and expansion into the Crisis Management team has triggered a requirement of Crisis Simulation to check the readiness of the Organisation for perceived crisis."
        step2Title="Support Extended"
        step2Description="MitKat BCM consulting team engaged with client’s crisis management team and identified 2 most relevant crisis scenarios and scheduled table top simulations for the same. At the end of the simulation, a gap assessment report was submitted to client highlighting the area of improvement and training needs for the team. Client BCM and Security team participated in the exercise along with Command centre and  country leadership team."
        step3Title="Result"
        step3Description="Client’s corporate BCM team got the inputs towards the improvement in the process required along with the need of training for team members."
      />
      <Footer />
    </>
  );
};

export default Casestudy07;
