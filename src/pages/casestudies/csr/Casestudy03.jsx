import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/CSR/Remediation-of-a-hacking-incident-and-designing-cybersecurity-policies-to-prevent-future-attack-for-a-leading-chain-of-healthcare-Organization.webp';

const Casestudy03 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/cyber-security-and-resilience"
        title="Remediation of a hacking incident and designing cybersecurity policies to prevent future attack for a leading Healthcare chain "
        step1Title="Client’s challenge"
        step1Description="A leading chain of clinics providing healthcare services faced data breach and client sensitive information was stolen."
        step2Title="Support Extended"
        step2Description="MitKat team  quickly assessed the breaches, and plugged the gap in corporate datacentre. A comprehensive cybersecurity assessment was carriers out, and the whole infrastructure was fortified with enhanced measures, including deployment of new security tools and a secure access management policy."
        step3Title="Result"
        step3Description="Any further leakage of information has been prevented. The Client has engaged MitKat to provide virtual Chief Information Security Officer (vCISO). A robust ISMS policy has been defined, documented, implemented, and is being continually reviewed."
      />
      <Footer />
    </>
  );
};

export default Casestudy03;
