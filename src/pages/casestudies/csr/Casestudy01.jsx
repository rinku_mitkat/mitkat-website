import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/CSR/ISMS-audit-of-an-engineering-service-provider-on-behalf-of-an-Indian-Bank.webp';

const Casestudy01 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/cyber-security-and-resilience"
        title="ISMS audit of an engineering service provider on behalf of an Indian Bank"
        step1Title="Client’s challenge"
        step1Description="A leading IT company engaged MitKat as external cyber security experts to carry out information security management system (ISMS) audit of Off-shore Development Centre (ODC), as well as vulnerability assessment and penetration testing (VAPT) of the applications for a leading Indian Bank."
        step2Title="Support Extended"
        step2Description="MitKat cybersecurity team is been consistently supporting by carrying out ODC audit and VAPT of applications. The client is being provided with gap analysis and non-conformance report along with critical defects identified in banking applications."
        step3Title="Result"
        step3Description="With the help of critical observations and inputs provided by MitKat cybersecurity team, client Organisation is able to maintain the cybersecurity posture of all the applications without any breach or violation. The client is able to retain the engagement with bank since last 5 years. "
      />
      <Footer />
    </>
  );
};

export default Casestudy01;
