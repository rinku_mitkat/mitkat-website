import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/CSR/Cyber-assurance-for-a-cloud-migration-initiative-of-a-leading-FMCG-client.webp';

const Casestudy04 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/cyber-security-and-resilience"
        title="Cyber assurance for a cloud migration initiative of a leading FMCG client"
        step1Title="Client’s challenge"
        step1Description="A leading FMCG company decided to migrate their on premises data centre to cloud infrastructure and was in need of a cybersecurity consultant to oversee the migration project from data protection and cybersecurity point of view."
        step2Title="Support Extended"
        step2Description="MitKat team collaborated with the client and its cloud migration service provider to understand the migration plan and carried out gap analysis in the plan. After suggesting changes into the migration plan from cybersecurity and data protection perspectives, MitKat team participated in the project execution with service provider."
        step3Title="Result"
        step3Description="The Client seamlessly and successfully migrated its ‘on prem’ data center to cloud infra; with necessary cyber security and data protection controls in place. The project was completed (ahead of schedule) in three weeks."
      />
      <Footer />
    </>
  );
};

export default Casestudy04;
