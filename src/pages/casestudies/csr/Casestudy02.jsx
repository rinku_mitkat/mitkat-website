import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/CSR/Cybersecurity-assessment-of-IT-infrastructure-of-a-leading-media-company.webp';

const Casestudy02 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/cyber-security-and-resilience"
        title="Cybersecurity assessment of IT infrastructure of a leading media company"
        step1Title="Client’s challenge"
        step1Description="A leading media house identified breach into their IT Infrastructure and loss of intellectual property. They doubted an external breach as well as an insider threat."
        step2Title="Support Extended"
        step2Description="MitKat team carried out cybersecurity assessment of the set of components of the entire IT infrastructure, Communication links and application deployed on premises and in cloud. A comprehensive gap assessment report and set of recommendations were provided the Client along with recommendations to achieve a robust security posture. A ‘zero trust’ policy has been enforced due to presence of a large number of BYOD devices and owing to remote work scenarios.  "
        step3Title="Result"
        step3Description="MitKat’s swift and effective response to identify the problem, and come up with a practical, implementable and cost-conscious solution, was lauded by the client. Immediate “to do’s” have been implemented. Robust, long term cyber security policy has been defined and documented, delivered to the client; its effective implementation would minimise the probability of any future breach or compromise."
      />
      <Footer />
    </>
  );
};

export default Casestudy02;
