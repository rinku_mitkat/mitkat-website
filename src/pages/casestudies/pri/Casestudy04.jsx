import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/PRI/Predictive Risk Intelligence for a multi-national Internet Technology company to continue business across APAC countries Banner l.webp';

const Casestudy04 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/predictive-risk-intelligence"
        title="Predictive Risk Intelligence for a multi-national Internet Technology company to continue business across APAC countries"
        step1Title="Client’s challenge"
        step1Description="A multi-national Internet Technology and Applications development company faces multiple challenges of business sustainability, security of their people and assets, and adverse brand perceptions."
        step2Title="Support Extended"
        step2Description="MitKat’s team extended threat intelligence support, understanding the reach of the organisation’s products. A detailed and continuous channel of communication was created to provide relevant and timely updates comprising the risk parameters that could impact the clients business operations, people and assets security/safety, brand image and impact due to diplomatic tensions between various nations in the APAC region. "
        step3Title="Result"
        step3Description="With customised and consistent support from MitKat, the client was able to take appropriate mitigation measures and take key decisions to ensure business sustainability during difficult times. MitKat team’s extended coverage for the APAC region provided the client with a single point of contact for all their requirements."
      />
      <Footer />
    </>
  );
};

export default Casestudy04;
