import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/PRI/Comprehensive risk assessment for Oil & Gas operations in North East India Bannerl.webp';

const Casestudy02 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/predictive-risk-intelligence"
        title="Comprehensive risk assessment for Oil & Gas operations in North East India"
        step1Title="Client’s challenge"
        step1Description="A prominent multi-national Oil & Gas company needed support in identification of geo-political, social, environmental, regulatory, security, administrative, logistical, community related risks / challenges in and around potential oil exploration sites."
        step2Title="Support Extended"
        step2Description="MitKat’s Predictive Risk Intel team provided a comprehensive risk assessment for the Oil & Gas client in North East India wherein operational, logistical and human (including labour) risks were highlighted along with mitigation measures and recommendations."
        step3Title="Result"
        step3Description="MitKat’s comprehensive geo-political risk assessment enabled the client to successfully plan the exploration activity in the challenging terrain of the four North Eastern states of Assam, Nagaland, Arunachal Pradesh and Tripura."
      />
      <Footer />
    </>
  );
};

export default Casestudy02;
