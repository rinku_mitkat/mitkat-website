import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/PRI/Ensuring employee safety during COVID-19 pandemic and enabling operational efficiency Bannerl.webp';

const Casestudy05 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/predictive-risk-intelligence"
        title="Ensuring employee safety during COVID-19 pandemic and enabling operational efficiency"
        step1Title="Client’s challenge"
        step1Description="The pandemic brought about a plethora of unforeseen challenges for our client with over 60 offices spread in 20 tier-1 & tier-2 cities across India. With over 400,000 employees in India, monitoring health and productivity of employees during the pandemic posed a difficult proposition."
        step2Title="Support Extended"
        step2Description="MitKat’s analytical team was able to support the technology giant by providing consistent and reliable data pertaining to COVID-19 pandemic district-wise distribution of cases, infection rate, containment zones (along with lat-long coordinates), state-wise regulations, lockdown status and quarantine guidelines in desired formats (dashboard, work-sheets, graphical representation on map and API integration for client platforms).  "
        step3Title="Result"
        step3Description="Inputs received from MitKat enabled the client to adopt selective off-site work arrangements  for their employees across India, by providing detailed analysis for each of the remote working locations created during the pandemic. MitKat’s proprietary technology integration enabled the client to make predictive analysis for resource planning and ensuring optimum efficiency."
      />
      <Footer />
    </>
  );
};

export default Casestudy05;
