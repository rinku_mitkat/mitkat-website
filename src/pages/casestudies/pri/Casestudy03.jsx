import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/PRI/Enabling business for a global multi-national Banking & Financial organisation Banner.webp';

const Casestudy03 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/predictive-risk-intelligence"
        title="Enabling business for a global multi-national Banking & Financial organisation"
        step1Title="Client’s challenge"
        step1Description="A multi-national Banking & Financial organisation continues to face a multitude of operational and business continuity challenges due to the dynamic nature of threats in the South Asian region."
        step2Title="Support Extended"
        step2Description="MitKat’s predictive risk intel team worked closely with the client’s security team to understand specific threats that impact business operations for the industry and for the company. Alerts and advisories are curated to specifically address clients concerns, and highlight high risk areas, assessments, and preventive measures. Risk monitoring is continuous for multiple tier-1 & tier-2 cities across South Asia, with customised industry-specific summaries.  "
        step3Title="Result"
        step3Description="Partnering with MitKat has helped the organisation ensure consistent business growth, enabling them to expand their business across all geographies in South Asia despite the challenges. Customised inputs have enabled the operations and security teams to ensure appropriate resource allocation while achieving optimum security. "
      />
      <Footer />
    </>
  );
};

export default Casestudy03;
