import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import CasestudySection from '../../../components/CasestudySection';
import bannerImg from '../../../assets/images/casestudies/PRI/Enabling rural electrification operations in challenging regions Banner.webp';

const Casestudy01 = () => {
  return (
    <>
      <Navbar />
      <CasestudySection
        imgsrc={bannerImg}
        backlink="/predictive-risk-intelligence"
        title="Enabling rural electrification operations in challenging regions"
        step1Title="Client’s challenge"
        step1Description="A power sector company responsible for rural electrification in difficult geographies including North East India, Jammu & Kashmir and Maoist affected states of India continues to face several challenges such as threat from insurgents, crime mafia, terrorists, extreme weather phenomenon, protests & disturbances and antagonism from local population / NGOs."
        step2Title="Support Extended"
        step2Description="Curating and synthesising threat intel from information collated from Open-Source Intelligence (OSINT), on-ground resources, inputs from local entities and Government authorities, MitKat analysts were able to create a coherent and predictive picture of the risk scenario which enabled business operations with requisite mitigation measures."
        step3Title="Result"
        step3Description="The client’s ground teams were able to plan and schedule their operations based on regular inputs provided by the MitKat team; and achieved incident-free and seamless operational efficiency."
      />
      <Footer />
    </>
  );
};

export default Casestudy01;
