import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import step1Icon from '../../../assets/images/casestudies/client.webp';
import step2Icon from '../../../assets/images/casestudies/description.webp';
import step3Icon from '../../../assets/images/casestudies/result.webp';
import bannerImg from '../../../assets/images/casestudies/IRM/Comprehensive Due Diligence Investigation for leading Japan-Headquartered MNC in India banner.webp';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
const Casestudy02 = () => {
  return (
    <>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" alt="Casestudy Image" />
        </div>
        <div className="col-12 col-md-10 col-lg-10">
          <div className="service-description">
            <div className="display-block m-a">
              <div className="content">
                <NavLink
                  exact
                  to="/integrity-risk-management"
                  className="main-header__link"
                >
                  Back to Casestudies &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br />
                <h1 className="my-3 main-header__header-font text-capitalize">
                  Comprehensive Due Diligence Investigation for leading
                  Japan-Headquartered MNC in India
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="py-5">
        <div className="container-fluid blue-bg">
          <div className="">
            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center bottom">
                <div className="circle">
                  {' '}
                  <img src={step1Icon} />{' '}
                </div>
              </div>
              <div className="col-8">
                <h5>Client’s challenge</h5>
                <p>
                  A prominent Japan-Headquartered MNC needed support to
                  understand the background, history and profile of the company
                  and its directors, insight on the target Company, its
                  management, its related businesses, work ethics, capabilities,
                  present infrastructure, reputation, financial standing,
                  adverse findings regarding the directors and the company,
                  management’s political affiliations, criminal records etc.
                </p>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner top-right"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner left-bottom"></div>
              </div>
            </div>

            <div className="row align-items-center justify-content-end how-it-works">
              <div className="col-8">
                <h5>Support extended</h5>
                <p>
                  MitKat’s Investigation team provided comprehensive integrity
                  risk management for Japan-headquartered MNC subsidiary
                  companies and its management based in India wherein fraud
                  risks were highlighted along with mitigation measures and
                  recommendations.
                </p>
              </div>
              <div className="col-2 text-center full">
                <div className="circle">
                  <img src={step2Icon} />
                </div>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner right-bottom"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner top-left"></div>
              </div>
            </div>

            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center top">
                <div className="circle">
                  <img src={step3Icon} />
                </div>
              </div>
              <div className="col-8">
                <h5>Result</h5>
                <ul className="unorder-list pt-1">
                  <li>
                    {' '}
                    The client gained an accurate and comprehensive
                    understanding of various vulnerabilities of the company{' '}
                  </li>
                  <li>
                    {' '}
                    As per the investigation, it appeared that the accounts had
                    been falsified and the Company’s accounting principles and
                    standards had been violated, indicative of possible
                    accounting fraud. The company admitted that its accounts
                    appeared to have been ‘falsified’ misrepresenting the fair
                    value and state of financial affairs{' '}
                  </li>
                  <li>
                    {' '}
                    The Company Board took stern action against its Managing
                    Director and CEO, the CFO, the Sr. VP and COO after they
                    were held responsible for causing grave loss to the company
                    and its stakeholders{' '}
                  </li>
                  <li>
                    {' '}
                    The company reported its highest ever loss in first half of
                    financial year 2016-17. Nevertheless, corrective measures
                    instituted thereafter remedied the situation{' '}
                  </li>
                  <li>
                    {' '}
                    The Board learnt that bribes were being paid by company
                    officials to authorities in few states to procure voluminous
                    Government tender business{' '}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Casestudy02;
