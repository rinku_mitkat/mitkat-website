import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import step1Icon from '../../../assets/images/casestudies/client.webp';
import step2Icon from '../../../assets/images/casestudies/description.webp';
import step3Icon from '../../../assets/images/casestudies/result.webp';
import bannerImg from '../../../assets/images/casestudies/IRM/Enabling Due Diligence Investigation on Procurement Fraud in a Multinational Company in India Banner.webp';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
const Casestudy01 = () => {
  return (
    <>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" alt="Casestudy Image" />
        </div>
        <div className="col-12 col-md-10 col-lg-10">
          <div className="service-description">
            <div className="display-block m-a">
              <div className="content">
                <NavLink
                  exact
                  to="/integrity-risk-management"
                  className="main-header__link"
                >
                  Back to Casestudies &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br />
                <h1 className="my-3 main-header__header-font text-capitalize">
                  Enabling Due Diligence Investigation on Procurement Fraud in a
                  Multinational Company in India
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="py-5">
        <div className="container-fluid blue-bg">
          <div className="">
            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center bottom">
                <div className="circle">
                  {' '}
                  <img src={step1Icon} />{' '}
                </div>
              </div>
              <div className="col-8">
                <h5>Client’s challenge</h5>
                <p>
                  Understanding the modus operandi of the suspected employees
                  and vendors, Lifestyle mapping of suspected employees, Asset
                  tracing of suspected employees, Understanding of ‘red flags’
                  in the procurement process, Assessing the the approximate
                  financial loss to the company.
                </p>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner top-right"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner left-bottom"></div>
              </div>
            </div>

            <div className="row align-items-center justify-content-end how-it-works">
              <div className="col-8">
                <h5>Support extended</h5>
                <p>
                  MitKat’s Investigation Team provided a comprehensive integrity
                  risk management for company based in India wherein various
                  fraud risks were highlighted along with mitigation measures
                  and recommendations.
                </p>
              </div>
              <div className="col-2 text-center full">
                <div className="circle">
                  <img src={step2Icon} />
                </div>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner right-bottom"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner top-left"></div>
              </div>
            </div>

            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center top">
                <div className="circle">
                  <img src={step3Icon} />
                </div>
              </div>
              <div className="col-8">
                <h5>Result</h5>
                <ul className="unorder-list pt-1">
                  <li>
                    {' '}
                    Investigation revealed the complete modus operandi of
                    suspected employees and vendors viz. mixing of substandard
                    product, prices manipulation, sharing of margins, illegal
                    cash transactions, profit percentage charge, accepting of
                    favours, various accounts of employees wherein cash was
                    deposited, manipulation of purchase system right from
                    quality to security checks to field buyers{' '}
                  </li>
                  <li>
                    {' '}
                    Investigation revealed that suppliers were coerced/
                    threatened to agree to suspected employees’ terms and
                    conditions{' '}
                  </li>
                  <li>
                    {' '}
                    Artificial manipulation of product prices citing low
                    production{' '}
                  </li>
                  <li>
                    {' '}
                    Suspected employees were found to be possessing assets in
                    excess of INR 10 crores while having a salary of approx. 1
                    lakh per month{' '}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Casestudy01;
