import React from 'react';
import Navbar from '../../../components/Navbar';
import Footer from '../../../components/Footer';
import step1Icon from '../../../assets/images/casestudies/client.webp';
import step2Icon from '../../../assets/images/casestudies/description.webp';
import step3Icon from '../../../assets/images/casestudies/result.webp';
import bannerImg from '../../../assets/images/casestudies/IRM/Comprehensive Due Diligence Investigation for leading Mauritius-based real estate MNC in India Banner.webp';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
const Casestudy03 = () => {
  return (
    <>
      <Navbar />
      <div className="row justify-content-center m-0">
        <div className="col-12 col-md-12 col-lg-12 p-0">
          <img src={bannerImg} width="100%" alt="Casestudy Image" />
        </div>
        <div className="col-12 col-md-10 col-lg-10">
          <div className="service-description">
            <div className="display-block m-a">
              <div className="content">
                <NavLink
                  exact
                  to="/integrity-risk-management"
                  className="main-header__link"
                >
                  Back to Casestudies &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br />
                <h1 className="my-3 main-header__header-font text-capitalize">
                  Comprehensive Due Diligence Investigation for leading
                  Mauritius-based real estate MNC in India
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="py-5">
        <div className="container-fluid blue-bg">
          <div className="">
            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center bottom">
                <div className="circle">
                  {' '}
                  <img src={step1Icon} />{' '}
                </div>
              </div>
              <div className="col-8">
                <h5>Client’s challenge</h5>
                <p>
                  A leading Mauritius-based real estate MNC continues to face a
                  multitude of challenges in the acquisition of India-based real
                  estate Company.
                </p>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner top-right"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner left-bottom"></div>
              </div>
            </div>

            <div className="row align-items-center justify-content-end how-it-works">
              <div className="col-8">
                <h5>Support extended</h5>
                <ul className="unorder-list pt-1">
                  <li>
                    {' '}
                    MitKat’s investigation team assisted by providing support
                    that helped understand the background, history and profile
                    of the company and its directors{' '}
                  </li>
                  <li>
                    {' '}
                    Gathering insight on the Subject Company, its management and
                    its related businesses, work ethics, capabilities, present
                    infrastructure and reputation{' '}
                  </li>
                  <li>
                    {' '}
                    Scrutinising of the Registrar of Companies (ROC) records
                    which included company registration and incorporation
                    records, shareholding structure and subsidiaries{' '}
                  </li>
                  <li> Understanding of financial standing of the company </li>
                  <li>
                    {' '}
                    Mapping the personal and professional credentials of company
                    directors{' '}
                  </li>
                  <li>
                    {' '}
                    Identifying any adverse findings regarding the directors and
                    company{' '}
                  </li>
                  <li>
                    {' '}
                    Conducting various database checks on directors and company
                    to find any adverse charges{' '}
                  </li>
                  <li>
                    {' '}
                    Checking the criminal records with respect to the company
                    directors{' '}
                  </li>
                </ul>
              </div>
              <div className="col-2 text-center full">
                <div className="circle">
                  <img src={step2Icon} />
                </div>
              </div>
            </div>

            <div className="row timeline">
              <div className="col-2">
                <div className="corner right-bottom"></div>
              </div>
              <div className="col-8">
                <hr />
              </div>
              <div className="col-2">
                <div className="corner top-left"></div>
              </div>
            </div>

            <div className="row align-items-center how-it-works">
              <div className="col-2 text-center top">
                <div className="circle">
                  <img src={step3Icon} />
                </div>
              </div>
              <div className="col-8">
                <h5>Result</h5>
                <ul className="unorder-list pt-1">
                  <li>
                    {' '}
                    Found that Anti-Corruption Branch (“ACB”) of the Delhi
                    government had registered an FIR against the company
                    management for their alleged involvement in a land scam{' '}
                  </li>
                  <li>
                    {' '}
                    Consumer complaints against the company on Indian Consumer
                    Complaints Forum on harassment of stakeholders{' '}
                  </li>
                  <li>
                    {' '}
                    Found property disputes lawsuit filed in the high court of
                    Punjab & Haryana against the company{' '}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default Casestudy03;
