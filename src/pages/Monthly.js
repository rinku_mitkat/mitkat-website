import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import Footer from '../components/Footer';
import Navbar from '../components/Navbar';
import { getAdvisories, setItemPlatform } from '../service/api';

const Monthly = () => {
  const [advisoryList, setAdvisoryList] = useState({
    success: '',
    message: '',
    output: { specialReports: [], monthly: [], riskReview: [], advisories: [] },
  });
  const [name, setName] = useState('');
  const [emailid, setEmailid] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [advisoryname, setAdvisoryname] = useState('');

  const [url, setUrl] = useState('');
  const [itemInputPlatform, setItemInputPlatform] = useState('');
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  function handleShow(url, advisoryname) {
    setShow(true);
    setUrl(url);
    setAdvisoryname(advisoryname);
  }
  useEffect(() => {
    getAdvisories().then((advisories) => {
      setAdvisoryList(advisories);
    });
    return;
  }, []);

  const formSubmitPlatform = (e) => {
    e.preventDefault();
    setItemPlatform({ name, emailid, phoneNumber, advisoryname }).then(() => {
      setItemInputPlatform('');
      window.open(url, '_blank');
      window.location.reload(false);
    });
  };
  return (
    <>
      <Helmet>
        <title>Advisories | MitKat Advisory</title>
      </Helmet>
      <Navbar />
      <section className="py-5">
        <div className="container">
          <div className="row ">
            <div className="service-description p-2 mt-0">
              <div className="display-block m-a">
                <div className="content">
                  <h1 className="mb-3 service-description__header text-capitalize text-center">
                    Monthly
                  </h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="py-5 bg-gray">
        <div className="container">
          <div className="row">
            {advisoryList.output.monthly.map((advisories) => (
              <div className="col-md-4 p-4 ">
                <div
                  className="advisory-Box"
                  style={{ background: `url(${advisories.displayURL})` }}
                >
                  <div className="advisory-header">
                    <p className="small">{advisories.date} </p>
                    <p>{advisories.title} </p>
                    <div className="advisory-footer">
                      <a
                        className="btn-view"
                        onClick={() =>
                          handleShow(advisories.reportURL, advisories.title)
                        }
                      >
                        View
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>
      <Footer />

      <div
        className={show ? ' modal fade show' : 'modal fade'}
        id="bookademoModal"
        tabindex="-1"
        aria-labelledby="bookademoModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header pb-0">
              <h5 className="modal-title" id="exampleModalLabel">
                {' '}
                Let us know you!{' '}
              </h5>
              <button type="button" className="close" onClick={handleClose}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body pt-0">
              <div className="d-flex bg-darker">
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-12 col-md-12 col-lg-12 m-a d-flex justify-content-center flex-column">
                      <p className="text-right text-white">
                        {' '}
                        <small> * required </small>{' '}
                      </p>
                      <form
                        className="contact-form px-3"
                        onSubmit={formSubmitPlatform}
                      >
                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group mb-3">
                              <label className="text-white">Name * </label>
                              <input
                                type="text"
                                value={name}
                                className="form-control contact-form__input"
                                onChange={(event) =>
                                  setName(event.target.value)
                                }
                                required
                              />
                            </div>
                            <div className="form-group mb-3">
                              <label className="text-white">Email Id * </label>
                              <input
                                type="email"
                                value={emailid}
                                className="form-control contact-form__input"
                                onChange={(event) =>
                                  setEmailid(event.target.value)
                                }
                                required
                              />
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group mb-3">
                              <label className="text-white">Phone * </label>
                              <input
                                type="text"
                                value={phoneNumber}
                                className="form-control contact-form__input"
                                onChange={(event) =>
                                  setPhoneNumber(event.target.value)
                                }
                                required
                              />
                            </div>
                            <div className="form-group mb-3">
                              <label className="text-white">
                                Advisory Name *{' '}
                              </label>
                              <input
                                type="text"
                                value={advisoryname}
                                className="form-control contact-form__input"
                                onChange={(event) =>
                                  setAdvisoryname(event.target.value)
                                }
                                readOnly
                              />
                            </div>
                          </div>

                          <div className="col-md-6 col-12">
                            <div className="form-group">
                              <input
                                type="submit"
                                name="btnSubmit"
                                className="btn contact-form__btn"
                                value="Submit"
                              />
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Monthly;
