import React, { useEffect, useState } from 'react';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faMapMarkerAlt,
  faEnvelopeOpen,
  faArrowRight,
} from '@fortawesome/free-solid-svg-icons';
import banner from '../assets/images/background/Careers.webp';
import { getCareerListWeb, getCareerdetails } from '../service/api';
import { Helmet } from 'react-helmet';

const Career = () => {
  const [careerList, setCareerList] = useState([]);
  const [careerDetails, setCareerDetails] = useState([]);
  const [careerListBlock, setCareerListBlock] = useState(false);
  const [careerListDetails, setCareerListDetails] = useState(false);

  function viewCareer(id) {
    let mounted = true;
    setCareerListBlock(true);
    window.scrollTo(0, 0);
    getCareerdetails(id).then((items) => {
      if (mounted) {
        setCareerDetails(items);
      }
    });
    return () => (mounted = false);
  }
  function backTocareers() {
    setCareerListBlock(false);
  }

  useEffect(() => {
    let mounted = true;
    getCareerListWeb().then((items) => {
      if (mounted) {
        setCareerList(items);
      }
    });

    return () => (mounted = false);
  }, []);
  return (
    <>
      <Helmet>
        <title>Careers | MitKat Advisory</title>
      </Helmet>
      <Navbar />
      {!careerListBlock ? (
        <>
          <div className="service-header">
            <div className="row justify-content-center m-0">
              <div className="col-12 col-md-12 col-lg-12 p-0">
                <img src={banner} width="100%" />
              </div>
              <div className="col-12 col-md-9 col-lg-9">
                <div className="service-description">
                  <div className="display-block m-a">
                    <div className="content">
                      <h1 className="mb-3 service-description__header text-capitalize text-center">
                        Join us
                      </h1>
                      <p className="content__subheader py-3 m-0">
                        We know that finding a meaningful and rewarding job can
                        be a long journey. Our goal is to make that process as
                        easy as possible for you, and to create a work
                        environment that's satisfying - one where you'll look
                        forward to coming to every day.
                        <br /> <br />
                        Start your journey with us.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <section className="py-5 bg-darker">
            <div className="container">
              <div className="row">
                <div className="col-lg-12 col-md-12 col-12">
                  <div className="content">
                    <h2 className="header-font text-capitalize text-white text-center">
                      {careerList.length}
                      {careerList.length > 10 ? '+' : null} Jobs Available
                    </h2>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section className="py-5">
            <div className="container">
              <div className="row pb-5">
                <div className="col-lg-12 col-md-12 col-12">
                  <ul className="job-list">
                    {careerList.map((item) => (
                      <li>
                        <a onClick={() => viewCareer(item.id)}>
                          {/* <p className="job-category"> Valid till: {item.validTillDate}</p> */}
                          <h2 className="job-title">{item.jobTitle} </h2>
                          <p className="job-summary text-justify pb-3">
                            {' '}
                            {item.summary}{' '}
                          </p>
                          <div className="job-location-list">
                            <FontAwesomeIcon icon={faMapMarkerAlt} />
                            <ul className="location-city-list">
                              <li> {item.location} </li>
                            </ul>
                          </div>
                        </a>
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
            </div>
          </section>
        </>
      ) : (
        <section className="py-5">
          <div className="container px-md-5 px-3">
            <div className="row px-md-5  px-3">
              <div className="service-description p-2 mt-0">
                <div className="display-block m-a">
                  <div className="content">
                    <a
                      className="main-header__link"
                      onClick={() => backTocareers()}
                    >
                      Back to Careers &nbsp; &nbsp;
                      <FontAwesomeIcon
                        icon={faArrowRight}
                        className="main-header__link__arrow"
                      />
                    </a>
                    <h1 className="mb-3 service-description__header text-capitalize text-center">
                      {careerDetails.title}
                    </h1>
                    <p className="smaller text-grey py-3 m-0">
                      <FontAwesomeIcon icon={faMapMarkerAlt} /> &nbsp;
                      {careerDetails.location}
                    </p>
                    <p className="content__subheader py-3 m-0 text-justify">
                      {careerDetails.summary}
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-12 col-md-12 col-12">
                <div className="content">
                  <h2 className="header-font text-capitalize text-center">
                    WHAT YOU'LL DO
                  </h2>
                  <p class="content__subheader py-3 pre-line">
                    {careerDetails.responsibilities}
                  </p>
                </div>
              </div>
              <div className="col-lg-12 col-md-12 col-12">
                <div className="content">
                  <h2 className="header-font text-capitalize text-center">
                    QUALIFICATIONS
                  </h2>
                  <p class="content__subheader py-3 pre-line">
                    {careerDetails.qualifications}
                  </p>
                </div>
              </div>
              <div className="col-lg-12 col-md-12 col-12 pt-5">
                <div className="content">
                  <p class="content__subheader smaller text-right py-3 ">
                    To apply please forword your resume on{' '}
                    <strong>purvi.poojari@mitkatadvisory.com </strong>{' '}
                    <FontAwesomeIcon icon={faEnvelopeOpen} />
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
      )}

      <Footer />
    </>
  );
};

export default Career;
