import React, { useRef } from 'react';
import { NavLink } from 'react-router-dom';
import News from '../components/News';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import aboutImg from '../assets/images/section/A Leading Asian Risk Consultancy.webp';
import whatWeDo from '../assets/images/section/What We Do.webp';
import rimePlatform from '../assets/images/platform/Risk Intelligence Platform.png';
import covidPlatform from '../assets/images/platform/Covid-19 Dashboard.webp';
import securityPlatform from '../assets/images/platform/Security Audit Platform-home.webp';
import priIcon from '../assets/images/service-icon/PRI.png';
import pessIcon from '../assets/images/service-icon/Physical-&-Environmental-Security.png';
import sdcpmIcon from '../assets/images/service-icon/Security-Design-Consulting.png';
import igsIcon from '../assets/images/service-icon/Infrastructure-&-Government-Services.png';
import isbIcon from '../assets/images/service-icon/Info-Sec-&-BCM.png';
import irmIcon from '../assets/images/service-icon/Integrity-Risk-Management.png';
import twdIcon from '../assets/images/service-icon/Training-&-Workforce-Development.png';
import mssIcon from '../assets/images/service-icon/Managed-Security-Services.png';
import tcsTestimonial from '../assets/images/testimonials/TCS.webp';
import standardCharteredTestimonial from '../assets/images/testimonials/Standard Chartered.webp';
import edelweissTestimonial from '../assets/images/testimonials/Edelweiss.webp';
import maerskTestimonial from '../assets/images/testimonials/Maersk.webp';
import tiaaTestimonial from '../assets/images/testimonials/TIAA.webp';
import gizTestimonial from '../assets/images/testimonials/GIZ.webp';
import morganStanleyTestimonial from '../assets/images/testimonials/Morgan Stanley.webp';
import axisTestimonial from '../assets/images/testimonials/axis.webp';
import asianTestimonial from '../assets/images/testimonials/asian.webp';
import noauraTestimonial from '../assets/images/testimonials/noaura.webp';
import vodafoneTestimonial from '../assets/images/testimonials/vodafone.webp';

import bvTestimonial from '../assets/images/testimonials/Black & Veatch.webp';
import futureGroupTestimonial from '../assets/images/testimonials/Future Group.webp';
import canonTestimonial from '../assets/images/testimonials/Canon.webp';

import TopEvents from '../components/TopEvents';
import UpcomingEvents from '../components/UpcomingEvents';
import { Helmet } from 'react-helmet';

const Home = () => {
  const serviceweoffer = useRef(null);

  const executeScroll = () => serviceweoffer.current.scrollIntoView();

  return (
    <>
      <Helmet>
        <title>Leading Asian Security Consultancy | MitKat Advisory</title>
      </Helmet>
      <Navbar />
      <div className="main-header d-flex">
        <div className="container-fluid">
          <div className="row main-header__row">
            <div className="col-12 col-md-6 col-lg-6 d-flex justify-content-center flex-column align-items-center">
              <div className="main-header__content">
                <h1 className=" main-header__header-font font-weight-bold text-capitalize">
                  MitKat Advisory Wins OSPAs (Outstanding Security Performance
                  Awards) India 2021 For Outstanding Risk Management Solution
                </h1>
                <p className="main-header__sub-header pb-3 ">
                  MitKat Advisory received the OSPAs (Outstanding Security
                  Performance Awards) India 2021 in the ‘Outstanding Risk
                  Management Solution’ category for its AI-enabled predictive
                  risk Intelligence platform. The digital platform, with global
                  coverage, provides a comprehensive, unified, customised view
                  of business-risks specific to organisations, and is used by
                  more than 50 of the top 100 global corporations.
                </p>
                <NavLink
                  exact
                  to="/risk-intelligence-monitoring-engine"
                  rel="noopener noreferrer"
                  className="main-header__link"
                >
                  Know More &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
                <br /> <br />
                {/* <a
                  href="https://mitkatrisktracker.com"
                  target="_blank"
                  className="main-header__link"
                >
                  Get lastest update &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </a> */}
              </div>
            </div>
          </div>
        </div>
      </div>
      <TopEvents />
      <UpcomingEvents />
      <section className="py-5 bg-darker">
        <div className="container section-md">
          <div className="row row-30 text-center">
            <div className="col-lg-3 col-md-3 col-6 px-0">
              <div className="figures text-white">
                <div className="figures__number">
                  <span className="text-white">2010</span>
                </div>
                <div className="figures__title text-uppercase">
                  Year of incorporation
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-6 px-0">
              <div className="figures text-white">
                <div className="figures__number">
                  <span className="text-white">350+</span>
                </div>
                <div className="figures__title text-uppercase">Customers</div>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-6 px-0">
              <div className="figures text-white">
                <div className="figures__number">
                  <span className="text-white">40+</span>
                </div>
                <div className="figures__title text-uppercase">
                  Worked in Countries
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-6 px-0">
              <div className="figures text-white">
                <div className="figures__number ">
                  <span className="text-white">100+</span>
                </div>
                <div className="figures__title text-uppercase">
                  Specialists{' '}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="section d-flex">
        <div className="container-fluid">
          <div className="row section__row">
            <div className="col-12 col-md-6 col-lg-6 p-0">
              <img
                src={aboutImg}
                alt="Asian Leading Company"
                className="section__img"
                alt="A Leading Asian Company"
              />
            </div>

            <div className="col-12 col-md-6 col-lg-6 d-flex justify-content-center flex-column bg-gray">
              <div className="section__content">
                <h2 className="mb-3 section__header-font font-weight-bold text-capitalize  text-shadow-none beforeline">
                  A Leading Asian Risk Consultancy{' '}
                </h2>
                <p className="section__sub-header  py-3 ">
                  Comprehensive Security and Risk Management support across
                  India and Indo-Pacific.{' '}
                </p>
                <NavLink exact to="/about" className="main-header__link">
                  Know More &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="section d-flex">
        <div className="container-fluid">
          <div className="row section__row">
            <div className="col-12 col-md-6 col-lg-6 d-flex justify-content-center flex-column bg-gray">
              <div className="section__content">
                <h2 className="mb-3 section__header-font font-weight-bold">
                  What we do{' '}
                </h2>
                <p className="section__sub-header  py-3 ">
                  MitKat is a global provider of integrated Security and Risk
                  Mitigation Solutions and Services. We work collaboratively
                  with clients to de-risk their businesses. Our clients include
                  the world’s most respected organisations.{' '}
                </p>
                <a onClick={executeScroll} className="main-header__link">
                  Know More &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </a>
              </div>
            </div>
            <div className="col-12 col-md-6 col-lg-6 p-0">
              <img
                src={whatWeDo}
                alt="What We Do"
                className="section__img"
                alt="What we do"
              />
            </div>
          </div>
        </div>
      </section>

      <section className="py-5">
        <div className="container">
          <div className="row pb-5">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="header-font text-capitalize text-center">
                  Our Platforms{' '}
                </h2>
              </div>
            </div>
          </div>

          <div className="row pb-5 justify-content-around">
            <div className="col-lg-3 col-md-3 col-12 platform-box">
              <NavLink
                exact
                to="/risk-intelligence-monitoring-engine"
                className="platform-card__content text-center"
              >
                <img
                  className="platform-card__img"
                  src={rimePlatform}
                  alt="RIME"
                />
                <p className="pb-3">Risk Intelligence Platform </p>
              </NavLink>
            </div>

            <NavLink
              exact
              to="/covid19-dashboard"
              className="col-lg-3 col-md-3 col-12 platform-box"
            >
              <div className="platform-card__content text-center">
                <img
                  className="platform-card__img"
                  src={covidPlatform}
                  alt="COVID Dashboard"
                />
                <p className="pb-3">
                  Covid-19 <br /> Platform{' '}
                </p>
              </div>
            </NavLink>

            <NavLink
              exact
              to="/security-audit"
              className="col-lg-3 col-md-3 col-12 platform-box"
            >
              <div className="platform-card__content text-center">
                <img
                  className="platform-card__img"
                  src={securityPlatform}
                  alt="Security Audit"
                />
                <p className="pb-3">Security Audit Platform </p>
              </div>
            </NavLink>
          </div>
        </div>
      </section>

      <section className="py-5 parallax" ref={serviceweoffer}>
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-5 header-font text-capitalize text-center text-white">
                  Our Services{' '}
                </h2>
              </div>
            </div>
          </div>

          <div className="row justify-content-center">
            <div className="col-lg-4 col-md-4 col-12 service-box text-white">
              <div className="service-box__card p-3">
                <img className="service-box__img" src={priIcon} alt="PRI" />
                <h3 className="text-white">Predictive Risk Intelligence </h3>
                <p className="text-white pt-3">
                  Predictive Risk Intelligence allows Evolving threat landscape
                  and its impact on business..
                </p>
                <NavLink
                  exact
                  to="/predictive-risk-intelligence"
                  className="main-header__link text-white pt-3"
                >
                  Read More &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
              </div>
            </div>

            <div className="col-lg-4 col-md-4 col-12 service-box text-white">
              <div className="service-box__card p-3">
                <img className="service-box__img" src={pessIcon} alt="PESS" />
                <h3 className="text-white">
                  Physical & Environmental Security and Safety{' '}
                </h3>
                <p className="text-white pt-3">
                  Risk-based approach, security and safety apps, global
                  standards...
                </p>
                <NavLink
                  exact
                  to="/physical-environmental-security-and-safety-risk-consulting"
                  className="main-header__link text-white pt-3"
                >
                  Read More &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
              </div>
            </div>

            <div className="col-lg-4 col-md-4 col-12 service-box text-white">
              <div className="service-box__card p-3">
                <img className="service-box__img" src={sdcpmIcon} alt="SDCPM" />
                <h3 className="text-white">
                  Security Design Consulting & Project Management{' '}
                </h3>
                <p className="text-white pt-3">
                  Security Design consulting is all about finding the Right
                  balance between security & convenience; costs, benefits &
                  risks...
                </p>
                <NavLink
                  exact
                  to="/security-design-consulting-and-project-management"
                  className="main-header__link text-white pt-3"
                >
                  Read More &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
              </div>
            </div>

            <div className="col-lg-4 col-md-4 col-12 service-box text-white">
              <div className="service-box__card p-3">
                <img className="service-box__img" src={igsIcon} alt="IGS" />
                <h3 className="text-white">
                  Infrastructure & <br /> Government Services{' '}
                </h3>
                <p className="text-white pt-3">
                  Our SMEs use their unique experience with the Government
                  and...
                </p>
                <NavLink
                  exact
                  to="/infrastructure-and-government-services"
                  className="main-header__link text-white pt-3"
                >
                  Read More &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
              </div>
            </div>

            <div className="col-lg-4 col-md-4 col-12 service-box text-white">
              <div className="service-box__card p-3">
                <img className="service-box__img" src={isbIcon} alt="ISB" />
                <h3 className="text-white">Cyber Security and Resilience </h3>
                <p className="text-white pt-3">
                  The cyber threat landscape is becoming diverse, diffused and
                  complex...
                </p>
                <NavLink
                  exact
                  to="/cyber-security-and-resilience"
                  className="main-header__link text-white pt-3"
                >
                  Read More &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
              </div>
            </div>

            <div className="col-lg-4 col-md-4 col-12 service-box text-white">
              <div className="service-box__card p-3">
                <img className="service-box__img" src={irmIcon} alt="IRM" />
                <h3 className="text-white">Integrity Risk Management </h3>
                <p className="text-white pt-3">
                  With Organisations losing considerable annual revenues to
                  fraud...
                </p>
                <NavLink
                  exact
                  to="/integrity-risk-management"
                  className="main-header__link text-white pt-3"
                >
                  Read More &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
              </div>
            </div>

            <div className="col-lg-4 col-md-4 col-12 service-box text-white">
              <div className="service-box__card p-3">
                <img className="service-box__img" src={twdIcon} alt="TWD" />
                <h3 className="text-white">
                  Training & <br /> Workforce Development{' '}
                </h3>
                <p className="text-white pt-3">
                  MitKat’s customised training solutions can provide your
                  organisation...
                </p>
                <NavLink
                  exact
                  to="/training-and-workforce-development"
                  className="main-header__link text-white pt-3"
                >
                  Read More &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
              </div>
            </div>

            <div className="col-lg-4 col-md-4 col-12 service-box text-white">
              <div className="service-box__card p-3">
                <img className="service-box__img" src={mssIcon} alt="MSS" />
                <h3 className="text-white">
                  Managed <br /> Security Services{' '}
                </h3>
                <p className="text-white pt-3">
                  While you focus on your core business, our Managed Security
                  Services...
                </p>
                <NavLink
                  exact
                  to="/managed-security-services"
                  className="main-header__link text-white pt-3"
                >
                  Read More &nbsp; &nbsp;
                  <FontAwesomeIcon
                    icon={faArrowRight}
                    className="main-header__link__arrow"
                  />
                </NavLink>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="py-5">
        <div className="container">
          <div className="row py-3">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-capitalize text-center">
                  Client Speak
                </h2>
              </div>
            </div>
          </div>
          <div className="row py-3">
            <div className="col-lg-12 col-md-12 col-12">
              <div
                id="carouselExampleIndicators"
                className="carousel slide"
                data-ride="carousel"
              >
                <ol className="carousel-indicators">
                  <li
                    data-target="#carouselExampleIndicators"
                    data-slide-to="0"
                    className="active"
                  ></li>
                  <li
                    data-target="#carouselExampleIndicators"
                    data-slide-to="1"
                  ></li>
                  <li
                    data-target="#carouselExampleIndicators"
                    data-slide-to="2"
                  ></li>
                  <li
                    data-target="#carouselExampleIndicators"
                    data-slide-to="3"
                  ></li>
                  <li
                    data-target="#carouselExampleIndicators"
                    data-slide-to="4"
                  ></li>
                  <li
                    data-target="#carouselExampleIndicators"
                    data-slide-to="5"
                  ></li>
                  <li
                    data-target="#carouselExampleIndicators"
                    data-slide-to="6"
                  ></li>
                  <li
                    data-target="#carouselExampleIndicators"
                    data-slide-to="7"
                  ></li>
                  <li
                    data-target="#carouselExampleIndicators"
                    data-slide-to="8"
                  ></li>
                  <li
                    data-target="#carouselExampleIndicators"
                    data-slide-to="9"
                  ></li>
                  <li
                    data-target="#carouselExampleIndicators"
                    data-slide-to="10"
                  ></li>
                  <li
                    data-target="#carouselExampleIndicators"
                    data-slide-to="11"
                  ></li>
                  <li
                    data-target="#carouselExampleIndicators"
                    data-slide-to="12"
                  ></li>
                  <li
                    data-target="#carouselExampleIndicators"
                    data-slide-to="13"
                  ></li>
                </ol>
                <div className="carousel-inner">
                  <div className="carousel-item active">
                    <div className="testimonials text-center d-md-flex justify-content-center align-items-center">
                      <div className="testimonials__img">
                        <span className="testimonials__img__span">
                          <img src={tcsTestimonial} alt="TCS" />
                        </span>
                      </div>
                      <div className="testimonials__quote">
                        <p className="mb-0">
                          Thanks for all your support provided with regular
                          information on potential crisis and appreciate your
                          quick turnaround on our swift requests as well.
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="carousel-item">
                    <div className="testimonials text-center d-md-flex justify-content-center align-items-center">
                      <div className="testimonials__img">
                        <span className="testimonials__img__span">
                          <img
                            src={standardCharteredTestimonial}
                            alt="Standard Chartered"
                          />
                        </span>
                      </div>
                      <div className="testimonials__quote">
                        <p className="mb-0">
                          We truly appreciate the valuable support provided by
                          MitKat to the Standard Chartered bank and also larger
                          risk management community over the years.
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="carousel-item">
                    <div className="testimonials text-center d-md-flex justify-content-center align-items-center">
                      <div className="testimonials__img">
                        <span className="testimonials__img__span">
                          <img src={edelweissTestimonial} alt="Edelweiss" />
                        </span>
                      </div>
                      <div className="testimonials__quote">
                        <p className="mb-0">
                          You people are doing great work in risk consulting,
                          uplifting our hopes. Your advisories during Covid-
                          pandemic crisis, have helped many Organisations in
                          strategic planning and business continuity.
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="carousel-item">
                    <div className="testimonials text-center d-md-flex justify-content-center align-items-center">
                      <div className="testimonials__img">
                        <span className="testimonials__img__span">
                          <img src={maerskTestimonial} alt="Maersk" />
                        </span>
                      </div>
                      <div className="testimonials__quote">
                        <p className="mb-0">
                          We are really thankful to the MitKat team for the
                          extra ordinary support extended in this critical time.
                          Your team is really quick in answering the queries and
                          responsible in validating the news from the social
                          media. You have amazing team, passionate and committed
                          to work, politely answering all queries and delivering
                          the great commitment with highest level of
                          satisfaction.
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="carousel-item">
                    <div className="testimonials text-center d-md-flex justify-content-center align-items-center">
                      <div className="testimonials__img">
                        <span className="testimonials__img__span">
                          <img src={tiaaTestimonial} alt="TIAA" />
                        </span>
                      </div>
                      <div className="testimonials__quote">
                        <p className="mb-0">
                          MitKat team is doing phenomenal job of collating
                          information and sharing with their clients which I am
                          sure the clients are using/would use these inputs for
                          their business operations and safety of employees. You
                          all have done an amazing job during difficult times
                          and please continue the same.
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="carousel-item">
                    <div className="testimonials text-center d-md-flex justify-content-center align-items-center">
                      <div className="testimonials__img">
                        <span className="testimonials__img__span">
                          <img src={gizTestimonial} alt="GIZ" />
                        </span>
                      </div>
                      <div className="testimonials__quote">
                        <p className="mb-0">
                          I would like to thank the whole of MitKat team for
                          their tireless effort to keep your clients day and
                          night updated about the COVID crisis scenario. It is
                          quite appreciated!
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="carousel-item">
                    <div className="testimonials text-center d-md-flex justify-content-center align-items-center">
                      <div className="testimonials__img">
                        <span className="testimonials__img__span">
                          <img
                            src={morganStanleyTestimonial}
                            alt="Morgan Stanley"
                          />
                        </span>
                      </div>
                      <div className="testimonials__quote">
                        <p className="mb-0">
                          You have been exceptional MitKat team. And you have
                          marked up your material, presentations, reports over
                          time.
                        </p>
                      </div>
                    </div>
                  </div>

                  {/* <div className="carousel-item">
                                    <div className="testimonials text-center d-md-flex justify-content-center align-items-center">
                                        <div className="testimonials__img"> 
                                            <span className="testimonials__img__span">
                                                <img src={itcTestimonial} alt="ITC" /> 
                                            </span>
                                        </div> 
                                        <div className="testimonials__quote"> 
                                            <p className="mb-0">                    
                                            ITC Hotels appreciates the trial offer of MitKat Advisory Services. It has been a great experience to understand the service offering. We appreciate the crisp detailing and analysis of the data.
                                            </p>
                                        </div>                                                        
                                    </div>
                                </div> */}

                  <div className="carousel-item">
                    <div className="testimonials text-center d-md-flex justify-content-center align-items-center">
                      <div className="testimonials__img">
                        <span className="testimonials__img__span">
                          <img src={bvTestimonial} alt="Black & Veatch" />
                        </span>
                      </div>
                      <div className="testimonials__quote">
                        <p className="mb-0">
                          MitKat’s reports and alerts are timely, granular to a
                          great degree and provide good ground level
                          information. They are exactly what we need, since we
                          blindly copy-paste the relevant sections as required
                          into our internal reports. Your alerts provide crisp
                          and functional recommendations, not vague and
                          exaggerated ones.
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="carousel-item">
                    <div className="testimonials text-center d-md-flex justify-content-center align-items-center">
                      <div className="testimonials__img">
                        <span className="testimonials__img__span">
                          <img
                            src={futureGroupTestimonial}
                            alt="Future Group"
                          />
                        </span>
                      </div>
                      <div className="testimonials__quote">
                        <p className="mb-0">
                          Heartiest Congratulations to Team MitKat Advisory
                          Services Pvt. Ltd. on completion of 10 successful
                          years in the field of Safety and Security. MitKat is
                          well known around the Globe nowadays for its
                          professional acumen and service to the society. It's a
                          tremendous achievement within a decade with strong
                          competition around. It speaks volumes of the
                          leadership and dedication of each member of the team.
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="carousel-item">
                    <div className="testimonials text-center d-md-flex justify-content-center align-items-center">
                      <div className="testimonials__img">
                        <span className="testimonials__img__span">
                          <img src={canonTestimonial} alt="Canon" />
                        </span>
                      </div>
                      <div className="testimonials__quote">
                        <p className="mb-0">
                          Heartiest congratulations for excellent work being
                          done by Team MitKat. In these difficult times, any
                          information I need in regards to the pandemic
                          situation, I know I can always look up to the very
                          regular and comprehensive reports from your team.
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="carousel-item">
                    <div className="testimonials text-center d-md-flex justify-content-center align-items-center">
                      <div className="testimonials__img">
                        <span className="testimonials__img__span">
                          <img src={axisTestimonial} alt="Canon" />
                        </span>
                      </div>
                      <div className="testimonials__quote">
                        <p className="mb-0">
                          MitKat carried out Fire Safety Audit for its 700+
                          branches pan-India. MitKat completed the assignment
                          well within the timeline with a very professional
                          approach and delivered with high quality standards.
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="carousel-item">
                    <div className="testimonials text-center d-md-flex justify-content-center align-items-center">
                      <div className="testimonials__img">
                        <span className="testimonials__img__span">
                          <img src={asianTestimonial} alt="Canon" />
                        </span>
                      </div>
                      <div className="testimonials__quote">
                        <p className="mb-0">
                          MitKat has provided strategic support to the ASIAN in
                          its seismic projects both in India and overseas -
                          managing community affairs & maintaining effective
                          liaison with the local authorities, in areas as
                          diverse as North Eastern India and Kurdistan.
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="carousel-item">
                    <div className="testimonials text-center d-md-flex justify-content-center align-items-center">
                      <div className="testimonials__img">
                        <span className="testimonials__img__span">
                          <img src={noauraTestimonial} alt="Canon" />
                        </span>
                      </div>
                      <div className="testimonials__quote">
                        <p className="mb-0">
                          Would like to convey a sincere thanks to the entire
                          team of MitKat for the excellent execution of the
                          training on "Advanced Executive Protection”. The value
                          adds that were extended beyond the scope of the
                          training program are truly appreciated.
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="carousel-item">
                    <div className="testimonials text-center d-md-flex justify-content-center align-items-center">
                      <div className="testimonials__img">
                        <span className="testimonials__img__span">
                          <img src={vodafoneTestimonial} alt="Canon" />
                        </span>
                      </div>
                      <div className="testimonials__quote">
                        <p className="mb-0">
                          MitKat Advisory Services have been our service
                          providers in various assignments pan-India. These
                          services have involved Risk Assessments, Security
                          Advisory Sevices and Security Support. Their service
                          have always been satisfactory & cost-effective.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <a
                  className="carousel-control-prev"
                  href="#carouselExampleIndicators"
                  role="button"
                  data-slide="prev"
                >
                  <span
                    className="carousel-control-prev-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="sr-only">Previous</span>
                </a>
                <a
                  className="carousel-control-next"
                  href="#carouselExampleIndicators"
                  role="button"
                  data-slide="next"
                >
                  <span
                    className="carousel-control-next-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="sr-only">Next</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="py-5 bg-gray">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <div className="content">
                <h2 className="mb-3 header-font text-capitalize text-center">
                  Recent News
                </h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12 col-md-12 col-12">
              <News />
            </div>
          </div>
        </div>
      </section>

      <Footer />
    </>
  );
};

export default Home;
