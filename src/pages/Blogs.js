import React, { useEffect, useState } from 'react';
import Footer from '../components/Footer';
import Navbar from '../components/Navbar';
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getBlogs, setItemPlatform } from '../service/api';
import ShowMoreText from 'react-show-more-text';
import { Helmet } from 'react-helmet';

const Blogs = () => {
  const [blogList, setBlogList] = useState([]);
  const monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'June',
    'July',
    'Aug',
    'Sept',
    'Oct',
    'Nov',
    'Dec',
  ];
  const [name, setName] = useState('');
  const [emailid, setEmailid] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [advisoryname, setAdvisoryname] = useState('');

  const [url, setUrl] = useState('');
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  function handleShow(url, blogtitle) {
    setShow(true);
    setUrl(url);
    setAdvisoryname(blogtitle);
  }
  const formSubmitPlatform = (e) => {
    e.preventDefault();
    setItemPlatform({ name, emailid, phoneNumber, advisoryname }).then(() => {
      window.open(url, '_blank');
      window.location.reload(false);
    });
  };
  useEffect(() => {
    const blogs = async () => {
      const blogData = await getBlogs();
      setBlogList(blogData);
    };
    blogs();
  }, []);
  return (
    <>
      <Helmet>
        <title>Blogs | MitKat Advisory</title>
      </Helmet>
      <Navbar />
      <section className="py-5">
        <div className="container px-md-5 px-3">
          <div className="row px-md-5  px-3">
            <div className="service-description p-2 mt-0">
              <div className="display-block m-a">
                <div className="content">
                  <h1 className="mb-3 service-description__header text-capitalize text-center">
                    Blogs
                  </h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="py-5 bg-gray">
        <div className="container px-md-5 px-3">
          <div className="row px-md-5  px-3">
            <div className="col-md-12 col-12  p-4">
              {blogList.map(
                (blog) => (
                  <div className="blog-Box">
                    <div
                      className="blog-img"
                      style={{ background: `url(${blog.coverpic})` }}
                    >
                      {/* <img src={blog.coverpic}  alt={blog.title}  /> */}
                    </div>
                    <div className="blog-header pl-3">
                      <p className="blog-title">{blog.title} </p>
                      <ShowMoreText
                        lines={3}
                        more="Show more"
                        less="Show less"
                        className="blog-summary"
                        anchorClassName="my-anchor-css-class"
                        expanded={false}
                        width={450}
                        keepNewLines={true}
                      >
                        {blog.summary}
                      </ShowMoreText>
                      <div className="blog-footer mt-3">
                        <p>
                          <FontAwesomeIcon icon={faCalendarAlt} />{' '}
                          <span> {`${new Date(blog.date).getDate()}`}</span>{' '}
                          {`${monthNames[new Date(blog.date).getMonth()]}`}{' '}
                          {`${new Date(blog.date).getFullYear()}`} by{' '}
                          <em> {blog.author} </em>
                        </p>
                        <a
                          className="btn-view"
                          onClick={() => handleShow(blog.document, blog.title)}
                        >
                          Download
                        </a>
                      </div>
                    </div>
                  </div>
                )

                // <div className="col-md-6 col-12  p-4">
                //     <div className="blog-Box"  style={{background: `url(${blog.coverpic})`}}>
                //         <div className="blog-float">
                //             <span> {`${new Date(blog.date).getDate()}`}</span>
                //             <small>{`${monthNames[new Date(blog.date).getMonth()]}`}  </small>
                //         </div>
                //         <div className="blog-header">

                //             <p className="blog-title">{blog.title} </p>
                //             <p className="blog-summary text-right">- {blog.author} </p>
                //             <div className="blog-footer">
                //                 <a className="btn-view"  onClick={() => handleShow(blog.document, blog.title)}  >View</a>
                //             </div>
                //         </div>

                //     </div>
                // </div>
              )}
            </div>
          </div>
        </div>
      </section>
      <Footer />
      <div
        className={show ? ' modal fade show' : 'modal fade'}
        id="bookademoModal"
        tabindex="-1"
        aria-labelledby="bookademoModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header pb-0">
              <h5 className="modal-title" id="exampleModalLabel">
                {' '}
                Let us know you!{' '}
              </h5>
              <button type="button" className="close" onClick={handleClose}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body pt-0">
              <div className="d-flex bg-darker">
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-12 col-md-12 col-lg-12 m-a d-flex justify-content-center flex-column">
                      <p className="text-right text-white">
                        {' '}
                        <small> * required </small>{' '}
                      </p>
                      <form
                        className="contact-form px-3"
                        onSubmit={formSubmitPlatform}
                      >
                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group mb-3">
                              <label className="text-white">Name * </label>
                              <input
                                type="text"
                                value={name}
                                className="form-control contact-form__input"
                                onChange={(event) =>
                                  setName(event.target.value)
                                }
                                required
                              />
                            </div>
                            <div className="form-group mb-3">
                              <label className="text-white">Email Id * </label>
                              <input
                                type="email"
                                value={emailid}
                                className="form-control contact-form__input"
                                onChange={(event) =>
                                  setEmailid(event.target.value)
                                }
                                required
                              />
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group mb-3">
                              <label className="text-white">Phone * </label>
                              <input
                                type="text"
                                value={phoneNumber}
                                className="form-control contact-form__input"
                                onChange={(event) =>
                                  setPhoneNumber(event.target.value)
                                }
                                required
                              />
                            </div>
                            <div className="form-group mb-3">
                              <label className="text-white">Blog Name * </label>
                              <input
                                type="text"
                                value={advisoryname}
                                className="form-control contact-form__input"
                                onChange={(event) =>
                                  setAdvisoryname(event.target.value)
                                }
                                readOnly
                              />
                            </div>
                          </div>

                          <div className="col-md-6 col-12">
                            <div className="form-group">
                              <input
                                type="submit"
                                name="btnSubmit"
                                className="btn contact-form__btn"
                                value="Submit"
                              />
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Blogs;
