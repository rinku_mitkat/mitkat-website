import React, { useEffect, useState } from 'react';
import Footer from '../components/Footer';
import Navbar from '../components/Navbar';
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getNews } from '../service/api';
import { Helmet } from 'react-helmet';

const RecentNews = () => {
  const [newsList, setNewsList] = useState([]);
  const monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'June',
    'July',
    'Aug',
    'Sept',
    'Oct',
    'Nov',
    'Dec',
  ];
  useEffect(() => {
    getNews().then((advisories) => {
      setNewsList(advisories);
    });
  }, []);
  return (
    <>
      <Helmet>
        <title>Recent News | MitKat Advisory</title>
      </Helmet>
      <Navbar />
      <section className="py-5">
        <div className="container">
          <div className="row">
            <div className="service-description p-2 mt-0">
              <div className="display-block m-a">
                <div className="content">
                  <h1 className="mb-3 service-description__header text-capitalize text-center">
                    Recent News
                  </h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="py-5 bg-gray">
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-12">
              {newsList.map((news) => (
                <div className="recentnews-Box">
                  <div
                    className="recentnews-img"
                    style={{ background: `url(${news.coverpic})` }}
                  ></div>
                  <div className="recentnews-header pl-3">
                    <a
                      href={news.link}
                      target="_blank"
                      className="recentnews-title"
                    >
                      {news.title}{' '}
                    </a>
                    <p className="recentnews-summary"> {news.summary}</p>
                    <p className="recentnews-date">
                      <FontAwesomeIcon icon={faCalendarAlt} />
                      &nbsp;
                      <span>
                        {`${new Date(news.date).getDate()}`}&nbsp;
                        {`${monthNames[new Date(news.date).getMonth()]}`}&nbsp;
                        {`${new Date(news.date).getFullYear()}`}
                      </span>
                    </p>
                    <p className="mt-3">
                      <a href={news.link} target="_blank">
                        View Video
                      </a>
                    </p>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default RecentNews;
