import React, { useEffect, useState } from 'react';
import {
  getCovidUsers,
  createCovidUser,
  deleteCovidUser,
  getCovidUserById,
} from '../../service/api';
import { NavLink } from 'react-router-dom';
import {
  faClipboard,
  faHeadset,
  faVirus,
  faEdit,
} from '@fortawesome/free-solid-svg-icons';
import { faListUl } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import AdminNavbar from '../../components/AdminNavbar';
import { faYoutube } from '@fortawesome/free-brands-svg-icons';

const CovidUsers = () => {
  const [userList, setUserList] = useState([]);
  const [show, setShow] = useState(false);
  const [id, setId] = useState(null);
  const [name, setName] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [active, setActive] = useState(true);

  const handleClose = () => setShow(false);

  const editButtonClick = async (itemId) => {
    setShow(true);
    setId(itemId);
    const covidUser = await getCovidUserById(itemId);
    setName(covidUser.name);
    setUsername(covidUser.username);
    setPassword(covidUser.password);
    setActive(covidUser.active);
  };

  const deleteButtonClick = (itemId) => {
    deleteCovidUser(itemId);
    window.location.reload(false);
  };
  function formSubmit(event) {
    event.preventDefault();
    createCovidUser({ id, name, username, password, active });
    setShow(false);
    window.location.reload(false);
  }
  function createUser() {
    setShow(true);
    setId();
    setName('');
    setUsername('');
    setPassword('');
    setActive(true);
  }

  useEffect(() => {
    let mounted = true;
    getCovidUsers().then((items) => {
      if (mounted) {
        setUserList(items);
      }
    });
    return () => (mounted = false);
  }, []);

  return (
    <>
      <AdminNavbar />

      <div className="container-fluid pt-4">
        <div className="row">
          <nav
            id="sidebarMenu"
            className="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse"
          >
            <div className="sidebar-sticky pt-3">
              <ul className="nav flex-column">
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/dashboard"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faHeadset} /> &nbsp; &nbsp; Contact
                    us
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/demo_dashboard"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faListUl} /> &nbsp; &nbsp; Demo List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/platform_dashboard"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faListUl} /> &nbsp; &nbsp; Advisory
                    List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/covid_user"
                    className="dashboard__link active pt-3"
                  >
                    <FontAwesomeIcon icon={faVirus} /> &nbsp; &nbsp; Covid Login
                    Users
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/career_list"
                    className="dashboard__link  pt-3"
                  >
                    <FontAwesomeIcon icon={faClipboard} /> &nbsp; &nbsp; Career
                    post
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/blog_list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Blog List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/recentnews-list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Recent News
                    List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/upcomingevents-list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Upcoming
                    Events
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/podcast-list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faYoutube} /> &nbsp; &nbsp; Podcast
                  </NavLink>
                </li>
              </ul>
            </div>
          </nav>

          <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <h2>
              Users list
              <a
                className="btn contact-form__btn float-right"
                onClick={() => createUser()}
              >
                Create User
              </a>
            </h2>
            <hr />
            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr>
                    <th>Sr No. </th>
                    <th>Name </th>
                    <th>User ID </th>
                    <th>Password </th>
                    <th>Edit </th>
                    <th>Delete </th>
                  </tr>
                </thead>
                <tbody>
                  {userList.map((item) => (
                    <tr>
                      <td key={item.id}>{item.id}</td>
                      <td>{item.name}</td>
                      <td>{item.username}</td>
                      <td>{item.password}</td>
                      <td>
                        <button
                          type="button"
                          className="btn btn-sm  btn-warning"
                          onClick={() => editButtonClick(item.id)}
                        >
                          Edit
                        </button>
                      </td>
                      <td>
                        <button
                          type="button"
                          className="btn btn-sm btn-danger"
                          onClick={() => deleteButtonClick(item.id)}
                        >
                          Delete
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </main>
        </div>{' '}
        <br /> <br />
        <br />
        <br />
        <br />
        <br />
      </div>
      <div
        class="toast align-items-center text-white bg-primary border-0"
        role="alert"
        aria-live="assertive"
        aria-atomic="true"
      >
        <div class="d-flex">
          <div class="toast-body">Hello, world! This is a toast message.</div>
          <button
            type="button"
            class="btn-close btn-close-white me-2 m-auto"
            data-bs-dismiss="toast"
            aria-label="Close"
          ></button>
        </div>
      </div>
      <div
        className={show ? ' modal fade show' : 'modal fade'}
        tabindex="-1"
        aria-labelledby="bookademoModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header pb-0">
              <h5 className="modal-title" id="exampleModalLabel">
                {' '}
                Create User{' '}
              </h5>
              <button type="button" className="close" onClick={handleClose}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body pt-0">
              <div className="d-flex bg-darker">
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-12 col-md-12 col-lg-12 m-a d-flex justify-content-center flex-column">
                      <p className="text-right text-white">
                        {' '}
                        <small> * required </small>{' '}
                      </p>
                      <form className="contact-form px-3" onSubmit={formSubmit}>
                        <div className="row">
                          <div className="col-md-12">
                            <div className="form-group mb-3">
                              <label className="text-white">Name * </label>
                              <input
                                type="text"
                                name="name"
                                className="form-control contact-form__input"
                                value={name}
                                onChange={(event) =>
                                  setName(event.target.value)
                                }
                                required
                              />
                            </div>
                            <div className="form-group mb-3">
                              <label className="text-white">User Id * </label>
                              <input
                                type="text"
                                name="username"
                                className="form-control contact-form__input"
                                value={username}
                                onChange={(event) =>
                                  setUsername(event.target.value)
                                }
                                required
                              />
                            </div>
                            <div className="form-group mb-3">
                              <label className="text-white">Password * </label>
                              <input
                                type="password"
                                name="password"
                                className="form-control contact-form__input"
                                value={password}
                                onChange={(event) =>
                                  setPassword(event.target.value)
                                }
                                required
                              />
                            </div>
                          </div>

                          <div className="col-md-6 col-12">
                            <div className="form-group">
                              <input
                                type="submit"
                                name="btnSubmit"
                                className="btn contact-form__btn"
                                value="Submit"
                              />
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CovidUsers;
