import React, { useEffect, useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { NavLink } from 'react-router-dom';
import AdminNavbar from '../../components/AdminNavbar';
import {
  faClipboard,
  faHeadset,
  faVirus,
  faListUl,
  faEdit,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  getUpcomingEvents,
  deleteUpcomingEvents,
  getUpcomingEventsDetails,
} from '../../service/api';
import { faYoutube } from '@fortawesome/free-brands-svg-icons';

const UpcomingEventList = () => {
  const [alert, setAlert] = useState(false);
  const [uplcomingEventList, setUplcomingEventList] = useState([]);
  const [show, setShow] = useState(false);
  const [id, setId] = useState(0);
  const [date, setDate] = useState();
  const [time, setTime] = useState('');
  const [title, setTitle] = useState('');
  const [link, setLink] = useState('');
  const [description, setDescription] = useState('');
  const [image, setImage] = useState();

  const deleteButtonClick = (id) => {
    deleteUpcomingEvents(id);
    window.location.reload(false);
  };

  const editButtonClick = async (id) => {
    setShow(true);
    setId(id);
    const eventsData = await getUpcomingEventsDetails(id);
    let dateD = new Date(eventsData.date);
    setDate(dateD);
    setTime(eventsData.time);
    setTitle(eventsData.title);
    setLink(eventsData.link);
    setDescription(eventsData.description);
    setImage(eventsData.image);
  };

  useEffect(() => {
    const event = async () => {
      const eventData = await getUpcomingEvents();
      setUplcomingEventList(eventData);
    };
    event();
  }, []);

  const handleClose = () => setShow(false);
  function createEvent() {
    setShow(true);
  }
  async function formSubmit(event) {
    event.preventDefault();

    const data = new FormData();
    let newDate = new Date(date);
    let dateMDY = `${newDate.getDate()}/${
      newDate.getMonth() + 1
    }/${newDate.getFullYear()}`;
    data.append('id', 0);
    data.append('date', dateMDY);
    data.append('time', time);
    data.append('title', title);
    data.append('link', link);
    data.append('description', description);
    data.append('image', image);
    const eventsData = await fetch(
      'https://www.mitkatadvisory.com/api/createupcomingevents',
      {
        method: 'POST',
        body: data,
      }
    );
    if (eventsData.status == 201) {
      setAlert(true);
    } else window.location.reload(false);
  }

  function refreshPage() {
    window.location.reload(false);
  }

  return (
    <>
      <AdminNavbar />
      <div className="container-fluid pt-4 bg-gray">
        <div className="row">
          <nav
            id="sidebarMenu"
            className="col-md-3 col-lg-2 d-md-block sidebar collapse"
          >
            <div className="sidebar-sticky pt-3">
              <ul className="nav flex-column">
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/dashboard"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faHeadset} /> &nbsp; &nbsp; Contact
                    us
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/demo_dashboard"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faListUl} /> &nbsp; &nbsp; Demo List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/platform_dashboard"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faListUl} /> &nbsp; &nbsp; Advisory
                    List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/covid_user"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faVirus} /> &nbsp; &nbsp; Covid Login
                    Users
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/career_list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faClipboard} /> &nbsp; &nbsp; Career
                    post
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/blog_list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Blog List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/recentnews-list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Recent News
                    List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/upcomingevents-list"
                    className="dashboard__link pt-3 active"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Upcoming
                    Events
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/podcast-list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faYoutube} /> &nbsp; &nbsp; Podcast
                  </NavLink>
                </li>
              </ul>
            </div>
          </nav>

          <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <h2>
              Upcoming Events
              <a
                className="btn contact-form__btn float-right"
                onClick={() => createEvent()}
              >
                Create Event
              </a>
            </h2>
            <hr />
            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr>
                    <th>Sr No. </th>
                    <th>Date </th>
                    <th>Title </th>
                    <th>Edit </th>
                    <th>Delete </th>
                  </tr>
                </thead>
                <tbody>
                  {uplcomingEventList.length == 0
                    ? null
                    : uplcomingEventList.map((item) => (
                        <tr>
                          <td key={item.id}>{item.id}</td>
                          <td>{item.date}</td>
                          <td>{item.title}</td>
                          <td>
                            <button
                              type="button"
                              className="btn btn-sm  btn-warning"
                              onClick={() => editButtonClick(item.id)}
                            >
                              Edit
                            </button>
                          </td>
                          <td>
                            <button
                              type="button"
                              className="btn btn-sm btn-danger"
                              onClick={() => deleteButtonClick(item.id)}
                            >
                              Delete
                            </button>
                          </td>
                        </tr>
                      ))}
                </tbody>
              </table>
            </div>
          </main>
        </div>
      </div>

      <div
        className={show ? ' modal fade show' : 'modal fade'}
        tabindex="-1"
        aria-labelledby="bookademoModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header pb-0">
              <h5 className="modal-title" id="exampleModalLabel">
                Upcoming Event List
              </h5>
              <button type="button" className="close" onClick={handleClose}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body pt-0">
              <div className="d-flex bg-darker">
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-12 col-md-12 col-lg-12 m-a d-flex justify-content-center flex-column">
                      <p className="text-right text-white">
                        {' '}
                        <small> * required </small>{' '}
                      </p>
                      <form className="contact-form px-3" onSubmit={formSubmit}>
                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group mb-3">
                              <label className="text-white">Title * </label>
                              <input
                                type="text"
                                className="form-control contact-form__input"
                                name="title"
                                value={title}
                                onChange={(event) =>
                                  setTitle(event.target.value)
                                }
                                required
                              />
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group mb-3">
                              <label className="text-white"> Date * </label>
                              <DatePicker
                                dateFormat="dd-MM-yyyy"
                                className="form-control contact-form__input"
                                selected={date}
                                onChange={(event) => setDate(event)}
                                required
                              />
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-group mb-3">
                              <label className="text-white"> Time * </label>
                              <input
                                type="text"
                                className="form-control contact-form__input"
                                name="time"
                                value={time}
                                onChange={(event) =>
                                  setTime(event.target.value)
                                }
                                required
                              />
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-group mb-3">
                              <label className="text-white">Link</label>
                              <input
                                type="text"
                                className="form-control contact-form__input"
                                name="link"
                                value={link}
                                onChange={(event) =>
                                  setLink(event.target.value)
                                }
                              />
                            </div>
                          </div>
                          <div className="col-md-12 col-12">
                            <div className="form-group mb-3">
                              <label className="text-white">description </label>
                              <textarea
                                type="text"
                                className="form-control contact-form__input"
                                name="description"
                                rows="5"
                                value={description}
                                onChange={(event) =>
                                  setDescription(event.target.value)
                                }
                              />
                            </div>
                          </div>
                          <div className="col-md-12 col-12">
                            <div className="form-group mb-3">
                              <label className="text-white">
                                Cover Picture *
                              </label>
                              <input
                                type="file"
                                className="form-control contact-form__input"
                                name="image"
                                onChange={(event) =>
                                  setImage(event.target.files[0])
                                }
                              />
                            </div>
                          </div>
                          <div className="col-md-12 col-12">
                            <div className="form-group">
                              <input
                                type="submit"
                                name="btnSubmit"
                                className="btn contact-form__btn"
                                value="Submit"
                              />
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {alert && (
        <div id="overlay" className="d-flex justify-content-center flex-column">
          <div className="success-checkmark">
            <div className="check-icon">
              <span className="icon-line line-tip"></span>
              <span className="icon-line line-long"></span>
              <div className="icon-circle"></div>
              <div className="icon-fix"></div>
            </div>
          </div>
          <center>
            <p className="text-success mb-4">Event added successfully ! </p>
            <button className="btn btn-success" onClick={refreshPage}>
              {' '}
              ok{' '}
            </button>
          </center>
        </div>
      )}
    </>
  );
};

export default UpcomingEventList;
