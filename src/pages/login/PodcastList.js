import React, { useEffect, useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import {
  createPodcast,
  getPodcastClient,
  getTopics,
  getPodcastdetails,
  deletePodcast,
} from '../../service/api';
import { NavLink } from 'react-router-dom';
import {
  faClipboard,
  faHeadset,
  faVirus,
  faEdit,
} from '@fortawesome/free-solid-svg-icons';
import { faListUl } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import AdminNavbar from '../../components/AdminNavbar';
import { faYoutube } from '@fortawesome/free-brands-svg-icons';

const PodcastList = () => {
  const [alert, setAlert] = useState(false);
  const [podcastList, setPodcastList] = useState([]);
  const [topics, setTopics] = useState([]);
  const [show, setShow] = useState(false);
  const [id, setId] = useState(null);
  const [dateString, setDateString] = useState();
  const [title, setTitle] = useState('');
  const [linkURL, setLinkURL] = useState('');
  const [topicid, setTopicid] = useState('');

  const handleClose = () => setShow(false);

  const deleteButtonClick = (id) => {
    deletePodcast(id);
    window.location.reload(false);
  };

  const editButtonClick = async (id) => {
    setId(id);
    const podcastData = await getPodcastdetails(id);
    let dateD = new Date(podcastData.date);
    console.log(podcastData);
    debugger;
    setDateString(dateD);
    setTitle(podcastData.title);
    setLinkURL(podcastData.linkURL);
    setTopicid(podcastData.topicid);
    setShow(true);
  };

  function formSubmit(event) {
    event.preventDefault();

    let dateMDY = `${dateString.getDate()}/${
      dateString.getMonth() + 1
    }/${dateString.getFullYear()}`;
    console.log(id, title, dateMDY, linkURL, topicid);
    debugger;
    createPodcast({
      title,
      dateString: dateMDY,
      linkURL,
      topicid,
    }).then((dataREs) => {
      console.log(dataREs);
      debugger;
      setAlert(true);
      setShow(false);
    });
  }
  function createPodcastBtn() {
    setShow(true);
    setId();
    setTitle('');
    setDateString();
    setLinkURL('');
    setTopicid('');
  }
  useEffect(() => {
    const podcast = async () => {
      const data = await getPodcastClient();
      setPodcastList(data);
    };
    podcast();
    const topics = async () => {
      const topicData = await getTopics();
      setTopics(topicData);
    };
    topics();
  }, []);
  function refreshPage() {
    window.location.reload(false);
  }
  return (
    <>
      <AdminNavbar />
      <div className="container-fluid pt-4 bg-gray">
        <div className="row">
          <nav
            id="sidebarMenu"
            className="col-md-3 col-lg-2 d-md-block sidebar collapse"
          >
            <div className="sidebar-sticky pt-3">
              <ul className="nav flex-column">
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/dashboard"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faHeadset} /> &nbsp; &nbsp; Contact
                    us
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/demo_dashboard"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faListUl} /> &nbsp; &nbsp; Demo List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/platform_dashboard"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faListUl} /> &nbsp; &nbsp; Advisory
                    List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/covid_user"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faVirus} /> &nbsp; &nbsp; Covid Login
                    Users
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/career_list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faClipboard} /> &nbsp; &nbsp; Career
                    post
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/blog_list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Blog List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/recentnews-list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Recent News
                    List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/upcomingevents-list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Upcoming
                    Events
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/podcast-list"
                    className="dashboard__link active pt-3"
                  >
                    <FontAwesomeIcon icon={faYoutube} /> &nbsp; &nbsp; Podcast
                  </NavLink>
                </li>
              </ul>
            </div>
          </nav>

          <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <h2>
              Podcast
              <a
                className="btn contact-form__btn float-right"
                onClick={() => createPodcastBtn()}
              >
                Create Podcast
              </a>
            </h2>
            <hr />
            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr>
                    <th>Sr No. </th>
                    <th>Date </th>
                    <th>Title </th>
                    <th>Author </th>
                    <th>Edit </th>
                    <th>Delete </th>
                  </tr>
                </thead>
                <tbody>
                  {podcastList.length == 0
                    ? null
                    : podcastList.map((item) => (
                        <tr>
                          <td key={item.id}>{item.id}</td>
                          <td>{item.dateString}</td>
                          <td>{item.title}</td>
                          <td> {item.linkURL}</td>

                          <td>
                            <button
                              type="button"
                              className="btn btn-sm  btn-warning"
                              onClick={() => editButtonClick(item.id)}
                            >
                              Edit
                            </button>
                          </td>
                          <td>
                            <button
                              type="button"
                              className="btn btn-sm btn-danger"
                              onClick={() => deleteButtonClick(item.id)}
                            >
                              Delete
                            </button>
                          </td>
                        </tr>
                      ))}
                </tbody>
              </table>
            </div>
          </main>
        </div>{' '}
        <br /> <br />
        <br />
        <br />
        <br />
        <br />
      </div>

      <div
        className={show ? ' modal fade show' : 'modal fade'}
        tabindex="-1"
        aria-labelledby="bookademoModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header pb-0">
              <h5 className="modal-title" id="exampleModalLabel">
                {' '}
                Podcast{' '}
              </h5>
              <button type="button" className="close" onClick={handleClose}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body pt-0">
              <div className="d-flex bg-darker">
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-12 col-md-12 col-lg-12 m-a d-flex justify-content-center flex-column">
                      <p className="text-right text-white">
                        {' '}
                        <small> * required </small>{' '}
                      </p>
                      <form className="contact-form px-3" onSubmit={formSubmit}>
                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group mb-3">
                              <label className="text-white"> Date * </label>
                              <DatePicker
                                dateFormat="dd-MM-yyyy"
                                className="form-control contact-form__input"
                                selected={dateString}
                                onChange={(event) => setDateString(event)}
                              />
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group mb-3">
                              <label className="text-white">topic * </label>
                              <select
                                className="form-control contact-form__input"
                                value={topicid}
                                onChange={(event) =>
                                  setTopicid(event.target.value)
                                }
                              >
                                <option selected disabled>
                                  Select Topic
                                </option>
                                {topics.map((item) => (
                                  <option value={item.id}>
                                    {' '}
                                    {item.topicName}{' '}
                                  </option>
                                ))}
                              </select>
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-group mb-3">
                              <label className="text-white">Title * </label>
                              <input
                                type="text"
                                className="form-control contact-form__input"
                                name="title"
                                value={title}
                                onChange={(event) =>
                                  setTitle(event.target.value)
                                }
                              />
                            </div>
                            <div className="form-group mb-3">
                              <label className="text-white">Link Url * </label>
                              <input
                                type="text"
                                className="form-control contact-form__input"
                                name="linkURL"
                                value={linkURL}
                                onChange={(event) =>
                                  setLinkURL(event.target.value)
                                }
                              />
                            </div>
                          </div>

                          <div className="col-md-12 col-12">
                            <div className="form-group">
                              <input
                                type="submit"
                                name="btnSubmit"
                                className="btn contact-form__btn"
                                value="Submit"
                              />
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {alert && (
        <div id="overlay" className="d-flex justify-content-center flex-column">
          <div className="success-checkmark">
            <div className="check-icon">
              <span className="icon-line line-tip"></span>
              <span className="icon-line line-long"></span>
              <div className="icon-circle"></div>
              <div className="icon-fix"></div>
            </div>
          </div>
          <center>
            <p className="text-success mb-4">Video Created successfully ! </p>
            <button className="btn btn-success" onClick={refreshPage}>
              ok
            </button>
          </center>
        </div>
      )}
    </>
  );
};

export default PodcastList;
