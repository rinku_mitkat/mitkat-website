import React, { useEffect, useState } from 'react';
import { getList } from '../../service/api';
import { NavLink } from 'react-router-dom';
import {
  faClipboard,
  faHeadset,
  faVirus,
  faEdit,
  faFileDownload,
} from '@fortawesome/free-solid-svg-icons';
import { faListUl } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import AdminNavbar from '../../components/AdminNavbar';
import { faYoutube } from '@fortawesome/free-brands-svg-icons';
import { saveAs } from 'file-saver';

const Dashboard = () => {
  const [commentList, setCommentList] = useState({
    success: '',
    message: '',
    output: [],
  });
  const [demoList, setDemoList] = useState({
    success: '',
    message: '',
    output: [],
  });

  useEffect(() => {
    const contact = async () => {
      const contactData = await getList();
      setCommentList(contactData);
    };
    contact();
  }, []);

  function exportToCSV() {
    return fetch('https://www.mitkatadvisory.com/api/downloadcontactus', {
      method: 'POST',
    })
      .then((response) => {
        return response.blob();
      })
      .then((blobObj) => {
        let objUrl = new Blob([blobObj], {
          type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8',
        });
        saveAs(objUrl, 'contactus.xlsx');
      })
      .catch((e) => {
        console.log(e);
      });
  }

  return (
    <>
      <AdminNavbar />

      <div className="container-fluid pt-4">
        <div className="row">
          <nav
            id="sidebarMenu"
            className="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse"
          >
            <div className="sidebar-sticky pt-3">
              <ul className="nav flex-column">
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/dashboard"
                    className="dashboard__link active pt-3"
                  >
                    <FontAwesomeIcon icon={faHeadset} /> &nbsp; &nbsp; Contact
                    us
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/demo_dashboard"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faListUl} /> &nbsp; &nbsp; Demo List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/platform_dashboard"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faListUl} /> &nbsp; &nbsp; Advisory
                    List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/covid_user"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faVirus} /> &nbsp; &nbsp; Covid Login
                    Users
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/career_list"
                    className="dashboard__link  pt-3"
                  >
                    <FontAwesomeIcon icon={faClipboard} /> &nbsp; &nbsp; Career
                    post
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/blog_list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Blog List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/recentnews-list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Recent News
                    List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/upcomingevents-list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Upcoming
                    Events
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/podcast-list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faYoutube} /> &nbsp; &nbsp; Podcast
                  </NavLink>
                </li>
              </ul>
            </div>
          </nav>

          <main role="main" className="col-md-7 ml-sm-auto col-lg-10 px-md-4">
            <h2 className="">
              Conatct Us list
              <a
                className="float-right export-link"
                onClick={() => exportToCSV()}
              >
                <FontAwesomeIcon icon={faFileDownload} />
              </a>
            </h2>
            <hr />
            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr>
                    <th>Sr No. </th>
                    <th>Name </th>
                    <th>Email ID </th>
                    <th>Phone No. </th>
                    <th>Company Name </th>
                    <th>Subject </th>
                    <th>Message </th>
                    <th>Date & Time </th>
                  </tr>
                </thead>
                <tbody>
                  {commentList.output.map((item) => (
                    <tr>
                      <td key={item.id}>{item.id}</td>
                      <td>{item.name}</td>
                      <td>{item.emailid}</td>
                      <td>{item.phoneNumber} </td>
                      <td>{item.companyname}</td>
                      <td>{item.subject}</td>
                      <td>{item.message}</td>
                      <td>{item.time}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </main>
        </div>{' '}
        <br /> <br />
        <br />
        <br />
        <br />
        <br />
      </div>
    </>
  );
};

export default Dashboard;
