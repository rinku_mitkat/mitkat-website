import React, { useEffect, useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import {
  getCareerList,
  createCareerposting,
  deleteCareerPost,
  getCareerdetails,
} from '../../service/api';
import { NavLink } from 'react-router-dom';
import {
  faClipboard,
  faHeadset,
  faVirus,
  faEdit,
} from '@fortawesome/free-solid-svg-icons';
import { faListUl } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import AdminNavbar from '../../components/AdminNavbar';
import { faYoutube } from '@fortawesome/free-brands-svg-icons';

const CareerList = () => {
  const [careerList, setCareerList] = useState([]);
  const [show, setShow] = useState(false);
  const [id, setId] = useState(null);
  const [title, setTitle] = useState('');
  const [summary, setSummary] = useState('');
  const [validTill, setValidTill] = useState();
  const [postingDate, setPostingDate] = useState();
  const [location, setLocation] = useState('');
  const [responsibilities, setResponsibilities] = useState('');
  const [qualifications, setQualifications] = useState('');

  const handleClose = () => setShow(false);

  // const view = async (id) => {
  //     debugger
  //     console.log(id);
  //     setShow(true);
  //     //const covidUser = await getCareerdetails(id);
  //     // setCareerPost(covidUser);
  // }

  const deleteButtonClick = (id) => {
    deleteCareerPost(id);
    window.location.reload(false);
  };

  const editButtonClick = async (id) => {
    setShow(true);
    setId(id);
    let careerPostData = await getCareerdetails(id);

    let validTillD = new Date(careerPostData.validTill);
    let postingDateD = new Date(careerPostData.postingDate);
    setTitle(careerPostData.title);
    setSummary(careerPostData.summary);
    setValidTill(validTillD);
    setPostingDate(postingDateD);
    setLocation(careerPostData.location);
    setResponsibilities(careerPostData.responsibilities);
    setQualifications(careerPostData.qualifications);
  };

  function formSubmit(event) {
    event.preventDefault();
    createCareerposting({
      id,
      title,
      summary,
      validTill,
      postingDate,
      location,
      responsibilities,
      qualifications,
    });
    setShow(false);
    window.location.reload(false);
  }
  function createCareerPost() {
    setShow(true);
    setId();
    setTitle('');
    setSummary('');
    setValidTill();
    setPostingDate();
    setLocation('');
    setResponsibilities('');
    setQualifications('');
  }

  useEffect(() => {
    let mounted = true;
    getCareerList().then((items) => {
      if (mounted) {
        setCareerList(items);
      }
    });
    return () => (mounted = false);
  }, []);

  return (
    <>
      <AdminNavbar />

      <div className="container-fluid pt-4 bg-gray">
        <div className="row">
          <nav
            id="sidebarMenu"
            className="col-md-3 col-lg-2 d-md-block sidebar collapse"
          >
            <div className="sidebar-sticky pt-3">
              <ul className="nav flex-column">
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/dashboard"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faHeadset} /> &nbsp; &nbsp; Contact
                    us
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/demo_dashboard"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faListUl} /> &nbsp; &nbsp; Demo List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/platform_dashboard"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faListUl} /> &nbsp; &nbsp; Advisory
                    List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/covid_user"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faVirus} /> &nbsp; &nbsp; Covid Login
                    Users
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/career_list"
                    className="dashboard__link active pt-3"
                  >
                    <FontAwesomeIcon icon={faClipboard} /> &nbsp; &nbsp; Career
                    post
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/blog_list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Blog List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/recentnews-list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Recent News
                    List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/upcomingevents-list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Upcoming
                    Events
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/podcast-list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faYoutube} /> &nbsp; &nbsp; Podcast
                  </NavLink>
                </li>
              </ul>
            </div>
          </nav>

          <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <h2>
              Career Post list
              <a
                className="btn contact-form__btn float-right"
                onClick={() => createCareerPost()}
              >
                Create Post
              </a>
            </h2>
            <hr />
            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr>
                    <th>Sr No. </th>
                    <th>Valid till </th>
                    <th>Job Title </th>
                    <th>Location </th>
                    {/* <th>View </th> */}
                    <th>Edit </th>
                    <th>Delete </th>
                  </tr>
                </thead>
                <tbody>
                  {careerList.map((item) => (
                    <tr>
                      <td key={item.id}>{item.id}</td>
                      <td>{item.validTillDate}</td>
                      <td>{item.jobTitle}</td>
                      <td>{item.location} </td>
                      {/* <td>
                                            <button type="button" className="btn btn-sm  btn-success" onClick={() => view(item.id)}>
                                                View
                                            </button>
                                        </td> */}
                      <td>
                        <button
                          type="button"
                          className="btn btn-sm  btn-warning"
                          onClick={() => editButtonClick(item.id)}
                        >
                          View / Edit
                        </button>
                      </td>
                      <td>
                        <button
                          type="button"
                          className="btn btn-sm btn-danger"
                          onClick={() => deleteButtonClick(item.id)}
                        >
                          Delete
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </main>
        </div>{' '}
        <br /> <br />
        <br />
        <br />
        <br />
        <br />
      </div>
      <div
        className={show ? ' modal fade show' : 'modal fade'}
        tabindex="-1"
        aria-labelledby="bookademoModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header pb-0">
              <h5 className="modal-title" id="exampleModalLabel">
                {' '}
                Career Post{' '}
              </h5>
              <button type="button" className="close" onClick={handleClose}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body pt-0">
              <div className="d-flex bg-darker">
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-12 col-md-12 col-lg-12 m-a d-flex justify-content-center flex-column">
                      <p className="text-right text-white">
                        {' '}
                        <small> * required </small>{' '}
                      </p>
                      <form className="contact-form px-3" onSubmit={formSubmit}>
                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group mb-3">
                              <label className="text-white">Job Title * </label>
                              <input
                                type="text"
                                className="form-control contact-form__input"
                                name="title"
                                value={title}
                                onChange={(event) =>
                                  setTitle(event.target.value)
                                }
                                required
                              />
                            </div>
                            <div className="form-group mb-3">
                              <label className="text-white">Location * </label>
                              <input
                                type="text"
                                className="form-control contact-form__input"
                                name="location"
                                value={location}
                                onChange={(event) =>
                                  setLocation(event.target.value)
                                }
                                required
                              />
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group mb-3">
                              <label className="text-white">
                                Valid till Date *{' '}
                              </label>
                              <DatePicker
                                dateFormat="dd-MM-yyyy"
                                className="form-control contact-form__input"
                                selected={validTill}
                                onChange={(event) => setValidTill(event)}
                              />
                            </div>
                            <div className="form-group mb-3">
                              <label className="text-white">
                                Posting Date *{' '}
                              </label>
                              <DatePicker
                                dateFormat="dd-MM-yyyy"
                                className="form-control contact-form__input"
                                selected={postingDate}
                                onChange={(event) => setPostingDate(event)}
                              />
                            </div>
                          </div>

                          <div className="col-md-12 col-12">
                            <div className="form-group mb-3">
                              <label className="text-white">Summary * </label>
                              <textarea
                                type="text"
                                className="form-control contact-form__input"
                                name="summary"
                                rows="15"
                                value={summary}
                                onChange={(event) =>
                                  setSummary(event.target.value)
                                }
                                required
                              />
                            </div>
                            <div className="form-group mb-3">
                              <label className="text-white">
                                Responsibilities *{' '}
                              </label>
                              <textarea
                                type="text"
                                className="form-control contact-form__input"
                                name="responsibilities"
                                rows="15"
                                value={responsibilities}
                                onChange={(event) =>
                                  setResponsibilities(event.target.value)
                                }
                                required
                              />
                            </div>
                            <div className="form-group mb-3">
                              <label className="text-white">
                                Qualifications *{' '}
                              </label>
                              <textarea
                                type="text"
                                className="form-control contact-form__input"
                                name="qualifications"
                                rows="15"
                                value={qualifications}
                                onChange={(event) =>
                                  setQualifications(event.target.value)
                                }
                                required
                              />
                            </div>
                          </div>

                          <div className="col-md-12 col-12">
                            <div className="form-group">
                              <input
                                type="submit"
                                name="btnSubmit"
                                className="btn contact-form__btn"
                                value="Submit"
                              />
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CareerList;
