import React, { useEffect, useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { NavLink } from 'react-router-dom';
import AdminNavbar from '../../../components/AdminNavbar';
import {
  faClipboard,
  faHeadset,
  faVirus,
  faListUl,
  faEdit,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  getBlogs,
  getTopics,
  getBlogdetails,
  deleteBlogPost,
} from '../../../service/api';
import { faYoutube } from '@fortawesome/free-brands-svg-icons';

const BlogList = () => {
  const [alert, setAlert] = useState(false);
  const [blogList, setBlogList] = useState([]);
  const [topics, setTopics] = useState([]);
  const [show, setShow] = useState(false);
  const [id, setId] = useState(0);
  const [date, setDate] = useState();
  const [title, setTitle] = useState('');
  const [summary, setSummary] = useState('');
  const [author, setAuthor] = useState('');
  const [topic, setTopic] = useState();
  const [coverpic, setCoverpic] = useState();
  const [blogfile, setBlogfile] = useState();

  const deleteButtonClick = (id) => {
    deleteBlogPost(id);
    window.location.reload(false);
  };

  const editButtonClick = async (id) => {
    setShow(true);
    setId(id);
    const blogData = await getBlogdetails(id);
    let dateD = new Date(blogData.date);
    setDate(dateD);
    setTitle(blogData.title);
    setSummary(blogData.summary);
    setAuthor(blogData.author);
    setTopic(blogData.topicid);
    setCoverpic(blogData.coverpic);
    setBlogfile(blogData.document);
  };

  useEffect(() => {
    const blogs = async () => {
      const blogData = await getBlogs();
      setBlogList(blogData);
    };
    blogs();
    const topics = async () => {
      const topicData = await getTopics();
      setTopics(topicData);
    };
    topics();
  }, []);

  const handleClose = () => setShow(false);
  function createBlog() {
    setShow(true);
  }
  async function formSubmit(event) {
    event.preventDefault();

    const data = new FormData();
    let newDate = new Date(date);
    let dateMDY = `${newDate.getDate()}/${
      newDate.getMonth() + 1
    }/${newDate.getFullYear()}`;
    data.append('id', 0);
    data.append('date', dateMDY);
    data.append('title', title);
    data.append('summary', summary);
    data.append('author', author);
    data.append('topic', topic);
    data.append('coverpic', coverpic);
    data.append('blogfile', blogfile);

    //debugger
    const topicData = await fetch(
      'https://www.mitkatadvisory.com/api/createblogpost',
      {
        method: 'POST',
        body: data,
      }
    );
    if (topicData.status == 201) {
      //debugger
      setAlert(true);
    } else window.location.reload(false);
  }

  function refreshPage() {
    window.location.reload(false);
  }

  return (
    <>
      <AdminNavbar />
      <div className="container-fluid pt-4 bg-gray">
        <div className="row">
          <nav
            id="sidebarMenu"
            className="col-md-3 col-lg-2 d-md-block sidebar collapse"
          >
            <div className="sidebar-sticky pt-3">
              <ul className="nav flex-column">
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/dashboard"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faHeadset} /> &nbsp; &nbsp; Contact
                    us
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/demo_dashboard"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faListUl} /> &nbsp; &nbsp; Demo List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/platform_dashboard"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faListUl} /> &nbsp; &nbsp; Advisory
                    List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/covid_user"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faVirus} /> &nbsp; &nbsp; Covid Login
                    Users
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/career_list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faClipboard} /> &nbsp; &nbsp; Career
                    post
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/blog_list"
                    className="dashboard__link active pt-3"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Blog List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/recentnews-list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Recent News
                    List
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/upcomingevents-list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faEdit} /> &nbsp; &nbsp; Upcoming
                    Events
                  </NavLink>
                </li>
                <li className="nav-item pt-2">
                  <NavLink
                    exact
                    to="/podcast-list"
                    className="dashboard__link pt-3"
                  >
                    <FontAwesomeIcon icon={faYoutube} /> &nbsp; &nbsp; Podcast
                  </NavLink>
                </li>
              </ul>
            </div>
          </nav>

          <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <h2>
              Blogs
              <a
                className="btn contact-form__btn float-right"
                onClick={() => createBlog()}
              >
                Create Blog
              </a>
            </h2>
            <hr />
            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr>
                    <th>Sr No. </th>
                    <th>Date </th>
                    <th>Title </th>
                    <th>Author </th>
                    <th>Edit </th>
                    <th>Delete </th>
                  </tr>
                </thead>
                <tbody>
                  {blogList.length == 0
                    ? null
                    : blogList.map((item) => (
                        <tr>
                          <td key={item.id}>{item.id}</td>
                          <td>{item.date}</td>
                          <td>{item.title}</td>
                          <td> {item.author}</td>

                          <td>
                            <button
                              type="button"
                              className="btn btn-sm  btn-warning"
                              onClick={() => editButtonClick(item.id)}
                            >
                              Edit
                            </button>
                          </td>
                          <td>
                            <button
                              type="button"
                              className="btn btn-sm btn-danger"
                              onClick={() => deleteButtonClick(item.id)}
                            >
                              Delete
                            </button>
                          </td>
                        </tr>
                      ))}
                </tbody>
              </table>
            </div>
          </main>
        </div>{' '}
        <br /> <br />
        <br />
        <br />
        <br />
        <br />
      </div>

      <div
        className={show ? ' modal fade show' : 'modal fade'}
        tabindex="-1"
        aria-labelledby="bookademoModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header pb-0">
              <h5 className="modal-title" id="exampleModalLabel">
                {' '}
                Blog Post{' '}
              </h5>
              <button type="button" className="close" onClick={handleClose}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body pt-0">
              <div className="d-flex bg-darker">
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-12 col-md-12 col-lg-12 m-a d-flex justify-content-center flex-column">
                      <p className="text-right text-white">
                        {' '}
                        <small> * required </small>{' '}
                      </p>
                      <form className="contact-form px-3" onSubmit={formSubmit}>
                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group mb-3">
                              <label className="text-white"> Date * </label>
                              <DatePicker
                                dateFormat="dd-MM-yyyy"
                                className="form-control contact-form__input"
                                selected={date}
                                onChange={(event) => setDate(event)}
                              />
                            </div>
                            <div className="form-group mb-3">
                              <label className="text-white">Title * </label>
                              <input
                                type="text"
                                className="form-control contact-form__input"
                                name="title"
                                value={title}
                                onChange={(event) =>
                                  setTitle(event.target.value)
                                }
                              />
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group mb-3">
                              <label className="text-white">Author * </label>
                              <input
                                type="text"
                                className="form-control contact-form__input"
                                name="author"
                                value={author}
                                onChange={(event) =>
                                  setAuthor(event.target.value)
                                }
                              />
                            </div>
                            <div className="form-group mb-3">
                              <label className="text-white">topic * </label>
                              <select
                                className="form-control contact-form__input"
                                value={topic}
                                onChange={(event) =>
                                  setTopic(event.target.value)
                                }
                              >
                                {topics.map((item) => (
                                  <option value={item.id}>
                                    {' '}
                                    {item.topicName}{' '}
                                  </option>
                                ))}
                              </select>
                            </div>
                          </div>

                          <div className="col-md-12 col-12">
                            <div className="form-group mb-3">
                              <label className="text-white">Summary * </label>
                              <textarea
                                type="text"
                                className="form-control contact-form__input"
                                name="summary"
                                rows="5"
                                value={summary}
                                onChange={(event) =>
                                  setSummary(event.target.value)
                                }
                              />
                            </div>
                          </div>
                          <div className="col-md-12 col-12">
                            <div className="form-group mb-3">
                              <small className="text-warning float-right">
                                {' '}
                                <em> Image size 1000x730 </em>{' '}
                              </small>
                              <label className="text-white">
                                Cover Picture *{' '}
                              </label>
                              <input
                                type="file"
                                className="form-control contact-form__input"
                                name="coverpic"
                                onChange={(event) =>
                                  setCoverpic(event.target.files[0])
                                }
                              />
                            </div>
                          </div>
                          <div className="col-md-12 col-12">
                            <div className="form-group mb-3">
                              <label className="text-white">Blog file * </label>
                              <input
                                type="file"
                                className="form-control contact-form__input"
                                name="blogfile"
                                onChange={(event) =>
                                  setBlogfile(event.target.files[0])
                                }
                              />
                            </div>
                          </div>

                          <div className="col-md-12 col-12">
                            <div className="form-group">
                              <input
                                type="submit"
                                name="btnSubmit"
                                className="btn contact-form__btn"
                                value="Submit"
                              />
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {alert && (
        <div id="overlay" className="d-flex justify-content-center flex-column">
          <div className="success-checkmark">
            <div className="check-icon">
              <span className="icon-line line-tip"></span>
              <span className="icon-line line-long"></span>
              <div className="icon-circle"></div>
              <div className="icon-fix"></div>
            </div>
          </div>
          <center>
            <p className="text-success mb-4">Blog added successfully ! </p>
            <button className="btn btn-success" onClick={refreshPage}>
              {' '}
              ok{' '}
            </button>
          </center>
        </div>
      )}
    </>
  );
};

export default BlogList;
