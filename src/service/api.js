export function getList() {
  return fetch('https://www.mitkatadvisory.com/api/contactuslist').then(
    (data) => data.json()
  );
}

export function getDemoList() {
  return fetch('https://www.mitkatadvisory.com/api/platformlist').then((data) =>
    data.json()
  );
}

export function getAdvisoryList() {
  return fetch('https://www.mitkatadvisory.com/api/advisorylist').then((data) =>
    data.json()
  );
}
export function getServiceList() {
  return fetch('https://www.mitkatadvisory.com/api/servicelist').then((data) =>
    data.json()
  );
}

export function getCountryList() {
  return fetch('https://www.mitkatadvisory.com/api/countrcodes').then((data) =>
    data.json()
  );
}

export function setItemPlatform(item) {
  return fetch('https://www.mitkatadvisory.com/api/advisoryrequest', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(item),
  }).then((data) => data.json());
}

export function setItemDemo(item) {
  return fetch('https://www.mitkatadvisory.com/api/demorequest', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(item),
  }).then((data) => data.json());
}

export function setItem(item) {
  return fetch('https://www.mitkatadvisory.com/api/contactus', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(item),
  }).then((data) => data.json());
}

export function getTopEvent() {
  return fetch('https://mitkatrisktracker.com/api/website/topevents').then(
    (data) => data.json()
  );
}

export function getAdvisories() {
  return fetch('https://mitkatrisktracker.com/api/website/advisories').then(
    (data) => data.json()
  );
}
// Covid Dashboard API
export function getCovidUsers() {
  return fetch('https://www.mitkatadvisory.com/api/getuserlist').then((data) =>
    data.json()
  );
}

export function createCovidUser(item) {
  return fetch('https://www.mitkatadvisory.com/api/createcoviduser', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(item),
  });
}

export function deleteCovidUser(item) {
  return fetch('https://www.mitkatadvisory.com/api/deletecoviduser/' + item, {
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

export function getCovidUserById(item) {
  return fetch('https://www.mitkatadvisory.com/api/getcoviduser/' + item, {
    headers: {
      'Content-Type': 'application/json',
    },
  }).then((data) => data.json());
}

// Career API
export function getCareerListWeb() {
  return fetch('https://www.mitkatadvisory.com/api/getpostinglistuser').then(
    (data) => data.json()
  );
}
export function getCareerList() {
  return fetch('https://www.mitkatadvisory.com/api/getpostinglistadmin').then(
    (data) => data.json()
  );
}
export function getCareerdetails(id) {
  return fetch('https://www.mitkatadvisory.com/api/getcareerdetails/' + id, {
    headers: {
      'Content-Type': 'application/json',
    },
  }).then((data) => data.json());
}
export function createCareerposting(id) {
  return fetch('https://www.mitkatadvisory.com/api/createcareerposting', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(id),
  });
}

export function deleteCareerPost(id) {
  return fetch('https://www.mitkatadvisory.com/api/deleteposting/' + id, {
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

// Blogs API

export function getBlogs() {
  return fetch('https://www.mitkatadvisory.com/api/getbloglist').then((data) =>
    data.json()
  );
}
export function getTopics() {
  return fetch('https://mitkatrisktracker.com/api/website/topics').then(
    (data) => data.json()
  );
}
export function deleteBlogPost(id) {
  return fetch('https://www.mitkatadvisory.com/api/deletepost/' + id, {
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

export function getBlogdetails(id) {
  return fetch('https://www.mitkatadvisory.com/api/getpost/' + id, {
    headers: {
      'Content-Type': 'application/json',
    },
  }).then((data) => data.json());
}

//Recent News
export function getTopNews() {
  return fetch('https://www.mitkatadvisory.com/api/gettopfivenewslist').then(
    (data) => data.json()
  );
}
export function getNews() {
  return fetch('https://www.mitkatadvisory.com/api/getnewslist').then((data) =>
    data.json()
  );
}
export function deleteRecentNews(id) {
  return fetch('https://www.mitkatadvisory.com/api/deletenews/' + id, {
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

export function getNewsDetails(id) {
  return fetch('https://www.mitkatadvisory.com/api/getnews/' + id, {
    headers: {
      'Content-Type': 'application/json',
    },
  }).then((data) => data.json());
}

// Upcoming Events
export function getUpcomingEventsClient() {
  return fetch('https://www.mitkatadvisory.com/api/getupcomingclient').then(
    (data) => data.json()
  );
}

export function getUpcomingEvents() {
  return fetch('https://www.mitkatadvisory.com/api/getallupcomingevents').then(
    (data) => data.json()
  );
}
export function deleteUpcomingEvents(id) {
  return fetch('https://www.mitkatadvisory.com/api/deleteupcomingevent/' + id, {
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

export function getUpcomingEventsDetails(id) {
  return fetch('https://www.mitkatadvisory.com/api/getupcomingevent/' + id, {
    headers: {
      'Content-Type': 'application/json',
    },
  }).then((data) => data.json());
}

// Podcast
export function getPodcastClient() {
  return fetch('https://www.mitkatadvisory.com/api/getvideolist').then((data) =>
    data.json()
  );
}
export function createPodcast(id) {
  return fetch('https://mitkatadvisory.com/api/createvideoevent', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(id),
  });
}

export function deletePodcast(id) {
  return fetch('https://www.mitkatadvisory.com/api/deletevideo/' + id, {
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

export function getPodcastdetails(id) {
  return fetch('https://www.mitkatadvisory.com/api/getvideodetails/' + id, {
    headers: {
      'Content-Type': 'application/json',
    },
  }).then((data) => data.json());
}
